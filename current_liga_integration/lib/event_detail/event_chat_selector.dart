import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:current_liga_integration/event_detail/EventTabUtils.dart';
import 'package:current_liga_integration/event_detail/chat/domain/bloc/chat_cubit.dart';
import 'package:current_liga_integration/event_detail/chat/utils/chat_constant.dart';
import 'package:current_liga_integration/event_detail/costil_change_notifire.dart';
import 'package:current_liga_integration/event_detail/event_details_tabs.dart';
import 'package:current_liga_integration/palette.dart';

class EventChatSelector extends StatelessWidget {
  const EventChatSelector({super.key});

  @override
  Widget build(BuildContext context) {
    final EventTabUtils _utils = EventTabUtils();

    return SliverPersistentHeader(
      pinned: true,
      delegate: _ChatSelectorDelegate(
        minHeight: 48.0,
        maxHeight: 48.0,
        child: DecoratedBox(
          decoration: const BoxDecoration(
            color: Palette.darkFootball,
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: Palette.darkFootball,
                offset: Offset(0, 2),
              ),
            ],
          ),
          child: Consumer<TabChatStreamCostil>(builder: (
            BuildContext context,
              TabChatStreamCostil tabChatStreamCostil,
            _,
          ) {
            return AppSwitcher(
              stream: tabChatStreamCostil.chatTabs,
              defaultTabList: _utils.tabListWithoutChat,
              tabList: _utils.getInitialChatTabList,
              singleTabSettingActiveIndex: (
                List<String> enums,
              ) {
                tabChatStreamCostil.setActiveTabIndex(
                  activeTabName: enums[0],
                  activeTabIndex: 0,
                );
              },
              settingActiveIndex: (
                int currentIndex,
                List<String> enums,
              ) {
                if (currentIndex == 1) {
                  context.read<ChatCubit>().openChat();
                }
                if (currentIndex == 0) {
                  context.read<ChatCubit>().closeChat();
                }
                tabChatStreamCostil.setActiveTabIndex(
                  activeTabName: enums[currentIndex],
                  activeTabIndex: currentIndex,
                );
              },
            );
          }),
        ),
      ),
    );
  }
}

class _ChatSelectorDelegate extends SliverPersistentHeaderDelegate {
  _ChatSelectorDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => maxHeight;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_ChatSelectorDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight || minHeight != oldDelegate.minHeight || child != oldDelegate.child;
  }
}
