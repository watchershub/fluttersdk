import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';
import 'package:current_liga_integration/event_detail/costil_change_notifire.dart';
import 'package:current_liga_integration/event_detail/event_details_tabs.dart';
import 'package:current_liga_integration/event_detail/event_details_top_of_header.dart';
import 'package:current_liga_integration/palette.dart';
import 'package:current_liga_integration/text_styles.dart';

class EventDetailsCommentOfHeader extends StatelessWidget {
  const EventDetailsCommentOfHeader({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Consumer<TabsStreamCostil>(builder: (
      BuildContext context,
      TabsStreamCostil tabsStreamCostil,
      _,
    ) {
      return StreamBuilder<AppSwitcherItemModel>(
        stream: tabsStreamCostil.eventTabs,
        builder: (BuildContext context, AsyncSnapshot<AppSwitcherItemModel> eventTabsSnapshot) {
          if (eventTabsSnapshot.data == null || eventTabsSnapshot.data?.activeTabName != EventTabTypeEnum.home.name) {
            return const SliverToBoxAdapter(
              child: SizedBox(),
            );
          } else {
            return SliverToBoxAdapter(
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  color: Palette.darkFootball,
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Palette.darkFootball,
                      offset: Offset(0, 2),
                    ),
                  ],
                ),
                child: ColoredBox(
                  color: Palette.darkFootball,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      AnimatedOpacity(
                        opacity: 1.0,
                        duration: const Duration(milliseconds: 350),
                        child: Column(
                          children: const <Widget>[
                            Padding(
                              padding: EdgeInsets.symmetric(horizontal: 15.0),
                              child: Text(
                                "state.event.comment",
                                key: Key("EventDetailsScreenKeys.commentTextKey"),
                                style: Main12(
                                  color: Palette.eventSubtitle,
                                ),
                                textAlign: TextAlign.left,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          }
        },
      );
    });
  }
}
