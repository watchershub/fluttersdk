import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:current_liga_integration/default_tab_bar.dart';
import 'package:current_liga_integration/palette.dart';

import '../text_styles.dart';

class EventDetailsNavigationBar extends StatelessWidget {
  const EventDetailsNavigationBar({
    required this.showContent,
    super.key,
  });

  final bool showContent;

  @override
  Widget build(BuildContext context) {
    return SliverDefaultTabBar(
      pinned: true,
      useZeroMinExtend: false,
      useTopSafeArea: false,
      tag: 'nav_eventID_bloc.uniqueKey',
      rectTween: (Rect? begin, Rect? end) {
        return EventDetailsCurves.createRectTween(
          begin ?? Rect.zero,
          end ?? Rect.zero,
          AnimType.nav,
        );
      },
      middle: AnimatedOpacity(
        opacity: showContent ? 1.0 : 0.0,
        duration: const Duration(milliseconds: 350),
        child: const Text(
          'state.sport',
          key: Key('EventDetailsScreenKeys.sportTitleKey'),
          style: Main17(color: Palette.white),
        ),
      ),
      backgroundColor: Palette.darkFootball,
      leading: GestureDetector(
        onTap: () {
          ///pop
        },
        child: Container(
          height: 44,
          width: 48,
          color: Palette.transparent,
          key: const Key('CartScreenKeys.closeEventDetailsButtonKey'),
          padding: const EdgeInsets.only(
            left: 12,
            right: 12,
          ),
          child: AnimatedOpacity(
            opacity: showContent ? 1.0 : 0.0,
            duration: const Duration(milliseconds: 350),
            child: const Center(
              child: Icon(
                Icons.arrow_back,
                size: 24.0,
                color: Palette.white,
              ),
            ),
          ),
        ),
      ),
      trailing: SizedBox(
        height: 44,
        width: 120,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            GestureDetector(
              onTap: () {},
              child: SizedBox(
                height: 30,
                width: 30,
                child: ColoredBox(
                  color: Palette.transparent,
                  child: AnimatedOpacity(
                    opacity: showContent ? 1.0 : 0.0,
                    duration: const Duration(milliseconds: 350),
                    child: const Center(
                      child: Icon(
                        Icons.share,
                        size: 24.0,
                        color: Palette.transparent,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            const SizedBox(width: 12),
            GestureDetector(
              onTap: () {
         ///add to favorite
              },
              child: SizedBox(
                height: 30,
                width: 30,
                child: ColoredBox(
                  color: Palette.transparent,
                  child: FavoriteStar(
                    showContent: showContent,
                    isFavorite: false,
                    key: const Key(
                        'EventDetailsScreenKeys.starFavoritesButtonKey'),
                    isEnd: true,
                  ),
                ),
              ),
            ),
            const SizedBox(
              width: 22,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<bool>('showContent', showContent));
  }
}

class FavoriteStar extends StatelessWidget {
  const FavoriteStar({
    required this.showContent,
    required this.isEnd,
    required this.isFavorite,
    super.key,
  });

  final bool showContent;
  final bool isEnd;
  final bool isFavorite;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 30,
      width: 30,
      child: isEnd
          ? const SizedBox(
              width: 30,
            )
          : AnimatedOpacity(
              opacity: showContent ? 1.0 : 0.0,
              duration: const Duration(milliseconds: 350),
              child: Center(
                child: isFavorite
                    ? const Icon(
                        Icons.favorite,
                        size: 24.0,
                        color: Palette.white,
                      )
                    : const Icon(
                        Icons.favorite_border,
                        size: 24.0,
                        color: Palette.white,
                      ),
              ),
            ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<bool>('showContent', showContent));
  }
}

class EventDetailsCurves {
  static Tween<Rect?> createRectTween(Rect begin, Rect end, AnimType animType) {
    switch (animType) {
      case AnimType.nav:
        return _RectMarketsTween(begin, end, curve: Curves.easeOutExpo);
      case AnimType.status:
        return _RectMarketsTween(begin, end, curve: Curves.easeOutExpo);
      case AnimType.header:
        return _RectMarketsTween(begin, end, curve: Curves.easeOutExpo);
      case AnimType.marketsHeader:
        return _RectMarketsTween(begin, end, curve: Curves.easeInOutQuint); // easeOutExpo); // elasticIn);
      case AnimType.marketsBody:
        return _RectMarketsTween(begin, end, curve: Curves.easeInOutQuint); // easeOutExpo); //elasticIn);
      default:
        return _RectMarketsTween(begin, end, curve: Curves.ease);
    }
  }
}

class _RectMarketsTween extends Tween<Rect?> {
  _RectMarketsTween(
      Rect begin,
      Rect end, {
        this.curve,
      }) : super(begin: begin, end: end);

  final Curve? curve;

  @override
  Rect? lerp(double t) {
    final double transformedTime = curve!.transform(t);

    return Rect.lerp(begin, end, transformedTime);
  }
}

enum AnimType {
  nav,
  status,
  header,
  marketsHeader,
  marketsBody,
}
