import 'package:flutter/material.dart';
import 'package:current_liga_integration/palette.dart';
import 'package:current_liga_integration/text_styles.dart';

import 'event_details_tabs.dart';

/// Translation constant
const String newDesignEventView = 'AB_TEST_NEW_LIST_OF_OUTCOMES';

/// Translation constant value that allows new design for all the users
const String allowAll = 'ForEverybody';

const String envTrue = 'true';
const String envFalse = 'false';

const double iconWidth = 17.0;

class EventTabUtils {
  static const String homeTabName = 'home';
  static const String videoTabName = 'video';
  static const String widgetTabName = 'widget';
  static const String statsTabName = 'stats';
  static const String betsTabName = 'bets';
  static const String chatTabName = 'chat';

  static const double _iconSize = 16.0;

  /// Returns list of tabs that available on event details view
  TabsAndWidgetsModel getEventTabList(Map<String, bool> tabs) {
    final List<Widget> widgets = <Widget>[];
    final List<String> enums = <String>[];
    if (tabs[homeTabName] == true) {
      widgets.add(const EventTab(icon: Icons.home, size: _iconSize));
      enums.add(homeTabName);
    }
    if (tabs[videoTabName] == true) {
      widgets.add(const EventTab(icon: Icons.play_arrow, size: _iconSize));
      enums.add(videoTabName);
    }
    if (tabs[widgetTabName] == true) {
      widgets.add(const EventTab(icon: Icons.ac_unit, size: _iconSize));
      enums.add(widgetTabName);
    }
    if (tabs[statsTabName] == true) {
      widgets.add(const EventTab(icon: Icons.stacked_bar_chart, size: _iconSize));
      enums.add(statsTabName);
    }

    return TabsAndWidgetsModel(
      widgetList: widgets,
      itemNamesList: enums,
    );
  }

  /// Default minimal tab list
  TabsAndWidgetsModel getMinimalTabList() {
    return TabsAndWidgetsModel(
      widgetList: <Widget>[const EventTab(icon: Icons.home, size: _iconSize)],
      itemNamesList: <String>[homeTabName],
    );
  }

  AppSwitcherItemModel initialTabs() {
    return AppSwitcherItemModel(
      tabsStatus: <String, bool>{
        homeTabName: true,
        videoTabName: true,
        widgetTabName: false,
        statsTabName: false,
      },
      activeTabName: homeTabName,
      activeTabIndex: 0,
    );
  }

  AppSwitcherItemModel initialChatTabs(bool chatAvailable) {
    return AppSwitcherItemModel(
      tabsStatus: <String, bool>{
        betsTabName: true,
        chatTabName: chatAvailable,
      },
      activeTabName: betsTabName,
      activeTabIndex: 0,
    );
  }

  /// Tabs without chat
  TabsAndWidgetsModel tabListWithoutChat() {
    return TabsAndWidgetsModel(
      widgetList: <Widget>[
        Tab(
          child: Center(
            child: Text(
              "localization.eventViewBets",
              style: const Main13().copyWith(color: Palette.white),
            ),
          ),
        ),
      ],
      itemNamesList: <String>[
        betsTabName,
      ],
    );
  }

  /// Default and non default chat tab list do not differ
  TabsAndWidgetsModel getInitialChatTabList(
    Map<String, bool> tabs,
  ) {
    final List<Widget> widgets = <Widget>[];
    final List<String> enums = <String>[];
    if (tabs[betsTabName] == true) {
      widgets.add(
        Tab(
          child: Center(
            child: Text(
              "localization.eventViewBets",
              style: const Main13().copyWith(color: Palette.white),
            ),
          ),
        ),
      );
      enums.add(betsTabName);
    }
    if (tabs[chatTabName] == true) {
      widgets.add(
        Tab(
          child: Center(
            child: Text(
              "localization.eventViewChat",
              style: const Main13().copyWith(color: Palette.white),
            ),
          ),
        ),
      );
      enums.add(chatTabName);
    }

    return TabsAndWidgetsModel(
      widgetList: widgets,
      itemNamesList: enums,
    );
  }
}

class EventTab extends StatelessWidget {
  const EventTab({
    required this.icon,
    required this.size,
    super.key,
  });

  final IconData icon;
  final double size;

  @override
  Widget build(BuildContext context) {
    return Tab(
      child: Center(
        child: Icon(
          icon,
          size: size,
          color: Palette.white,
        ),
      ),
    );
  }
}
