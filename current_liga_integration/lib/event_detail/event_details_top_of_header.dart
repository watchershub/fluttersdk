import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:current_liga_integration/event_detail/costil_change_notifire.dart';
import 'package:current_liga_integration/event_detail/event_details_tabs.dart';
import 'package:current_liga_integration/palette.dart';
import 'package:current_liga_integration/text_styles.dart';

import 'chat/utils/keyboard_size_provider.dart';

enum EventTabTypeEnum { home, video, widget, stats, bets, chat }

class EventDetailsTopOfHeader extends StatelessWidget {
  const EventDetailsTopOfHeader({super.key});
  @override
  Widget build(BuildContext context) {
    return Consumer<TabsStreamCostil>(builder: (
      BuildContext context,
      TabsStreamCostil tabsStreamCostil,
      _,
    ) {
      return StreamBuilder<AppSwitcherItemModel>(
        stream: tabsStreamCostil.eventTabs,
        builder: (BuildContext context, AsyncSnapshot<AppSwitcherItemModel> eventTabsSnapshot) {
          if (eventTabsSnapshot.data == null || eventTabsSnapshot.data?.activeTabName != EventTabTypeEnum.home.name) {
            return const SliverToBoxAdapter(
              child: SizedBox(),
            );
          } else {
            return SliverToBoxAdapter(
              child: DecoratedBox(
                decoration: const BoxDecoration(
                  color: Palette.darkFootball,
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Palette.darkFootball,
                      offset: Offset(0, 2),
                    ),
                  ],
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    AnimatedOpacity(
                      opacity: 1.0,
                      duration: const Duration(milliseconds: 350),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 15.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Expanded(
                              child: Text(
                                'state.event.tournamentTitle, state.event.categoryTitle',
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: const Main12().copyWith(color: Palette.darkTextLinkVisited),
                                key: const Key(
                                  "EventDetailsScreenKeys.tournamentTitleKey)",
                                ),
                              ),
                            ),
                            const SizedBox(width: 8.0),
                            Text(
                              "state.event.fullRestylingCardEventDate",
                              key: const Key("EventDetailsScreenKeys.eventDateTitleKey"),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                              style: const Main12().copyWith(color: Palette.white),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            );
          }
        },
      );
    });
  }
}
