import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:current_liga_integration/event_detail/EventTabUtils.dart';
import 'package:current_liga_integration/event_detail/event_details_tabs.dart';

class TabsStreamCostil extends ChangeNotifier {
  TabsStreamCostil();

  final BehaviorSubject<AppSwitcherItemModel> _tabsListSubject = BehaviorSubject<AppSwitcherItemModel>();

  Stream<AppSwitcherItemModel> get eventTabs => _tabsListSubject.stream;
  final AppSwitcherItemModel _eventTabStateModel = EventTabUtils().initialTabs();

  void setActiveTabIndex({
    required String activeTab,
    required int activeTabIndex,
    required BuildContext context,
  }) {
    _eventTabStateModel.activeTabName = activeTab;
    _eventTabStateModel.activeTabIndex = activeTabIndex;
    _tabsListSubject.sink.add(_eventTabStateModel);
    if (activeTab == EventTabUtils.videoTabName) {
    } else {}
    WidgetsBinding.instance.addPostFrameCallback((_) => notifyListeners());
  }
}

class TabChatStreamCostil extends ChangeNotifier {
  final BehaviorSubject<AppSwitcherItemModel> _chatTabsListSubject = BehaviorSubject<AppSwitcherItemModel>();

  Stream<AppSwitcherItemModel> get chatTabs => _chatTabsListSubject.stream;
  final AppSwitcherItemModel _eventChatTabStateModel = EventTabUtils().initialChatTabs(true);

  AppSwitcherItemModel get eventChatTabState => _eventChatTabStateModel;

  void setActiveTabIndex({
    required String activeTabName,
    required int activeTabIndex,
  }) {
    _eventChatTabStateModel.activeTabName = activeTabName;
    _eventChatTabStateModel.activeTabIndex = activeTabIndex;
    _chatTabsListSubject.sink.add(_eventChatTabStateModel);
    WidgetsBinding.instance.addPostFrameCallback((_) => notifyListeners());
  }
}
