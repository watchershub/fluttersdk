import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:watchers_widget/watchers_widget.dart';

import '../../domain/bloc/chat_cubit.dart';

class Chat extends StatelessWidget {
  const Chat({super.key});

  @override
  Widget build(BuildContext context) {
    Widget? chat;

    return BlocBuilder<ChatCubit, ChatState>(
      builder: (BuildContext context, ChatState state) {
        if (state is ChatOpenState) {
          chat = MediaQuery.removePadding(
            key: key,
            context: context,
            removeBottom: true,
            child:
            WatchersWidget(
              controller: WatchersWidgetController(
                params: WatchersParams(
                  userId: state.userId,
                  roomId: state.roomId,
                  statusName: state.statusName == null ? null : StatusNameX.fromString(state.statusName!.toUpperCase()),
                  enableDev: true,
                  // title: "Англия - Иран",
                ),
              ),
            ),
          );
        } else if (state is ChatWebViewOpenState) {
          // chat = WebView(
          //   key: state.webViewKey,
          //   gestureRecognizers: const <Factory<OneSequenceGestureRecognizer>>{
          //     Factory<OneSequenceGestureRecognizer>(EagerGestureRecognizer.new),
          //   },
          //   javascriptMode: JavascriptMode.unrestricted,
          //   javascriptChannels: <JavascriptChannel>{
          //     JavascriptChannel(
          //       name: 'Copy',
          //       onMessageReceived: (JavascriptMessage message) async {
          //         await Clipboard.setData(
          //           ClipboardData(text: message.message),
          //         );
          //       },
          //     )
          //   },
          //   initialUrl: state.url,
          // );
        }

        return SliverOffstage(
          offstage: state is! ChatOpenState && state is! ChatWebViewOpenState,
          sliver: SliverFillRemaining(
            child: chat ?? const SizedBox(),
          ),
        );
      },
    );
  }
}