///Amplitude
const String chatOpenAmplitude = 'Social_ButtonClick_Chat';
const String chatClosedAmplitude = 'Social_ButtonClick_ChatClose';

const String watchersChatApiVer = '3.0';
const String beta = 'beta';

///параметры для аналитики Social_ButtonClick_ChatClose, свойство openedFrom
enum ChatOpenedFrom {
  chat,
  // ignore: constant_identifier_names
  last_message,
  // ignore: constant_identifier_names
  last_message_stub,
}
