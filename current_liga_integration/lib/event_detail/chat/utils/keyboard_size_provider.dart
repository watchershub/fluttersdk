import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

export 'package:provider/provider.dart';

class KeyboardSizeProvider extends StatelessWidget {
  const KeyboardSizeProvider({super.key, this.child = const SizedBox()});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final ScreenHeight _screenHeight = ScreenHeight(
      initialHeight: MediaQuery.of(context).size.height,
    );
    _screenHeight.change(MediaQuery.of(context).viewInsets.bottom);

    return ChangeNotifierProvider<ScreenHeight>.value(value: _screenHeight, child: child);
  }
}

class ScreenHeight extends ChangeNotifier {
  ScreenHeight({this.initialHeight = 0});

  /// the height of opened keyboard
  double keyboardHeight = 0;

  /// initial screen height
  double initialHeight;

  /// getter for keyboard status
  bool get isOpen => keyboardHeight > 1;

  /// getter for full screen height
  double get screenHeight => initialHeight;

  void change(double a) {
    keyboardHeight = a;
    notifyListeners();
  }
}
