import 'dart:async';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'chat_cubit.freezed.dart';

part 'chat_state.dart';

class ChatCubit extends Cubit<ChatState> {
  ChatCubit(
    // required this.service,
  ) : super(
          ChatState.init(),
        ) {
    webViewKey = ValueKey<String>(_eventId.toString());
    chatKey = ValueKey<String>(_eventId.toString());
  }

  // Future<Either<ChatError, ChatModel>>? chatModelFuture;
  // final ChatService service;
  late final StreamSubscription<int?> currentEventDetailSubscribe;
  late ValueKey<String> webViewKey;
  late ValueKey<String> chatKey;
  String url = '';
  final int _eventId = 123;

  Future<void> openChat() async {
    if (false) {
      _needAuthDialog();
    } else {
      openChatSdk(_eventId);
    }
  }

  void closeChat() {
    SystemChannels.textInput.invokeMethod<void>('TextInput.hide');
    emit(
      ChatState.closeChat(),
    );
  }

  String encryptedUserId(int userId) {
    final encrypt.Key key =
        encrypt.Key.fromUtf8(r'''MbQeThWmYq3t6w9z$C&F)J@NcRfUjXn2''');
    final encrypt.IV iv = encrypt.IV.fromLength(16);

    final encrypt.Encrypter encrypter =
        encrypt.Encrypter(encrypt.AES(key, mode: encrypt.AESMode.ecb));

    final encrypt.Encrypted encrypted =
        encrypter.encrypt(userId.toString(), iv: iv);

    return encrypted.base64;
  }

  void openChatSdk(int eventId) {
    final String userId = encryptedUserId(123);
    emit(
      ChatState.openChat(
        userId: userId,
        roomId: eventId.toString(),
        title: _getTitle(eventId),
        statusName: "VipStatus",
        chatKey: chatKey,
      ),
    );
  }

  Future<void> tryOpenChatLink(VoidCallback onError) async {
    // try {
    //   await service
    //       .getLink(
    //           apiVer: watchersChatApiVer,
    //           clientId: '123',
    //           eventId: _eventId,
    //           title: _getTitle(_eventId),
    //           statusName: 'vip')
    //       .then<void>((Either<ChatError, ChatModel> response) async {
    //     response.fold((_) => print('не удалось получить ссылку на чат'),
    //         (ChatModel response) {
    //       if (response.url != url) {
    //         url = response.url;
    //         webViewKey = ValueKey<String>(url);
    //       }
    //       emit(
    //         ChatState.openChatWebView(
    //             url: response.url, webViewKey: webViewKey),
    //       );
    //     });
    //   });
    // } catch (ex) {
    //   onError();
    // }
  }

  void _needAuthDialog() {}

  String _getTitle(int? eventId) {
    String title;

    title = 'team1 - team2';

    return title;
  }

  @override
  Future<void> close() async {
    await super.close();
  }
}
