// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'chat_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ChatState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String error) error,
    required TResult Function() notShowChat,
    required TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)
        openChat,
    required TResult Function(String url, ValueKey<String> webViewKey) openChatWebView,
    required TResult Function() closeChat,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String error)? error,
    TResult? Function()? notShowChat,
    TResult? Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult? Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult? Function()? closeChat,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String error)? error,
    TResult Function()? notShowChat,
    TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult Function()? closeChat,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatInitState value) init,
    required TResult Function(ChatErrorState value) error,
    required TResult Function(NotShowChatState value) notShowChat,
    required TResult Function(ChatOpenState value) openChat,
    required TResult Function(ChatWebViewOpenState value) openChatWebView,
    required TResult Function(ChatCloseState value) closeChat,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatInitState value)? init,
    TResult? Function(ChatErrorState value)? error,
    TResult? Function(NotShowChatState value)? notShowChat,
    TResult? Function(ChatOpenState value)? openChat,
    TResult? Function(ChatWebViewOpenState value)? openChatWebView,
    TResult? Function(ChatCloseState value)? closeChat,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatInitState value)? init,
    TResult Function(ChatErrorState value)? error,
    TResult Function(NotShowChatState value)? notShowChat,
    TResult Function(ChatOpenState value)? openChat,
    TResult Function(ChatWebViewOpenState value)? openChatWebView,
    TResult Function(ChatCloseState value)? closeChat,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatStateCopyWith<$Res> {
  factory $ChatStateCopyWith(ChatState value, $Res Function(ChatState) then) = _$ChatStateCopyWithImpl<$Res, ChatState>;
}

/// @nodoc
class _$ChatStateCopyWithImpl<$Res, $Val extends ChatState> implements $ChatStateCopyWith<$Res> {
  _$ChatStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$ChatInitStateCopyWith<$Res> {
  factory _$$ChatInitStateCopyWith(_$ChatInitState value, $Res Function(_$ChatInitState) then) =
      __$$ChatInitStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ChatInitStateCopyWithImpl<$Res> extends _$ChatStateCopyWithImpl<$Res, _$ChatInitState>
    implements _$$ChatInitStateCopyWith<$Res> {
  __$$ChatInitStateCopyWithImpl(_$ChatInitState _value, $Res Function(_$ChatInitState) _then) : super(_value, _then);
}

/// @nodoc

class _$ChatInitState with DiagnosticableTreeMixin implements ChatInitState {
  _$ChatInitState();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatState.init()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'ChatState.init'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other.runtimeType == runtimeType && other is _$ChatInitState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String error) error,
    required TResult Function() notShowChat,
    required TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)
        openChat,
    required TResult Function(String url, ValueKey<String> webViewKey) openChatWebView,
    required TResult Function() closeChat,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String error)? error,
    TResult? Function()? notShowChat,
    TResult? Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult? Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult? Function()? closeChat,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String error)? error,
    TResult Function()? notShowChat,
    TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult Function()? closeChat,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatInitState value) init,
    required TResult Function(ChatErrorState value) error,
    required TResult Function(NotShowChatState value) notShowChat,
    required TResult Function(ChatOpenState value) openChat,
    required TResult Function(ChatWebViewOpenState value) openChatWebView,
    required TResult Function(ChatCloseState value) closeChat,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatInitState value)? init,
    TResult? Function(ChatErrorState value)? error,
    TResult? Function(NotShowChatState value)? notShowChat,
    TResult? Function(ChatOpenState value)? openChat,
    TResult? Function(ChatWebViewOpenState value)? openChatWebView,
    TResult? Function(ChatCloseState value)? closeChat,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatInitState value)? init,
    TResult Function(ChatErrorState value)? error,
    TResult Function(NotShowChatState value)? notShowChat,
    TResult Function(ChatOpenState value)? openChat,
    TResult Function(ChatWebViewOpenState value)? openChatWebView,
    TResult Function(ChatCloseState value)? closeChat,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class ChatInitState implements ChatState {
  factory ChatInitState() = _$ChatInitState;
}

/// @nodoc
abstract class _$$ChatErrorStateCopyWith<$Res> {
  factory _$$ChatErrorStateCopyWith(_$ChatErrorState value, $Res Function(_$ChatErrorState) then) =
      __$$ChatErrorStateCopyWithImpl<$Res>;
  @useResult
  $Res call({String error});
}

/// @nodoc
class __$$ChatErrorStateCopyWithImpl<$Res> extends _$ChatStateCopyWithImpl<$Res, _$ChatErrorState>
    implements _$$ChatErrorStateCopyWith<$Res> {
  __$$ChatErrorStateCopyWithImpl(_$ChatErrorState _value, $Res Function(_$ChatErrorState) _then) : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? error = null,
  }) {
    return _then(_$ChatErrorState(
      error: null == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ChatErrorState with DiagnosticableTreeMixin implements ChatErrorState {
  _$ChatErrorState({required this.error});

  @override
  final String error;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatState.error(error: $error)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChatState.error'))
      ..add(DiagnosticsProperty('error', error));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatErrorState &&
            (identical(other.error, error) || other.error == error));
  }

  @override
  int get hashCode => Object.hash(runtimeType, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChatErrorStateCopyWith<_$ChatErrorState> get copyWith =>
      __$$ChatErrorStateCopyWithImpl<_$ChatErrorState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String error) error,
    required TResult Function() notShowChat,
    required TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)
        openChat,
    required TResult Function(String url, ValueKey<String> webViewKey) openChatWebView,
    required TResult Function() closeChat,
  }) {
    return error(this.error);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String error)? error,
    TResult? Function()? notShowChat,
    TResult? Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult? Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult? Function()? closeChat,
  }) {
    return error?.call(this.error);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String error)? error,
    TResult Function()? notShowChat,
    TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult Function()? closeChat,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this.error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatInitState value) init,
    required TResult Function(ChatErrorState value) error,
    required TResult Function(NotShowChatState value) notShowChat,
    required TResult Function(ChatOpenState value) openChat,
    required TResult Function(ChatWebViewOpenState value) openChatWebView,
    required TResult Function(ChatCloseState value) closeChat,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatInitState value)? init,
    TResult? Function(ChatErrorState value)? error,
    TResult? Function(NotShowChatState value)? notShowChat,
    TResult? Function(ChatOpenState value)? openChat,
    TResult? Function(ChatWebViewOpenState value)? openChatWebView,
    TResult? Function(ChatCloseState value)? closeChat,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatInitState value)? init,
    TResult Function(ChatErrorState value)? error,
    TResult Function(NotShowChatState value)? notShowChat,
    TResult Function(ChatOpenState value)? openChat,
    TResult Function(ChatWebViewOpenState value)? openChatWebView,
    TResult Function(ChatCloseState value)? closeChat,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class ChatErrorState implements ChatState {
  factory ChatErrorState({required final String error}) = _$ChatErrorState;

  String get error;
  @JsonKey(ignore: true)
  _$$ChatErrorStateCopyWith<_$ChatErrorState> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$NotShowChatStateCopyWith<$Res> {
  factory _$$NotShowChatStateCopyWith(_$NotShowChatState value, $Res Function(_$NotShowChatState) then) =
      __$$NotShowChatStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$NotShowChatStateCopyWithImpl<$Res> extends _$ChatStateCopyWithImpl<$Res, _$NotShowChatState>
    implements _$$NotShowChatStateCopyWith<$Res> {
  __$$NotShowChatStateCopyWithImpl(_$NotShowChatState _value, $Res Function(_$NotShowChatState) _then)
      : super(_value, _then);
}

/// @nodoc

class _$NotShowChatState with DiagnosticableTreeMixin implements NotShowChatState {
  _$NotShowChatState();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatState.notShowChat()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'ChatState.notShowChat'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other.runtimeType == runtimeType && other is _$NotShowChatState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String error) error,
    required TResult Function() notShowChat,
    required TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)
        openChat,
    required TResult Function(String url, ValueKey<String> webViewKey) openChatWebView,
    required TResult Function() closeChat,
  }) {
    return notShowChat();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String error)? error,
    TResult? Function()? notShowChat,
    TResult? Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult? Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult? Function()? closeChat,
  }) {
    return notShowChat?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String error)? error,
    TResult Function()? notShowChat,
    TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult Function()? closeChat,
    required TResult orElse(),
  }) {
    if (notShowChat != null) {
      return notShowChat();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatInitState value) init,
    required TResult Function(ChatErrorState value) error,
    required TResult Function(NotShowChatState value) notShowChat,
    required TResult Function(ChatOpenState value) openChat,
    required TResult Function(ChatWebViewOpenState value) openChatWebView,
    required TResult Function(ChatCloseState value) closeChat,
  }) {
    return notShowChat(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatInitState value)? init,
    TResult? Function(ChatErrorState value)? error,
    TResult? Function(NotShowChatState value)? notShowChat,
    TResult? Function(ChatOpenState value)? openChat,
    TResult? Function(ChatWebViewOpenState value)? openChatWebView,
    TResult? Function(ChatCloseState value)? closeChat,
  }) {
    return notShowChat?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatInitState value)? init,
    TResult Function(ChatErrorState value)? error,
    TResult Function(NotShowChatState value)? notShowChat,
    TResult Function(ChatOpenState value)? openChat,
    TResult Function(ChatWebViewOpenState value)? openChatWebView,
    TResult Function(ChatCloseState value)? closeChat,
    required TResult orElse(),
  }) {
    if (notShowChat != null) {
      return notShowChat(this);
    }
    return orElse();
  }
}

abstract class NotShowChatState implements ChatState {
  factory NotShowChatState() = _$NotShowChatState;
}

/// @nodoc
abstract class _$$ChatOpenStateCopyWith<$Res> {
  factory _$$ChatOpenStateCopyWith(_$ChatOpenState value, $Res Function(_$ChatOpenState) then) =
      __$$ChatOpenStateCopyWithImpl<$Res>;
  @useResult
  $Res call({String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName});
}

/// @nodoc
class __$$ChatOpenStateCopyWithImpl<$Res> extends _$ChatStateCopyWithImpl<$Res, _$ChatOpenState>
    implements _$$ChatOpenStateCopyWith<$Res> {
  __$$ChatOpenStateCopyWithImpl(_$ChatOpenState _value, $Res Function(_$ChatOpenState) _then) : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? userId = null,
    Object? roomId = null,
    Object? title = null,
    Object? chatKey = null,
    Object? statusName = freezed,
  }) {
    return _then(_$ChatOpenState(
      userId: null == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as String,
      roomId: null == roomId
          ? _value.roomId
          : roomId // ignore: cast_nullable_to_non_nullable
              as String,
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
      chatKey: null == chatKey
          ? _value.chatKey
          : chatKey // ignore: cast_nullable_to_non_nullable
              as ValueKey<String>,
      statusName: freezed == statusName
          ? _value.statusName
          : statusName // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc

class _$ChatOpenState with DiagnosticableTreeMixin implements ChatOpenState {
  _$ChatOpenState(
      {required this.userId, required this.roomId, required this.title, required this.chatKey, this.statusName});

  @override
  final String userId;
  @override
  final String roomId;
  @override
  final String title;
  @override
  final ValueKey<String> chatKey;
  @override
  final String? statusName;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatState.openChat(userId: $userId, roomId: $roomId, title: $title, chatKey: $chatKey, statusName: $statusName)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChatState.openChat'))
      ..add(DiagnosticsProperty('userId', userId))
      ..add(DiagnosticsProperty('roomId', roomId))
      ..add(DiagnosticsProperty('title', title))
      ..add(DiagnosticsProperty('chatKey', chatKey))
      ..add(DiagnosticsProperty('statusName', statusName));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatOpenState &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.roomId, roomId) || other.roomId == roomId) &&
            (identical(other.title, title) || other.title == title) &&
            (identical(other.chatKey, chatKey) || other.chatKey == chatKey) &&
            (identical(other.statusName, statusName) || other.statusName == statusName));
  }

  @override
  int get hashCode => Object.hash(runtimeType, userId, roomId, title, chatKey, statusName);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChatOpenStateCopyWith<_$ChatOpenState> get copyWith =>
      __$$ChatOpenStateCopyWithImpl<_$ChatOpenState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String error) error,
    required TResult Function() notShowChat,
    required TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)
        openChat,
    required TResult Function(String url, ValueKey<String> webViewKey) openChatWebView,
    required TResult Function() closeChat,
  }) {
    return openChat(userId, roomId, title, chatKey, statusName);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String error)? error,
    TResult? Function()? notShowChat,
    TResult? Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult? Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult? Function()? closeChat,
  }) {
    return openChat?.call(userId, roomId, title, chatKey, statusName);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String error)? error,
    TResult Function()? notShowChat,
    TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult Function()? closeChat,
    required TResult orElse(),
  }) {
    if (openChat != null) {
      return openChat(userId, roomId, title, chatKey, statusName);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatInitState value) init,
    required TResult Function(ChatErrorState value) error,
    required TResult Function(NotShowChatState value) notShowChat,
    required TResult Function(ChatOpenState value) openChat,
    required TResult Function(ChatWebViewOpenState value) openChatWebView,
    required TResult Function(ChatCloseState value) closeChat,
  }) {
    return openChat(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatInitState value)? init,
    TResult? Function(ChatErrorState value)? error,
    TResult? Function(NotShowChatState value)? notShowChat,
    TResult? Function(ChatOpenState value)? openChat,
    TResult? Function(ChatWebViewOpenState value)? openChatWebView,
    TResult? Function(ChatCloseState value)? closeChat,
  }) {
    return openChat?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatInitState value)? init,
    TResult Function(ChatErrorState value)? error,
    TResult Function(NotShowChatState value)? notShowChat,
    TResult Function(ChatOpenState value)? openChat,
    TResult Function(ChatWebViewOpenState value)? openChatWebView,
    TResult Function(ChatCloseState value)? closeChat,
    required TResult orElse(),
  }) {
    if (openChat != null) {
      return openChat(this);
    }
    return orElse();
  }
}

abstract class ChatOpenState implements ChatState {
  factory ChatOpenState(
      {required final String userId,
      required final String roomId,
      required final String title,
      required final ValueKey<String> chatKey,
      final String? statusName}) = _$ChatOpenState;

  String get userId;
  String get roomId;
  String get title;
  ValueKey<String> get chatKey;
  String? get statusName;
  @JsonKey(ignore: true)
  _$$ChatOpenStateCopyWith<_$ChatOpenState> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChatWebViewOpenStateCopyWith<$Res> {
  factory _$$ChatWebViewOpenStateCopyWith(_$ChatWebViewOpenState value, $Res Function(_$ChatWebViewOpenState) then) =
      __$$ChatWebViewOpenStateCopyWithImpl<$Res>;
  @useResult
  $Res call({String url, ValueKey<String> webViewKey});
}

/// @nodoc
class __$$ChatWebViewOpenStateCopyWithImpl<$Res> extends _$ChatStateCopyWithImpl<$Res, _$ChatWebViewOpenState>
    implements _$$ChatWebViewOpenStateCopyWith<$Res> {
  __$$ChatWebViewOpenStateCopyWithImpl(_$ChatWebViewOpenState _value, $Res Function(_$ChatWebViewOpenState) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? url = null,
    Object? webViewKey = null,
  }) {
    return _then(_$ChatWebViewOpenState(
      url: null == url
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
      webViewKey: null == webViewKey
          ? _value.webViewKey
          : webViewKey // ignore: cast_nullable_to_non_nullable
              as ValueKey<String>,
    ));
  }
}

/// @nodoc

class _$ChatWebViewOpenState with DiagnosticableTreeMixin implements ChatWebViewOpenState {
  _$ChatWebViewOpenState({required this.url, required this.webViewKey});

  @override
  final String url;
  @override
  final ValueKey<String> webViewKey;

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatState.openChatWebView(url: $url, webViewKey: $webViewKey)';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties
      ..add(DiagnosticsProperty('type', 'ChatState.openChatWebView'))
      ..add(DiagnosticsProperty('url', url))
      ..add(DiagnosticsProperty('webViewKey', webViewKey));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatWebViewOpenState &&
            (identical(other.url, url) || other.url == url) &&
            (identical(other.webViewKey, webViewKey) || other.webViewKey == webViewKey));
  }

  @override
  int get hashCode => Object.hash(runtimeType, url, webViewKey);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ChatWebViewOpenStateCopyWith<_$ChatWebViewOpenState> get copyWith =>
      __$$ChatWebViewOpenStateCopyWithImpl<_$ChatWebViewOpenState>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String error) error,
    required TResult Function() notShowChat,
    required TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)
        openChat,
    required TResult Function(String url, ValueKey<String> webViewKey) openChatWebView,
    required TResult Function() closeChat,
  }) {
    return openChatWebView(url, webViewKey);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String error)? error,
    TResult? Function()? notShowChat,
    TResult? Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult? Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult? Function()? closeChat,
  }) {
    return openChatWebView?.call(url, webViewKey);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String error)? error,
    TResult Function()? notShowChat,
    TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult Function()? closeChat,
    required TResult orElse(),
  }) {
    if (openChatWebView != null) {
      return openChatWebView(url, webViewKey);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatInitState value) init,
    required TResult Function(ChatErrorState value) error,
    required TResult Function(NotShowChatState value) notShowChat,
    required TResult Function(ChatOpenState value) openChat,
    required TResult Function(ChatWebViewOpenState value) openChatWebView,
    required TResult Function(ChatCloseState value) closeChat,
  }) {
    return openChatWebView(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatInitState value)? init,
    TResult? Function(ChatErrorState value)? error,
    TResult? Function(NotShowChatState value)? notShowChat,
    TResult? Function(ChatOpenState value)? openChat,
    TResult? Function(ChatWebViewOpenState value)? openChatWebView,
    TResult? Function(ChatCloseState value)? closeChat,
  }) {
    return openChatWebView?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatInitState value)? init,
    TResult Function(ChatErrorState value)? error,
    TResult Function(NotShowChatState value)? notShowChat,
    TResult Function(ChatOpenState value)? openChat,
    TResult Function(ChatWebViewOpenState value)? openChatWebView,
    TResult Function(ChatCloseState value)? closeChat,
    required TResult orElse(),
  }) {
    if (openChatWebView != null) {
      return openChatWebView(this);
    }
    return orElse();
  }
}

abstract class ChatWebViewOpenState implements ChatState {
  factory ChatWebViewOpenState({required final String url, required final ValueKey<String> webViewKey}) =
      _$ChatWebViewOpenState;

  String get url;
  ValueKey<String> get webViewKey;
  @JsonKey(ignore: true)
  _$$ChatWebViewOpenStateCopyWith<_$ChatWebViewOpenState> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChatCloseStateCopyWith<$Res> {
  factory _$$ChatCloseStateCopyWith(_$ChatCloseState value, $Res Function(_$ChatCloseState) then) =
      __$$ChatCloseStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$ChatCloseStateCopyWithImpl<$Res> extends _$ChatStateCopyWithImpl<$Res, _$ChatCloseState>
    implements _$$ChatCloseStateCopyWith<$Res> {
  __$$ChatCloseStateCopyWithImpl(_$ChatCloseState _value, $Res Function(_$ChatCloseState) _then) : super(_value, _then);
}

/// @nodoc

class _$ChatCloseState with DiagnosticableTreeMixin implements ChatCloseState {
  _$ChatCloseState();

  @override
  String toString({DiagnosticLevel minLevel = DiagnosticLevel.info}) {
    return 'ChatState.closeChat()';
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty('type', 'ChatState.closeChat'));
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other.runtimeType == runtimeType && other is _$ChatCloseState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function(String error) error,
    required TResult Function() notShowChat,
    required TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)
        openChat,
    required TResult Function(String url, ValueKey<String> webViewKey) openChatWebView,
    required TResult Function() closeChat,
  }) {
    return closeChat();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? init,
    TResult? Function(String error)? error,
    TResult? Function()? notShowChat,
    TResult? Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult? Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult? Function()? closeChat,
  }) {
    return closeChat?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function(String error)? error,
    TResult Function()? notShowChat,
    TResult Function(String userId, String roomId, String title, ValueKey<String> chatKey, String? statusName)?
        openChat,
    TResult Function(String url, ValueKey<String> webViewKey)? openChatWebView,
    TResult Function()? closeChat,
    required TResult orElse(),
  }) {
    if (closeChat != null) {
      return closeChat();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ChatInitState value) init,
    required TResult Function(ChatErrorState value) error,
    required TResult Function(NotShowChatState value) notShowChat,
    required TResult Function(ChatOpenState value) openChat,
    required TResult Function(ChatWebViewOpenState value) openChatWebView,
    required TResult Function(ChatCloseState value) closeChat,
  }) {
    return closeChat(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(ChatInitState value)? init,
    TResult? Function(ChatErrorState value)? error,
    TResult? Function(NotShowChatState value)? notShowChat,
    TResult? Function(ChatOpenState value)? openChat,
    TResult? Function(ChatWebViewOpenState value)? openChatWebView,
    TResult? Function(ChatCloseState value)? closeChat,
  }) {
    return closeChat?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ChatInitState value)? init,
    TResult Function(ChatErrorState value)? error,
    TResult Function(NotShowChatState value)? notShowChat,
    TResult Function(ChatOpenState value)? openChat,
    TResult Function(ChatWebViewOpenState value)? openChatWebView,
    TResult Function(ChatCloseState value)? closeChat,
    required TResult orElse(),
  }) {
    if (closeChat != null) {
      return closeChat(this);
    }
    return orElse();
  }
}

abstract class ChatCloseState implements ChatState {
  factory ChatCloseState() = _$ChatCloseState;
}
