part of 'chat_cubit.dart';

@freezed
class ChatState with _$ChatState {
  factory ChatState.init() = ChatInitState;

  factory ChatState.error({required String error}) = ChatErrorState;

  factory ChatState.notShowChat() = NotShowChatState;

  factory ChatState.openChat({
    required String userId,
    required String roomId,
    required String title,
    required ValueKey<String> chatKey,
    String? statusName,
  }) = ChatOpenState;

  factory ChatState.openChatWebView({
    required String url,
    required ValueKey<String> webViewKey,
  }) = ChatWebViewOpenState;

  factory ChatState.closeChat() = ChatCloseState;
}
