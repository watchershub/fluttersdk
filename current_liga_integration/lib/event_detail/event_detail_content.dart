import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:current_liga_integration/event_detail/bottom_empty_space.dart';
import 'package:current_liga_integration/event_detail/event_chat_selector.dart';
import 'package:current_liga_integration/event_detail/event_detail_player.dart';
import 'package:current_liga_integration/event_detail/event_details_comment_of_header.dart';
import 'package:current_liga_integration/event_detail/event_details_tabs.dart';
import 'package:current_liga_integration/event_detail/event_details_top_of_header.dart';
import 'package:current_liga_integration/palette.dart';

import 'chat/domain/bloc/chat_cubit.dart';
import 'chat/presentation/widgets/chat_widget.dart';
import 'event_detail_navigation_bar.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'event_details_bottom_of_header.dart';
import 'event_markets_container.dart';

class EventDetailsContent extends StatelessWidget {
  const EventDetailsContent({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final double bottomPadding = MediaQueryData.fromWindow(ui.window).padding.bottom;

    return SafeArea(
      child: Scaffold(
        backgroundColor: Palette.lightGrey999,
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: const Align(
          alignment: Alignment.bottomRight,
          child: Padding(
            padding: EdgeInsets.only(bottom: 20.0, right: 20),
            // child: HomeFab(),
          ),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              child: BlocConsumer<ChatCubit, ChatState>(
                listener: (_, ChatState state) {
                  // if (state is ChatOpenState || state is ChatWebViewOpenState) {
                  //   scrollController?.jumpTo(0);
                  // }
                },
                builder: (BuildContext context, ChatState state) {
                  return Stack(
                    children: <Widget>[
                      CustomScrollView(
                        // key: const Key('key'),
                        // controller: scrollController,
                        physics: state is! ChatOpenState && state is! ChatWebViewOpenState
                            ? const ClampingScrollPhysics()
                            : const NeverScrollableScrollPhysics(),
                        slivers: const <Widget>[
                          EventDetailsNavigationBar(
                            showContent: true,
                            key: Key('key1'),
                          ),
                          EventDetailsTabs(),
                          EventDetailsPlayer(),
                          EventDetailsTopOfHeader(),
                          EventDetailsBottomOfHeader(),
                          EventDetailsCommentOfHeader(),
                          EventChatSelector(),
                          Chat(),
                          // if (state is! ChatOpenState &&
                          //     state is! ChatWebViewOpenState) ...<Widget>[
                          //   const LastMessageWidget(),
                          EventMarketsContainer(),
                          BottomEmptySpace(),
                          SliverToBoxAdapter(
                            child: SizedBox(),
                          ),
                        ],
                      ),
                      if (state is! ChatOpenState && state is! ChatWebViewOpenState)
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Padding(
                            padding: const EdgeInsets.only(bottom: 20.0),
                            child: Container(
                              height: 20,
                              width: 40,
                              color: Colors.cyan,
                            ),
                          ),
                        ),
                    ],
                  );
                },
              ),
            ),
            Container(
              color: ColorSchemePaletteLight.white,
              height: bottomPadding,
            )
          ],
        ),
      ),
    );
  }
}
