import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:marquee/marquee.dart';
import 'package:provider/provider.dart';
import 'package:current_liga_integration/event_detail/costil_change_notifire.dart';
import 'package:current_liga_integration/event_detail/event_details_tabs.dart';
import 'package:current_liga_integration/event_detail/event_details_top_of_header.dart';
import 'package:current_liga_integration/palette.dart';
import 'package:current_liga_integration/text_styles.dart';

const double _minHeightOfBottomOfHeader = 83.0;
const double _maxHeightOfBottomOfHeader = 120.0;

const double _tableHeight = 70.0;
const double _overallGap = 15.0;

class EventDetailsBottomOfHeader extends StatelessWidget {
  const EventDetailsBottomOfHeader({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<TabsStreamCostil>(builder: (
      BuildContext context,
      TabsStreamCostil tabsStreamCostil,
      _,
    ) {
      return StreamBuilder<AppSwitcherItemModel>(
        stream: tabsStreamCostil.eventTabs,
        builder: (BuildContext context, AsyncSnapshot<AppSwitcherItemModel> eventTabsSnapshot) {
          if (eventTabsSnapshot.data == null || eventTabsSnapshot.data?.activeTabName != EventTabTypeEnum.home.name) {
            return const SliverToBoxAdapter(
              child: SizedBox(),
            );
          } else {
            return SliverPersistentHeader(
              pinned: true,
              delegate: _BottomOfHeaderDelegate(
                minHeight: _maxHeightOfBottomOfHeader,
                maxHeight: _maxHeightOfBottomOfHeader,
                child: DecoratedBox(
                  decoration: const BoxDecoration(
                    color: Palette.darkFootball,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                        color: Palette.darkFootball,
                        offset: Offset(0, 2),
                      ),
                    ],
                  ),
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(15.0, 8.0, 15.0, 5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Expanded(
                          child: LayoutBuilder(
                            builder: (BuildContext context, BoxConstraints constraints) {
                              // 32 - logo size
                              // 15 - overall gap
                              final double _maxWidth = constraints.widthConstraints().biggest.width - 32 - _overallGap;

                              return Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Material(
                                    type: MaterialType.transparency,
                                    child: Container(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      alignment: Alignment.centerLeft,
                                      child: Hero(
                                        tag: 'header__bloc.eventId_bloc.uniqueKey_team_one_title',
                                        transitionOnUserGestures: true,
                                        flightShuttleBuilder: (
                                          BuildContext context,
                                          Animation<double> anim,
                                          HeroFlightDirection direction,
                                          BuildContext fromContext,
                                          BuildContext toContext,
                                        ) {
                                          final Hero toHero = toContext.widget as Hero;
                                          if (direction == HeroFlightDirection.pop) {
                                            return FadeTransition(
                                              opacity: const AlwaysStoppedAnimation<double>(0),
                                              child: toHero.child,
                                            );
                                          } else {
                                            return toHero.child;
                                          }
                                        },
                                        child: Material(
                                          type: MaterialType.transparency,
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              const FlutterLogo(),
                                              const SizedBox(width: 4.0),
                                              Expanded(
                                                child: AutoMarqueeText(
                                                  "state.event.team1",
                                                  maxWidth: _maxWidth,
                                                  style: const MarketFilterTextStyle().copyWith(
                                                    color: Palette.white,
                                                    fontSize: 14.0,
                                                  ),
                                                  key: const Key(
                                                    "EventDetailsScreenKeys.teamTitleKey",
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(bottom: 8.0),
                          child: SizedBox(
                            child: Text(
                              "state.event.preEventDate",
                              style: const MarketFilterTextStyle().copyWith(
                                color: Palette.white,
                                fontSize: 12.0,
                              ),
                              textAlign: TextAlign.right,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            );
          }
        },
      );
    });
  }
}

class _BottomOfHeaderDelegate extends SliverPersistentHeaderDelegate {
  _BottomOfHeaderDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => minHeight;

  @override
  double get maxExtent => maxHeight;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  bool shouldRebuild(_BottomOfHeaderDelegate oldDelegate) {
    return maxHeight != oldDelegate.maxHeight || minHeight != oldDelegate.minHeight || child != oldDelegate.child;
  }
}

class AutoMarqueeText extends StatelessWidget {
  const AutoMarqueeText(
    this.text, {
    required this.style,
    this.height = 30,
    this.maxWidth = 100,
    super.key,
  });

  final String text;
  final double height;
  final double maxWidth;
  final TextStyle style;

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        if (_willTextOverflow(text: text, style: style)) {
          return SizedBox(
            height: height,
            width: maxWidth,
            child: Marquee(
              text: text,
              style: style,
              blankSpace: maxWidth / 3,
              velocity: 30,
              pauseAfterRound: const Duration(seconds: 3),
            ),
          );
        } else {
          return SizedBox(
            height: height,
            width: maxWidth,
            child: Text(
              text,
              maxLines: 1,
              style: style,
            ),
          );
        }
      },
    );
  }

  bool _willTextOverflow({required String text, required TextStyle style}) {
    final TextPainter textPainter = TextPainter(
      text: TextSpan(text: text, style: style),
      maxLines: 1,
      textDirection: TextDirection.ltr,
    )..layout(maxWidth: maxWidth);

    return textPainter.didExceedMaxLines;
  }
}
