import 'package:flutter/material.dart';
import 'package:current_liga_integration/event_detail/EventTabUtils.dart';
import 'package:current_liga_integration/event_detail/costil_change_notifire.dart';
import 'package:current_liga_integration/palette.dart';
import 'dart:async';
import 'package:async/async.dart';
import 'chat/utils/keyboard_size_provider.dart';
import 'event_detail_bloc.dart';

/// Tab bar that chooses event states like event info, video, statistics etc.

class EventDetailsTabs extends StatelessWidget {
  const EventDetailsTabs({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    final OldEventDetailsBloc _eventDetailsBloc = Provider.of<OldEventDetailsBloc>(context, listen: false);
    final EventTabUtils _utils = EventTabUtils();

    return Consumer<ScreenHeight>(
      builder: (BuildContext context, ScreenHeight screenHeight, Widget? child) {
        return SliverOffstage(
          offstage: screenHeight.isOpen,
          sliver: SliverToBoxAdapter(
            child: DecoratedBox(
              decoration: const BoxDecoration(
                color: Palette.darkFootball,
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: Palette.darkFootball,
                    offset: Offset(0, 2),
                  ),
                ],
              ),
              child: Consumer<TabsStreamCostil>(builder: (
                BuildContext context,
                TabsStreamCostil tabsStreamCostil,
                _,
              ) {
                return AppSwitcher(
                  stream: tabsStreamCostil.eventTabs,
                  defaultTabList: _utils.getMinimalTabList,
                  tabList: _utils.getEventTabList,
                  singleTabSettingActiveIndex: (
                    List<String> enums,
                  ) {
                    tabsStreamCostil.setActiveTabIndex(
                      activeTab: enums[0],
                      activeTabIndex: 0,
                      context: context,
                    );
                  },
                  settingActiveIndex: (
                    int currentIndex,
                    List<String> enums,
                  ) {
                    tabsStreamCostil.setActiveTabIndex(
                      activeTab: enums[currentIndex],
                      activeTabIndex: currentIndex,
                      context: context,
                    );
                  },
                  specificTabAction: (int index) {
                    // _eventDetailsBloc.startVideoAfterDeeplinkNew(index);
                  },
                );
              }),
            ),
          ),
        );
      },
    );
  }
}

class AppSwitcher extends StatefulWidget {
  const AppSwitcher({
    required this.stream,
    required this.defaultTabList,
    required this.tabList,
    required this.singleTabSettingActiveIndex,
    required this.settingActiveIndex,
    this.specificTabAction,
    super.key,
  });

  /// Sets required stream that would return [AppSwitcherItemModel]
  final Stream<AppSwitcherItemModel> stream;

  /// What to show before StreamBuilder would load the data
  final TabsAndWidgetsModel Function() defaultTabList;

  /// List of available tabs
  final TabsAndWidgetsModel Function(
    Map<String, bool> tabsStatus,
  ) tabList;

  /// Sets active tab for the case when only one tab is available
  final Function(
    List<String> tabNames,
  ) singleTabSettingActiveIndex;

  /// Sets active tab
  final Function(
    int currentIndex,
    List<String> tabNames,
  ) settingActiveIndex;

  /// Specific manipulation with tab list
  final Function(int index)? specificTabAction;

  @override
  State<AppSwitcher> createState() => _AppSwitcherState();
}

class _AppSwitcherState extends State<AppSwitcher> with TickerProviderStateMixin {
  late TabController? _tabController;
  Timer? _debounceTimer;
  CancelableOperation<ActiveTabModel>? _cancelableSwitch;

  @override
  void dispose() {
    _debounceTimer?.cancel();
    _cancelableSwitch?.cancel();
    super.dispose();
  }

  CancelableOperation<ActiveTabModel> _getCancelableTabSwitch({
    required ActiveTabModel model,
  }) {
    return CancelableOperation<ActiveTabModel>.fromFuture(
      Future<ActiveTabModel>.microtask(() => model),
    );
  }

  Timer _getDebounceTimer({
    required ActiveTabModel model,
  }) {
    return Timer(
      const Duration(milliseconds: 800),
      () {
        final CancelableOperation<ActiveTabModel> cancelableSwitch = _getCancelableTabSwitch(
          model: model,
        )..then((ActiveTabModel model) {
            _cancelableSwitch = null;
            widget.settingActiveIndex(
              model.index,
              model.enums,
            );
          });
        _cancelableSwitch = cancelableSwitch;
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<AppSwitcherItemModel>(
      stream: widget.stream,
      builder: (BuildContext context, AsyncSnapshot<AppSwitcherItemModel> snapshot) {
        final TabsAndWidgetsModel minimal = widget.defaultTabList();
        List<Widget> tabs = minimal.widgetList;
        List<String> tabNames = minimal.itemNamesList;
        if (snapshot.hasData && snapshot.data != null) {
          final TabsAndWidgetsModel eventTabs = widget.tabList(snapshot.data!.tabsStatus);
          tabs = eventTabs.widgetList;
          tabNames = eventTabs.itemNamesList;
          if (widget.specificTabAction != null) {
            widget.specificTabAction!(tabNames.indexWhere((String element) => element == 'video'));
          }
        }

        /// If only one tab is available, do not show the tab bar
        if (tabs.length <= 1) {
          widget.singleTabSettingActiveIndex(tabNames);
          return const SizedBox();
        }
        _tabController = TabController(
          length: tabs.length,
          initialIndex: snapshot.data?.activeTabIndex ?? 0,
          vsync: this,
        );
        _tabController!.addListener(() {
          if (_tabController?.indexIsChanging == true) {
            final int currentIndex = _tabController!.index;
            _cancelableSwitch?.cancel();
            _cancelableSwitch = null;
            final ActiveTabModel model = ActiveTabModel(
              index: currentIndex,
              enums: tabNames,
              context: context,
            );
            _debounceTimer?.cancel();
            _debounceTimer = _getDebounceTimer(model: model);
          }
        });
        return SizedBox(
          height: 48.0,
          child: ColoredBox(
            color: ColorSchemePaletteLight.green[800]!,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(64.0, 4.0, 64.0, 8.0),
              child: DecoratedBox(
                decoration: BoxDecoration(
                  color: ColorSchemePaletteLight.green[1100],
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: ConstrainedBox(
                    constraints: const BoxConstraints(
                      maxHeight: 26.0,
                      maxWidth: 280.0,
                    ),
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: ColorSchemePaletteLight.green[1100],
                        borderRadius: BorderRadius.circular(8.0),
                      ),
                      child: TabBar(
                        controller: _tabController,
                        labelColor: ColorSchemePaletteLight.green[1100],
                        unselectedLabelColor: ColorSchemePaletteLight.green[1100],
                        // labelStyle: const Caption.medium(),
                        // unselectedLabelStyle: const Caption.medium(),
                        indicator: BoxDecoration(
                          borderRadius: BorderRadius.circular(3.0),
                          color: ColorSchemePaletteLight.green[800]!,
                        ),
                        tabs: tabs,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }
}

class AppSwitcherItemModel {
  AppSwitcherItemModel({
    required this.tabsStatus,
    required this.activeTabName,
    required this.activeTabIndex,
  });

  Map<String, bool> tabsStatus;
  String activeTabName;
  int activeTabIndex;
}

class TabsAndWidgetsModel {
  TabsAndWidgetsModel({
    required this.widgetList,
    required this.itemNamesList,
  });

  final List<Widget> widgetList;
  final List<String> itemNamesList;
}

class ActiveTabModel {
  ActiveTabModel({
    required this.index,
    required this.enums,
    required this.context,
  });

  final int index;
  final List<String> enums;
  final BuildContext context;
}
