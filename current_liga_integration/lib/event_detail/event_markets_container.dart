import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class EventMarketsContainer extends StatelessWidget {
  const EventMarketsContainer({super.key});

  @override
  Widget build(BuildContext context) {
    return SliverList(
      delegate: SliverChildListDelegate(
        List.generate(
          30,
          (index) => Container(
            margin: EdgeInsets.only(top: 4),
            height: 50,
            color: Colors.red,
          ),
        ),
      ),
    );
  }
}
