import 'package:flutter/material.dart';
import 'package:current_liga_integration/event_detail/costil_change_notifire.dart';
import 'package:current_liga_integration/event_detail/event_details_tabs.dart';
import 'package:current_liga_integration/event_detail/event_details_top_of_header.dart';

import 'chat/utils/keyboard_size_provider.dart';

class EventDetailsPlayer extends StatelessWidget {
  const EventDetailsPlayer({super.key});

  @override
  Widget build(BuildContext context) {
    return Consumer<TabsStreamCostil>(
      builder: (
        BuildContext context,
        TabsStreamCostil tabsStreamCostil,
        _,
      ) {
        return StreamBuilder<AppSwitcherItemModel>(
          stream: tabsStreamCostil.eventTabs,
          builder: (BuildContext context, AsyncSnapshot<AppSwitcherItemModel> eventTabsSnapshot) {
            if (eventTabsSnapshot.data == null ||
                eventTabsSnapshot.data?.activeTabName == EventTabTypeEnum.video.name) {
              return Consumer<ScreenHeight>(
                builder: (BuildContext context, ScreenHeight screenHeight, Widget? child) {
                  return SliverPersistentHeader(
                    pinned: true,
                    delegate: EventDetailsAppBarDelegate(
                      minHeight: _calculateHeight(
                        aspectRatio: 1,
                        width: MediaQuery.of(context).size.width,
                        screenHeight: screenHeight,
                      ),
                      maxHeight: _calculateHeight(
                        aspectRatio: 1,
                        width: MediaQuery.of(context).size.width,
                        screenHeight: screenHeight,
                      ),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width,
                        height: _calculateHeight(
                          aspectRatio: 1,
                          width: MediaQuery.of(context).size.width,
                          screenHeight: screenHeight,
                        ),
                        child: ColoredBox(
                          color: Colors.tealAccent,
                        ),
                      ),
                    ),
                  );
                },
              );
            } else {
              return const SliverToBoxAdapter(
                child: SizedBox(),
              );
            }
          },
        );
      },
    );
  }

  double _calculateHeight({required double aspectRatio, required double width, required ScreenHeight screenHeight}) {
    if ((screenHeight.keyboardHeight + (width / aspectRatio) + 50) >= screenHeight.screenHeight) {
      return 0;
    } else {
      return screenHeight.isOpen ? (width / aspectRatio) / 2 : width / aspectRatio;
    }
  }
}

class EventDetailsAppBarDelegate extends SliverPersistentHeaderDelegate {
  EventDetailsAppBarDelegate({
    required this.minHeight,
    required this.maxHeight,
    required this.child,
  });

  final double minHeight;
  final double maxHeight;
  final Widget child;

  @override
  double get minExtent => maxHeight;

  @override
  double get maxExtent => maxHeight;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    return child;
  }

  @override
  bool shouldRebuild(EventDetailsAppBarDelegate oldDelegate) {
    return child != oldDelegate.child;
  }
}
