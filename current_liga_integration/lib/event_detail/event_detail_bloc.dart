import 'dart:async';

import 'package:built_collection/built_collection.dart';
import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';
import 'package:current_liga_integration/event_detail/EventTabUtils.dart';

import 'event_details_tabs.dart';

class OldEventDetailsBloc {
  OldEventDetailsBloc() : super() {
    init();
  }

  // final BehaviorSubject<EventDetailsNavigationBarModel> _navigationBarModelSubject =
  // BehaviorSubject<EventDetailsNavigationBarModel>();
  // final BehaviorSubject<EventDetailsWithMarketsModel> _eventWithMarketsModelSubject =
  // BehaviorSubject<EventDetailsWithMarketsModel>();
  // final BehaviorSubject<EventDetailsModel> _eventDetailsSubject = BehaviorSubject<EventDetailsModel>();
  // final BehaviorSubject<EventDetailsStatusModel> _statusSubject = BehaviorSubject<EventDetailsStatusModel>();
  // final BehaviorSubject<EventDetailsMarketsHeaderModel> _marketsHeaderSubject =
  // BehaviorSubject<EventDetailsMarketsHeaderModel>();
  // final BehaviorSubject<EventDetailsSortType?> _sortTypeSubject = BehaviorSubject<EventDetailsSortType?>();
  // final BehaviorSubject<bool> _isVideoLoadingSubject = BehaviorSubject<bool>.seeded(false);
  final BehaviorSubject<AppSwitcherItemModel> _tabsListSubject = BehaviorSubject<AppSwitcherItemModel>();
  final BehaviorSubject<AppSwitcherItemModel> _chatTabsListSubject = BehaviorSubject<AppSwitcherItemModel>();

  StreamSubscription<bool>? _statsTabSubscription;
  StreamSubscription<bool>? _openVideoTabSubscription;
  StreamSubscription<int?>? _videoTabIndexSubscription;
  StreamSubscription<bool>? _closeVideoSubscription;

  late AppSwitcherItemModel _eventTabStateModel;
  int? _previousTabIndex;
  String? _previousTab;

  AppSwitcherItemModel get eventTabState => _eventTabStateModel;

  late AppSwitcherItemModel _eventChatTabStateModel;

  AppSwitcherItemModel get eventChatTabState => _eventChatTabStateModel;

  Stream<AppSwitcherItemModel> get eventTabs => _tabsListSubject.stream;

  Stream<AppSwitcherItemModel> get chatTabs => _chatTabsListSubject.stream;

  late final EventTabUtils _utils;

  @override
  Future<void> init() async {
    _utils = EventTabUtils();
    _eventTabStateModel = _utils.initialTabs();
    _eventChatTabStateModel = _utils.initialChatTabs(true);
    _subscribe();
  }

  // void setActiveTabIndex({
  //   required String activeTab,
  //   required int activeTabIndex,
  //   required BuildContext context,
  // }) {
  //   _eventTabStateModel.activeTabName = activeTab;
  //   _eventTabStateModel.activeTabIndex = activeTabIndex;
  //   if (activeTab == EventTabUtils.videoTabName) {
  //     actions.videoTabIndex(activeTabIndex);
  //     // Check if player is already started
  //     if (currentPlayerModel == null) {
  //       onVideoStreamButtonTap();
  //     }
  //   } else {
  //     if (_eventTabStateModel.tabsStatus[EventTabUtils.videoTabName] == true) {
  //       stopVideo();
  //     }
  //   }
  // }

  void goBackFromStats() {
    // _eventTabStateModel.activeTabName = _previousTab ?? EventTabUtils.homeTabName;
    // _eventTabStateModel.activeTabIndex = _previousTabIndex ?? 0;
    // if (_previousTab != null && _previousTab == EventTabUtils.videoTabName && currentPlayerModel == null) {
    //   // onVideoStreamButtonTap();
    // }
    // // _updateEventTabs();
  }

  void goVideoTab() {
    // if (state.videoTabIndex != null) {
    //   actions.openVideoTab(false);
    //   _eventTabStateModel.activeTabName = EventTabUtils.videoTabName;
    //   _eventTabStateModel.activeTabIndex = state.videoTabIndex!;
    //   _updateEventTabs();
    // }
  }

  void setActiveChatTabIndex({
    required String activeTabName,
    required int activeTabIndex,
  }) {
    _eventChatTabStateModel.activeTabName = activeTabName;
    _eventChatTabStateModel.activeTabIndex = activeTabIndex;
    // _updateChatTabs();
  }

  @override
  void dispose() {
    _statsTabSubscription?.cancel();
    _openVideoTabSubscription?.cancel();
    _videoTabIndexSubscription?.cancel();
    _closeVideoSubscription?.cancel();
    _tabsListSubject.close();
    _chatTabsListSubject.close();
  }


  void _subscribe() {
    // _videoTabSubscription = eventDetailsModelStream.listen((EventDetailsModel model) {
    //   if (_userAuthenticated) {
    //     _eventTabStateModel.tabsStatus[EventTabUtils.videoTabName] = model.videoButtonAvailable;
    //     _updateEventTabs();
    //   }
    // });
  //
  //   _statsTabSubscription = statisticsStream.listen((bool event) {
  //     _eventTabStateModel.tabsStatus[EventTabUtils.statsTabName] = event;
  //     if (this.event != null) {
  //       _eventTabStateModel.tabsStatus[EventTabUtils.widgetTabName] =
  //           chechAvaliableSportForBetRadarWidget(this.event!.gameId) && event;
  //     }
  //     _updateEventTabs();
  //   });
  //
  //   _openVideoTabSubscription = _openVideoTabSubscribe().listen(_dealWithOpeningVideoTab);
  //
  // void _updateEventTabs() {
  //   _tabsListSubject.add(_eventTabStateModel);
  // }
  //
  // void _updateChatTabs() {
  //   _chatTabsListSubject.add(_eventChatTabStateModel);
  // }
  //
  // Stream<bool> _openVideoTabSubscribe() {
  //   return appStateStream((AppState state) => state.videoTabOpened);
  }
}
