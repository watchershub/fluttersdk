import 'package:flutter/material.dart';
import 'event_detail_content.dart';

class EventDetailsView extends StatefulWidget {
  const EventDetailsView({super.key});

  @override
  _EventDetailsViewState createState() => _EventDetailsViewState();
}

class _EventDetailsViewState extends State<EventDetailsView> {
  @override
  Widget build(BuildContext context) {
    return const EventDetailsContent();
  }
}
