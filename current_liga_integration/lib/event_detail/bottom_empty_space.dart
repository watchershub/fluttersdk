import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:current_liga_integration/palette.dart';

import 'chat/domain/bloc/chat_cubit.dart';

class BottomEmptySpace extends StatelessWidget {
  const BottomEmptySpace({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<ChatCubit, ChatState>(
      builder: (BuildContext context, ChatState state) {
        if (state is ChatOpenState) {
          return const SliverToBoxAdapter(
            child: SizedBox(),
          );
        } else {
          return SliverToBoxAdapter(
            child: SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height / 8,
              child: const ColoredBox(
                color: Palette.lightGrey999,
              ),
            ),
          );
        }
      },
    );
  }
}
