import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class Palette {
  // ignore: regexp_exclude
  static const Color transparent = Color(0x00000000);

  /// 0, 206, 147
  // ignore: regexp_exclude
  static const Color primaryLight = Color(0xFF00CE93);

  /// 204, 245, 233
  // ignore: regexp_exclude
  static const Color primaryLight20 = Color(0xFFCCF5E9);

  /// 0, 154, 110
  // ignore: regexp_exclude
  static const Color primary = Color(0xFF009A6E);

  /// 31, 166, 127
  // ignore: regexp_exclude
  static const Color primary12 = Color(0xFF1FA67F);

  /// 153, 214, 197
  // ignore: regexp_exclude
  static const Color primary60 = Color(0xFF99D6C5);

  /// 204, 234, 226
  // ignore: regexp_exclude
  static const Color primary80 = Color(0xFFCCEAE2);

  /// 0, 106, 74
  // ignore: regexp_exclude
  static const Color primaryDark = Color(0xFF006A4A);

  /// 128, 180, 164
  // ignore: regexp_exclude
  static const Color primaryDark50 = Color(0xFF80B4A4);

  // 255, 255, 255, 255
  // ignore: regexp_exclude
  static const Color white = Color(0xFFFFFFFF);

  /// 244, 244, 244  = grey[100]
  // ignore: regexp_exclude
  static const Color backgroundWhite = Color(0xFFF4F4F4);

  /// 211, 211, 211
  // ignore: regexp_exclude
  static const Color lightGray = Color(0xFFD3D3D3);

  /// 225, 225, 225 = grey[300]
  // ignore: regexp_exclude
  static const Color gray12 = Color(0xFFE1E1E1);

  /// 199, 199, 199
  // ignore: regexp_exclude
  static const Color gray22 = Color(0xFFC7C7C7);

  /// 242, 242, 242
  // ignore: regexp_exclude
  static const Color loaderBackground = Color(0xFFF2F2F2);

  /// 153, 153, 153
  // ignore: regexp_exclude
  static const Color gray40 = Color(0xFF999999);

  /// 121, 121, 121
  // ignore: regexp_exclude
  static const Color gray50 = Color(0xFF797979);

  /// 74, 74, 74
  // ignore: regexp_exclude
  static const Color gray70 = Color(0xFF4A4A4A);

  /// 38, 38, 38
  // ignore: regexp_exclude
  static const Color gray85 = Color(0xFF262626);

  /// 14, 14, 14
  // ignore: regexp_exclude
  static const Color gray90 = Color(0xFF0E0E0E);

  /// 14, 14, 14
  // ignore: regexp_exclude
  static const Color gray95 = Color(0xFF535353);

  // ignore: regexp_exclude
  static const Color grayBrown = Color(0xFF434343);

  /// 0, 0, 0
  // ignore: regexp_exclude
  static const Color black = Color(0xFF000000);

  /// 255, 203, 0
  // ignore: regexp_exclude
  static const Color warning = Color(0xFFFFCB00);

  /// 255, 161, 0
  // ignore: regexp_exclude
  static const Color accent = Color(0xFFFFA100);

  /// 213, 28, 28
  // ignore: regexp_exclude
  static const Color danger = Color(0xFFD51C1C);

  /// 121, 231, 117
  // ignore: regexp_exclude
  static const Color startProgress = Color(0xFF79E775);

  ///  255, 204, 117
  // ignore: regexp_exclude
  static const Color averageProgress = Color(0xFFFFCC75);

  /// 255, 167, 167
  // ignore: regexp_exclude
  static const Color finalProgress = Color(0xFFFFA7A7);

  /// 238, 164, 164
  // ignore: regexp_exclude
  static const Color dangerLight = Color(0xFFEEA4A4);

  //todo(Luvti): below not in design system
  //0, 0, 0, 0.1
  // ignore: regexp_exclude
  static const Color black10 = Color(0x1A000000);

  //0, 0, 0, 0.2
  // ignore: regexp_exclude
  static const Color black20 = Color(0x33000000);

  //0, 0, 0, 0.3
  // ignore: regexp_exclude
  static const Color black30 = Color(0x4D000000);

  //0, 0, 0, 0.5
  // ignore: regexp_exclude
  static const Color black50 = Color(0x80000000);

  //0, 0, 0, 0.8
  // ignore: regexp_exclude
  static const Color black80 = Color(0xCC000000);

  //0, 0, 0, 0.9
  // ignore: regexp_exclude
  static const Color black90 = Color(0xE6000000);

  // ignore: regexp_exclude
  static const Color white10 = Color(0x1AFFFFFF);

  /// 255. 255, 255, 20
  // ignore: regexp_exclude
  static const Color white20 = Color(0x33FFFFFF);

  /// 255. 255, 255, 50 #0x7dFFFFFF
  // ignore: regexp_exclude
  static const Color white50 = Color(0x7DFFFFFF);

  /// 255. 255, 255, 80
  // ignore: regexp_exclude
  static const Color white80 = Color(0xCCFFFFFF);

  /// 0, 154, 110
  // ignore: regexp_exclude
  static const Color primary50 = Color(0x7D009A6E);

  /// 218, 196, 15
  // ignore: regexp_exclude
  static const Color gold = Color(0xFFDAC40F);

  /// 241, 229, 12
  // ignore: regexp_exclude
  static const Color yellow = Color(0xFFF1E60C);

  // 218, 246, 43
  // ignore: regexp_exclude
  static const Color wheelYellow = Color(0xFFDAF62B);

  // ignore: regexp_exclude
  static const Color wheelDarkGreen = Color(0xFF004D32);

  // ignore: regexp_exclude
  static const Color wheelGreenOne = Color(0xFF017D4C);

  // ignore: regexp_exclude
  static const Color wheelGreenTwo = Color(0xFF0AB175);

  // ignore: regexp_exclude
  static const Color wheelGreenThree = Color(0xFF004B1E);

  // ignore: regexp_exclude
  static const Color wheelLightGreen = Color(0xFF00CE84);

  // ignore: regexp_exclude
  static const Color jackpotEveryDay = Color(0xFF009D42);

  // ignore: regexp_exclude
  static const Color jackpotEveryWeek = Color(0xFFA82BFF);

  // ignore: regexp_exclude
  static const Color jackpotEveryMoth = Color(0xFFFF611D);

  // ignore: regexp_exclude
  static const Color topEventsPrimary = Color(0xFF0A9575);

  // ignore: regexp_exclude
  static const Color topEventsSecondary = Color(0xFF2E435C);

  // 2, 103, 46, 0.5
  // ignore: regexp_exclude
  static const Color clubStatus = Color(0x7D02672E);

  // 85, 92, 95, 0.6
  // ignore: regexp_exclude
  static const Color silverStatus = Color(0x99555C5F);

  // 204, 164, 77, 0.6
  // ignore: regexp_exclude
  static const Color goldStatus = Color(0x99CCA44D);

  // Color.blue
  // ignore: regexp_exclude
  static const Color blue = Color(0xFF2196F3);

  // Color.blue[300]
  // ignore: regexp_exclude
  static const Color blueSecondary = Color(0xFF64B5F6);

  // ignore: regexp_exclude
  static const Color blueAccent = Color(0xFF448AFF);

  // ignore: regexp_exclude
  static const Color updateBlue = Color(0xFF2E435C);

  // ignore: regexp_exclude
  static const Color updateBlue2 = Color(0xFF303C5A);

  // ignore: regexp_exclude
  static const Color pink = Color(0xFFE91E63);

  static const Color systemNavigationBar = primaryDark;

  /// 20, 20, 20
  // ignore: regexp_exclude
  static const Color gray88 = Color(0xFF141414);

  /// 33, 33, 33
  // ignore: regexp_exclude
  static const Color gray86 = Color(0xFF212121);

  /// 156, 156, 156
  // ignore: regexp_exclude
  static const Color textBodyNewYear = Color(0xFF9C9C9C);

  /// 104, 74, 9
  // ignore: regexp_exclude
  static const Color newYearJackpot = Color(0xFF684A09);

  /// 198, 137, 45
  // ignore: regexp_exclude
  static const Color newYearPresent = Color(0xFFC6892D);

  /// 228, 173, 57
  // ignore: regexp_exclude
  static const Color newYearStatistics = Color(0xFFE4AD39);

  /// 255, 220, 98
  // ignore: regexp_exclude
  static const Color collectTheSameButtonTopGradient = Color(0xFFFFDC62);

  /// 251, 127, 14
  // ignore: regexp_exclude
  static const Color collectTheSameButtonBottomGradient = Color(0xFFFB7F0E);

  // ignore: regexp_exclude
  static const Color highYellow = Color(0xFFF8E700);

  // ignore: regexp_exclude
  static const Color blackAlternative = Color(0xFF3A3A3A);

  // ignore: regexp_exclude
  static const Color eventDetailChatAlertShadow = Color(0xFF37403D);

  // ignore: regexp_exclude
  static const Color eventDetailChatDivider = Color(0xFFE0E0E0);

  //invite a friend colors
  // ignore: regexp_exclude
  static const Color inviteAFriendBack = Color(0xFFEDF9F3);

  // ignore: regexp_exclude
  static const Color shareButton = Color(0xFF00AB4E);

  // ignore: regexp_exclude
  static const Color waitingForCode = Color(0xFFFFB545);

  // ignore: regexp_exclude
  static const Color expiredCode = Color(0xFFFF6666);

  // ignore: regexp_exclude
  static const Color getCode = Color(0xFF64C993);

  // ignore: regexp_exclude
  static const Color code = Color(0xFF7A7A7A);

  // ignore: regexp_exclude
  static const Color copyCode = Color(0xFF292929);

  // ignore: regexp_exclude
  static const Color currency = Color(0xFFA1A3A1);

  // ignore: regexp_exclude
  static const Color notification = Color(0xFFE5FFF1);

  // ignore: regexp_exclude
  static const Color notificationBorder = Color(0xFF8DF1BB);

  // ignore: regexp_exclude
  static const Color checkCircle = Color(0xFF48B078);

  // ignore: regexp_exclude
  static const Color promoCodeBorder = Color(0XFFD5D3D3);

  // ignore: regexp_exclude
  static const Color clearIcon = Color(0xFF555555);

  static Color hexToColor(String code) {
    // ignore: regexp_exclude
    return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

  // ignore: regexp_exclude
  static const Color darkFootball = Color(0xFF00AB4E);
  // ignore: regexp_exclude
  static const Color darkSolidBodyHover = Color(0xFF007133);
  // ignore: regexp_exclude
  static const Color darkTextLinkVisited = Color(0xFFA6FFCE);
  // ignore: regexp_exclude
  static const Color lightGrey999 = Color(0xFFF5F5F5);
  // ignore: regexp_exclude
  static const Color lightTextSecondary = Color(0xFF7A7A7A);
  // ignore: regexp_exclude
  static const Color lightTextTertiary = Color(0xFFA1A3A1);
  // ignore: regexp_exclude
  static const Color lightGrey900 = Color(0xFFF0F0F0);
  // ignore: regexp_exclude
  static const Color grino = Color(0xFFF8FFFD);
  // ignore: regexp_exclude
  static const Color eclipse = Color(0xFF3B3B3B);
  // ignore: regexp_exclude
  static const Color eventClockGrey = Color(0xFF292929);
  // ignore: regexp_exclude
  static const Color lightVolleyball = Color(0xFF00A775);
  // ignore: regexp_exclude
  static const Color tabItemBorder = Color(0xFFE9E9E9);
  // ignore: regexp_exclude
  static const Color lightStrokeWarning = Color(0xFFFF4944);
  // ignore: regexp_exclude
  static const Color questionColor = Color(0xFF007DC5);
  // ignore: regexp_exclude
  static const Color backgroundDarkZero = Color(0xFF050505);
  // ignore: regexp_exclude
  static const Color lightWarning = Color(0xffff8345);
  // ignore: regexp_exclude
  static const Color keyboardBackgroundColor = Color(0xFFCCCED3);
  // ignore: regexp_exclude
  static const Color keyboardDelimeterColor = Color(0xFFBCC0C5);
  // ignore: regexp_exclude
  static const Color decreaseCoefficient = Color(0xFFC73834);
  // ignore: regexp_exclude
  static const Color increaseCoefficient = Color(0xFF00AB4E);
  // ignore: regexp_exclude
  static const Color eventSubtitle = Color(0xFF005B2A);
  // ignore: regexp_exclude
  static const Color phoneBackground = Color(0xFFECFFF9);
  // ignore: regexp_exclude
  static const Color refreshButtonColor = Color(0xFFE87841);
  // ignore: regexp_exclude
  static const Color closeButtonBackground = Color(0xFFE2E2E2);
}

class ColorSchemePaletteLight {
  static const Color white = Colors.white;

  /// rgba(242, 255, 248, 1) - 50
  ///
  /// rgba(204, 255, 227, 1) - 100
  ///
  /// rgba(166, 255, 206, 1) - 200
  ///
  /// rgba(124, 255, 184, 1) - 300
  ///
  /// rgba(34, 243, 130, 1) - 400
  ///
  /// rgba(0, 231, 105, 1)- 500
  ///
  /// rgba(0, 214, 97, 1)- 600
  ///
  /// rgba(0, 189, 86, 1)- 700
  ///
  /// rgba(0, 171, 78, 1) - 800
  ///
  ///rgba(0, 150, 68, 1) - 900
  ///
  ///rgba(0, 131, 60, 1) - 1000
  ///
  ///rgba(0, 113, 51, 1)- 1100
  ///
  ///rgba(0, 91, 42, 1) - 1200
  ///
  ///rgba(0, 72, 33, 1) - 1300
  ///
  ///rgba(0, 44, 20, 1) - 1400
  static const Map<int, Color> green = <int, Color>{
    50: Color(0xFFf2fff8),
    100: Color(0xFFccffe3),
    200: Color(0xFFa6ffce),
    300: Color(0xFF7cffb8),
    400: Color(0xFF22f382),
    500: Color(0xFF00e769),
    600: Color(0xFF00d661),
    700: Color(0xff00bd56),
    800: Color(0xFF00AB4E),
    900: Color(0xFF009644),
    1000: Color(0xFF00833C),
    1100: Color(0xFF007133),
    1200: Color(0xFF005B2A),
    1300: Color(0xFF004821),
    1400: Color(0xFF002C14),
  };

  ///rgba(249, 249, 255, 1) - 50
  ///
  /// rgba(236, 236, 255, 1) - 100
  ///
  /// rgba(198, 198, 255, 1) - 200
  ///
  /// rgba(148, 148, 255, 1) - 300
  ///
  /// rgba(90, 90, 255, 1) - 400
  ///
  /// rgba(64, 64, 255, 1) - 500
  ///
  /// rgba(54, 54, 207, 1) - 600
  ///
  /// rgba(40, 40, 146, 1) - 700
  ///
  /// rgba(15, 15, 55, 1) - 800
  static const Map<int, Color> darkBlue = <int, Color>{
    50: Color(0xFFF9F9FF),
    100: Color(0xFFECECFF),
    200: Color(0xFFC6C6FF),
    300: Color(0xFF9494FF),
    400: Color(0xFF5A5AFF),
    500: Color(0xFF4040FF),
    600: Color(0xFF3636CF),
    700: Color(0xff282892),
    800: Color(0xFF0F0F37),
  };

  ///rgba(250, 250, 250, 1) - 50
  ///
  /// rgba(246, 246, 246, 1) - 100
  ///
  /// rgba(245, 245, 245, 1) - 200
  ///
  /// rgba(240, 240, 240, 1) - 300
  ///
  /// rgba(233, 233, 233, 1) - 400
  ///
  /// rgba(212, 211, 211, 1) - 500
  ///
  /// rgba(193, 193, 193, 1) - 600
  ///
  /// rgba(161, 163, 161, 1) - 700
  ///
  /// rgba(138, 139, 138, 1) - 800
  ///
  /// rgba(122, 122, 122, 1) - 900
  ///
  /// rgba(105, 105, 105, 1) - 1000
  ///
  /// rgba(85, 85, 85, 1) - 1100
  ///
  /// rgba(60, 60, 60, 1) - 1200
  ///
  /// rgba(41, 41, 41, 1) - 1300
  ///
  /// rgba(32, 32, 32, 1) - 1400
  static const Map<int, Color> gray = <int, Color>{
    50: Color(0xFFFAFAFA),
    100: Color(0xFFF6F6F6),
    200: Color(0xFFF5F5F5),
    300: Color(0xFFF0F0F0),
    400: Color(0xFFE9E9E9),
    500: Color(0xFFD4D3D3),
    600: Color(0xFFC1C1C1),
    700: Color(0xFFA1A3A1),
    800: Color(0xFF8A8B8A),
    900: Color(0xFF7A7A7A),
    1000: Color(0xFF696969),
    1100: Color(0xFF555555),
    1200: Color(0xFF3C3C3C),
    1300: Color(0xFF292929),
    1400: Color(0xFF202020),
  };

  /// rgba(255, 247, 246, 1) - 50
  ///
  /// rgba(255, 213, 211, 1) - 100
  ///
  /// rgba(255, 177, 175, 1) - 200
  ///
  /// rgba(255, 148, 144, 1) - 300
  ///
  /// rgba(255, 116, 113, 1) - 400
  ///
  /// rgba(255, 73, 68, 1) - 500
  ///
  /// rgba(241, 68, 63, 1) - 600
  ///
  /// rgba(228, 65, 61, 1) - 700
  ///
  /// rgba(199, 56, 52, 1) - 800
  ///
  /// rgba(177, 51, 47, 1) - 900
  ///
  /// rgba(152, 44, 41, 1) - 1000
  ///
  /// rgba(123, 35, 33, 1) - 1100
  ///
  /// rgba(100, 30, 28, 1) - 1200
  ///
  /// rgba(74, 22, 21, 1) - 1300
  ///
  /// rgba(49, 15, 14, 1) - 1400
  static const Map<int, Color> red = <int, Color>{
    50: Color(0xFFFFF7F6),
    100: Color(0xFFFFD5D3),
    200: Color(0xFFFFB1AF),
    300: Color(0xFFFF9490),
    400: Color(0xFFFF7471),
    500: Color(0xFFFF4944),
    600: Color(0xFFF1443F),
    700: Color(0xFFE4413D),
    800: Color(0xFFC73834),
    900: Color(0xFFB1332F),
    1000: Color(0xFF982C29),
    1100: Color(0xFF7B2321),
    1200: Color(0xFF641E1C),
    1300: Color(0xFF4A1615),
    1400: Color(0xFF310F0E),
  };

  /// rgba(255, 248, 244, 1) - 50
  ///
  /// rgba(255, 237, 227, 1) - 100
  ///
  /// rgba(255, 220, 202, 1) - 200
  ///
  /// rgba(255, 201, 174, 1) - 300
  ///
  /// rgba(255, 181, 144, 1) - 400
  ///
  /// rgba(255, 160, 113, 1) - 500
  ///
  /// rgba(255, 131, 69, 1) - 600
  ///
  /// rgba(232, 120, 65, 1) - 700
  ///
  /// rgba(212, 110, 59, 1) - 800
  ///
  /// rgba(197, 103, 56, 1) - 900
  ///
  /// rgba(175, 92, 50, 1) - 1000
  ///
  /// rgba(155, 80, 43, 1) - 1100
  ///
  /// rgba(129, 66, 35, 1) - 1200
  ///
  /// rgba(88, 46, 25, 1) - 1300
  ///
  /// rgba(56, 29, 16, 1) - 1400
  static const Map<int, Color> orange = <int, Color>{
    50: Color(0xFFFFF8F4),
    100: Color(0xFFFFEDE3),
    200: Color(0xFFFFDCCA),
    300: Color(0xFFFFC9AE),
    400: Color(0xFFFFB590),
    500: Color(0xFFFFA071),
    600: Color(0xFFFF8345),
    700: Color(0xFFE87841),
    800: Color(0xFFD46E3B),
    900: Color(0xFFC56738),
    1000: Color(0xFFAF5C32),
    1100: Color(0xFF9B502B),
    1200: Color(0xFF814223),
    1300: Color(0xFF582E19),
    1400: Color(0xFF381D10),
  };

  /// rgba(244, 251, 255, 1) - 50
  ///
  /// rgba(212, 239, 255, 1) - 100
  ///
  /// rgba(187, 230, 255, 1) - 200
  ///
  /// rgba(153, 218, 255, 1) - 300
  ///
  /// rgba(111, 202, 255, 1) - 400
  ///
  /// rgba(64, 185, 255, 1) - 500
  ///
  /// rgba(1, 162, 255, 1) - 600
  ///
  /// rgba(0, 147, 231, 1) - 700
  ///
  /// rgba(0, 125, 197, 1) - 800
  ///
  /// rgba(0, 103, 163, 1) - 900
  ///
  /// rgba(0, 90, 141, 1) - 1000
  ///
  /// rgba(0, 75, 121, 1) - 1100
  ///
  /// rgba(0, 56, 89, 1) - 1200
  ///
  /// rgba(0, 28, 45, 1) - 1300
  ///
  /// rgba(0, 21, 33, 1) - 1400
  static const Map<int, Color> blue = <int, Color>{
    50: Color(0xFFF4FBFF),
    100: Color(0xFFD4EFFF),
    200: Color(0xFFBBE6FF),
    300: Color(0xFF99DAFF),
    400: Color(0xFF6FCAFF),
    500: Color(0xFF40B9FF),
    600: Color(0xFF01A2FF),
    700: Color(0xFF0093E7),
    800: Color(0xFF007DC5),
    900: Color(0xFF0067A3),
    1000: Color(0xFF005A8D),
    1100: Color(0xFF004B79),
    1200: Color(0xFF003859),
    1300: Color(0xFF001C2D),
    1400: Color(0xFF001521),
  };

  /// rgba(255, 254, 242, 1) - 50
  ///
  /// rgba(255, 251, 214, 1) - 100
  ///
  /// rgba(255, 245, 159, 1) - 200
  ///
  /// rgba(253, 237, 96, 1) - 300
  ///
  /// rgba(255, 230, 0, 1) - 400
  ///
  /// rgba(241, 217, 0, 1) - 500
  ///
  /// rgba(219, 197, 0, 1) - 600
  ///
  /// rgba(197, 177, 0, 1) - 700
  ///
  /// rgba(177, 160, 0, 1) - 800
  ///
  /// rgba(160, 144, 0, 1) - 900
  ///
  /// rgba(146, 131, 0, 1) - 1000
  ///
  /// rgba(131, 118, 0, 1) - 1100
  ///
  /// rgba(121, 109, 0, 1) - 1200
  ///
  /// rgba(89, 80, 0, 1) - 1300
  ///
  /// rgba(44, 39, 0, 1) - 1400
  static const Map<int, Color> yellow = <int, Color>{
    50: Color(0xFFFFFEF2),
    100: Color(0xFFFFFBD6),
    200: Color(0xFFFFF59F),
    300: Color(0xFFFDED60),
    400: Color(0xFFFFE600),
    500: Color(0xFFF1D900),
    600: Color(0xFFDBC500),
    700: Color(0xFFC5B100),
    800: Color(0xFFB1A000),
    900: Color(0xFFA09000),
    1000: Color(0xFF928300),
    1100: Color(0xFF837600),
    1200: Color(0xFF796D00),
    1300: Color(0xFF595000),
    1400: Color(0xFF2C2700),
  };

  ///background
  static const Color black = Color(0xFF000000);
  static const Color backgroundDark = Color(0x99050505);
  static const Color zero = Color(0xFFFFFFFF);
  static const Color grey = Color(0xFFFAFAFA);
  static const Color greenMain = Color(0xFFF2FFF8);
  static const Color navy = Color(0xFFF9F9FF);
  static const Color backgroundRed = Color(0xFFFFF7F7);
  static const Color backgroundOrange = Color(0xFFFFF8F4);
  static const Color backgroundGreen = Color(0xFFECFFF9);
  static const Color backgroundBlue = Color(0xFFF4FBFF);
  static const Color backgroundYellow = Color(0xFFFFFEF2);
  static const Color backgroundGrino = Color(0xFFF8FFFD);
  static const Color football = Color(0xFF00AB4E);
  static const Color hockey = Color(0xFFC73834);
  static const Color basketball = Color(0xFFD46E3B);
  static const Color volleyball = Color(0xFF00A775);
  static const Color tennis = Color(0xFF007DC5);
  static const Color cyber = Color(0xFFB1A000);

  ///palette
  static const Color brandPrimary = Color(0xFF00AB4E);
  static const Color brandSecondary = Color(0xFF0F0F37);
  static const Color gray000 = Color(0xFF292929);
  static const Color gray100 = Color(0xFF3B3B3B);
  static const Color gray200 = Color(0xFF555555);
  static const Color gray300 = Color(0xFF7A7A7A);
  static const Color gray400 = Color(0xFF8A8B8A);
  static const Color gray500 = Color(0xFFA1A3A1);
  static const Color gray600 = Color(0xFFC1C1C1);
  static const Color gray700 = Color(0xFFD5D3D3);
  static const Color gray800 = Color(0xFFE9E9E9);
  static const Color gray900 = Color(0xFFF0F0F0);
  static const Color gray999 = Color(0xFFF5F5F5);
  static const Color warning = Color(0xFFFF8345);

  ///button
  static const Color solidBodyNormal = Color(0xFF00AB4E);
  static const Color solidBodyClick = Color(0xFF005B2A);
  static const Color solidText = Color(0xFFFFFFFF);
  static const Color strokeText = Color(0xFF292929);
  static const Color strokeHover = Color(0xFF7A7A7A);
  static const Color strokeNormal = Color(0xFF00AB4E);
  static const Color strokeClick = Color(0xFF292929);
  static const Color strokeWarning = Color(0xFFFF4944);
  static const Color strokeAlarm = Color(0xFFFFE500);
  static const Color solidBodyNormalWarning = Color(0xFFFF4944);
  static const Color solidBodyAlarm = Color(0xFFFF4944);
  static const Color transparentHover = Color(0xFFcceedc);
  static const Color transparentClick = Color(0xFFd8d8d8);
  static const Color xLgSmartShadow = Color(0x1A000000);

  ///statuses
  static const Color statusGreen = Color(0xFF64C993);
  static const Color statusRed = Color(0xFFFF6666);
  static const Color statusYellow = Color(0xFFFFB545);
  static const Color statusGrey = Color(0xFFC1C1C1);

  ///text
  static const Color textZero = Color(0xFFFFFFFF);
  static const Color textMain = Color(0xFF292929);
  static const Color textSecondary = Color(0xFF7A7A7A);
  static const Color textTertiary = Color(0xFFA1A3A1);
  static const Color textLink = Color(0xFF00AB4E);
  static const Color textLinkVisited = Color(0xFF005B2A);
  static const Color textDanger = Color(0xFFFF4944);

  ///alerts
  static const Color solidError = Color(0xFFFFEDED);
  static const Color solidBetResultError = Color(0xFFF1443F);
  static const Color solidInfo = Color(0xFFF8F8F8);
  static const Color solidWarning = Color(0xFFFFF9EC);
  static const Color solidBetResultWarning = Color(0xFFFFA071);
  static const Color solidSuccess = Color(0xFFE5FFF1);
  static const Color solidBetResultSuccess = Color(0xFF009644);
  static const Color iconWarning = Color(0xFFFF8800);
  static const Color iconBetResultWarning = Color(0xFFFF8345);
  static const Color strokeInfo = Color(0xFFC8C8C8);
  static const Color strokeError = Color(0xFFFFBEBE);
  static const Color strokeBetResultError = Color(0xFFFF7471);
  static const Color strokeSuccess = Color(0xFF8DF1BB);
  static const Color alertStrokeWarning = Color(0xFFFFD9AD);
  static const Color strokeBetResultWarning = Color(0xFFFFEDE3);
  static const Color iconSuccess = Color(0xFF48B078);
  static const Color iconInfo = Color(0xFF7C7C7C);
  static const Color iconError = Color(0xFFE53535);

  ///status
  static const Color statusSuccess = Color(0xFF64C993);
  static const Color statusRejected = Color(0xFFFF6666);
  static const Color statusWaiting = Color(0xFFFFB545);

  ///profile gradients
  static const Color profileLightClub = Color(0xFF00C55A);
  static const Color profileDarkClub = Color(0xFF007A38);
  static const Color profileLightSilver = Color(0xFF7F8A8D);
  static const Color profileDarkSilver = Color(0xFF595E60);
  static const Color profileLightGold = Color(0xFFE4BA67);
  static const Color profileDarkGold = Color(0xFF98732A);
  static const Color profileLightPlatinum = Color(0xFF688088);
  static const Color profileDarkPlatinum = Color(0xFF353F43);
}
