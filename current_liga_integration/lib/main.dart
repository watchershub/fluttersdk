import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:current_liga_integration/event_detail/chat/domain/bloc/chat_cubit.dart';
import 'package:current_liga_integration/event_detail/chat/utils/keyboard_size_provider.dart';
import 'package:current_liga_integration/event_detail/costil_change_notifire.dart';

import 'event_detail/event_detail_bloc.dart';
import 'event_detail/event_detail_content.dart';
import 'event_detail/event_detail_view.dart';
import 'event_detail/event_details_tabs.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: ChangeNotifierProvider<TabsStreamCostil>.value(
        value: TabsStreamCostil(),
        child: ChangeNotifierProvider<TabChatStreamCostil>.value(
          value: TabChatStreamCostil(),
          child: KeyboardSizeProvider(
            child: Provider<OldEventDetailsBloc>(
              create: (_) => OldEventDetailsBloc(),
              dispose: (_, OldEventDetailsBloc bloc) => bloc.dispose(),
              child: BlocProvider<ChatCubit>(
                create: (_) => ChatCubit(),
                child: const EventDetailsView(),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

