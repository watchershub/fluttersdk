import 'package:flutter/cupertino.dart';
import 'package:current_liga_integration/palette.dart';

/// Regular 10 13
class Main10 extends TextStyle {
  const Main10({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 10,
    height: 13 / 10,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

/// Regular 11 13
class Main11 extends TextStyle {
  const Main11({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 11,
    height: 13 / 11,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

/// Regular 12 16
class Main12 extends TextStyle {
  const Main12({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    height: 16 / 12,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

/// Regular 13 17
class Main13 extends TextStyle {
  const Main13({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 13,
    height: 17 / 13,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

/// Regular 15 20
class Main15 extends TextStyle {
  const Main15({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 15,
    height: 20 / 15,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

/// Regular 17 22
class Main17 extends TextStyle {
  const Main17({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 17,
    height: 22 / 17,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

/// Medium 10 13
class FullscreenVideo10 extends TextStyle {
  const FullscreenVideo10({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w500,
    fontSize: 10,
    height: 13 / 10,
    // fontFamily: Fonts.medium,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Semibold 10 13
class MainSemibold10 extends TextStyle {
  const MainSemibold10({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w600,
    fontSize: 10,
    height: 13 / 10,
    // fontFamily: Fonts.semibold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Bold 10 13 - UPPERCASE!
class EventStatusTextStyle extends TextStyle {
  const EventStatusTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 10,
    height: 13 / 10,
    // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Regular 12 14
class CounterTextStyle extends TextStyle {
  const CounterTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 12,
    height: 14 / 12,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Medium 12 14
class FullscreenVideo12 extends TextStyle {
  const FullscreenVideo12({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w500,
    fontSize: 12,
    height: 14 / 12,
    // fontFamily: Fonts.medium,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Regular 13 20
class TeamNames extends TextStyle {
  const TeamNames({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 13,
    height: 20 / 13,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Semibold 12 16
class SemiBold12 extends TextStyle {
  const SemiBold12({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w600,
    fontSize: 12,
    height: 16 / 12,
    // fontFamily: Fonts.semibold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Semibold 13 17
class MarketFilterTextStyle extends TextStyle {
  const MarketFilterTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w600,
    fontSize: 13,
    height: 17 / 13,
    // fontFamily: Fonts.semibold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Bold 13 17
class MainBold13 extends TextStyle {
  const MainBold13({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 13,
    height: 17 / 13,
    // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Semibold 15 18
class MainSemibold15 extends TextStyle {
  const MainSemibold15({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w600,
    fontSize: 15,
    height: 18 / 15,
    // fontFamily: Fonts.semibold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Semibold 16 22
class Coefficient extends TextStyle {
  const Coefficient({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w600,
    fontSize: 16,
    height: 22 / 16,
    // fontFamily: Fonts.semibold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Semibold 17 22 - replacing and removing of Operation status
class MainSemibold17 extends TextStyle {
  const MainSemibold17({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w600,
    fontSize: 17,
    height: 22 / 17,
    // fontFamily: Fonts.semibold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Bold 17 22 - Promocode Value
class FreebetValue extends TextStyle {
  const FreebetValue({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 17,
    height: 22 / 17,
    // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Bold 16 19
class AlertFigmaHeaderTextStyle extends TextStyle {
  const AlertFigmaHeaderTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 16,
    height: 19 / 16,
    // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Invite styles

class Main16 extends TextStyle {
  const Main16({
    this.color = Palette.code,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 16,
    // // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

class MainBold14 extends TextStyle {
  const MainBold14({
    this.color = Palette.copyCode,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 14,
    // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

class MainBold18 extends TextStyle {
  const MainBold18({
    this.color = Palette.copyCode,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 18,
    // // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Bold 14
class Main14 extends TextStyle {
  const Main14({
    this.color = Palette.copyCode,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 14,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Bold 19 23
class AlertHeaderTextStyle extends TextStyle {
  const AlertHeaderTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 19,
    height: 23 / 19,
    // // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Regular 20 24
class ActionSheetButtonsTextStyle extends TextStyle {
  const ActionSheetButtonsTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 20,
    height: 24 / 20,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Semibold 20 24
class ActionSheetMainButtonTextStyle extends TextStyle {
  const ActionSheetMainButtonTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w600,
    fontSize: 20,
    height: 24 / 20,
    // // fontFamily: Fonts.semibold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Medium 21 25
class RegistrationHeaderTextStyle extends TextStyle {
  const RegistrationHeaderTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w500,
    fontSize: 21,
    height: 25 / 21,
    // fontFamily: Fonts.medium,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Heavy 22 26
class MenuHeaderTextStyle extends TextStyle {
  const MenuHeaderTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w900,
    fontSize: 22,
    height: 26 / 22,
    // fontFamily: Fonts.heavy,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Regular 24 30 - Numeric Code from SMS
class CodeTextStyle extends TextStyle {
  const CodeTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 24,
    height: 30 / 24,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Regular 24 30 - Bold header
class HeaderTextStyle extends TextStyle {
  const HeaderTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 24,
    height: 30 / 24,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

///  Bold 30 40 Only at Fast Bet and Success Bet Cards
class BigOutcomeTextStyle extends TextStyle {
  const BigOutcomeTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 30,
    height: 40 / 30,
    // // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
}

class PinTextStyle extends TextStyle {
  const PinTextStyle({
    this.color = Palette.white,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 36,
    height: 44 / 36,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
}

class FooTextStyle extends TextStyle {
  const FooTextStyle({
    this.color = Palette.transparent,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 0,
    height: 0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
}

class CustomBoldStyle extends TextStyle {
  const CustomBoldStyle({
    this.color = Palette.black90,
    this.fontSize = 38,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: fontSize,
    height: 1,
    // // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
  @override
  final double fontSize;
}

class CustomStyle extends TextStyle {
  const CustomStyle({
    this.color = Palette.black90,
    this.fontSize = 38,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: fontSize,
    height: 1,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
  @override
  final double fontSize;
}

/// NOT STANDART FONTS BELOW
class CupertinoIconsStyle extends TextStyle {
  const CupertinoIconsStyle({
    this.color = Palette.transparent,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 34,
    fontFamily: CupertinoIcons.iconFont,
    package: CupertinoIcons.iconFontPackage,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
}

class SpinTextStyle extends TextStyle {
  const SpinTextStyle({
    this.color = Palette.primaryDark,
  }) : super(
    color: color,
    fontWeight: FontWeight.w900,
    fontSize: 14,
    height: 21 / 14,
    // fontFamily: Fonts.wheel,
    letterSpacing: 0.05,
    fontStyle: FontStyle.italic,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
}

class SpinBonusTextStyle extends TextStyle {
  const SpinBonusTextStyle({
    this.color = Palette.primaryDark,
  }) : super(
    color: color,
    fontWeight: FontWeight.w900,
    fontSize: 22,
    height: 28 / 22,
    // fontFamily: Fonts.wheel,
    letterSpacing: 0.05,
    fontStyle: FontStyle.italic,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
}

class WheelTextStyle extends TextStyle {
  const WheelTextStyle({
    this.color = Palette.black90,
  }) : super(
    color: color,
    fontWeight: FontWeight.w800,
    fontSize: 28,
    height: 1.2,
    // fontFamily: Fonts.wheel,
    letterSpacing: 0.05,
    fontStyle: FontStyle.italic,
    decoration: TextDecoration.none,
  );
  @override
  final Color color;
}

/// Update screen styles
class UpdateScreenStyle28 extends TextStyle {
  const UpdateScreenStyle28({
    this.color = Palette.white,
  }) : super(
    color: color,
    fontWeight: FontWeight.w700,
    fontSize: 28.0,
    height: 23.0 / 19.0,
    // // fontFamily: Fonts.bold,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

class UpdateScreenStyle15 extends TextStyle {
  const UpdateScreenStyle15({
    this.color = Palette.white,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 15.0,
    height: 20.0 / 15.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

class UpdateScreenStyle17 extends TextStyle {
  const UpdateScreenStyle17({
    this.color = Palette.white,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: 17.0,
    height: 22.0 / 17.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;
}

class EventMainText10 extends TextStyle {
  const EventMainText10({
    this.color = Palette.black,
    this.fontWeight = FontWeight.w400,
  }) : super(
    color: color,
    fontWeight: fontWeight,
    fontSize: 10.0,
    height: 16.0 / 10.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final FontWeight fontWeight;
}

class EventMainText12 extends TextStyle {
  const EventMainText12({
    this.color = Palette.black,
    this.fontWeight = FontWeight.w400,
  }) : super(
    color: color,
    fontWeight: fontWeight,
    fontSize: 12.0,
    height: 16.0 / 12.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final FontWeight fontWeight;
}

class EventMainText13 extends TextStyle {
  const EventMainText13({
    this.color = Palette.black,
    this.fontWeight = FontWeight.w400,
  }) : super(
    color: color,
    fontWeight: fontWeight,
    fontSize: 13.0,
    height: 16.0 / 13.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final FontWeight fontWeight;
}

class EventMainText14 extends TextStyle {
  const EventMainText14({
    this.color = Palette.black,
    this.fontWeight = FontWeight.w400,
  }) : super(
    color: color,
    fontWeight: fontWeight,
    fontSize: 14.0,
    height: 24.0 / 14.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final FontWeight fontWeight;
}

class EventMainText16 extends TextStyle {
  const EventMainText16({
    this.color = Palette.black,
    this.fontWeight = FontWeight.w400,
  }) : super(
    color: color,
    fontWeight: fontWeight,
    fontSize: 16.0,
    height: 24.0 / 16.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final FontWeight fontWeight;
}

class EventMainText18 extends TextStyle {
  const EventMainText18({
    this.color = Palette.black,
    this.fontWeight = FontWeight.w400,
  }) : super(
    color: color,
    fontWeight: fontWeight,
    fontSize: 18.0,
    height: 24.0 / 18.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final FontWeight fontWeight;
}

class EventSecondaryText13 extends TextStyle {
  const EventSecondaryText13({
    this.color = Palette.black,
    this.fontWeight = FontWeight.w400,
  }) : super(
    color: color,
    fontWeight: fontWeight,
    fontSize: 13.0,
    height: 16.0 / 13.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final FontWeight fontWeight;
}

class LastMessageTime extends TextStyle {
  const LastMessageTime({
    this.color = Palette.lightTextTertiary,
    this.fontWeight = FontWeight.w400,
  }) : super(
    color: color,
    fontWeight: fontWeight,
    fontSize: 8.0,
    height: 2.0,
    // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final FontWeight fontWeight;
}

class ForecastFontStyle extends TextStyle {
  const ForecastFontStyle({
    this.color = Palette.black90,
    this.fontSize = 13.0,
  }) : super(
    color: color,
    fontWeight: FontWeight.w400,
    fontSize: fontSize,
    height: 17 / fontSize,
    // // fontFamily: Fonts.regular,
    decoration: TextDecoration.none,
  );

  @override
  final Color color;

  @override
  final double fontSize;
}

class KeyAdditionalTitleStyle extends TextStyle {
  const KeyAdditionalTitleStyle() : super(fontSize: 10, fontWeight: FontWeight.w700);
}
