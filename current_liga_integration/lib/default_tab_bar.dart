import 'package:flutter/cupertino.dart' show ObstructingPreferredSizeWidget;
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:current_liga_integration/palette.dart';

import 'liga_cupertino_app_bar.dart';

class LigaAppBarParams {
  LigaAppBarParams._();
  
  static const double kNavBarPersistentHeight = 44.0;
  static const double kMinExtend = 0;
  static const double kAppBarIconSize = 22.0;
  static const double kLeftNavBarPadding = 12;
  static const double kRightNavBarPadding = 12;
  static const double kTapAreaPadding = 0;
  static const double kMinButtonWidth = 48;

  static double get totalPadding => kLeftNavBarPadding + kRightNavBarPadding;
  static double get totalWidth => kNavBarPersistentHeight + kLeftNavBarPadding + kRightNavBarPadding;
  static EdgeInsets get padding => const EdgeInsets.only(
    left: LigaAppBarParams.kLeftNavBarPadding,
    right: LigaAppBarParams.kRightNavBarPadding,
  );
}

void _defaultLeadingTap(BuildContext context) {
  Navigator.of(context).pop();
}

class DefaultTabBar extends StatelessWidget implements ObstructingPreferredSizeWidget {
  const DefaultTabBar({
    super.key,
    this.middle,
    this.onLeadingTap,
    this.useDefaultTrailing = true,
    this.useDefaultLeading = true,
    this.backgroundColor,
    this.trailing,
    this.leading,
    this.height = LigaAppBarParams.kNavBarPersistentHeight,
  });

  final Widget? middle;
  final VoidCallback? onLeadingTap;
  final bool useDefaultTrailing;
  final bool useDefaultLeading;
  final Color? backgroundColor;
  final Widget? trailing;
  final Widget? leading;
  final double height;

  bool get _fullObstruction => true;

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  Widget build(BuildContext context) {
    return LigaCupertinoNavigationBar(
      transitionBetweenRoutes: false,
      border: null,
      backgroundColor: backgroundColor ?? Palette.primary,
      leading: useDefaultLeading ? _defaultLeading(context, leading) : (leading ?? const SizedBox.shrink()),
      middle: middle,
      trailing: useDefaultTrailing
          ? Padding(
        key: const Key('CommonScreenKeys.closeButtonKey'),
        padding: const EdgeInsets.only(
          left: LigaAppBarParams.kLeftNavBarPadding,
          right: LigaAppBarParams.kRightNavBarPadding,
        ),
        child: trailing,
      )
          : trailing,
      padding: EdgeInsetsDirectional.zero,
    );
  }

  @override
  bool shouldFullyObstruct(BuildContext context) {
    return _fullObstruction;
  }

  Widget _defaultLeading(BuildContext context, Widget? leading) {
    return (leading != null)
        ? Padding(
      padding: const EdgeInsets.only(
        left: LigaAppBarParams.kLeftNavBarPadding,
        right: LigaAppBarParams.kRightNavBarPadding,
      ),
      child: leading,
    )
        : GestureDetector(
      onTap: onLeadingTap ?? () => _defaultLeadingTap(context),
      child: Container(
        constraints: const BoxConstraints(minWidth: LigaAppBarParams.kMinButtonWidth),
        color: Palette.transparent,
        padding: const EdgeInsets.only(
          left: LigaAppBarParams.kLeftNavBarPadding,
        ),
        child: const Icon(
          Icons.arrow_back,
          color: Palette.white,
          size: LigaAppBarParams.kAppBarIconSize,
          key: Key('CommonScreenKeys.backIconKey'),
        ),
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(ObjectFlagProperty<VoidCallback?>.has('onLeadingTap', onLeadingTap));
    properties.add(DiagnosticsProperty<bool>('useDefaultTrailing', useDefaultTrailing));
    properties.add(DiagnosticsProperty<bool>('useDefaultLeading', useDefaultLeading));
    properties.add(ColorProperty('backgroundColor', backgroundColor));
    properties.add(DoubleProperty('height', height));
  }
}

class SliverDefaultTabBar extends DefaultTabBar {
  const SliverDefaultTabBar({
    super.middle,
    super.onLeadingTap,
    super.backgroundColor,
    super.trailing,
    super.leading,
    this.useZeroMinExtend = true,
    this.pinned = false,
    this.tag,
    this.useTopSafeArea = true,
    this.rectTween,
    super.key,
  });

  final bool useZeroMinExtend;
  final bool pinned;
  final Object? tag;
  final bool useTopSafeArea;
  final CreateRectTween? rectTween;

  @override
  Widget build(BuildContext context) {
    return SliverPersistentHeader(
      pinned: pinned,
      delegate: AppBarSliverDelegate(
        middle: middle,
        onLeadingTap: onLeadingTap,
        backgroundColor: backgroundColor,
        trailing: trailing,
        expandedHeight: LigaAppBarParams.kNavBarPersistentHeight,
        leading: leading,
        topPadding: MediaQuery.of(context).padding.top,
        useZeroMinExtend: useZeroMinExtend,
        tag: tag,
        useTopSafeArea: useTopSafeArea,
        rectTween: rectTween,
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(DiagnosticsProperty<bool>('useZeroMinExtend', useZeroMinExtend));
    properties.add(DiagnosticsProperty<bool>('pinned', pinned));
    properties.add(DiagnosticsProperty<Object?>('tag', tag));
    properties.add(DiagnosticsProperty<bool>('useTopSafeArea', useTopSafeArea));
    properties.add(ObjectFlagProperty<CreateRectTween?>.has('rectTween', rectTween));
  }
}

class AppBarSliverDelegate extends SliverPersistentHeaderDelegate {
  AppBarSliverDelegate({
    required this.expandedHeight,
    this.useZeroMinExtend = true,
    this.useTopSafeArea = true,
    this.leading,
    this.topPadding,
    this.middle,
    this.onLeadingTap,
    this.backgroundColor,
    this.trailing,
    this.tag,
    this.rectTween,
  });

  final double expandedHeight;
  final bool useZeroMinExtend;
  final bool useTopSafeArea;
  final Widget? middle;
  final VoidCallback? onLeadingTap;
  final Color? backgroundColor;
  final Widget? trailing;
  final Widget? leading;
  final double? topPadding;
  final Object? tag;
  final CreateRectTween? rectTween;

  @override
  double get maxExtent => LigaAppBarParams.kNavBarPersistentHeight + topPadding!;

  @override
  double get minExtent =>
      useZeroMinExtend ? LigaAppBarParams.kMinExtend : LigaAppBarParams.kNavBarPersistentHeight + topPadding!;

  @override
  Widget build(BuildContext context, double shrinkOffset, bool overlapsContent) {
    if (tag == null) {
      return _content(context, shrinkOffset, overlapsContent);
    }

    return Hero(
      tag: tag!,
      createRectTween: rectTween,
      child: _content(context, shrinkOffset, overlapsContent),
    );
  }

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }

  Widget _content(BuildContext context, double shrinkOffset, bool _) {
    double opacityPercent = 1.0;

    if (useZeroMinExtend) {
      final double appBarSize = expandedHeight - shrinkOffset;
      final double proportion = 2 - (expandedHeight / appBarSize);
      opacityPercent = proportion < 0 || proportion > 1 ? 0.0 : proportion;
    }

    return LigaCupertinoNavigationBar(
      useTopSafeArea: useTopSafeArea,
      heroTag: tag!,
      transitionBetweenRoutes: false,
      border: null,
      backgroundColor: backgroundColor ?? Palette.primary,
      leading: Opacity(
        opacity: opacityPercent,
        child: (leading != null)
            ? leading
            : GestureDetector(
          onTap: onLeadingTap ?? () => _defaultLeadingTap(context),
          child: Container(
            constraints: const BoxConstraints(minWidth: LigaAppBarParams.kMinButtonWidth),
            color: Palette.transparent,
            padding: const EdgeInsets.only(
              left: LigaAppBarParams.kLeftNavBarPadding,
            ),
            child: const Icon(
              Icons.arrow_back,
              color: Palette.white,
              size: LigaAppBarParams.kAppBarIconSize,
            ),
          ),
        ),
      ),
      middle: Opacity(
        opacity: opacityPercent,
        child: middle,
      ),
      trailing: Opacity(
        opacity: opacityPercent,
        child: trailing,
      ),
      padding: EdgeInsetsDirectional.zero,
    );
  }
}
