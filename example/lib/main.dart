import 'package:flutter/material.dart';
import 'package:watchers_widget/watchers_widget.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Watchers Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.dark(),
      home: SelectEnvScreen(),
    );
  }
}

enum StatusNameValue { none, club, silver, gold, platinum }

extension StatusNameValueX on StatusNameValue {
  StatusName? get statusName {
    switch (this) {
      case StatusNameValue.none:
        return null;
      case StatusNameValue.club:
        return StatusName.club;
      case StatusNameValue.silver:
        return StatusName.silver;
      case StatusNameValue.gold:
        return StatusName.gold;
      case StatusNameValue.platinum:
        return StatusName.platinum;
    }
  }
}

const List<StatusNameValue> statuses = StatusNameValue.values;

// ignore: must_be_immutable
class SelectEnvScreen extends StatelessWidget {
  SelectEnvScreen();

  final userIdController = TextEditingController();
  final roomIdController = TextEditingController();
  StatusNameValue statusNameValue = StatusNameValue.none;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title: const WatchersChatTitle(),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextField(
                    controller: userIdController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'UserId...',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: TextField(
                    controller: roomIdController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'RoomId...',
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: DropdownButtonWidget(
                    statusName: statusNameValue,
                    onUpdate: (value) {
                      statusNameValue = value;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
                  child: ElevatedButton(
                    child: const Text('Gooo'),
                    onPressed: () {
                      Navigator.of(context).push(HomePage.route(
                        userIdController.text,
                        roomIdController.text,
                        statusNameValue.statusName,
                      ));
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DropdownButtonWidget extends StatefulWidget {
  final StatusNameValue statusName;
  final void Function(StatusNameValue statusName) onUpdate;

  const DropdownButtonWidget({
    required this.onUpdate,
    required this.statusName,
  });

  @override
  State<DropdownButtonWidget> createState() => _DropdownButtonWidgetState();
}

class _DropdownButtonWidgetState extends State<DropdownButtonWidget> {
  late StatusNameValue dropdownValue;

  @override
  void initState() {
    dropdownValue = widget.statusName;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButton<StatusNameValue>(
      value: dropdownValue,
      icon: const Icon(Icons.arrow_downward),
      elevation: 16,
      onChanged: (StatusNameValue? value) {
        // This is called when the user selects an item.
        setState(() {
          dropdownValue = value!;
          widget.onUpdate(dropdownValue);
        });
      },
      items: statuses.map<DropdownMenuItem<StatusNameValue>>((StatusNameValue value) {
        return DropdownMenuItem<StatusNameValue>(
          value: value,
          child: Text(value.name),
        );
      }).toList(),
    );
  }
}

class HomePage extends StatefulWidget {
  final String userId;
  final String roomId;
  final StatusName? statusName;

  const HomePage({
    required this.roomId,
    required this.userId,
    required this.statusName,
  });

  static Route route(
    String userId,
    String roomId,
    StatusName? statusName,
  ) =>
      MaterialPageRoute(
        builder: (context) => HomePage(
          roomId: roomId,
          userId: userId,
          statusName: statusName,
        ),
      );

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const WatchersChatTitle(),
      ),
      body: SafeArea(
        child: NotificationListener<UserScrollNotification>(
          // It is preferable to put the chat in a scrollable component
          child: CustomScrollView(
            physics: const NeverScrollableScrollPhysics(),
            slivers: <Widget>[
              SliverToBoxAdapter(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.2,
                  // -
                  //  ((MediaQuery.of(context).viewInsets.bottom > 50) ? 50 : 0),
                  width: MediaQuery.of(context).size.width,
                  color: Colors.grey,
                  child: const Center(child: Text('Information')),
                ),
              ),
              SliverFillRemaining(
                child: WatchersWidget(
                  controller: WatchersWidgetController(
                    params: WatchersParams(
                      userId: widget.userId,
                      roomId: widget.roomId,
                      statusName: widget.statusName,
                      enableDev: true,
                      // title: "Англия - Иран",
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class WatchersChatTitle extends StatelessWidget {
  const WatchersChatTitle();

  @override
  Widget build(BuildContext context) {
    return const Text('Chat 0.0.20');
  }
}

class NoGlowScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
