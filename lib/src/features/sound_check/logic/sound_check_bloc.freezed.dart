// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'sound_check_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SoundCheckEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() showRateDialog,
    required TResult Function(int soundCheckRating) showIssueDialog,
    required TResult Function(List<String> issue) showAskCheckSpeed,
    required TResult Function() showSpeedChecking,
    required TResult Function(String speed) showConclusionDialog,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ShowRateDialog value) showRateDialog,
    required TResult Function(_ShowIssueDialog value) showIssueDialog,
    required TResult Function(_ShowAskCheckSpeed value) showAskCheckSpeed,
    required TResult Function(_ShowSpeedChecking value) showSpeedChecking,
    required TResult Function(_ShowConclusionDialog value) showConclusionDialog,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SoundCheckEventCopyWith<$Res> {
  factory $SoundCheckEventCopyWith(
          SoundCheckEvent value, $Res Function(SoundCheckEvent) then) =
      _$SoundCheckEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$SoundCheckEventCopyWithImpl<$Res>
    implements $SoundCheckEventCopyWith<$Res> {
  _$SoundCheckEventCopyWithImpl(this._value, this._then);

  final SoundCheckEvent _value;
  // ignore: unused_field
  final $Res Function(SoundCheckEvent) _then;
}

/// @nodoc
abstract class _$$_InitCopyWith<$Res> {
  factory _$$_InitCopyWith(_$_Init value, $Res Function(_$_Init) then) =
      __$$_InitCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitCopyWithImpl<$Res> extends _$SoundCheckEventCopyWithImpl<$Res>
    implements _$$_InitCopyWith<$Res> {
  __$$_InitCopyWithImpl(_$_Init _value, $Res Function(_$_Init) _then)
      : super(_value, (v) => _then(v as _$_Init));

  @override
  _$_Init get _value => super._value as _$_Init;
}

/// @nodoc

class _$_Init implements _Init {
  const _$_Init();

  @override
  String toString() {
    return 'SoundCheckEvent.init()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Init);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() showRateDialog,
    required TResult Function(int soundCheckRating) showIssueDialog,
    required TResult Function(List<String> issue) showAskCheckSpeed,
    required TResult Function() showSpeedChecking,
    required TResult Function(String speed) showConclusionDialog,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ShowRateDialog value) showRateDialog,
    required TResult Function(_ShowIssueDialog value) showIssueDialog,
    required TResult Function(_ShowAskCheckSpeed value) showAskCheckSpeed,
    required TResult Function(_ShowSpeedChecking value) showSpeedChecking,
    required TResult Function(_ShowConclusionDialog value) showConclusionDialog,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements SoundCheckEvent {
  const factory _Init() = _$_Init;
}

/// @nodoc
abstract class _$$_ShowRateDialogCopyWith<$Res> {
  factory _$$_ShowRateDialogCopyWith(
          _$_ShowRateDialog value, $Res Function(_$_ShowRateDialog) then) =
      __$$_ShowRateDialogCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ShowRateDialogCopyWithImpl<$Res>
    extends _$SoundCheckEventCopyWithImpl<$Res>
    implements _$$_ShowRateDialogCopyWith<$Res> {
  __$$_ShowRateDialogCopyWithImpl(
      _$_ShowRateDialog _value, $Res Function(_$_ShowRateDialog) _then)
      : super(_value, (v) => _then(v as _$_ShowRateDialog));

  @override
  _$_ShowRateDialog get _value => super._value as _$_ShowRateDialog;
}

/// @nodoc

class _$_ShowRateDialog implements _ShowRateDialog {
  const _$_ShowRateDialog();

  @override
  String toString() {
    return 'SoundCheckEvent.showRateDialog()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ShowRateDialog);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() showRateDialog,
    required TResult Function(int soundCheckRating) showIssueDialog,
    required TResult Function(List<String> issue) showAskCheckSpeed,
    required TResult Function() showSpeedChecking,
    required TResult Function(String speed) showConclusionDialog,
  }) {
    return showRateDialog();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
  }) {
    return showRateDialog?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showRateDialog != null) {
      return showRateDialog();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ShowRateDialog value) showRateDialog,
    required TResult Function(_ShowIssueDialog value) showIssueDialog,
    required TResult Function(_ShowAskCheckSpeed value) showAskCheckSpeed,
    required TResult Function(_ShowSpeedChecking value) showSpeedChecking,
    required TResult Function(_ShowConclusionDialog value) showConclusionDialog,
  }) {
    return showRateDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
  }) {
    return showRateDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showRateDialog != null) {
      return showRateDialog(this);
    }
    return orElse();
  }
}

abstract class _ShowRateDialog implements SoundCheckEvent {
  const factory _ShowRateDialog() = _$_ShowRateDialog;
}

/// @nodoc
abstract class _$$_ShowIssueDialogCopyWith<$Res> {
  factory _$$_ShowIssueDialogCopyWith(
          _$_ShowIssueDialog value, $Res Function(_$_ShowIssueDialog) then) =
      __$$_ShowIssueDialogCopyWithImpl<$Res>;
  $Res call({int soundCheckRating});
}

/// @nodoc
class __$$_ShowIssueDialogCopyWithImpl<$Res>
    extends _$SoundCheckEventCopyWithImpl<$Res>
    implements _$$_ShowIssueDialogCopyWith<$Res> {
  __$$_ShowIssueDialogCopyWithImpl(
      _$_ShowIssueDialog _value, $Res Function(_$_ShowIssueDialog) _then)
      : super(_value, (v) => _then(v as _$_ShowIssueDialog));

  @override
  _$_ShowIssueDialog get _value => super._value as _$_ShowIssueDialog;

  @override
  $Res call({
    Object? soundCheckRating = freezed,
  }) {
    return _then(_$_ShowIssueDialog(
      soundCheckRating: soundCheckRating == freezed
          ? _value.soundCheckRating
          : soundCheckRating // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_ShowIssueDialog implements _ShowIssueDialog {
  const _$_ShowIssueDialog({required this.soundCheckRating});

  @override
  final int soundCheckRating;

  @override
  String toString() {
    return 'SoundCheckEvent.showIssueDialog(soundCheckRating: $soundCheckRating)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ShowIssueDialog &&
            const DeepCollectionEquality()
                .equals(other.soundCheckRating, soundCheckRating));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(soundCheckRating));

  @JsonKey(ignore: true)
  @override
  _$$_ShowIssueDialogCopyWith<_$_ShowIssueDialog> get copyWith =>
      __$$_ShowIssueDialogCopyWithImpl<_$_ShowIssueDialog>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() showRateDialog,
    required TResult Function(int soundCheckRating) showIssueDialog,
    required TResult Function(List<String> issue) showAskCheckSpeed,
    required TResult Function() showSpeedChecking,
    required TResult Function(String speed) showConclusionDialog,
  }) {
    return showIssueDialog(soundCheckRating);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
  }) {
    return showIssueDialog?.call(soundCheckRating);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showIssueDialog != null) {
      return showIssueDialog(soundCheckRating);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ShowRateDialog value) showRateDialog,
    required TResult Function(_ShowIssueDialog value) showIssueDialog,
    required TResult Function(_ShowAskCheckSpeed value) showAskCheckSpeed,
    required TResult Function(_ShowSpeedChecking value) showSpeedChecking,
    required TResult Function(_ShowConclusionDialog value) showConclusionDialog,
  }) {
    return showIssueDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
  }) {
    return showIssueDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showIssueDialog != null) {
      return showIssueDialog(this);
    }
    return orElse();
  }
}

abstract class _ShowIssueDialog implements SoundCheckEvent {
  const factory _ShowIssueDialog({required final int soundCheckRating}) =
      _$_ShowIssueDialog;

  int get soundCheckRating;
  @JsonKey(ignore: true)
  _$$_ShowIssueDialogCopyWith<_$_ShowIssueDialog> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ShowAskCheckSpeedCopyWith<$Res> {
  factory _$$_ShowAskCheckSpeedCopyWith(_$_ShowAskCheckSpeed value,
          $Res Function(_$_ShowAskCheckSpeed) then) =
      __$$_ShowAskCheckSpeedCopyWithImpl<$Res>;
  $Res call({List<String> issue});
}

/// @nodoc
class __$$_ShowAskCheckSpeedCopyWithImpl<$Res>
    extends _$SoundCheckEventCopyWithImpl<$Res>
    implements _$$_ShowAskCheckSpeedCopyWith<$Res> {
  __$$_ShowAskCheckSpeedCopyWithImpl(
      _$_ShowAskCheckSpeed _value, $Res Function(_$_ShowAskCheckSpeed) _then)
      : super(_value, (v) => _then(v as _$_ShowAskCheckSpeed));

  @override
  _$_ShowAskCheckSpeed get _value => super._value as _$_ShowAskCheckSpeed;

  @override
  $Res call({
    Object? issue = freezed,
  }) {
    return _then(_$_ShowAskCheckSpeed(
      issue: issue == freezed
          ? _value._issue
          : issue // ignore: cast_nullable_to_non_nullable
              as List<String>,
    ));
  }
}

/// @nodoc

class _$_ShowAskCheckSpeed implements _ShowAskCheckSpeed {
  const _$_ShowAskCheckSpeed({required final List<String> issue})
      : _issue = issue;

  final List<String> _issue;
  @override
  List<String> get issue {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_issue);
  }

  @override
  String toString() {
    return 'SoundCheckEvent.showAskCheckSpeed(issue: $issue)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ShowAskCheckSpeed &&
            const DeepCollectionEquality().equals(other._issue, _issue));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_issue));

  @JsonKey(ignore: true)
  @override
  _$$_ShowAskCheckSpeedCopyWith<_$_ShowAskCheckSpeed> get copyWith =>
      __$$_ShowAskCheckSpeedCopyWithImpl<_$_ShowAskCheckSpeed>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() showRateDialog,
    required TResult Function(int soundCheckRating) showIssueDialog,
    required TResult Function(List<String> issue) showAskCheckSpeed,
    required TResult Function() showSpeedChecking,
    required TResult Function(String speed) showConclusionDialog,
  }) {
    return showAskCheckSpeed(issue);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
  }) {
    return showAskCheckSpeed?.call(issue);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showAskCheckSpeed != null) {
      return showAskCheckSpeed(issue);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ShowRateDialog value) showRateDialog,
    required TResult Function(_ShowIssueDialog value) showIssueDialog,
    required TResult Function(_ShowAskCheckSpeed value) showAskCheckSpeed,
    required TResult Function(_ShowSpeedChecking value) showSpeedChecking,
    required TResult Function(_ShowConclusionDialog value) showConclusionDialog,
  }) {
    return showAskCheckSpeed(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
  }) {
    return showAskCheckSpeed?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showAskCheckSpeed != null) {
      return showAskCheckSpeed(this);
    }
    return orElse();
  }
}

abstract class _ShowAskCheckSpeed implements SoundCheckEvent {
  const factory _ShowAskCheckSpeed({required final List<String> issue}) =
      _$_ShowAskCheckSpeed;

  List<String> get issue;
  @JsonKey(ignore: true)
  _$$_ShowAskCheckSpeedCopyWith<_$_ShowAskCheckSpeed> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ShowSpeedCheckingCopyWith<$Res> {
  factory _$$_ShowSpeedCheckingCopyWith(_$_ShowSpeedChecking value,
          $Res Function(_$_ShowSpeedChecking) then) =
      __$$_ShowSpeedCheckingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ShowSpeedCheckingCopyWithImpl<$Res>
    extends _$SoundCheckEventCopyWithImpl<$Res>
    implements _$$_ShowSpeedCheckingCopyWith<$Res> {
  __$$_ShowSpeedCheckingCopyWithImpl(
      _$_ShowSpeedChecking _value, $Res Function(_$_ShowSpeedChecking) _then)
      : super(_value, (v) => _then(v as _$_ShowSpeedChecking));

  @override
  _$_ShowSpeedChecking get _value => super._value as _$_ShowSpeedChecking;
}

/// @nodoc

class _$_ShowSpeedChecking implements _ShowSpeedChecking {
  const _$_ShowSpeedChecking();

  @override
  String toString() {
    return 'SoundCheckEvent.showSpeedChecking()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ShowSpeedChecking);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() showRateDialog,
    required TResult Function(int soundCheckRating) showIssueDialog,
    required TResult Function(List<String> issue) showAskCheckSpeed,
    required TResult Function() showSpeedChecking,
    required TResult Function(String speed) showConclusionDialog,
  }) {
    return showSpeedChecking();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
  }) {
    return showSpeedChecking?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showSpeedChecking != null) {
      return showSpeedChecking();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ShowRateDialog value) showRateDialog,
    required TResult Function(_ShowIssueDialog value) showIssueDialog,
    required TResult Function(_ShowAskCheckSpeed value) showAskCheckSpeed,
    required TResult Function(_ShowSpeedChecking value) showSpeedChecking,
    required TResult Function(_ShowConclusionDialog value) showConclusionDialog,
  }) {
    return showSpeedChecking(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
  }) {
    return showSpeedChecking?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showSpeedChecking != null) {
      return showSpeedChecking(this);
    }
    return orElse();
  }
}

abstract class _ShowSpeedChecking implements SoundCheckEvent {
  const factory _ShowSpeedChecking() = _$_ShowSpeedChecking;
}

/// @nodoc
abstract class _$$_ShowConclusionDialogCopyWith<$Res> {
  factory _$$_ShowConclusionDialogCopyWith(_$_ShowConclusionDialog value,
          $Res Function(_$_ShowConclusionDialog) then) =
      __$$_ShowConclusionDialogCopyWithImpl<$Res>;
  $Res call({String speed});
}

/// @nodoc
class __$$_ShowConclusionDialogCopyWithImpl<$Res>
    extends _$SoundCheckEventCopyWithImpl<$Res>
    implements _$$_ShowConclusionDialogCopyWith<$Res> {
  __$$_ShowConclusionDialogCopyWithImpl(_$_ShowConclusionDialog _value,
      $Res Function(_$_ShowConclusionDialog) _then)
      : super(_value, (v) => _then(v as _$_ShowConclusionDialog));

  @override
  _$_ShowConclusionDialog get _value => super._value as _$_ShowConclusionDialog;

  @override
  $Res call({
    Object? speed = freezed,
  }) {
    return _then(_$_ShowConclusionDialog(
      speed: speed == freezed
          ? _value.speed
          : speed // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_ShowConclusionDialog implements _ShowConclusionDialog {
  const _$_ShowConclusionDialog({required this.speed});

  @override
  final String speed;

  @override
  String toString() {
    return 'SoundCheckEvent.showConclusionDialog(speed: $speed)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ShowConclusionDialog &&
            const DeepCollectionEquality().equals(other.speed, speed));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(speed));

  @JsonKey(ignore: true)
  @override
  _$$_ShowConclusionDialogCopyWith<_$_ShowConclusionDialog> get copyWith =>
      __$$_ShowConclusionDialogCopyWithImpl<_$_ShowConclusionDialog>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() showRateDialog,
    required TResult Function(int soundCheckRating) showIssueDialog,
    required TResult Function(List<String> issue) showAskCheckSpeed,
    required TResult Function() showSpeedChecking,
    required TResult Function(String speed) showConclusionDialog,
  }) {
    return showConclusionDialog(speed);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
  }) {
    return showConclusionDialog?.call(speed);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? showRateDialog,
    TResult Function(int soundCheckRating)? showIssueDialog,
    TResult Function(List<String> issue)? showAskCheckSpeed,
    TResult Function()? showSpeedChecking,
    TResult Function(String speed)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showConclusionDialog != null) {
      return showConclusionDialog(speed);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ShowRateDialog value) showRateDialog,
    required TResult Function(_ShowIssueDialog value) showIssueDialog,
    required TResult Function(_ShowAskCheckSpeed value) showAskCheckSpeed,
    required TResult Function(_ShowSpeedChecking value) showSpeedChecking,
    required TResult Function(_ShowConclusionDialog value) showConclusionDialog,
  }) {
    return showConclusionDialog(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
  }) {
    return showConclusionDialog?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ShowRateDialog value)? showRateDialog,
    TResult Function(_ShowIssueDialog value)? showIssueDialog,
    TResult Function(_ShowAskCheckSpeed value)? showAskCheckSpeed,
    TResult Function(_ShowSpeedChecking value)? showSpeedChecking,
    TResult Function(_ShowConclusionDialog value)? showConclusionDialog,
    required TResult orElse(),
  }) {
    if (showConclusionDialog != null) {
      return showConclusionDialog(this);
    }
    return orElse();
  }
}

abstract class _ShowConclusionDialog implements SoundCheckEvent {
  const factory _ShowConclusionDialog({required final String speed}) =
      _$_ShowConclusionDialog;

  String get speed;
  @JsonKey(ignore: true)
  _$$_ShowConclusionDialogCopyWith<_$_ShowConclusionDialog> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SoundCheckState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() rateScreen,
    required TResult Function() issueScreen,
    required TResult Function() speedCheckAsk,
    required TResult Function() speedCheckProgress,
    required TResult Function() conclusion,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_RateScreen value) rateScreen,
    required TResult Function(_IssueScreen value) issueScreen,
    required TResult Function(_SpeedCheckAsk value) speedCheckAsk,
    required TResult Function(_SpeedCheckProgress value) speedCheckProgress,
    required TResult Function(_Conclusion value) conclusion,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SoundCheckStateCopyWith<$Res> {
  factory $SoundCheckStateCopyWith(
          SoundCheckState value, $Res Function(SoundCheckState) then) =
      _$SoundCheckStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SoundCheckStateCopyWithImpl<$Res>
    implements $SoundCheckStateCopyWith<$Res> {
  _$SoundCheckStateCopyWithImpl(this._value, this._then);

  final SoundCheckState _value;
  // ignore: unused_field
  final $Res Function(SoundCheckState) _then;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res> extends _$SoundCheckStateCopyWithImpl<$Res>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, (v) => _then(v as _$_Loading));

  @override
  _$_Loading get _value => super._value as _$_Loading;
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'SoundCheckState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() rateScreen,
    required TResult Function() issueScreen,
    required TResult Function() speedCheckAsk,
    required TResult Function() speedCheckProgress,
    required TResult Function() conclusion,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_RateScreen value) rateScreen,
    required TResult Function(_IssueScreen value) issueScreen,
    required TResult Function(_SpeedCheckAsk value) speedCheckAsk,
    required TResult Function(_SpeedCheckProgress value) speedCheckProgress,
    required TResult Function(_Conclusion value) conclusion,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements SoundCheckState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_RateScreenCopyWith<$Res> {
  factory _$$_RateScreenCopyWith(
          _$_RateScreen value, $Res Function(_$_RateScreen) then) =
      __$$_RateScreenCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_RateScreenCopyWithImpl<$Res>
    extends _$SoundCheckStateCopyWithImpl<$Res>
    implements _$$_RateScreenCopyWith<$Res> {
  __$$_RateScreenCopyWithImpl(
      _$_RateScreen _value, $Res Function(_$_RateScreen) _then)
      : super(_value, (v) => _then(v as _$_RateScreen));

  @override
  _$_RateScreen get _value => super._value as _$_RateScreen;
}

/// @nodoc

class _$_RateScreen implements _RateScreen {
  const _$_RateScreen();

  @override
  String toString() {
    return 'SoundCheckState.rateScreen()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_RateScreen);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() rateScreen,
    required TResult Function() issueScreen,
    required TResult Function() speedCheckAsk,
    required TResult Function() speedCheckProgress,
    required TResult Function() conclusion,
  }) {
    return rateScreen();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
  }) {
    return rateScreen?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
    required TResult orElse(),
  }) {
    if (rateScreen != null) {
      return rateScreen();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_RateScreen value) rateScreen,
    required TResult Function(_IssueScreen value) issueScreen,
    required TResult Function(_SpeedCheckAsk value) speedCheckAsk,
    required TResult Function(_SpeedCheckProgress value) speedCheckProgress,
    required TResult Function(_Conclusion value) conclusion,
  }) {
    return rateScreen(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
  }) {
    return rateScreen?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
    required TResult orElse(),
  }) {
    if (rateScreen != null) {
      return rateScreen(this);
    }
    return orElse();
  }
}

abstract class _RateScreen implements SoundCheckState {
  const factory _RateScreen() = _$_RateScreen;
}

/// @nodoc
abstract class _$$_IssueScreenCopyWith<$Res> {
  factory _$$_IssueScreenCopyWith(
          _$_IssueScreen value, $Res Function(_$_IssueScreen) then) =
      __$$_IssueScreenCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_IssueScreenCopyWithImpl<$Res>
    extends _$SoundCheckStateCopyWithImpl<$Res>
    implements _$$_IssueScreenCopyWith<$Res> {
  __$$_IssueScreenCopyWithImpl(
      _$_IssueScreen _value, $Res Function(_$_IssueScreen) _then)
      : super(_value, (v) => _then(v as _$_IssueScreen));

  @override
  _$_IssueScreen get _value => super._value as _$_IssueScreen;
}

/// @nodoc

class _$_IssueScreen implements _IssueScreen {
  const _$_IssueScreen();

  @override
  String toString() {
    return 'SoundCheckState.issueScreen()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_IssueScreen);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() rateScreen,
    required TResult Function() issueScreen,
    required TResult Function() speedCheckAsk,
    required TResult Function() speedCheckProgress,
    required TResult Function() conclusion,
  }) {
    return issueScreen();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
  }) {
    return issueScreen?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
    required TResult orElse(),
  }) {
    if (issueScreen != null) {
      return issueScreen();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_RateScreen value) rateScreen,
    required TResult Function(_IssueScreen value) issueScreen,
    required TResult Function(_SpeedCheckAsk value) speedCheckAsk,
    required TResult Function(_SpeedCheckProgress value) speedCheckProgress,
    required TResult Function(_Conclusion value) conclusion,
  }) {
    return issueScreen(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
  }) {
    return issueScreen?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
    required TResult orElse(),
  }) {
    if (issueScreen != null) {
      return issueScreen(this);
    }
    return orElse();
  }
}

abstract class _IssueScreen implements SoundCheckState {
  const factory _IssueScreen() = _$_IssueScreen;
}

/// @nodoc
abstract class _$$_SpeedCheckAskCopyWith<$Res> {
  factory _$$_SpeedCheckAskCopyWith(
          _$_SpeedCheckAsk value, $Res Function(_$_SpeedCheckAsk) then) =
      __$$_SpeedCheckAskCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SpeedCheckAskCopyWithImpl<$Res>
    extends _$SoundCheckStateCopyWithImpl<$Res>
    implements _$$_SpeedCheckAskCopyWith<$Res> {
  __$$_SpeedCheckAskCopyWithImpl(
      _$_SpeedCheckAsk _value, $Res Function(_$_SpeedCheckAsk) _then)
      : super(_value, (v) => _then(v as _$_SpeedCheckAsk));

  @override
  _$_SpeedCheckAsk get _value => super._value as _$_SpeedCheckAsk;
}

/// @nodoc

class _$_SpeedCheckAsk implements _SpeedCheckAsk {
  const _$_SpeedCheckAsk();

  @override
  String toString() {
    return 'SoundCheckState.speedCheckAsk()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SpeedCheckAsk);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() rateScreen,
    required TResult Function() issueScreen,
    required TResult Function() speedCheckAsk,
    required TResult Function() speedCheckProgress,
    required TResult Function() conclusion,
  }) {
    return speedCheckAsk();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
  }) {
    return speedCheckAsk?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
    required TResult orElse(),
  }) {
    if (speedCheckAsk != null) {
      return speedCheckAsk();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_RateScreen value) rateScreen,
    required TResult Function(_IssueScreen value) issueScreen,
    required TResult Function(_SpeedCheckAsk value) speedCheckAsk,
    required TResult Function(_SpeedCheckProgress value) speedCheckProgress,
    required TResult Function(_Conclusion value) conclusion,
  }) {
    return speedCheckAsk(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
  }) {
    return speedCheckAsk?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
    required TResult orElse(),
  }) {
    if (speedCheckAsk != null) {
      return speedCheckAsk(this);
    }
    return orElse();
  }
}

abstract class _SpeedCheckAsk implements SoundCheckState {
  const factory _SpeedCheckAsk() = _$_SpeedCheckAsk;
}

/// @nodoc
abstract class _$$_SpeedCheckProgressCopyWith<$Res> {
  factory _$$_SpeedCheckProgressCopyWith(_$_SpeedCheckProgress value,
          $Res Function(_$_SpeedCheckProgress) then) =
      __$$_SpeedCheckProgressCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_SpeedCheckProgressCopyWithImpl<$Res>
    extends _$SoundCheckStateCopyWithImpl<$Res>
    implements _$$_SpeedCheckProgressCopyWith<$Res> {
  __$$_SpeedCheckProgressCopyWithImpl(
      _$_SpeedCheckProgress _value, $Res Function(_$_SpeedCheckProgress) _then)
      : super(_value, (v) => _then(v as _$_SpeedCheckProgress));

  @override
  _$_SpeedCheckProgress get _value => super._value as _$_SpeedCheckProgress;
}

/// @nodoc

class _$_SpeedCheckProgress implements _SpeedCheckProgress {
  const _$_SpeedCheckProgress();

  @override
  String toString() {
    return 'SoundCheckState.speedCheckProgress()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_SpeedCheckProgress);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() rateScreen,
    required TResult Function() issueScreen,
    required TResult Function() speedCheckAsk,
    required TResult Function() speedCheckProgress,
    required TResult Function() conclusion,
  }) {
    return speedCheckProgress();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
  }) {
    return speedCheckProgress?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
    required TResult orElse(),
  }) {
    if (speedCheckProgress != null) {
      return speedCheckProgress();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_RateScreen value) rateScreen,
    required TResult Function(_IssueScreen value) issueScreen,
    required TResult Function(_SpeedCheckAsk value) speedCheckAsk,
    required TResult Function(_SpeedCheckProgress value) speedCheckProgress,
    required TResult Function(_Conclusion value) conclusion,
  }) {
    return speedCheckProgress(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
  }) {
    return speedCheckProgress?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
    required TResult orElse(),
  }) {
    if (speedCheckProgress != null) {
      return speedCheckProgress(this);
    }
    return orElse();
  }
}

abstract class _SpeedCheckProgress implements SoundCheckState {
  const factory _SpeedCheckProgress() = _$_SpeedCheckProgress;
}

/// @nodoc
abstract class _$$_ConclusionCopyWith<$Res> {
  factory _$$_ConclusionCopyWith(
          _$_Conclusion value, $Res Function(_$_Conclusion) then) =
      __$$_ConclusionCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ConclusionCopyWithImpl<$Res>
    extends _$SoundCheckStateCopyWithImpl<$Res>
    implements _$$_ConclusionCopyWith<$Res> {
  __$$_ConclusionCopyWithImpl(
      _$_Conclusion _value, $Res Function(_$_Conclusion) _then)
      : super(_value, (v) => _then(v as _$_Conclusion));

  @override
  _$_Conclusion get _value => super._value as _$_Conclusion;
}

/// @nodoc

class _$_Conclusion implements _Conclusion {
  const _$_Conclusion();

  @override
  String toString() {
    return 'SoundCheckState.conclusion()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Conclusion);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function() rateScreen,
    required TResult Function() issueScreen,
    required TResult Function() speedCheckAsk,
    required TResult Function() speedCheckProgress,
    required TResult Function() conclusion,
  }) {
    return conclusion();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
  }) {
    return conclusion?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function()? rateScreen,
    TResult Function()? issueScreen,
    TResult Function()? speedCheckAsk,
    TResult Function()? speedCheckProgress,
    TResult Function()? conclusion,
    required TResult orElse(),
  }) {
    if (conclusion != null) {
      return conclusion();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_RateScreen value) rateScreen,
    required TResult Function(_IssueScreen value) issueScreen,
    required TResult Function(_SpeedCheckAsk value) speedCheckAsk,
    required TResult Function(_SpeedCheckProgress value) speedCheckProgress,
    required TResult Function(_Conclusion value) conclusion,
  }) {
    return conclusion(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
  }) {
    return conclusion?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_RateScreen value)? rateScreen,
    TResult Function(_IssueScreen value)? issueScreen,
    TResult Function(_SpeedCheckAsk value)? speedCheckAsk,
    TResult Function(_SpeedCheckProgress value)? speedCheckProgress,
    TResult Function(_Conclusion value)? conclusion,
    required TResult orElse(),
  }) {
    if (conclusion != null) {
      return conclusion(this);
    }
    return orElse();
  }
}

abstract class _Conclusion implements SoundCheckState {
  const factory _Conclusion() = _$_Conclusion;
}
