import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:speed_test_dart/speed_test_dart.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/feedback/post_soundcheck_feedback.dart';
import 'package:watchers_widget/src/features/common/models/sound_check.dart';
import 'package:watchers_widget/watchers_widget.dart';

part 'sound_check_bloc.freezed.dart';
part 'sound_check_event.dart';
part 'sound_check_state.dart';

class SoundCheckBloc extends Bloc<SoundCheckEvent, SoundCheckState> {
  final PostSoundcheckFeedback _postSoundcheckFeedBack;
  final WatchersParamsProvider _watchersParamsProvider;

  SoundCheckBloc(this._postSoundcheckFeedBack,this._watchersParamsProvider) : super(const SoundCheckState.loading()) {
    on<SoundCheckEvent>((event, emit) => event.mapOrNull(
          init: (event) => _onInit(event, emit),
          showRateDialog: (event) => _onShowRateScreen(event, emit),
          showIssueDialog: (event) => _onShowIssueDialog(event, emit),
          showAskCheckSpeed: (event) => _onShowAskCheckSpeed(event, emit),
          showSpeedChecking: (event) => _onShowSpeedChecking(event, emit),
          showConclusionDialog: (event) => _onShowConclusionDialog(event, emit),
        ));
    add(const SoundCheckEvent.init());
  }

  late SoundCheck _soundCheck;
  SoundCheck get soundCheck => _soundCheck;

  Future<void> _onInit(_Init event, Emitter<SoundCheckState> emit) async {
    _soundCheck = SoundCheck.empty();

    final deviceInfoMap = await getDeviceInfo();
    _soundCheck = _soundCheck.copyWith(
        osName: deviceInfoMap['osName'],
        osVersion: deviceInfoMap['osVersion'],
        deviceType: deviceInfoMap['deviceType'],
        externalRoomId: _watchersParamsProvider.params.roomId,
    );

    add(const SoundCheckEvent.showRateDialog());
  }

  Future<void> _onShowRateScreen(_ShowRateDialog event, Emitter<SoundCheckState> emit) async {
    emit(const SoundCheckState.rateScreen());
  }

  Future<void> _onShowIssueDialog(_ShowIssueDialog event, Emitter<SoundCheckState> emit) async {
    _soundCheck = _soundCheck.copyWith(connectRate: event.soundCheckRating);

    if (event.soundCheckRating == 5) {
      await postCurrentSoundCheck();
    } else {
      emit(const SoundCheckState.issueScreen());
    }
  }

  Future<void> _onShowAskCheckSpeed(_ShowAskCheckSpeed event, Emitter<SoundCheckState> emit) async {
    _soundCheck = _soundCheck.copyWith(issue: event.issue);
    emit(const SoundCheckState.speedCheckAsk());
  }

  Future<void> _onShowSpeedChecking(_ShowSpeedChecking event, Emitter<SoundCheckState> emit) async {
    emit(const SoundCheckState.speedCheckProgress());
  }

  Future<void> _onShowConclusionDialog(
      _ShowConclusionDialog event, Emitter<SoundCheckState> emit) async {
    _soundCheck = _soundCheck.copyWith(speed: event.speed);
    postCurrentSoundCheck();
    emit(const SoundCheckState.conclusion());
  }

  Future<void> postCurrentSoundCheck() async {
    _postSoundcheckFeedBack.call(soundCheck: soundCheck);
  }

  Future<double> checkInternetSpeed() async {
    final SpeedTestDart tester = SpeedTestDart();

    final settings = await tester.getSettings();
    final servers = settings.servers;

    final _bestServersList = await tester.getBestServers(
      servers: servers,
    );

    final downloadRate = await tester.testDownloadSpeed(servers: _bestServersList);

    return downloadRate.roundToDouble();
  }

  Future<Map<String, String>> getDeviceInfo() async {
    Map<String, String> info = {};

    final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

    if (Platform.isAndroid) {
      final AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      info.addAll({'osName': "Android", 'deviceType': androidInfo.model, 'osVersion': "API ${androidInfo.version.sdkInt}"});
    }
    if (Platform.isIOS) {
      final IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      info.addAll({
        'osName': "IOS",
        'deviceType': iosInfo.model ?? 'mobile',
        'osVersion': iosInfo.systemVersion ?? 'undefined',
      });
    }
    return info;
  }
}
