part of 'sound_check_bloc.dart';

@freezed
class SoundCheckEvent with _$SoundCheckEvent {
  const factory SoundCheckEvent.init() = _Init;

  const factory SoundCheckEvent.showRateDialog() = _ShowRateDialog;

  const factory SoundCheckEvent.showIssueDialog({
    required int soundCheckRating,
  }) = _ShowIssueDialog;

  const factory SoundCheckEvent.showAskCheckSpeed({
    required List<String> issue,
  }) = _ShowAskCheckSpeed;

  const factory SoundCheckEvent.showSpeedChecking() = _ShowSpeedChecking;

  const factory SoundCheckEvent.showConclusionDialog({
    required String speed,
}) = _ShowConclusionDialog;
}
