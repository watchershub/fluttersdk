part of 'sound_check_bloc.dart';

@freezed
class SoundCheckState with _$SoundCheckState {

  const factory SoundCheckState.loading() = _Loading;

  const factory SoundCheckState.rateScreen() = _RateScreen;

  const factory SoundCheckState.issueScreen() = _IssueScreen;

  const factory SoundCheckState.speedCheckAsk() = _SpeedCheckAsk;

  const factory SoundCheckState.speedCheckProgress() = _SpeedCheckProgress;

  const factory SoundCheckState.conclusion() = _Conclusion;

}
