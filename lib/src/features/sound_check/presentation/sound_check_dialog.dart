import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/bloc_injectable_state.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/utils/transitions.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/confirm_dialog.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/info_dialog.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/item_pick_dialog.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/progress_dialog.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/rating_dialog.dart';
import 'package:watchers_widget/src/features/sound_check/logic/sound_check_bloc.dart';

class SoundCheckDialog extends StatefulWidget {
  const SoundCheckDialog();

  static Route route() => Transitions.buildFadeTransition(const SoundCheckDialog());

  @override
  _SoundCheckDialogState createState() => _SoundCheckDialogState();
}

class _SoundCheckDialogState extends BlocInjectableState<SoundCheckDialog, SoundCheckBloc,
    SoundCheckEvent, SoundCheckState> {
  @override
  Widget builder(BuildContext context, SoundCheckState state) {
    return Container(
      color: Colors.transparent,
      child: state.mapOrNull(
          rateScreen: (state) => RatingDialog(
              titleText: 'Оцените качество звука',
              confirmButtonText: 'Оценить',
              onConfirm: (rating) {
                bloc.add(SoundCheckEvent.showIssueDialog(soundCheckRating: rating));
                if (rating == 5) {
                  Navigator.of(context)
                    ..pop()
                    ..pop();
                }
              },
              onCancel: () {
                Navigator.of(context)
                  ..pop()
                  ..pop();
              }),
          issueScreen: (state) => ItemPickDialog(
                  titleText: 'Что пошло не так?',
                  confirmButtonText: 'Продолжить',
                  onConfirm: (pickedIssues) {
                    bloc.add(SoundCheckEvent.showAskCheckSpeed(issue: pickedIssues));
                  },
                  onCancel: () {
                    bloc.postCurrentSoundCheck();
                    Navigator.of(context)
                      ..pop()
                      ..pop();
                  },
                  items: const [
                    'Ничего не было слышно',
                    'Звук прерывался',
                    'Не получилось воспользоваться микрофоном',
                  ]),
          speedCheckAsk: (state) => ConfirmDialog(
              titleText: 'Протестируйте качество соединения',
              subtitleText: 'Проверка займет не больше минуты и поможет найти причину проблемы',
              confirmTextColor: CustomColors.primary,
              confirmButtonText: 'Проверить соединение',
              onConfirm: () {
                bloc.add(const SoundCheckEvent.showSpeedChecking());
              },
              onCancel: () {
                bloc.postCurrentSoundCheck();
                Navigator.of(context)
                  ..pop()
                  ..pop();
              }),
          speedCheckProgress: (state) => ProgressDialog<double>(
                titleText: 'Собираем статистику...',
                progress: () {
                  return bloc.checkInternetSpeed();
                },
                onProgressFinished: (speedInMbps) {
                  bloc.add(SoundCheckEvent.showConclusionDialog(speed: '$speedInMbps Mbps'));
                },
                onConfirm: () {
                  bloc.postCurrentSoundCheck();
                  Navigator.of(context)
                    ..pop()
                    ..pop();
                },
                confirmText: 'Закрыть',
              ),
          conclusion: (state) => InfoDialog(
                titleText: 'Спасибо!',
                subtitleText: 'Мы изучим,что пошло не так, и обязательно исправим.',
                confirmText: 'Закрыть',
                onConfirm: () {
                  Navigator.of(context)
                    ..pop()
                    ..pop();
                },
              )),
    );
  }
}
