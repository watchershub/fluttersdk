import 'package:watchers_widget/src/features/common/data/shared_preferences_storage/shared_preferences_storage.dart';

enum TooltipTrigger implements SharedPreferencesEntity<bool> {
  firstBlock(defaultValue: true),
  requestMic(defaultValue: true),
  hasHands(defaultValue: true),
  longTapEmoji(defaultValue: true);

  const TooltipTrigger({required this.defaultValue});

  @override
  final bool defaultValue;

  @override
  String get key => toString();

  T map<T>({
    required T Function() longTapEmoji,
    required T Function() firstBlock,
    required T Function() requestMic,
    required T Function() hasHands,
  }) {
    switch (this) {
      case TooltipTrigger.longTapEmoji:
        return longTapEmoji();
      case TooltipTrigger.firstBlock:
        return firstBlock();
      case TooltipTrigger.requestMic:
        return requestMic();
      case TooltipTrigger.hasHands:
        return hasHands();
    }
  }

  String get text => map(
      longTapEmoji: () => 'Удерживайте, чтобы сменить',
      firstBlock: () => 'Просмотреть список заблокированных пользователей можно здесь.',
      requestMic: () => 'Нажмите, чтобы запросить право голоса.',
      hasHands: () => 'Кто-то хочет высказаться.');
}
