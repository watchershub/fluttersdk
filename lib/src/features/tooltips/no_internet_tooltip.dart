import 'dart:async';

import 'package:flutter/material.dart';
import 'package:just_the_tooltip/just_the_tooltip.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/utils/text_utils.dart';

class NoInternetTooltip extends StatefulWidget {
  final AxisDirection preferredDirection;
  final double offset;
  final String tooltipText;
  final Widget child;

  const NoInternetTooltip({
    required this.tooltipText,
    required this.preferredDirection,
    required this.child,
    this.offset = 0.0,
  });

  @override
  State<NoInternetTooltip> createState() => _NoInternetTooltipState();
}

class _NoInternetTooltipState extends State<NoInternetTooltip> {
  final JustTheController _justTheController = JustTheController();

  void _onTap() {
    if (_justTheController.value == TooltipStatus.isHidden) {
      _justTheController.showTooltip();

      Timer(const Duration(seconds: 3), () {
        if (mounted) {
          _justTheController.hideTooltip();
        }
      });
      return;
    }

    if (_justTheController.value == TooltipStatus.isShowing) {
      _justTheController.hideTooltip();
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    final textTextStyle = TextStyles.onPrimary();

    final textSize = TextUtils.calcTextSize(
      text: widget.tooltipText,
      textStyle: textTextStyle,
      maxWidth: 80.w - 32,
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: _onTap,
          child: widget.child,
        ),
        Transform.translate(
          offset: Offset((-textSize.width / 2) + 16, 0),
          child: JustTheTooltip(
            elevation: 0,
            margin: EdgeInsets.zero,
            controller: _justTheController,
            triggerMode: TooltipTriggerMode.manual,
            offset: widget.offset,
            backgroundColor: CustomColors.tooltipBackground,
            tailBaseWidth: 20.0,
            tailLength: 10,
            preferredDirection: widget.preferredDirection,
            borderRadius: BorderRadius.circular(8.0),
            tailBuilder: (tip, point2, point3) =>
                _tailBuilder(tip, point2, point3, Offset((textSize.width / 2) - 16, 0)),
            content: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: 100.0,
                  maxWidth: 80.w,
                ),
                child: Text(
                  widget.tooltipText,
                  textAlign: TextAlign.left,
                  textWidthBasis: TextWidthBasis.longestLine,
                  style: TextStyles.onPrimary(),
                ),
              ),
            ),
            child: const SizedBox(),
          ),
        ),
      ],
    );
  }

  static Path _tailBuilder(
    Offset tip,
    Offset point2,
    Offset point3,
    Offset offset,
  ) {
    return Path()
      ..moveTo(tip.dx + offset.dx, tip.dy + offset.dy)
      ..lineTo(point2.dx + offset.dx, point2.dy + offset.dy)
      ..lineTo(point3.dx + offset.dx, point3.dy + offset.dy)
      ..close();
  }
}
