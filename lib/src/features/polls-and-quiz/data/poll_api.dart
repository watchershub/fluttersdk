import 'package:dio/dio.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/models/vote_request.dart';

class PollApi {
  final Dio _dio;
  final String Function() _getRoomId;

  const PollApi(
    this._dio,
    this._getRoomId,
  );

  Future<Response> vote(VoteRequest voteRequest) => _dio.post(
        'poll/${_getRoomId()}/vote',
        data: voteRequest.toMap(),
      );

  Future<Response> get() => _dio.get(
        'poll/${_getRoomId()}',
      );
}
