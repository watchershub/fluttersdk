import 'package:watchers_widget/src/core/extensions/future_dio_response_x.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/models/vote_request.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/models/vote_response.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/poll_api.dart';

import 'models/poll_info.dart';

abstract class IPollRepository {
  Future<VoteResponse> vote(VoteRequest voteRequest);
  Future<PollInfo> get();
}

class PollRepository implements IPollRepository {
  final PollApi _pollApi;

  const PollRepository(this._pollApi);

  @override
  Future<VoteResponse> vote(VoteRequest voteRequest) =>
      _pollApi.vote(voteRequest).computeResponse(VoteResponse.fromMap);

  @override
  Future<PollInfo> get() => _pollApi.get().computeResponse(PollInfo.fromMap);
}
