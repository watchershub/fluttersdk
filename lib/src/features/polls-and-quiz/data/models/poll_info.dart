import 'dart:convert';

import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_option.dart';

class PollInfo {
  final Poll? poll;
  final bool alreadyVoted;
  final List<PollOption> chosenOptions;

  const PollInfo({
    required this.poll,
    required this.alreadyVoted,
    required this.chosenOptions,
  });

  factory PollInfo.fromMap(Map<String, dynamic> map) {
    return PollInfo(
      poll: map['poll'] != null ? Poll.fromMap(map['poll']) : null,
      alreadyVoted: map['alreadyVoted'] ?? false,
      chosenOptions:
          List<PollOption>.from(map['chosenOptions']?.map((x) => PollOption.fromMap(x)) ?? []),
    );
  }

  factory PollInfo.fromJson(String source) => PollInfo.fromMap(json.decode(source));
}
