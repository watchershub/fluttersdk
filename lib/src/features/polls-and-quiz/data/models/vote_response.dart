import 'dart:convert';

import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_option.dart';

class VoteResponse {
  // final PollOption chosenOption;
  final List<PollOption> chosenOptions;
  final List<PollOption> pollResults;

  const VoteResponse({
    // required this.chosenOption,
    required this.chosenOptions,
    required this.pollResults,
  });

  factory VoteResponse.fromMap(Map<String, dynamic> map) {
    return VoteResponse(
      // chosenOption: PollOption.fromMap(map['chosenOption']),
      chosenOptions: List<PollOption>.from(map['chosenOptions']?.map((x) => PollOption.fromMap(x))),
      pollResults: List<PollOption>.from(map['pollResults']?.map((x) => PollOption.fromMap(x))),
    );
  }

  factory VoteResponse.fromJson(String source) => VoteResponse.fromMap(json.decode(source));
}
