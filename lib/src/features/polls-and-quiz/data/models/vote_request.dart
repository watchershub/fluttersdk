import 'dart:convert';

class VoteRequest {
  final int? optionId;
  final List<int>? optionIds;

  VoteRequest({
    this.optionId,
    this.optionIds,
  }) : assert(optionId != null && optionIds == null || optionIds != null && optionId == null);

  Map<String, dynamic> toMap() {
    return {
      if (optionId != null) 'optionId': optionId,
      if (optionIds != null) 'optionIds': optionIds,
    };
  }

  String toJson() => json.encode(toMap());
}
