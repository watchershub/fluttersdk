import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';

class PollIconWidget extends StatelessWidget {
  final bool isNotAnimated;
  final bool isActive;

  const PollIconWidget({
    required this.isActive,
    this.isNotAnimated = false,
  });

  @override
  Widget build(BuildContext context) {
    final _mainColor = isActive || !isNotAnimated
        ? CustomColors.pollIconActiveColor
        : CustomColors.pollIconInactiveColor;

    return Container(
      height: 24,
      width: 24,
      decoration: BoxDecoration(
        boxShadow: [
          if (isNotAnimated)
            const BoxShadow(
              color: Colors.black26,
              blurRadius: 4,
              spreadRadius: 0,
              offset: Offset(0, 4),
            ),
        ],
        color: isActive
            ? CustomColors.pollIconActiveBackground
            : CustomColors.pollIconInactiveBackground,
        shape: BoxShape.circle,
      ),
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              margin: const EdgeInsets.all(1),
              height: 10,
              width: 3,
              decoration: BoxDecoration(
                  color: _mainColor,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(6)),
            ),
            Container(
              margin: const EdgeInsets.all(1),
              height: 12,
              width: 4,
              decoration: BoxDecoration(
                  color: _mainColor,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(6)),
            ),
            Container(
              margin: const EdgeInsets.all(1),
              height: 10,
              width: 3,
              decoration: BoxDecoration(
                  color: _mainColor,
                  shape: BoxShape.rectangle,
                  borderRadius: BorderRadius.circular(6)),
            )
          ],
        ),
      ),
    );
  }
}
