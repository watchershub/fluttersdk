import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/poll_icon_widget.dart';

class AnimatedPollIconWidget extends StatefulWidget {
  final bool isTooltipEnabled;
  final bool isActive;

  const AnimatedPollIconWidget({
    required this.isTooltipEnabled,
    required this.isActive,
  });

  @override
  State<AnimatedPollIconWidget> createState() => _AnimatedPollIconWidgetState();
}

class _AnimatedPollIconWidgetState extends State<AnimatedPollIconWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _voteTipAnimationController;
  late Animation<double> _buttonWidthAnimation;
  late Animation<double> _buttonHeightAnimation;
  late Animation<double> _buttonHozirontalPaddingAnimation;

  @override
  void initState() {
    _voteTipAnimationController =
        AnimationController(duration: const Duration(milliseconds: 300), vsync: this);

    _buttonWidthAnimation = Tween<double>(begin: 24, end: 120).animate(_voteTipAnimationController);
    _buttonHeightAnimation = Tween<double>(begin: 24, end: 36).animate(_voteTipAnimationController);

    _buttonHozirontalPaddingAnimation =
        Tween<double>(begin: 0, end: 10).animate(_voteTipAnimationController)
          ..addListener(() {
            setState(() {});
          })
          ..addStatusListener((status) {
            if (status == AnimationStatus.completed) {
              Future.delayed(
                const Duration(seconds: 8),
                () {
                  if (mounted) {
                    _voteTipAnimationController.reverse();
                  }
                },
              );
            }
          });

    Future.delayed(
      const Duration(seconds: 4),
      () {
        if (mounted && widget.isTooltipEnabled && widget.isActive) {
          _voteTipAnimationController.forward();
        }
      },
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: CustomColors.primary,
      ),
      height: _buttonHeightAnimation.value,
      width: _buttonWidthAnimation.value,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          SizedBox(width: _buttonHozirontalPaddingAnimation.value),
          PollIconWidget(
            isActive: widget.isActive,
            isNotAnimated: _voteTipAnimationController.status == AnimationStatus.dismissed,
          ),
          const Expanded(
            child: Text(
              'Голосуйте',
              style: TextStyle(
                color: CustomColors.onPrimary,
                fontWeight: FontWeight.w600,
                fontSize: 13,
              ),
              overflow: TextOverflow.fade,
              maxLines: 1,
            ),
          ),
          SizedBox(width: _buttonHozirontalPaddingAnimation.value),
        ],
      ),
    );
  }
}
