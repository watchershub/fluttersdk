import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class PollItemWidget extends StatelessWidget {
  final String? pic;
  final String title;
  final double? percent;
  final Color backgroundColor;
  final bool isSelected;
  final bool isQuiz;
  final bool isRight;
  final bool isMultiple;
  final void Function()? onSelect;
  final bool alreadyVoted;
  final TextStyle? nameStyle;
  final bool isAnswerMessage;
  final bool isAnswerRight;
  final double? rightPadding;
  final Color? progressColor;
  final Color? percentColor;
  final bool isPollResults;

  const PollItemWidget({
    this.isAnswerMessage = false,
    this.isAnswerRight = false,
    required this.title,
    required this.backgroundColor,
    required this.isQuiz,
    required this.isRight,
    required this.isMultiple,
    required this.isSelected,
    this.pic,
    this.percent,
    this.onSelect,
    this.nameStyle,
    this.alreadyVoted = false,
    this.rightPadding,
    this.progressColor,
    this.percentColor,
    this.isPollResults = false,
  });

  bool get isQuizResult => isQuiz && alreadyVoted;

  Color _calcItemColor() {
    if (isAnswerMessage) {
      return backgroundColor;
    }

    return isQuizResult && isSelected
        ? isRight
            ? CustomColors.primary
            : CustomColors.wrongAnswer
        : backgroundColor;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onSelect,
      child: Container(
        height: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4.0),
          color: _calcItemColor(),
        ),
        child: Row(
          children: [
            if (pic != null && pic!.isNotEmpty) ...[
              ClipRRect(
                borderRadius: BorderRadius.circular(isAnswerMessage ? 5 : 0),
                child: _buildImage(pic!, isAnswerMessage),
              ),
              const SizedBox(width: 10),
            ] else
              const SizedBox(width: 16),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment:
                    isAnswerMessage ? MainAxisAlignment.center : MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      title,
                      style: nameStyle ?? TextStyles.primary,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  if (percent != null && alreadyVoted && !isQuiz) ...[
                    const SizedBox(height: 5),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          (percent! * 100).toInt().toString() + ' %',
                          style: TextStyles.title(
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                          ).copyWith(color: progressColor),
                        ),
                        SizedBox(width: isPollResults ? 9.fw : 18.fw),
                        Expanded(
                          child: LayoutBuilder(
                            builder: (_, constraints) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 3),
                                child: Row(
                                  children: [
                                    Container(
                                      height: 5,
                                      width: constraints.maxWidth * percent!,
                                      decoration: BoxDecoration(
                                        color: percentColor ?? CustomColors.primary,
                                        borderRadius: BorderRadius.circular(100),
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        ),
                        SizedBox(width: 18.fw),
                      ],
                    ),
                  ],
                ],
              ),
            ),
            if (!isPollResults) _buildCheckbox(),
            SizedBox(width: rightPadding ?? 16.fw),
          ],
        ),
      ),
    );
  }

  Widget _buildCheckbox() {
    return Container(
      height: 24,
      width: 24,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: _calcCheckBoxBackgroundColor(),
        border: alreadyVoted
            ? null
            : Border.all(
                strokeAlign: BorderSide.strokeAlignInside,
                color: CustomColors.choseColor,
              ),
      ),
      child: (isSelected || isQuizResult)
          ? Padding(
              padding: const EdgeInsets.all(6.0),
              child: SvgIcon(
                _buildIcon(),
                color: _calcCheckboxIconColor(),
              ),
            )
          : null,
    );
  }

  String _buildIcon() {
    return (isRight || isSelected && !alreadyVoted || !isQuiz && (isAnswerMessage || alreadyVoted))
        ? Resources.selected_mark
        : Resources.close;
  }

  Color _calcCheckBoxBackgroundColor() {
    if (isAnswerMessage) {
      return CustomColors.myMessageColor;
    }

    if (isSelected) {
      return CustomColors.choseColor;
    }

    if (isQuizResult) {
      return isRight ? CustomColors.rightAnswer : CustomColors.wrongAnswer.withOpacity(0.2);
    }

    return isSelected ? CustomColors.choseColor : Colors.transparent;
  }

  Widget _buildImage(String image, bool isInAnswer) {
    return SizedBox(
        width: isInAnswer ? 42 : 50,
        child: UniversalPicture(image, null, isInAnswer ? 42 : 50, BoxFit.cover));
  }

  Color _calcCheckboxIconColor() {
    if (isAnswerMessage) {
      if (!isQuiz) return Colors.black;
      return isRight ? CustomColors.rightAnswer : CustomColors.wrongAnswer;
    }

    if (isQuizResult && isSelected) {
      return isRight ? CustomColors.rightAnswer : CustomColors.wrongAnswer;
    }

    if (!isQuiz && alreadyVoted) {
      return CustomColors.rightAnswer;
    }

    if (isSelected) {
      return Colors.black;
    }

    return isRight ? Colors.white : CustomColors.wrongAnswer;
  }
}
