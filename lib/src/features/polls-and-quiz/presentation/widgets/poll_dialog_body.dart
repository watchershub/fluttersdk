import 'package:collection/collection.dart';
import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/bloc_dependent_state.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/models/vote_request.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/logic/polls_bloc.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/poll_item_widget.dart';

class PollDialogBody extends StatefulWidget {
  const PollDialogBody();

  @override
  State<PollDialogBody> createState() => _PollDialogBodyState();
}

class _PollDialogBodyState
    extends BlocDependentState<PollDialogBody, PollsBloc, PollsEvent, PollsState> {
  @override
  bool buildWhen(PollsState previous, PollsState current) {
    return current.poll != null;
  }

  @override
  Widget builder(BuildContext context, PollsState state) {
    final options = state.poll!.options;
    double votesSum = 0;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        ListView(
          shrinkWrap: true,
          children: options.mapIndexed((i, pollOption) {
            if (options.last != pollOption && state.votesSum != 0) {
              votesSum = votesSum + ((pollOption.votesLength! / state.votesSum) * 100).toInt();
            }
            return Padding(
              padding: EdgeInsets.only(bottom: i < options.length - 1 ? 2.0 : 0),
              child: PollItemWidget(
                pic: pollOption.pic.isEmpty ? null : pollOption.pic,
                title: pollOption.text,
                backgroundColor: i.isOdd ? CustomColors.oddPollItem : CustomColors.evenPollItem,
                isQuiz: state.poll!.isQuiz,
                isMultiple: state.poll!.isMultiple,
                isSelected: state.checkSelection(pollOption),
                percent: pollOption.votesLength != null && state.votesSum != 0
                    ? options.last == pollOption
                        ? (100 - votesSum)/100
                        : pollOption.votesLength! / state.votesSum
                    : null,
                onSelect: () async {
                  if (state.alreadyVoted) return;

                  if (state.poll!.isMultiple) {
                    bloc.toggleOption(PollsEvent.choseOption(pollOption));
                    return;
                  }

                  bloc.submitAnswer(PollsEvent.submitAnswer(VoteRequest(optionId: pollOption.id)));
                },
                alreadyVoted: state.alreadyVoted,
                isRight: pollOption.isRight,
              ),
            );
          }).toList(),
        ),
      ],
    );
  }
}
