import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/dependent_stateless_widget.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/features/common/widgets/submit_button.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/logic/polls_bloc.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/poll_dialog_body.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/poll_icon_widget.dart';

class PollDialog extends BlocDependentStatelessWidget<PollsBloc, PollsEvent, PollsState> {
  final String title;
  final String question;
  final String? buttonText;

  const PollDialog({
    required this.title,
    required this.question,
    this.buttonText,
  });

  @override
  bool buildWhen(PollsState previous, PollsState current) {
    return current.poll != null;
  }

  @override
  bool listenWhen(PollsState previous, PollsState current) {
    /// Закрываем диалог, если обновился опрос
    return previous.poll != null && previous.poll?.id != current.poll?.id;
  }

  @override
  void listener(BuildContext context, PollsState state, PollsBloc cubit) {
    Navigator.maybePop(context);
  }

  @override
  Widget builder(BuildContext context, PollsState state, PollsBloc bloc) {
    return Column(
      children: [
        Dialog(
          insetPadding: const EdgeInsets.fromLTRB(16, 91, 16, 0),
          backgroundColor: CustomColors.modalBackground,
          child: Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.topCenter,
            children: [
              const Positioned(
                top: -18,
                child: PollIconWidget(isActive: true),
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  const SizedBox(height: 8),
                  Text(
                    title,
                    style: TextStyles.secondary(
                      fontSize: 13,
                    ).copyWith(color: CustomColors.dialogFontColor),
                  ),
                  const SizedBox(height: 12),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      question,
                      textAlign: TextAlign.center,
                      style: TextStyles.contribution.copyWith(
                        color: CustomColors.dialogFontColor,
                        fontWeight: FontWeight.w500,
                        fontSize: 15,
                      ),
                    ),
                  ),
                  const SizedBox(height: 12),
                  const PollDialogBody(),
                ],
              ),
            ],
          ),
        ),
        if (!state.alreadyVoted && state.poll!.isMultiple) ...[
          const SizedBox(height: 8),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: SubmitButton.textual(
              text: buttonText ?? 'Голосовать',
              onTap: () {
                bloc.submitSelectedAnswers(PollsEvent.submitSelectedAnswers());
              },
              isActive: state.chosenOptions.isNotEmpty,
            ),
          ),
        ],
      ],
    );
  }
}
