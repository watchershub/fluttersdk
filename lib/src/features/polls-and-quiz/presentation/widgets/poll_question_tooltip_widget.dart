import 'package:flutter/material.dart';
import 'package:just_the_tooltip/just_the_tooltip.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/core/utils/text_utils.dart';

class PollQuestionTooltipWidget extends StatefulWidget {
  final String questionText;
  final bool isQuestionTooltipShown;
  final void Function() onTapVote;
  final void Function() onDismiss;

  const PollQuestionTooltipWidget({
    required this.questionText,
    required this.isQuestionTooltipShown,
    required this.onTapVote,
    required this.onDismiss,
  });

  @override
  State<PollQuestionTooltipWidget> createState() => _PollQuestionTooltipWidgetState();
}

class _PollQuestionTooltipWidgetState extends State<PollQuestionTooltipWidget> {
  final _tooltipController = JustTheController();

  @override
  void initState() {
    if (widget.isQuestionTooltipShown) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        _tooltipController.showTooltip();
      });
    }
    super.initState();
  }

  @override
  void didUpdateWidget(covariant PollQuestionTooltipWidget oldWidget) {
    if (widget.isQuestionTooltipShown) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        _tooltipController.hideTooltip();
        _tooltipController.showTooltip();
      });
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final questionTextStyle = TextStyles.subhead.copyWith(color: CustomColors.textMain);

    final textHeight = TextUtils.calcTextHeight(
      text: widget.questionText,
      textStyle: questionTextStyle,
      maxWidth: 80.w - 48,
    );

    return Padding(
      padding: EdgeInsets.only(top: textHeight),
      child: JustTheTooltip(
        onDismiss: widget.onDismiss,
        elevation: 0,
        triggerMode: TooltipTriggerMode.manual,
        isModal: true,
        backgroundColor: CustomColors.tooltipPollBackground,
        controller: _tooltipController,
        tailBaseWidth: 20.0,
        preferredDirection: AxisDirection.left,
        borderRadius: BorderRadius.circular(8.0),
        tailLength: 8,
        offset: 6,
        tailBuilder: (point1, point2, point3) =>
            _tailBuilder(point1, point2, point3, textHeight / 2),
        content: Padding(
          padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
          child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 80.w),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Flexible(
                      child: Text(
                        widget.questionText,
                        textAlign: TextAlign.left,
                        textWidthBasis: TextWidthBasis.longestLine,
                        style: questionTextStyle,
                      ),
                    ),
                    const SizedBox(width: 4),
                    InkWell(
                      onTap: () => _tooltipController.hideTooltip(),
                      child: const Padding(
                        padding: EdgeInsets.all(4.0),
                        child: SvgIcon(
                          Resources.close,
                          color: CustomColors.tooltipCloseIcon,
                          size: 10,
                        ),
                      ),
                    ),
                  ],
                ),
                const SizedBox(height: 8),
                InkWell(
                  onTap: () {
                    widget.onTapVote.call();
                    _tooltipController.hideTooltip();
                  },
                  child: Text(
                    'Голосовать',
                    textAlign: TextAlign.left,
                    textWidthBasis: TextWidthBasis.longestLine,
                    style: TextStyles.subhead.copyWith(color: CustomColors.primary),
                  ),
                ),
              ],
            ),
          ),
        ),
        child: const SizedBox.shrink(),
      ),
    );
  }

  static Path _tailBuilder(
    Offset tip,
    Offset point2,
    Offset point3,
    double yOffset,
  ) {
    return Path()
      ..moveTo(tip.dx, tip.dy - yOffset)
      ..lineTo(point2.dx, point2.dy - yOffset)
      ..lineTo(point3.dx, point3.dy - yOffset)
      ..close();
  }
}
