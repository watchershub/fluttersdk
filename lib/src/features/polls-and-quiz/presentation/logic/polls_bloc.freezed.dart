// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'polls_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$PollsState {
  Poll? get poll => throw _privateConstructorUsedError;
  bool get alreadyVoted => throw _privateConstructorUsedError;
  Set<PollOption> get chosenOptions => throw _privateConstructorUsedError;
  bool get isQuestionTooltipShown => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $PollsStateCopyWith<PollsState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $PollsStateCopyWith<$Res> {
  factory $PollsStateCopyWith(
          PollsState value, $Res Function(PollsState) then) =
      _$PollsStateCopyWithImpl<$Res>;
  $Res call(
      {Poll? poll,
      bool alreadyVoted,
      Set<PollOption> chosenOptions,
      bool isQuestionTooltipShown});
}

/// @nodoc
class _$PollsStateCopyWithImpl<$Res> implements $PollsStateCopyWith<$Res> {
  _$PollsStateCopyWithImpl(this._value, this._then);

  final PollsState _value;
  // ignore: unused_field
  final $Res Function(PollsState) _then;

  @override
  $Res call({
    Object? poll = freezed,
    Object? alreadyVoted = freezed,
    Object? chosenOptions = freezed,
    Object? isQuestionTooltipShown = freezed,
  }) {
    return _then(_value.copyWith(
      poll: poll == freezed
          ? _value.poll
          : poll // ignore: cast_nullable_to_non_nullable
              as Poll?,
      alreadyVoted: alreadyVoted == freezed
          ? _value.alreadyVoted
          : alreadyVoted // ignore: cast_nullable_to_non_nullable
              as bool,
      chosenOptions: chosenOptions == freezed
          ? _value.chosenOptions
          : chosenOptions // ignore: cast_nullable_to_non_nullable
              as Set<PollOption>,
      isQuestionTooltipShown: isQuestionTooltipShown == freezed
          ? _value.isQuestionTooltipShown
          : isQuestionTooltipShown // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_PollsStateCopyWith<$Res>
    implements $PollsStateCopyWith<$Res> {
  factory _$$_PollsStateCopyWith(
          _$_PollsState value, $Res Function(_$_PollsState) then) =
      __$$_PollsStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Poll? poll,
      bool alreadyVoted,
      Set<PollOption> chosenOptions,
      bool isQuestionTooltipShown});
}

/// @nodoc
class __$$_PollsStateCopyWithImpl<$Res> extends _$PollsStateCopyWithImpl<$Res>
    implements _$$_PollsStateCopyWith<$Res> {
  __$$_PollsStateCopyWithImpl(
      _$_PollsState _value, $Res Function(_$_PollsState) _then)
      : super(_value, (v) => _then(v as _$_PollsState));

  @override
  _$_PollsState get _value => super._value as _$_PollsState;

  @override
  $Res call({
    Object? poll = freezed,
    Object? alreadyVoted = freezed,
    Object? chosenOptions = freezed,
    Object? isQuestionTooltipShown = freezed,
  }) {
    return _then(_$_PollsState(
      poll: poll == freezed
          ? _value.poll
          : poll // ignore: cast_nullable_to_non_nullable
              as Poll?,
      alreadyVoted: alreadyVoted == freezed
          ? _value.alreadyVoted
          : alreadyVoted // ignore: cast_nullable_to_non_nullable
              as bool,
      chosenOptions: chosenOptions == freezed
          ? _value._chosenOptions
          : chosenOptions // ignore: cast_nullable_to_non_nullable
              as Set<PollOption>,
      isQuestionTooltipShown: isQuestionTooltipShown == freezed
          ? _value.isQuestionTooltipShown
          : isQuestionTooltipShown // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_PollsState implements _PollsState {
  const _$_PollsState(
      {required this.poll,
      required this.alreadyVoted,
      required final Set<PollOption> chosenOptions,
      required this.isQuestionTooltipShown})
      : _chosenOptions = chosenOptions;

  @override
  final Poll? poll;
  @override
  final bool alreadyVoted;
  final Set<PollOption> _chosenOptions;
  @override
  Set<PollOption> get chosenOptions {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableSetView(_chosenOptions);
  }

  @override
  final bool isQuestionTooltipShown;

  @override
  String toString() {
    return 'PollsState(poll: $poll, alreadyVoted: $alreadyVoted, chosenOptions: $chosenOptions, isQuestionTooltipShown: $isQuestionTooltipShown)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_PollsState &&
            const DeepCollectionEquality().equals(other.poll, poll) &&
            const DeepCollectionEquality()
                .equals(other.alreadyVoted, alreadyVoted) &&
            const DeepCollectionEquality()
                .equals(other._chosenOptions, _chosenOptions) &&
            const DeepCollectionEquality()
                .equals(other.isQuestionTooltipShown, isQuestionTooltipShown));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(poll),
      const DeepCollectionEquality().hash(alreadyVoted),
      const DeepCollectionEquality().hash(_chosenOptions),
      const DeepCollectionEquality().hash(isQuestionTooltipShown));

  @JsonKey(ignore: true)
  @override
  _$$_PollsStateCopyWith<_$_PollsState> get copyWith =>
      __$$_PollsStateCopyWithImpl<_$_PollsState>(this, _$identity);
}

abstract class _PollsState implements PollsState {
  const factory _PollsState(
      {required final Poll? poll,
      required final bool alreadyVoted,
      required final Set<PollOption> chosenOptions,
      required final bool isQuestionTooltipShown}) = _$_PollsState;

  @override
  Poll? get poll;
  @override
  bool get alreadyVoted;
  @override
  Set<PollOption> get chosenOptions;
  @override
  bool get isQuestionTooltipShown;
  @override
  @JsonKey(ignore: true)
  _$$_PollsStateCopyWith<_$_PollsState> get copyWith =>
      throw _privateConstructorUsedError;
}
