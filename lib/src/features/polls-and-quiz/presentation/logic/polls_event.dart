part of 'polls_bloc.dart';

abstract class PollsEvent {
  const PollsEvent();

  static const init = _Init._;
  static const updatePoll = _UpdatePoll._;
  static const resetPoll = _ResetPoll._;
  static const showPoll = _ShowPoll._;
  static const submitAnswer = _SubmitAnswer._;
  static const choseOption = _ToggleOption._;
  static const submitSelectedAnswers = _SubmitSelectedAnswers._;
  static const onTooltipClosed = _OnTooltipClosed._;
}

class _Init extends PollsEvent {
  const _Init._();
}

class _UpdatePoll extends PollsEvent {
  final Poll? poll;
  final bool isQuestionTooltipShown;

  const _UpdatePoll._(this.poll, this.isQuestionTooltipShown);
}

class _ResetPoll extends PollsEvent {
  const _ResetPoll._();
}

class _ShowPoll extends PollsEvent {
  const _ShowPoll._();
}

class _SubmitAnswer extends PollsEvent {
  final VoteRequest voteRequest;
  const _SubmitAnswer._(this.voteRequest);
}

class _SubmitSelectedAnswers extends PollsEvent {
  const _SubmitSelectedAnswers._();
}

class _ToggleOption extends PollsEvent {
  final PollOption pollOption;
  const _ToggleOption._(this.pollOption);
}

class _OnTooltipClosed extends PollsEvent {
  const _OnTooltipClosed._();
}
