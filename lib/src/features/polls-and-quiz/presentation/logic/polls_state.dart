part of 'polls_bloc.dart';

@freezed
class PollsState with _$PollsState {
  const factory PollsState({
    required Poll? poll,
    required bool alreadyVoted,
    required Set<PollOption> chosenOptions,
    required bool isQuestionTooltipShown,
  }) = _PollsState;

  factory PollsState.initial() => const PollsState(
        poll: null,
        alreadyVoted: false,
        chosenOptions: {},
        isQuestionTooltipShown: false,
      );
}

extension PollsStateX on PollsState {
  bool checkSelection(PollOption pollOption) =>
      chosenOptions.map((e) => e.id).contains(pollOption.id);

  int get votesSum => poll!.options.fold(0, (sum, option) => sum + (option.votesLength ?? 0));
}
