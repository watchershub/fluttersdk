import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:watchers_widget/src/core/bloc_mixins.dart';
import 'package:watchers_widget/src/features/chat/data/chat_api.dart';
import 'package:watchers_widget/src/features/chat/data/request/send_message/send_vote_message_request.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/send_message_use_case.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/models/vote_request.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_option.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_text.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/use_cases/get_poll_use_case.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/use_cases/vote_use_case.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/poll_dialog.dart';

part 'polls_bloc.freezed.dart';
part 'polls_event.dart';
part 'polls_state.dart';

class PollsBloc extends Bloc<PollsEvent, PollsState> with ContextAware {
  final ChatApi _chatApi;
  final VoteUseCase _voteUseCase;
  final SendMessageUseCase _sendMessageUseCase;
  final GetPollUseCase _getPollUseCase;

  PollsBloc(
    this._chatApi,
    this._voteUseCase,
    this._sendMessageUseCase,
    this._getPollUseCase,
  ) : super(PollsState.initial()) {
    on<_Init>(_onInit);
    on<_UpdatePoll>(_updatePoll);
    on<_ResetPoll>(_resetPoll);
    on<_ShowPoll>(_showPoll);
    on<_SubmitAnswer>(_submitAnswer);
    on<_ToggleOption>(_toggleOption);
    on<_SubmitSelectedAnswers>(_submitSelectedAnswers, transformer: droppable());
    on<_OnTooltipClosed>(_onTooltipClosed);
    _init();
  }

  Future<void> _onInit(_Init event, Emitter<PollsState> emit) async {
    final pollInfo = (await _getPollUseCase()).successValue;

    emit(state.copyWith(
      alreadyVoted: pollInfo.alreadyVoted,
      poll: pollInfo.poll,
      chosenOptions: pollInfo.chosenOptions.toSet(),
    ));
  }

  void _init() {
    // Получаем текущий пул
    add(PollsEvent.init());

    // Подписываемся на обновления
    _chatApi.socketIsolateController.stream.listen((e) {
      final event = e['event'];
      final data = e['data'];

      if (event == 'pollCreated') {
        final poll = Poll.fromMap(data['poll']);
        return add(PollsEvent.updatePoll(poll, true));
      }

      if (event == 'voteCreated') {
        final List<PollOption> options = [...state.poll!.options];

        // Инкрементируем votesLength в ручную в realtime
        final pollOption = PollOption.fromMap(data['pollOption']).copyWith(
            votesLength: (state.poll?.options
                        .firstWhere((element) => element.id == data['pollOption']['id'])
                        .votesLength ??
                    0) +
                1);

        options[state.poll!.options.indexWhere((element) => element.id == pollOption.id)] =
            pollOption;

        return add(PollsEvent.updatePoll(state.poll!.copyWith(options: options), false));
      }

      if (event == 'pollDeleted') {
        // final poll = Poll.fromMap(data['poll']);

        return add(PollsEvent.resetPoll());
      }

      // Не используется? (походу вместо редактирования удаление + создание нового)
      if (event == 'pollUpdated') {
        final poll = Poll.fromMap(data['poll']);
        return add(PollsEvent.updatePoll(poll, true));
      }

      if (event == 'pollEnded') {
        // Я думаю нет смысла проверять, что именно в нашей комнате закончился опрос,
        // а в какой еще join был по externalRoomId?
        // final String externalRoomId = data['externalRoomId'];
        return add(PollsEvent.resetPoll());
      }
    });
  }

  void _updatePoll(_UpdatePoll event, Emitter<PollsState> emit) {
    emit(state.copyWith(
      poll: event.poll,
      isQuestionTooltipShown: event.isQuestionTooltipShown,
    ));
  }

  void _resetPoll(_ResetPoll event, Emitter<PollsState> emit) {
    emit(PollsState.initial());
  }

  void showPoll(_ShowPoll event) => add(event);

  void _showPoll(_ShowPoll event, Emitter<PollsState> emit) {
    if (state.poll == null) return;

    String title = '';
    if (state.poll!.isQuiz) {
      if (state.poll!.isMultiple) {
        title = 'Отметь верные варианты';
      } else {
        title = 'Дай верный ответ';
      }
    } else {
      if (state.poll!.isMultiple) {
        title = 'Можно отметить несколько вариантов';
      } else {
        title = 'Поделись мнением';
      }
    }

    showDialog(
      context: context,
      useRootNavigator: false,
      builder: (context) {
        return BlocProvider.value(
          value: this,
          child: PollDialog(
            title: title,
            buttonText: state.poll!.isQuiz?'Ответить':'Голосовать',
            question: state.poll!.text,
          ),
        );
      },
    );
  }

  void submitAnswer(_SubmitAnswer event) => add(event);

  Future<void> _submitAnswer(_SubmitAnswer event, Emitter<PollsState> emit) => _vote(
        voteRequest: event.voteRequest,
        emit: emit,
      );

  void toggleOption(_ToggleOption event) => add(event);

  Future<void> _toggleOption(_ToggleOption event, Emitter<PollsState> emit) async {
    final chosenOptions = Set<PollOption>.from(state.chosenOptions);

    final isAdd = !state.checkSelection(event.pollOption);

    if (isAdd) {
      chosenOptions..add(event.pollOption);
    } else {
      chosenOptions.removeWhere((e) => e.id == event.pollOption.id);
    }

    emit(state.copyWith(chosenOptions: chosenOptions));
  }

  void submitSelectedAnswers(_SubmitSelectedAnswers event) => add(event);

  Future<void> _submitSelectedAnswers(
    _SubmitSelectedAnswers event,
    Emitter<PollsState> emit,
  ) =>
      _vote(
        voteRequest: VoteRequest(optionIds: state.chosenOptions.map((e) => e.id).toList()),
        emit: emit,
      );

  Future<void> _vote({
    required VoteRequest voteRequest,
    required Emitter<PollsState> emit,
  }) async {
    final voteResult = await _voteUseCase.call(
      voteRequest: voteRequest,
    );

    if (voteResult.isError || state.poll == null) return;

    final voteResponse = voteResult.successValue;

    await _sendMessageUseCase.call(
      sendMessageRequest: SendVoteMessageRequest(
        type: MessageType.vote,
        pollText: PollText(
          poll: state.poll!,
          pollOptionsChosen: voteResponse.chosenOptions,
        ),
      ),
    );

    emit(state.copyWith(
      alreadyVoted: true,
      chosenOptions: voteResponse.chosenOptions.toSet(),
      poll: state.poll!.copyWith(options: voteResponse.pollResults),
    ));
  }

  void onTooltipClosed() => add(const _OnTooltipClosed._());

  void _onTooltipClosed(_OnTooltipClosed event, Emitter<PollsState> emit) =>
      emit(state.copyWith(isQuestionTooltipShown: false));
}
