import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/bloc_dependent_state.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/features/common/services/models/gradient_border_side.dart';
import 'package:watchers_widget/src/features/common/widgets/super_ellips_card.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/logic/polls_bloc.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/animated_poll_icon_widget.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/poll_question_tooltip_widget.dart';

class PollsWidget extends StatefulWidget {
  final VoidCallback? onTap;

  const PollsWidget({this.onTap});

  @override
  State<PollsWidget> createState() => _PollsWidgetState();
}

class _PollsWidgetState extends BlocDependentState<PollsWidget, PollsBloc, PollsEvent, PollsState> {
  @override
  Widget builder(BuildContext context, PollsState state) {
    if (state.poll == null) return const SizedBox.shrink();

    // if (state.alreadyVoted) return _buildInactiveControl();

    return SizedBox(
      height: 50,
      child: InkWell(
          onTap: () {
            widget.onTap?.call();
            bloc.showPoll(PollsEvent.showPoll());
          },
          child: Row(
            children: [
              const PollPictures(),
              SizedBox(
                width: state.alreadyVoted && state.poll!.isQuiz ? 18 : 5,
              ),
              PollQuestionTooltipWidget(
                questionText: state.poll!.text,
                onTapVote: () => bloc.showPoll(PollsEvent.showPoll()),
                isQuestionTooltipShown: state.isQuestionTooltipShown,
                onDismiss: () => bloc.onTooltipClosed(),
              ),
              AnimatedPollIconWidget(
                isTooltipEnabled: !state.isQuestionTooltipShown,
                isActive: !state.alreadyVoted,
              ),
            ],
          )),
    );
  }
}

class PollPictures extends StatefulWidget {
  const PollPictures({super.key});

  @override
  State<PollPictures> createState() => _PollPicturesState();
}

class _PollPicturesState
    extends BlocDependentState<PollPictures, PollsBloc, PollsEvent, PollsState> {
  @override
  Widget builder(BuildContext context, PollsState state) {
    final bool hasPictures = state.poll!.options.first.pic.isNotEmpty;

    double votesSum = 0;

    if (hasPictures && !state.isQuestionTooltipShown) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          ...state.poll!.options.map((e) {
            if (state.poll!.options.last != e && state.votesSum!=0) {
              votesSum = votesSum + ((e.votesLength! / state.votesSum) * 100).toInt();
            }
            return Padding(
              padding: const EdgeInsets.all(5),
              child: Stack(
                clipBehavior: Clip.none,
                children: [
                  SuperEllipseCard(
                    side: state.alreadyVoted ? null : const GradientBorderSide(),
                    child: UniversalPicture(e.pic, 36, 36, BoxFit.cover),
                  ),
                  if (state.alreadyVoted && !state.poll!.isQuiz)
                    Positioned(
                        left: 10,
                        bottom: 0,
                        child: Container(
                          padding: const EdgeInsets.symmetric(horizontal: 2,vertical: 2),
                          //width: 35,
                          //height: 20,
                          decoration: BoxDecoration(
                              color: CustomColors.pollIconInactiveBackground,
                              borderRadius: BorderRadius.circular(12)),
                          child: Center(
                            child: Text(
                              state.poll!.options.last == e
                                  ? (100 - votesSum).toInt().toString() + '%'
                                  : (((e.votesLength! / state.votesSum) * 100).toInt()).toString()+'%',
                              style: TextStyles.title(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w500,
                                  color: CustomColors.textMain),
                            ),
                          ),
                        ))
                ],
              ),
            );
          }).toList(),
        ],
      );
    }
    return const SizedBox();
  }
}
