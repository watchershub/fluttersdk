import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/models/poll_info.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/poll_repository.dart';

class GetPollUseCase {
  final IPollRepository _pollRepository;

  const GetPollUseCase(
    this._pollRepository,
  );

  Future<Result<PollInfo, Exception>> call() async {
    try {
      return Success(await _pollRepository.get());
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
