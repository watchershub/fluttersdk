import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/models/vote_request.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/models/vote_response.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/poll_repository.dart';

class VoteUseCase {
  final IPollRepository _pollRepository;

  const VoteUseCase(
    this._pollRepository,
  );

  Future<Result<VoteResponse, Exception>> call({
    required VoteRequest voteRequest,
  }) async {
    try {
      return Success(await _pollRepository.vote(voteRequest));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
