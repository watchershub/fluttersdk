import 'dart:convert';

import 'package:equatable/equatable.dart';

import 'package:watchers_widget/src/features/common/models/room.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_option.dart';

class Poll extends Equatable {
  final int id;
  final String text;
  final bool isMultiple;
  final String type;
  final String status;
  final String createdAt;
  // final Talker? creatorTalker;
  final Room? room;
  final List<PollOption> options;

  const Poll({
    required this.id,
    required this.text,
    required this.isMultiple,
    required this.type,
    required this.status,
    required this.createdAt,
    required this.options,
    // this.creatorTalker,
    this.room,
  });

  bool get isQuiz => type == 'QUIZ';

  int get votesSum => options.fold(0, (sum, option) => sum + (option.votesLength ?? 0));

  Poll copyWith({
    int? id,
    String? text,
    bool? isMultiple,
    String? type,
    String? status,
    String? createdAt,
    Talker? creatorTalker,
    Room? room,
    List<PollOption>? options,
  }) {
    return Poll(
      id: id ?? this.id,
      text: text ?? this.text,
      isMultiple: isMultiple ?? this.isMultiple,
      type: type ?? this.type,
      status: status ?? this.status,
      createdAt: createdAt ?? this.createdAt,
      // creatorTalker: creatorTalker ?? this.creatorTalker,
      room: room ?? this.room,
      options: options ?? this.options,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'text': text,
      'isMultiple': isMultiple,
      'type': type,
      'status': status,
      'createdAt': createdAt,
      // 'creatorTalker': creatorTalker?.toMap(),
      'room': room?.toMap(),
      'options': options.map((x) => x.toMap()).toList(),
    };
  }

  factory Poll.fromMap(Map<String, dynamic> map) {
    return Poll(
      id: map['id']?.toInt() ?? 0,
      text: map['text'] ?? '',
      isMultiple: map['isMultiple'] ?? false,
      type: map['type'] ?? '',
      status: map['status'] ?? '',
      createdAt: map['createdAt'] ?? '',
      // creatorTalker: map['creatorTalker'] != null ? Talker.fromMap(map['creatorTalker']) : null,
      room: map['room'] == null ? null : Room.fromMap(map['room']),
      options: List<PollOption>.from(map['options']?.map((x) => PollOption.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory Poll.fromJson(String source) => Poll.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Poll(id: $id, text: $text, isMultiple: $isMultiple, type: $type, status: $status, createdAt: $createdAt, room: $room, options: $options)';
  }

  @override
  List<Object> get props {
    return [
      id,
      text,
      isMultiple,
      type,
      status,
      createdAt,
      options,
    ];
  }
}
