import 'dart:convert';

import 'package:equatable/equatable.dart';

class PollOption extends Equatable {
  final int id;
  final String text;
  final String pic;
  final bool isRight;
  final int? votesLength;

  const PollOption({
    required this.id,
    required this.text,
    required this.pic,
    required this.isRight,
    required this.votesLength,
  });

  PollOption copyWith({
    int? id,
    String? text,
    String? pic,
    bool? isRight,
    int? votesLength,
  }) {
    return PollOption(
      id: id ?? this.id,
      text: text ?? this.text,
      pic: pic ?? this.pic,
      isRight: isRight ?? this.isRight,
      votesLength: votesLength ?? this.votesLength,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'text': text,
      'pic': pic,
      'isRight': isRight,
      'votesLength': votesLength,
    };
  }

  factory PollOption.fromMap(Map<String, dynamic> map) {
    return PollOption(
      id: map['id']?.toInt() ?? 0,
      text: map['text'] ?? '',
      pic: map['pic'] ?? '',
      isRight: map['isRight'] ?? false,
      votesLength: map['votesLength']?.toInt(),
    );
  }

  String toJson() => json.encode(toMap());

  factory PollOption.fromJson(String source) => PollOption.fromMap(json.decode(source));

  @override
  String toString() {
    return 'PollOption(id: $id, text: $text, pic: $pic, isRight: $isRight, votesLength: $votesLength)';
  }

  @override
  List<Object?> get props => [
        id,
        text,
        pic,
        isRight,
        votesLength,
      ];
}
