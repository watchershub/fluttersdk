import 'dart:convert';

import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_option.dart';

class PollText {
  final Poll poll;
  final List<PollOption> pollOptionsChosen;

  const PollText({
    required this.poll,
    required this.pollOptionsChosen,
  });

  Map<String, dynamic> toMap() {
    return {
      'poll': poll.toMap(),
      'pollOptionsChosen': pollOptionsChosen.map((x) => x.toMap()).toList(),
    };
  }

  factory PollText.fromMap(Map<String, dynamic> map) {
    return PollText(
      poll: Poll.fromMap(map['poll']),
      pollOptionsChosen:
          List<PollOption>.from(map['pollOptionsChosen']?.map((x) => PollOption.fromMap(x))),
    );
  }

  String toJson() => json.encode(toMap());

  factory PollText.fromJson(String source) => PollText.fromMap(json.decode(source));
}
