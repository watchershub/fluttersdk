import 'package:agora_rtc_engine/agora_rtc_engine.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:watchers_widget/src/core/log/i_log.dart';

class AgoraApi {
  late RtcEngine _engine;

  Future<void> joinVoiceChat(
      {required String appId,
      required String agoraToken,
      required int talkerId,
      required String channelId,
      required bool enableLocalAudio}) async {
    _engine = createAgoraRtcEngine();

    await _engine.initialize(RtcEngineContext(
      appId: appId,
      channelProfile: ChannelProfileType.channelProfileLiveBroadcasting,
      audioScenario: AudioScenarioType.audioScenarioChatroom,
      logConfig: const LogConfig(
        level: LogLevel.logLevelNone,
      ),
    ));

    _engine.registerEventHandler(
      RtcEngineEventHandler(
        onError: (error, e) {
          print('error log');
          print(error);
          print(e);
        },
      ),
    );

    await _engine.joinChannel(
      token: agoraToken,
      channelId: channelId,
      uid: talkerId,
      options: const ChannelMediaOptions(
          clientRoleType: ClientRoleType.clientRoleBroadcaster,
          autoSubscribeAudio: true,
          channelProfile: ChannelProfileType.channelProfileLiveBroadcasting),
    );

    PermissionStatus permissionStatus = await Permission.microphone.status;

    if (enableLocalAudio && permissionStatus != PermissionStatus.granted) {
      if (permissionStatus.isPermanentlyDenied) {
        openAppSettings();
      } else {
        permissionStatus = await Permission.microphone.request();
      }
    }
    await _engine.enableLocalAudio(enableLocalAudio && permissionStatus.isGranted);
  }

  Future<void> setLocalAudio({required bool audioState}) async {
    await _engine.enableLocalAudio(audioState);
    return;
  }

  Future<void> leaveVoiceChat() async {
    log.debug('Agora room left');
    await _engine.leaveChannel();
    return;
  }

}
