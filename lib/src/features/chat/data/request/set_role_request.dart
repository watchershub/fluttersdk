import 'dart:convert';

enum TalkerRole { speaker, guest }

extension TalkerRoleX on TalkerRole {
  String get toStr => name.toUpperCase();
}

class SetRoleRequest {
  final int userId;
  final TalkerRole role;

  const SetRoleRequest({
    required this.userId,
    required this.role,
  });

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'role': role.toStr,
    };
  }

  String toJson() => json.encode(toMap());
}
