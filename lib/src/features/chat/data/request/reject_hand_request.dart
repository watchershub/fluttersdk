import 'dart:convert';

class RejectHandRequest {
  final int talkerId;
  RejectHandRequest({
    required this.talkerId,
  });

  Map<String, dynamic> toMap() {
    return {
      'talkerId': talkerId,
    };
  }

  String toJson() => json.encode(toMap());
}
