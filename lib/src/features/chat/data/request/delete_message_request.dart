import 'dart:convert';

class DeleteMessageRequest {
  final String messageId;

  const DeleteMessageRequest({
    required this.messageId,
  });

  Map<String, dynamic> toMap() {
    return {
      'messageId': int.tryParse(messageId),
    };
  }

  String toJson() => json.encode(toMap());
}
