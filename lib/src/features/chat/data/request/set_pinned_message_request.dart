import 'dart:convert';

class SetPinnedMessageRequest {
  final String? messageId;

  const SetPinnedMessageRequest({
    required this.messageId,
  });

  Map<String, dynamic> toMap() {
    return {
      'messageId': messageId,
    };
  }

  String toJson() => json.encode(toMap());
}
