import 'dart:convert';

class SetMessagesVisibleRequest {
  final String userId;
  final bool isVisible;

  const SetMessagesVisibleRequest({
    required this.userId,
    required this.isVisible,
  });

  Map<String, dynamic> toMap() {
    return {
      'userId': int.parse(userId),
      'isVisible': isVisible,
    };
  }

  String toJson() => json.encode(toMap());
}
