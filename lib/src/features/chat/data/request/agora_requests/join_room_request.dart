class JoinRoomRequest {
  final int talkerId;
  final String appId;
  final String agoraToken;
  final String channelId;

  const JoinRoomRequest(
      {required this.talkerId,
      required this.appId,
      required this.agoraToken,
      required this.channelId});
}
