import 'dart:convert';

class SetMuteRequest {
  final int userId;
  final bool isMuted;

  const SetMuteRequest({
    required this.userId,
    required this.isMuted,
  });

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'isMuted': isMuted,
    };
  }

  String toJson() => json.encode(toMap());
}
