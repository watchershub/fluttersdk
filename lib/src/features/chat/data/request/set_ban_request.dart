import 'dart:convert';

class SetBanRequest {
  final int userId;
  final bool isBanned;
  final int? banReason;

  const SetBanRequest({
    required this.userId,
    required this.isBanned,
    this.banReason,
  });

  Map<String, dynamic> toMap() {
    return {
      'userId': userId,
      'isBanned': isBanned,
      if (banReason!=null) 'reason' : banReason,
    };
  }

  String toJson() => json.encode(toMap());
}
