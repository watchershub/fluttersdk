import 'dart:convert';

class SetMessageVisibleRequest {
  final int messageId;
  final bool isVisible;

  const SetMessageVisibleRequest({
    required this.messageId,
    required this.isVisible,
  });

  Map<String, dynamic> toMap() {
    return {
      'messageId': messageId,
      'isVisible': isVisible,
    };
  }

  String toJson() => json.encode(toMap());
}
