import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_text.dart';

import 'send_message_requst_mixin.dart';

enum MessageType {
  sticker,
}

class SendStickerMessageRequest implements SendMessageRequestMixin {
  final int stickerId;

  SendStickerMessageRequest({
    required this.stickerId,
  });

  @override
  Map<String, dynamic> toMap() {
    return {
      'type': 'STICKER',
      'stickerId' : stickerId,
    };
  }

  String toJson() => json.encode(toMap());
}
