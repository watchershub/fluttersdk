import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_text.dart';

import 'send_message_requst_mixin.dart';

enum MessageType {
  user,
  vote,
}

class SendVoteMessageRequest implements SendMessageRequestMixin {
  final MessageType type;
  final PollText pollText;

  SendVoteMessageRequest({
    required this.type,
    required this.pollText,
  });

  @override
  Map<String, dynamic> toMap() {
    return {
      'type': describeEnum(type).toUpperCase(),
      'pollText': pollText.toJson(),
    };
  }

  String toJson() => json.encode(toMap());
}
