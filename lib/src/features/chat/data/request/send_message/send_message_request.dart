import 'dart:convert';

import 'send_message_requst_mixin.dart';

class SendMessageRequest implements SendMessageRequestMixin {
  final String text;
  final int? mentionMessageId;
  final bool? hasPreview;

  SendMessageRequest({
    required this.text,
    this.mentionMessageId,
    this.hasPreview,
  });

  @override
  Map<String, dynamic> toMap() {
    return {
      'text': text,
      if (mentionMessageId != null) 'mentionMessageId': mentionMessageId.toString(),
      if (hasPreview != null) 'hasPreview': hasPreview,
    };
  }

  String toJson() => json.encode(toMap());
}
