import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:dio/dio.dart';
import 'package:isolation/isolation.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:watchers_widget/src/core/constants/constants.dart';
import 'package:watchers_widget/src/core/log/i_log.dart';
import 'package:watchers_widget/src/features/chat/data/request/delete_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/edit_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/reject_hand_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/report_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/send_message/send_message_requst_mixin.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_ban_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_message_visible_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_messages_visible_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_mute_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_pinned_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/domain/models/emotion.dart';

import 'dtos/create_delete_reaction_dto.dart';
import 'dtos/slomode_dto.dart';

typedef JsonMap = Map<String, dynamic>;
// typedef CommandData = Map<String, JsonMap>;

// Future<void> _parser(IsolateController<JsonMap, String> controller) =>
//     controller.stream.forEach((json) {
//       final result = jsonDecode(json) as Object?;
//       (result is JsonMap)
//           ? controller.add(result)
//           : controller.addError(const FormatException('Invalid JSON'));
//     });

// {'command': 'joinRoom', data: {}}
Future<void> _handleSocketEvents(IsolateController<JsonMap, JsonMap> controller) async {
  late Socket _socket;

  controller.stream.listen((event) {
    final String command = event['command'];

    /// {uri, opts, externalRoomId, title, eventHandler}
    final data = event['data'];

    if (command == 'joinRoom') {
      _socket = io(
        data['uri'],
        data['opts'],
      )
        ..onAny((event, data) {
          controller.add({'event': event, 'data': data});
        })
        ..onConnect((_) async {
          _socket.emit('join', {
            'externalRoomId': data['externalRoomId'],
            if (data['title'] != null) 'roomName': data['title'],
          });
        })
        ..connect();
      return;
    }

    /// {}
    if (command == 'close') {
      _socket.close();
      return;
    }

    _socket.emit(command, data);
  });
}

class ChatApi {
  final Dio _dio;

  final Dio _dioPreview = Dio(BaseOptions(
    baseUrl: Constants.previewBaseUrl,
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
  ));

  late String _realExternalRoomId;

  // Другие апи завязанные на чате хотят знать текущий roomId
  String getRealExternalRoomId() => _realExternalRoomId;

  final Connectivity _connectivity;

  final List<StreamSubscription> _subscriptions = [];

  final String _baseUrl;

  final _socketIsolateController = IsolateController<JsonMap, JsonMap>(
    _handleSocketEvents,
    lazy: false,
  );

  IsolateController<JsonMap, JsonMap> get socketIsolateController => _socketIsolateController;

  ChatApi(
    Dio dio,
    String baseUrl,
  )   : _dio = dio,
        _baseUrl = baseUrl,
        _connectivity = Connectivity() {
    _subscriptions.add(_connectivity.onConnectivityChanged.listen(_onConnectivityChanged));
  }

  Future<Response> getRoom() => _dio.get(
        'room/$_realExternalRoomId',
      );

  Future<Response> _checkRoom(String externalRoomId, String name) => _dio.post('/room/real', data: {
        'externalRoomId': externalRoomId,
        'name': name,
      });

  Future<Response> getMessages(Map<String, dynamic> requestParams) =>
      _dio.get('message/room/$_realExternalRoomId', queryParameters: requestParams);

  Future<Response> getPinnedMessage() => _dio.get(
        'message/room/$_realExternalRoomId/pinned',
      );

  Future<Response> getMessageById(String messageId) => _dio.get(
        'message/room/$_realExternalRoomId/$messageId',
      );

  Future<Response> getUserLastMessage(String userId) => _dio.get(
    'message/last/room/$_realExternalRoomId/$userId',
  );

  Future<Response> reportMessage(ReportMessageRequest reportMessageRequest) => _dio.post(
        'report',
        data: reportMessageRequest.toMap(),
      );

  Future<void> joinRoom(
    String? title,
    String externalRoomId,
    String token,
    Function(String, dynamic) eventHandler,
  ) async {
    _realExternalRoomId = title != null
        ? await _checkRoom(externalRoomId, title)
            .then((response) => response.data['externalRoomId'] as String)
        : externalRoomId;

    // _socket = io(
    //     _baseUrl,
    //     OptionBuilder()
    //         .setAuth({'token': token})
    //         .setTransports(['websocket'])
    //         .disableAutoConnect()
    //         .build())
    //   ..onAny(eventHandler)
    //   ..onConnect((_) async {
    //     emitAndLog('join', {
    //       'externalRoomId': _realExternalRoomId,
    //       if (title != null) 'roomName': title,
    //     });
    //   })
    //   ..connect();
    log.debug(
        'Sended >>> event: joinRoom, data: {title: $title, roomId: $_realExternalRoomId, token: $token}');
    _socketIsolateController.add({
      'command': 'joinRoom',
      'data': {
        'uri': _baseUrl,
        'opts': OptionBuilder()
            .setAuth({'token': token})
            .setTransports(['websocket'])
            .disableAutoConnect()
            .build(),
        'externalRoomId': _realExternalRoomId,
        'title': title,
        // 'eventHandler': eventHandler,
      },
    });

    _subscriptions.add(_socketIsolateController.stream.listen((event) {
      eventHandler(event['event'], event['data']);
    }));
  }

  final StreamController<ConnectivityResult> _connectivityResultsStreamConroller =
      StreamController.broadcast();

  Stream<ConnectivityResult> get onConnectivityChanged$ =>
      _connectivityResultsStreamConroller.stream;

  void _onConnectivityChanged(ConnectivityResult connectivityResult) {
    log.debug('Connectivity changed to $connectivityResult');
    _connectivityResultsStreamConroller.add(connectivityResult);
  }

  // Socket leaveRoom() => _socket.disconnect();

  Map<String, dynamic> _addExternalRoomId(Map<String, dynamic> params) => params
    ..addAll({
      'externalRoomId': _realExternalRoomId,
    });

  void sendMessage(SendMessageRequestMixin sendMessageRequest) {
    emitAndLog('message', _addExternalRoomId(sendMessageRequest.toMap()));
  }

  void toggleHand() {
    emitAndLog('toggleHand', _addExternalRoomId({}));
  }

  void rejectHand(RejectHandRequest rejectHandRequest) {
    emitAndLog('rejectHand', _addExternalRoomId(rejectHandRequest.toMap()));
  }

  void setRole(SetRoleRequest setRoleRequest) {
    emitAndLog('setRole', _addExternalRoomId(setRoleRequest.toMap()));
  }

  void setMute(SetMuteRequest setMuteRequest) {
    emitAndLog('setMute', _addExternalRoomId(setMuteRequest.toMap()));
  }

  void editMessage(EditMessageRequest editMessageRequest) {
    emitAndLog('editMessage', _addExternalRoomId(editMessageRequest.toMap()));
  }

  void deleteMessage(DeleteMessageRequest deleteMessageRequest) {
    emitAndLog('deleteMessage', _addExternalRoomId(deleteMessageRequest.toMap()));
  }

  void sendEmotion(Emotion emotion) {
    emitAndLog('emotion', {
      'emotion': emotion.toMap(),
      'externalRoomId': _realExternalRoomId,
    });
  }

  void setMessageVisible(SetMessageVisibleRequest setMessageVisibleRequest) {
    emitAndLog('setMessageVisible', _addExternalRoomId(setMessageVisibleRequest.toMap()));
  }

  void setMessagesVisible(SetMessagesVisibleRequest setMessagesVisibleRequest) {
    emitAndLog('setMessagesVisible', _addExternalRoomId(setMessagesVisibleRequest.toMap()));
  }

  void setBan(SetBanRequest setBanRequest) {
    emitAndLog('setBan', _addExternalRoomId(setBanRequest.toMap()));
  }

  void setPinnedMessage(SetPinnedMessageRequest setPinnedMessageRequest) {
    emitAndLog('pinMessage', _addExternalRoomId(setPinnedMessageRequest.toMap()));
  }

  void emitAndLog(String event, [dynamic data]) {
    log.debug('Sended >>> event: $event, data: $data');
    _socketIsolateController.add({'command': event, 'data': data});
  }

  Future<Response> getPreview({required String url}) => _dioPreview.get("?url=$url");

  void createReaction(ReactionDto reactionDto) {
    emitAndLog('createReaction', _addExternalRoomId(reactionDto.toMap()));
  }

  void deleteReaction(ReactionDto reactionDto) {
    emitAndLog('deleteReaction', _addExternalRoomId(reactionDto.toMap()));
  }

  void setSlowmode(SlowmodeDto slowmodeDto) {
    emitAndLog('setSlowmode', _addExternalRoomId(slowmodeDto.toMap()));
  }

  void close() {
    log.debug('Socket was closed');
    _socketIsolateController.add({'command': 'close'});
    _socketIsolateController.close();

    for (final sub in _subscriptions) {
      sub.cancel();
    }
  }
}
