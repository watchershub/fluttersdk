import 'dart:async';
import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:watchers_widget/src/core/extensions/future_dio_response_x.dart';
import 'package:watchers_widget/src/features/chat/data/chat_api.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/slomode_dto.dart';
import 'package:watchers_widget/src/features/chat/data/request/delete_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/edit_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/reject_hand_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/report_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_ban_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_message_visible_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_messages_visible_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_mute_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/data/sticker_api.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';
import 'package:watchers_widget/src/features/chat/domain/models/emotion.dart';
import 'package:watchers_widget/src/features/chat/domain/models/sticker.dart';
import 'package:watchers_widget/src/features/common/data/shared_preferences_storage/shared_preferences_storage.dart';
import 'package:watchers_widget/src/features/common/models/app_settings.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/models/preview.dart';
import 'package:watchers_widget/src/features/common/models/room.dart';

import 'dtos/create_delete_reaction_dto.dart';
import 'request/send_message/send_message_requst_mixin.dart';
import 'request/set_pinned_message_request.dart';

class ChatRepository implements IChatRepository {
  final SharedPreferencesEntityStorage _entityStorage;
  final ChatApi _chatApi;
  final StickerApi _stickerApi;

  ChatRepository(this._entityStorage, this._chatApi, this._stickerApi);

  @override
  Future<Message?> getMessageById({required String messageId}) async {
    final responseData = await _chatApi.getMessageById(messageId).then((response) => response.data);

    if (responseData != null) {
      return Message.fromMap(responseData);
    }
  }

  @override
  Future<List<Message>> getMessages({
    String? limit,
    String? lastId,
  }) async {
    final messages = await _chatApi.getMessages(
      {
        if (limit != null) 'limit': limit,
        if (lastId != null) 'lastId': lastId,
      },
    ).computeResponseList(Message.fromMap);

    final userMap = jsonDecode(await _entityStorage.get(AppSettings.user));
    final List<Message> filteredMessages = [];

    for (final message in messages) {
      message.isMyMessage = userMap['id'] == message.talker.user.id;
      if (message.poll != null && !message.isMyMessage! && !message.isPollResults) {
        continue;
      }
      filteredMessages.add(message.copyWith(
        showMention: message.mentionMessage != null && message.mentionMessage!.isVisible,
        isVisible: userMap['id'] == message.talker.user.id || message.isVisible,
      ));
    }
    return filteredMessages;
  }

  @override
  Future<Message?> getPinnedMessage() async {
    final responseData = await _chatApi.getPinnedMessage().then((response) => response.data);

    if ((responseData != null) && (responseData.isNotEmpty)) {
      return Message.fromMap(responseData);
    }
  }

  @override
  Future<Room?> getRoom() async {
    final responseData = await _chatApi.getRoom().then((response) => response.data);

    if (responseData != null) {
      return Room.fromMap(responseData);
    }
  }

  @override
  Future<void> reportMessage({required ReportMessageRequest reportMessageRequest}) async {
    await _chatApi.reportMessage(reportMessageRequest);
  }

  @override
  Future<void> joinRoom({
    String? title,
    required String externalRoomId,
    required Function(String, dynamic) eventHandler,
  }) async {
    final userMap = jsonDecode(await _entityStorage.get(AppSettings.user));
    _chatApi.joinRoom(title, externalRoomId, userMap['token'], eventHandler);
  }

  @override
  void sendMessage({required SendMessageRequestMixin sendMessageRequest}) {
    _chatApi.sendMessage(sendMessageRequest);
  }

  @override
  void toggleHand() {
    _chatApi.toggleHand();
  }

  @override
  Future<Message> checkMessage({
    required Message message,
  }) async {
    final userMap = jsonDecode(await _entityStorage.get(AppSettings.user));
    message.isMyMessage = message.creatorId == userMap['id'];
    message.isMentionMe =
        message.mentionMessage != null && message.mentionMessage!.creatorId == userMap['id'];
    return message;
  }

  final _myEmotionsStream = StreamController<Emotion>.broadcast();
  @override
  Stream<Emotion> get $myEmotions => _myEmotionsStream.stream;

  @override
  void sendEmotion({
    required Emotion emotion,
  }) {
    _myEmotionsStream.add(emotion);
    _chatApi.sendEmotion(emotion);
  }

  @override
  void setMessageVisible({required SetMessageVisibleRequest setMessageVisibleRequest}) {
    _chatApi.setMessageVisible(setMessageVisibleRequest);
  }

  @override
  void setMessagesVisible({required SetMessagesVisibleRequest setMessagesVisibleRequest}) {
    _chatApi.setMessagesVisible(setMessagesVisibleRequest);
  }

  @override
  void setBan({required SetBanRequest setBanRequest}) {
    _chatApi.setBan(setBanRequest);
  }

  @override
  void editMessage({required EditMessageRequest editMessageRequest}) {
    _chatApi.editMessage(editMessageRequest);
  }

  @override
  void deleteMessage({required DeleteMessageRequest deleteMessageRequest}) {
    _chatApi.deleteMessage(deleteMessageRequest);
  }

  @override
  void setPinnedMessage({required SetPinnedMessageRequest setPinnedMessageRequest}) {
    return _chatApi.setPinnedMessage(setPinnedMessageRequest);
  }

  @override
  void close() {
    _myEmotionsStream.close();
    _chatApi.close();
  }

  @override
  Stream<ConnectivityResult> get onConnectivityChanged$ => _chatApi.onConnectivityChanged$;

  @override
  Future<Preview> getPreview({required String url}) async {
    final responseData = await _chatApi.getPreview(url: url).then((response) => response.data);

    return Preview.fromMap(responseData['metadata']);
  }

  @override
  void rejectHand(RejectHandRequest rejectHandRequest) {
    _chatApi.rejectHand(rejectHandRequest);
  }

  @override
  void setRole(SetRoleRequest setRoleRequest) {
    _chatApi.setRole(setRoleRequest);
  }

  @override
  void setMute(SetMuteRequest setMuteRequest) {
    _chatApi.setMute(setMuteRequest);
  }

  @override
  Future<List<Sticker>> getStickers() async {
    final stickers = await _stickerApi.getStickers().computeResponseList(Sticker.fromMap);

    return stickers;
  }

  @override
  void createReaction({required ReactionDto reactionDto}) {
    _chatApi.createReaction(reactionDto);
  }

  @override
  void deleteReaction({required ReactionDto reactionDto}) {
    _chatApi.deleteReaction(reactionDto);
  }

  @override
  void setSlowmode({required SlowmodeDto slowmodeDto}) {
    _chatApi.setSlowmode(slowmodeDto);
  }

  @override
  Future<Message?> getUserLastMessage({
    required String userId,
  }) async {
    final responseData = await _chatApi.getUserLastMessage(userId).then((response) => response.data);

    if (responseData != null) {
      return Message.fromMap(responseData);
    }
  }
}
