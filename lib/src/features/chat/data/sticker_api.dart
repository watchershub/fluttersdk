import 'package:dio/dio.dart';

class StickerApi {
  final Dio _dio;

  const StickerApi(
      this._dio,
      );

  Future<Response> getStickers() => _dio.get(
    'sticker',
  );

}
