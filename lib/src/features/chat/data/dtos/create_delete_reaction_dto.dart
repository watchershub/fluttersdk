import 'dart:convert';

class ReactionDto {
  final String? emotion;
  final String? externalRoomId;
  final String? messageId;

  ReactionDto({
    this.emotion,
    this.externalRoomId,
    this.messageId
  });

  Map<String, dynamic> toMap() {
    return {
      if (emotion != null) 'emotion': emotion,
      if (externalRoomId != null) 'externalRoomId': externalRoomId,
      if (messageId != null) 'messageId': int.parse(messageId!),
    };
  }

  String toJson() => json.encode(toMap());
}