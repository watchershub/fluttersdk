import 'dart:convert';

class SlowmodeDto {
  final bool isSlowmode;
  final int slowmodeDelayMS;

  SlowmodeDto({
    required this.isSlowmode,
    required this.slowmodeDelayMS,
  });

  Map<String, dynamic> toMap() {
    return {
      'isSlowmode' : isSlowmode,
      'slowmodeDelayMS' : slowmodeDelayMS,
    };
  }

  String toJson() => json.encode(toMap());
}