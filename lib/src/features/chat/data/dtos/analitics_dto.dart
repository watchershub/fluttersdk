import 'dart:convert';

class AnaliticsDto {
  final String? user_id;
  final String? event_id;
  final String? user_role;
  final String? user_type;

  AnaliticsDto({
    this.user_id,
    this.event_id,
    this.user_role,
    this.user_type,
  });

  Map<String, dynamic> toMap() {
    return {
      if (user_id != null) 'user_id': user_id,
      if (event_id != null) 'event_id': event_id,
      if (user_role != null) 'user_role': user_role,
      if (user_type != null) 'user_type': user_type,
    };
  }

  String toJson() => json.encode(toMap());
}
