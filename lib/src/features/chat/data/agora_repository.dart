import 'package:watchers_widget/src/features/chat/domain/i_agora_repository.dart';

import 'agora_api.dart';

class AgoraRepository implements IAgoraRepository {
  final AgoraApi _agoraApi;

  const AgoraRepository(this._agoraApi);

  @override
  Future<void> joinRoom(
      {required String appId,
      required String agoraToken,
      required int talkerId,
      required String channelId,
      required bool enableLocalAudio}) async {
    await _agoraApi.joinVoiceChat(
        appId: appId,
        agoraToken: agoraToken,
        talkerId: talkerId,
        channelId: channelId,
        enableLocalAudio: enableLocalAudio);
  }

  @override
  Future<void> setLocalAudio({required bool audioState}) async {
    await _agoraApi.setLocalAudio(audioState: audioState);
    return;
  }

  @override
  Future<void> leaveRoom() async {
    await _agoraApi.leaveVoiceChat();
    return;
  }
}
