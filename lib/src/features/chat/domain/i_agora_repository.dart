abstract class IAgoraRepository {
  Future<void> joinRoom(
      {required String appId,
      required String agoraToken,
      required int talkerId,
      required String channelId,
      required bool enableLocalAudio});

  Future<void> setLocalAudio({required bool audioState});

  Future<void> leaveRoom();
}
