import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';

class PinnedMessageView extends StatelessWidget {
  final void Function() onMessageTap;
  final void Function() onButtonTap;
  final Message pinnedMessage;
  final Talker talker;

  const PinnedMessageView({
    required this.talker,
    required this.pinnedMessage,
    required this.onMessageTap,
    required this.onButtonTap,
  });

  @override
  Widget build(BuildContext context) {
    final messageText = pinnedMessage.advertisement == null
        ? pinnedMessage.text
        : pinnedMessage.advertisement!.title != null
            ? pinnedMessage.advertisement!.title!
            : pinnedMessage.advertisement!.text ?? '';

    final messageTextWithoutLineBreaks = messageText.replaceAll('\n', ' ');

    return Container(
      constraints: const BoxConstraints(maxHeight: 58),
      decoration: const BoxDecoration(
        color: CustomColors.modalBackground,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(
                child: InkWell(
                  onTap: onMessageTap,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: 14.fw, vertical: 8),
                    child: Row(
                      children: [
                        const SvgIcon(
                          Resources.pin,
                          color: CustomColors.gray400,
                        ),
                        SizedBox(width: 12.fw),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                'Закрепленное сообщение',
                                style: TextStyles.title(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w400,
                                ),
                              ),
                              SizedBox(
                                height: 4.fh,
                              ),
                              Row(
                                children: [
                                  Flexible(
                                    child: Text(
                                      messageTextWithoutLineBreaks,
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyles.secondary(
                                        fontSize: 12,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              if (talker.role == "ADMIN" || talker.isModer)
                InkWell(
                  onTap: onButtonTap,
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 14, vertical: 8),
                    child: SvgIcon(
                      Resources.close,
                      color: CustomColors.textMain,
                      size: 14,
                    ),
                  ),
                ),
            ],
          ),
          const Divider(indent: 0, thickness: 1, height: 1, color: CustomColors.inputBorder),
        ],
      ),
    );
  }
}
