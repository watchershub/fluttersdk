import 'dart:math';

import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class NewMessagesFlag extends StatefulWidget {
  NewMessagesFlag({
    required this.messagesCount,
    required this.onCommonUnreadTap,
    required this.onMentionUnreadTap,
    required this.mentionUnreadMessagesCount,
    required this.additionalBottomPadding,
    required this.onArrowDownTap,
  });

  final double additionalBottomPadding;
  final int mentionUnreadMessagesCount;
  final int messagesCount;
  final void Function() onCommonUnreadTap;
  final void Function() onMentionUnreadTap;
  final void Function(int tapCount) onArrowDownTap;

  @override
  _NewMessagesFlagState createState() => _NewMessagesFlagState();
}

int tapCount = 0;

class _NewMessagesFlagState extends State<NewMessagesFlag> {
  late int tapCount;

  @override
  void initState() {
    super.initState();
    tapCount = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
        right: 16,
        bottom: widget.additionalBottomPadding + 8,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            // if (mentionUnreadMessagesCount != 0)
            //   InkWell(
            //     splashColor: Colors.transparent,
            //     onTap: onMentionUnreadTap,
            //     child: buildUnreadForMention(),
            //   ),
            // if ((mentionUnreadMessagesCount != 0) &&
            //     ((messagesCount - mentionUnreadMessagesCount) > 0))
            //   const SizedBox(
            //     height: 6,
            //   ),
            buildCommonUnread(),
          ],
        ));
  }

  Widget buildUnreadForMention() {
    return InkWell(
        splashColor: Colors.transparent,
        onTap: widget.onMentionUnreadTap,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
                height: 24,
                width: 24,
                child: Container(
                  decoration: const BoxDecoration(
                    shape: BoxShape.circle,
                    color: CustomColors.gray200,
                    boxShadow: [
                      BoxShadow(
                        color: CustomColors.mentionShadowColor,
                        offset: Offset(0, 4),
                        blurRadius: 8,
                      ),
                    ],
                  ),
                  child: const Center(
                      child: SvgIcon(
                    Resources.email,
                    size: 16,
                    color: CustomColors.onPrimary,
                  )),
                )),
            const SizedBox(
              width: 6,
            ),
            CounterWidget(count: widget.messagesCount.toString()),
          ],
        ));
  }

  Widget buildCommonUnread() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        if (widget.mentionUnreadMessagesCount > 0)
          buildUnreadForMention()
        else
          InkWell(
            splashColor: Colors.transparent,
            onTap: widget.onCommonUnreadTap,
            child: CounterWidget(
                count: (widget.messagesCount - widget.mentionUnreadMessagesCount).toString()),
          ),
        SizedBox(
          height: 6.fh,
        ),
        InkWell(
            splashColor: Colors.transparent,
            onTap: () {
              widget.onArrowDownTap(tapCount);
              tapCount += 1;
            },
            child: Container(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: CustomColors.onPrimary,
                boxShadow: [
                  BoxShadow(
                    color: CustomColors.arrowShadowColor,
                    offset: Offset(0, 0),
                    blurRadius: 2,
                  ),
                ],
              ),
              child: Transform.rotate(
                angle: pi,
                child: const SvgIcon(
                  Resources.send,
                  size: 32,
                  color: CustomColors.primary,
                ),
              ),
            )),
      ],
    );
  }
}

class CounterWidget extends StatelessWidget {
  const CounterWidget({
    required this.count,
  });

  final String count;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 32,
      width: 32,
      child: Container(
        decoration: const BoxDecoration(
          shape: BoxShape.circle,
          color: CustomColors.primary,
          boxShadow: [
            BoxShadow(
              color: CustomColors.counterShadowColor,
              offset: Offset(0, 4),
              blurRadius: 8,
            ),
          ],
        ),
        child: Center(
          child: Text(
            count,
            style: TextStyles.subtitle1(color: CustomColors.onPrimary),
          ),
        ),
      ),
    );
  }
}
