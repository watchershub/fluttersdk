import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/chat_toast_widget.dart';

@Deprecated('Скорее всего больне не будет использоваться по дизайну')
class LargeMessageToast extends StatefulWidget {
  final TextEditingController inputController;
  final void Function()? onClose;
  final void Function(String toastText)? setToastTextHeight;

  const LargeMessageToast({
    required this.inputController,
    this.onClose,
    this.setToastTextHeight,
  });

  @override
  _LargeMessageWidgetState createState() => _LargeMessageWidgetState();
}

class _LargeMessageWidgetState extends State<LargeMessageToast> {
  late int textLength;

  @override
  void initState() {
    textLength = widget.inputController.text.length;
    widget.inputController.addListener(_updateTextLength);
    super.initState();
  }

  @override
  void dispose() {
    widget.inputController.removeListener(_updateTextLength);
    super.dispose();
  }

  void _updateTextLength() {
    setState(() {
      textLength = widget.inputController.text.length;
    });
  }

  @override
  Widget build(BuildContext context) {
    final toastText = "Вы можете отправлять сообщения до 500 знаков. $textLength/500";
    widget.setToastTextHeight?.call(toastText);

    return ChatToastWidget(
      iconPath: Resources.disabled,
      text: toastText,
    );
  }
}
