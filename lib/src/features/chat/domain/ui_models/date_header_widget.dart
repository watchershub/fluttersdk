import 'package:flutter/cupertino.dart';
import 'package:watchers_widget/src/app/di/locator.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';

class DateHeaderWidget extends StatelessWidget {
  final DateTime date;

  const DateHeaderWidget({required this.date});

  String buildFormattedDate({required DateTime date}) {
    final DateTime today = DateTime.now().toLocal();
    if ((date.day == today.day) && (date.month == today.month) && (date.year == today.year)) {
      return 'Сегодня';
    }
    if ((date.day == (today.day - 1)) && (date.month == today.month) && (date.year == today.year)) {
      return 'Вчера';
    }
    if (date.year == today.year) {
      return buildDateFormat('dd MMMM').format(date);
    }
    return buildDateFormat('dd MMMM yyyy').format(date);
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 20),
        child: Center(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 8),
            decoration: BoxDecoration(
              color: CustomColors.gray600,
              border: Border.all(color: CustomColors.inputBorder),
              borderRadius: BorderRadius.circular(74),
            ),
            child: Text(
              buildFormattedDate(date: date.toLocal()),
              style: TextStyles.onPrimary(
                fontSize: 11,
                color: CustomColors.dateTextColor,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
