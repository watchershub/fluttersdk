import 'package:flutter/cupertino.dart';
import 'package:watchers_widget/src/core/base/dependent_stateless_widget.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message/loader_for_messages.dart';
import 'package:watchers_widget/src/features/common/models/preview.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class PreviewWidget extends BlocDependentStatelessWidget<ChatBloc, ChatEvent, ChatState> {
  final String url;

  const PreviewWidget({required this.url});

  @override
  Widget builder(BuildContext context, ChatState state, ChatBloc bloc) {
    final loadedState = bloc.state as ChatStateLoaded;

    if (loadedState.previewCache[url] != null) {
      return _buildPreviewContent(loadedState.previewCache[url]!);
    }
    return FutureBuilder(
        future: bloc.getPreview(url: url),
        builder: (context, AsyncSnapshot<Preview?> snapshot) {
          if (snapshot.hasData &&
              snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            final preview = snapshot.data!;
            if (loadedState.previewCache[url] == null) {
              bloc.addPreviewToCache(ChatEventAddPreviewToCache(preview: preview, url: url));
            }
            return _buildPreviewContent(snapshot.data!);
          }
          return const LoaderForMessages();
        });
  }

  Widget _buildPreviewContent(Preview preview) {
    return Container(
      margin: const EdgeInsets.only(top: 6),
      padding: const EdgeInsets.only(left: 5),
      decoration: const BoxDecoration(
          border: Border(left: BorderSide(width: 2, color: CustomColors.previewWidgetBorderColor))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            preview.hostName,
            style: TextStyles.primary.copyWith(fontWeight: FontWeight.w700),
          ),
          if (preview.description.isNotEmpty)
            Padding(
                padding: const EdgeInsets.only(top: 4),
                child: Text(
                  preview.description,
                  style: TextStyles.primary,
                )),
          if (preview.image != null)
            Padding(
                padding: const EdgeInsets.only(top: 5),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: UniversalPicture(preview.image!, null, null, BoxFit.fill)))
        ],
      ),
    );
  }
}
