import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/packages/linkify/flutter_linkify.dart';
import 'package:watchers_widget/src/core/packages/linkify/src/linkify.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/core/utils/text_utils.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/preview_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/mension_message_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message/poll_results_message.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message/reactions_row.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message_popup_menu.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/sticker_view.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/talker_badges_widget.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll.dart';

class MessageView extends StatelessWidget {
  final Message message;
  final bool isModer;
  final void Function() onMentionTap;
  final bool showAvatar;
  final bool showNickName;
  final Poll? pollForPollResults;

  const MessageView({
    required this.message,
    required this.isModer,
    required this.onMentionTap,
    required this.pollForPollResults,
    this.showAvatar = true,
    this.showNickName = true,
  });

  @override
  Widget build(BuildContext context) {
    final isMyMessage = message.isMyMessage ?? false;

    if (!message.isVisible && !isModer) return const SizedBox.shrink();

    return Opacity(
      opacity: message.isVisible ? 1.0 : 0.5,
      child: Container(
          margin: const EdgeInsets.all(4),
          child: Align(
              alignment: isMyMessage ? Alignment.topRight : Alignment.topLeft,
              child: Row(
                mainAxisAlignment: isMyMessage ? MainAxisAlignment.end : MainAxisAlignment.start,
                children: [
                  Flexible(
                    child: Column(
                      crossAxisAlignment:
                          isMyMessage ? CrossAxisAlignment.end : CrossAxisAlignment.start,
                      children: [
                        if (message.text.isNotEmpty || pollForPollResults != null || message.sticker != null)
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              if (isMyMessage)
                                const SizedBox.shrink()
                              else if (showAvatar)
                                _buildAvatar(message)
                              else
                                const ClipOval(
                                  child: CircleAvatar(
                                    backgroundColor: Colors.transparent,
                                    radius: 20,
                                  ),
                                ),
                              const SizedBox(width: 4),
                              Flexible(
                                child: _Bubble(
                                  showNickName: showNickName,
                                  isModer: isModer,
                                  isSentByUser: isMyMessage,
                                  message: message,
                                  onMentionTap: onMentionTap,
                                  pollForPollResults: pollForPollResults,
                                ),
                              ),
                            ],
                          )
                        else
                          const SizedBox.shrink(),
                      ],
                    ),
                  ),
                ],
              ))),
    );
  }

  Widget _buildAvatar(Message message) {
    final child = ClipOval(
      child: CircleAvatar(
        radius: 20,
        child: UniversalPicture(
          message.talker.user.pic,
        ),
      ),
    );

    if (!message.talker.user.isDeleted &&
        !(message.talker.isModer || message.talker.role == "ADMIN")) {
      return child;
      // return AvatarPopupMenu(
      //   message: message,
      //   isModer: isModer,
      //   child: child,
      // );
    }

    return child;
  }
}

class _Bubble extends StatelessWidget {
  final Message message;
  final bool isSentByUser;
  final bool isModer;
  final void Function() onMentionTap;
  final bool showNickName;
  final Poll? pollForPollResults;

  const _Bubble({
    required this.showNickName,
    required this.isSentByUser,
    required this.message,
    required this.isModer,
    required this.onMentionTap,
    required this.pollForPollResults,
  });

  @override
  Widget build(BuildContext context) {
    final child = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        Flexible(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 12.fw, vertical: 8),
            constraints: BoxConstraints(maxWidth: 310.fw),
            decoration: BoxDecoration(
              color: isSentByUser
                  ? pollForPollResults != null
                      ? CustomColors.pollResultsBackgroundMyMessage
                      :message.sticker!=null?Colors.transparent:CustomColors.myMessageColor
                  : message.sticker!=null?Colors.transparent:CustomColors.modalBackground,
              borderRadius: BorderRadius.circular(12),
            ),
            child: pollForPollResults != null
                ? _buildPollResults()
                : message.sticker != null
                ? _buildSticker():_buildMessageContent(isSentByUser, showNickName),
          ),
        ),
        if (!message.isVisible) ...[
          const SizedBox(width: 8),
          const SvgIcon(Resources.disabled),
        ],
      ],
    );
    return message.talker.user.isDeleted
        ? child
        : MessagePopupMenu(
            message: message,
            isSentByUser: isSentByUser,
            isModer: isModer,
            child: child,
          );
  }

  Widget _buildPollResults() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildTitle(),
        PollResultsMessage(
          poll: pollForPollResults!,
          isMyMessage: isSentByUser,
        ),
      ],
    );
  }

  Widget _buildSticker() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        _buildTitle(),
        StickerView(
          sticker: message.sticker!,
        ),
      ],
    );
  }

  Widget _buildTitle() => !isSentByUser && showNickName
      ? Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
              child: Text(
                message.talker.user.name,
                style: TextStyle(
                  color: CustomColors.getUserColor(message.talker.user.color),
                  fontFamily: '.SF UI Display',
                  fontWeight: FontWeight.w400,
                  fontSize: 15,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
            ),
            TalkerBadgesWidget(talker: message.talker),
          ],
        )
      : const SizedBox();

  Widget _buildMessageContent(bool isSentByUser, bool showNickName) {
    final bool shouldInlineTime = message.hasPreview == false &&
        message.updatedAt == null &&
        message.mentionMessage == null &&
        message.reactions == null &&
        TextUtils.canFitText(
          text: message.text + message.createdAt,
          textStyle: TextStyles.primary,
          width: 310.fw - 25,
        );

    final TextPainter textPainter = TextPainter(
      text: TextSpan(
        text: message.text,
        style: TextStyles.primary,
      ),
      textDirection: TextDirection.ltr,
      textWidthBasis: TextWidthBasis.longestLine,
    )..layout(maxWidth: 310.fw - 35);

    final dateText =
        message.updatedAt != null ? 'Изменено ${message.updatedAt}' : message.createdAt;
    final messageColumn = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        // Title
        _buildTitle(),

        // Mension message
        if ((message.mentionMessage != null) && (message.showMention)) ...[
          MentionMessageWidget(
            mentionMessage: message.mentionMessage!,
            onTap: onMentionTap,
          ),
          const SizedBox(height: 2),
        ],

        // Message text
        Row(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Container(
              constraints: BoxConstraints(maxWidth: textPainter.width),
              child: Linkify(
                options: const LinkifyOptions(
                  humanize: false,
                ),
                textWidthBasis: TextWidthBasis.longestLine,
                onOpen: message.talker.isModerOrAdmin
                    ? (link) async {
                        if (!await launchUrl(
                          Uri.parse(link.url.contains('http') ? link.url : 'https://${link.url}'),
                          mode: LaunchMode.externalApplication,
                        )) {
                          throw 'Could not launch ${link.url}';
                        }
                      }
                    : (_) {},
                text: message.text,
                style: TextStyles.primary,
                linkStyle: message.talker.isModerOrAdmin ? null : TextStyles.primary,
              ),
            ),
            SizedBox(width: 8.fw),
            if (shouldInlineTime)
              Text(
                dateText,
                style: TextStyles.subtitle2,
                maxLines: 1,
                overflow: TextOverflow.visible,
              ),
          ],
        ),

        if (message.hasPreview && message.talker.isModerOrAdmin)
          Padding(
              padding: const EdgeInsets.only(top: 6),
              child: PreviewWidget(
                  url: linkify(message.text)
                      .firstWhere((element) => element is LinkableElement)
                      .text)),

        const SizedBox(height: 3),

        if (message.reactions != null)
          ...[
            const SizedBox(
              height: 6,
            ),
            ReactionsRow(reactions: message.reactions!,),
          ],

        // Bottom
        if (!shouldInlineTime)
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Text(
                dateText,
                style: TextStyles.subtitle2,
                textAlign: TextAlign.end,
              ),
            ],
          ),
      ],
    );

    return shouldInlineTime ? messageColumn : IntrinsicWidth(child: messageColumn);
  }
}
