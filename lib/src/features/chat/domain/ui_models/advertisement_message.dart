import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message_popup_menu.dart';
import 'package:watchers_widget/src/features/common/models/advertisement.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class AdvertisementMessage extends StatelessWidget {
  final Message message;
  final bool isModer;
  final bool extendUpperPadding;
  final bool extendLowerPadding;

  const AdvertisementMessage({
    required this.message,
    required this.isModer,
    required this.extendLowerPadding,
    required this.extendUpperPadding,
  });

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.centerLeft,
        child: MessagePopupMenu(
          message: message,
          isSentByUser: false,
          isModer: isModer,
          isOnAdv: true,
          child: _buildAdvContainer(),
        ));
  }

  Widget _buildAdvContainer() {
    return Container(
        constraints: BoxConstraints(
          maxWidth: 310.fw,
        ),
        decoration: BoxDecoration(
          color: CustomColors.modalBackground,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: EdgeInsets.only(
            top: extendUpperPadding ? 16 : 10,
            bottom: extendLowerPadding ? 16 : 10,
            right: 4,
            left: 48),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
                child: _Constrainer(
                    advType: message.advertisement!.template,
                    child: _AdvertisementContent(advertisement: message.advertisement!))),
          ],
        ));
  }
}

class _AdvertisementContent extends StatelessWidget {
  final Advertisement advertisement;

  const _AdvertisementContent({required this.advertisement});

  @override
  Widget build(BuildContext context) {
    switch (advertisement.template) {
      case 0:
        return Stack(alignment: Alignment.bottomCenter, clipBehavior: Clip.none, children: [
          if (advertisement.pic != null)
            ClipRRect(
                borderRadius: BorderRadius.circular(12.0),
                child: UniversalPicture(advertisement.pic!, null, null, BoxFit.fitWidth)),
          Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                if (advertisement.title != null)
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          constraints: BoxConstraints(maxWidth: 310.fw - 32),
                          child: Padding(
                              padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                              child: Text(
                                advertisement.title!,
                                style: TextStyles.highlightText(
                                    color: Colors.white, fontsize: 17, fontWeight: FontWeight.w600),
                                maxLines: 2,
                                softWrap: true,
                                textAlign: TextAlign.start,
                              ))),
                    ],
                  ),
                if (advertisement.text != null) _buildAdvText(advertisement.text!),
                if (advertisement.link != null && advertisement.link!.isNotEmpty)
                  _buildLinkButton(link: advertisement.link!, linkText: advertisement.linkText!)
              ])
        ]);
      case 1:
        return Stack(alignment: Alignment.bottomCenter, clipBehavior: Clip.none, children: [
          if (advertisement.pic != null)
            ClipRRect(
                borderRadius: BorderRadius.circular(12.0),
                child: UniversalPicture(advertisement.pic!, null, null, BoxFit.fitWidth)),
          Container(
              decoration: BoxDecoration(
                color: CustomColors.highlightMessageBackground,
                borderRadius: BorderRadius.circular(12),
              ),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    if (advertisement.title != null)
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                              constraints: BoxConstraints(maxWidth: 310.fw - 32),
                              child: Padding(
                                  padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                                  child: Text(
                                    advertisement.title!,
                                    style: TextStyles.highlightText(
                                        color: Colors.white,
                                        fontsize: 17,
                                        fontWeight: FontWeight.w600),
                                    maxLines: 2,
                                    softWrap: true,
                                    textAlign: TextAlign.start,
                                  ))),
                        ],
                      ),
                    if (advertisement.text != null) _buildAdvText(advertisement.text!),
                    if (advertisement.link != null && advertisement.link!.isNotEmpty)
                      _buildLinkButton(link: advertisement.link!, linkText: advertisement.linkText!)
                  ]))
        ]);
      case 2:
        return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (advertisement.pic != null)
                Padding(
                    padding: const EdgeInsets.only(left: 10, top: 10, right: 10, bottom: 5),
                    child: ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: UniversalPicture(
                          advertisement.pic!,
                        ))),
              if (advertisement.title != null)
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                        constraints: BoxConstraints(maxWidth: 310.fw - 32),
                        child: Padding(
                            padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                            child: Text(
                              advertisement.title!,
                              style: TextStyles.highlightText(
                                  color: Colors.black, fontsize: 13, fontWeight: FontWeight.w600),
                              maxLines: 2,
                              softWrap: true,
                              textAlign: TextAlign.start,
                            ))),
                  ],
                ),
              if (advertisement.text != null) _buildAdvText(advertisement.text!, Colors.black),
              if (advertisement.link != null && advertisement.link!.isNotEmpty)
                _buildLinkButton(link: advertisement.link!, linkText: advertisement.linkText!)
            ]);
      case 3:
        //Звезда внутри яйца(пиздец)
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child: UniversalPicture(
                        Resources.advertisements[0]['picture']!, 400, null, BoxFit.fitWidth)),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (advertisement.title != null)
                      Container(
                          constraints: BoxConstraints(maxWidth: 310.fw - 124),
                          child: Padding(
                              padding: const EdgeInsets.only(right: 8, left: 16),
                              child: Text(
                                advertisement.title!,
                                maxLines: 2,
                                softWrap: true,
                                style: TextStyles.highlightHeader(color: Colors.white),
                              ))),
                    const Spacer(),
                    UniversalPicture(Resources.advertisements[0]['icon']!)
                  ],
                )
              ],
            ),
            if (advertisement.text != null) _buildAdvText(advertisement.text!, Colors.black),
            if (advertisement.link != null && advertisement.link!.isNotEmpty)
              _buildLinkButton(link: advertisement.link!, linkText: advertisement.linkText!)
          ],
        );
      case 4:
        //Раскраска на темном фоне
        return Stack(alignment: Alignment.bottomCenter, children: [
          Positioned.fill(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: UniversalPicture(
                      Resources.advertisements[1]['picture']!, null, null, BoxFit.fitWidth))),
          Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (advertisement.title != null)
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                          constraints: BoxConstraints(maxWidth: 310.fw - 32),
                          child: Padding(
                              padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                              child: Text(
                                advertisement.title!,
                                style: TextStyles.highlightText(color: Colors.white, fontsize: 34),
                                maxLines: 2,
                                softWrap: true,
                                textAlign: TextAlign.start,
                              ))),
                    ],
                  ),
                if (advertisement.text != null) _buildAdvText(advertisement.text!),
                if (advertisement.link != null && advertisement.link!.isNotEmpty)
                  _buildLinkButton(link: advertisement.link!, linkText: advertisement.linkText!)
              ])
        ]);
      case 5:
        //Диагональный текст(концептуально)
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (advertisement.title != null)
              Stack(
                alignment: Alignment.center,
                clipBehavior: Clip.none,
                children: [
                  UniversalPicture(
                      Resources.advertisements[2]['picture']!, 500, null, BoxFit.fitHeight),
                  Positioned(
                      left: 20,
                      top: 22,
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Transform.rotate(
                              alignment: Alignment.centerLeft,
                              angle: -math.pi / 40.0,
                              child: Container(
                                  constraints: BoxConstraints(maxWidth: 310.fw - 132),
                                  child: Padding(
                                      padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                                      child: Text(
                                        advertisement.title!,
                                        style: TextStyles.highlightHeader(
                                            color: Colors.black, fontsize: 20),
                                        maxLines: 2,
                                        softWrap: true,
                                        textAlign: TextAlign.start,
                                      )))),
                        ],
                      )),
                  Positioned(
                    right: 0,
                    top: 10,
                    child: UniversalPicture(Resources.advertisements[2]['icon']!),
                  ),
                ],
              ),
            if (advertisement.text != null) _buildAdvText(advertisement.text!, Colors.black),
            if (advertisement.link != null && advertisement.link!.isNotEmpty)
              _buildLinkButton(link: advertisement.link!, linkText: advertisement.linkText!)
          ],
        );
      case 6:
        //Рупор
        return Stack(alignment: Alignment.center, children: [
          Positioned.fill(
              child: ClipRRect(
                  borderRadius: BorderRadius.circular(12.0),
                  child: UniversalPicture(
                      Resources.advertisements[3]['picture']!, null, null, BoxFit.fitWidth))),
          Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment:
                      (advertisement.title != null && advertisement.title!.isNotEmpty)
                          ? MainAxisAlignment.start
                          : MainAxisAlignment.center,
                  children: [
                    if (advertisement.title != null && advertisement.title!.isNotEmpty)
                      Container(
                          constraints: BoxConstraints(
                            maxWidth: 310.fw - 32 - 96,
                            maxHeight: 101,
                          ),
                          child: Padding(
                              padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    advertisement.title!,
                                    style:
                                        TextStyles.highlightText(color: Colors.white, fontsize: 34),
                                    maxLines: 2,
                                    softWrap: true,
                                    textAlign: TextAlign.start,
                                  ),
                                  const SizedBox(
                                    height: 4,
                                  ),
                                  Container(
                                    height: 2,
                                    width: 106,
                                    decoration: BoxDecoration(
                                      color: CustomColors.underscoreColor,
                                      borderRadius: BorderRadius.circular(2.0),
                                    ),
                                  )
                                ],
                              ))),
                    Padding(
                        padding: (advertisement.title != null && advertisement.title!.isNotEmpty)
                            ? EdgeInsets.zero
                            : const EdgeInsets.symmetric(horizontal: 100),
                        child: SizedBox(
                          height: 90,
                          width: 114,
                          child: UniversalPicture(
                            Resources.advertisements[3]['icon']!,
                            null,
                            null,
                          ),
                        )),
                  ],
                ),
                if (advertisement.text != null) _buildAdvText(advertisement.text!),
                if (advertisement.link != null && advertisement.link!.isNotEmpty)
                  _buildLinkButton(link: advertisement.link!, linkText: advertisement.linkText!)
              ])
        ]);
      case 7:
        //Airdrop
        return Stack(
          children: [
            Padding(
                padding: const EdgeInsets.only(right: 87),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (advertisement.title != null)
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                                constraints: BoxConstraints(maxWidth: 310.fw - 32 - 87),
                                child: Padding(
                                    padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                                    child: Text(
                                      advertisement.title!,
                                      style: TextStyles.highlightText(
                                          color: Colors.black, fontsize: 34),
                                      maxLines: 2,
                                      softWrap: true,
                                      textAlign: TextAlign.start,
                                    ))),
                          ],
                        ),
                      if (advertisement.text != null)
                        _buildAdvText(advertisement.text!, Colors.black),
                      if (advertisement.link != null && advertisement.link!.isNotEmpty)
                        Center(
                            child: _buildLinkButton(
                                link: advertisement.link!, linkText: advertisement.linkText!))
                    ])),
            Positioned(
              right: 0,
              child: UniversalPicture(Resources.advertisements[4]['icon']!),
            ),
          ],
        );
      case 8:
        //Golden toys

        return Stack(
          children: [
            Positioned(left: 0, child: UniversalPicture(Resources.advertisements[5]['icon_left']!)),
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 40),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      if (advertisement.title != null)
                        Container(
                            constraints: BoxConstraints(maxWidth: 310.fw - 32),
                            child: Padding(
                                padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                                child: Text(
                                  advertisement.title!,
                                  style:
                                      TextStyles.highlightText(color: Colors.black, fontsize: 34),
                                  maxLines: 2,
                                  softWrap: true,
                                  textAlign: TextAlign.center,
                                ))),
                      if (advertisement.text != null)
                        _buildAdvText(advertisement.text!, Colors.black),
                      if (advertisement.link != null && advertisement.link!.isNotEmpty)
                        _buildLinkButton(
                            link: advertisement.link!, linkText: advertisement.linkText!)
                    ])),
            Positioned(
                right: 0, child: UniversalPicture(Resources.advertisements[5]['icon_right']!)),
          ],
        );
      case 9:
        //Blue corners
        return Stack(
          alignment: Alignment.center,
          children: [
            Positioned.fill(
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(12.0),
                    child: UniversalPicture(
                        Resources.advertisements[6]['picture']!, null, null, BoxFit.fitWidth))),
            Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  if (advertisement.title != null)
                    Container(
                        constraints: BoxConstraints(maxWidth: 310.fw - 32),
                        child: Padding(
                            padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                            child: Text(
                              advertisement.title!,
                              style: TextStyles.highlightText(color: Colors.white, fontsize: 34),
                              maxLines: 2,
                              softWrap: true,
                              textAlign: TextAlign.center,
                            ))),
                  if (advertisement.text != null) _buildAdvText(advertisement.text!),
                  if (advertisement.link != null && advertisement.link!.isNotEmpty)
                    _buildLinkButton(link: advertisement.link!, linkText: advertisement.linkText!)
                ])
          ],
        );
      default:
        return Container();
    }
  }

  Widget _buildAdvText(String text, [Color? color]) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
            constraints: BoxConstraints(maxWidth: 310.fw - 32),
            child: Padding(
                padding: const EdgeInsets.only(top: 15, left: 16, right: 16),
                child: Text(
                  advertisement.text!,
                  style: TextStyles.highlightText(color: color ?? Colors.white),
                  maxLines: 4,
                  overflow: TextOverflow.ellipsis,
                  softWrap: true,
                  textAlign: TextAlign.start,
                ))),
      ],
    );
  }

  Widget _buildLinkButton({required String link, required String linkText}) {
    return Padding(
        padding: const EdgeInsets.symmetric(vertical: 15),
        child: _LinkButton.textual(
            onTap: () async {
              if (!await launchUrl(
                Uri.parse(link),
                mode: LaunchMode.externalApplication,
              )) {
                throw 'Could not launch ${advertisement.link!}';
              }
            },
            text: linkText));
  }
}

class _LinkButton extends StatelessWidget {
  final void Function() onPressed;
  final Widget child;
  final bool isActive;

  const _LinkButton({
    required this.onPressed,
    required this.child,
    this.isActive = true,
  });

  factory _LinkButton.textual({
    required void Function() onTap,
    required String text,
    final bool isActive = true,
  }) =>
      _LinkButton(
        isActive: isActive,
        onPressed: onTap,
        child: Text(
          text,
          style: TextStyle(
            color: isActive ? CustomColors.onPrimary : CustomColors.onPrimary.withOpacity(0.2),
            fontFamily: '.SF Pro Text',
            fontSize: 17,
          ),
          textAlign: TextAlign.center,
        ),
      );

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 34,
      margin: const EdgeInsets.symmetric(horizontal: 16),
      child: TextButton(
        style: ButtonStyle(
          alignment: Alignment.center,
          minimumSize: MaterialStateProperty.all<Size>(const Size.fromWidth(double.infinity)),
          backgroundColor: MaterialStateProperty.all<Color>(
            isActive ? CustomColors.primary : CustomColors.primary.withOpacity(0.4),
          ),
          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(6.0),
              side: BorderSide.none,
            ),
          ),
        ),
        onPressed: isActive ? onPressed : null,
        child: child,
      ),
    );
  }
}

class _Constrainer extends StatelessWidget {
  final int advType;
  final Widget child;

  const _Constrainer({required this.advType, required this.child});

  BoxConstraints _constrainsFromType({required int type}) {
    if (type <= 1) {
      return const BoxConstraints(minWidth: 300, maxWidth: 300);
    }
    if (type == 2) {
      return const BoxConstraints(minWidth: 210, maxWidth: 210);
    }
    if ((type > 2 && type <= 6) || (type == 9)) {
      return const BoxConstraints(minWidth: 280, maxWidth: 280);
    }

    if (type > 6 && type < 9) {
      return const BoxConstraints(minWidth: 315, maxWidth: 315);
    }

    return const BoxConstraints(minWidth: 300);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: _constrainsFromType(type: advType),
      child: child,
    );
  }
}
