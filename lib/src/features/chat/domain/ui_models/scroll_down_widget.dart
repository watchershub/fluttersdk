import 'dart:math';

import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class ScrollDownWidget extends StatefulWidget {
  final ScrollController scrollController;
  final double additionalArrowBottomPadding;

  const ScrollDownWidget({
    required this.onTap,
    required this.scrollController,
    required this.additionalArrowBottomPadding,
  });

  final void Function() onTap;

  @override
  State<ScrollDownWidget> createState() => _ScrollDownWidgetState();
}

class _ScrollDownWidgetState extends State<ScrollDownWidget> {
  bool showScrollDown = false;

  @override
  void initState() {
    widget.scrollController.addListener(_listenScroll);
    super.initState();
  }

  void _listenScroll() {
    final prev = showScrollDown;
    showScrollDown = widget.scrollController.hasClients && widget.scrollController.offset > 300;

    if (prev != showScrollDown) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    widget.scrollController.removeListener(_listenScroll);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!showScrollDown) return const SizedBox.shrink();

    return Positioned(
      right: 16,
      bottom: widget.additionalArrowBottomPadding + 8,
      child: InkWell(
        splashColor: Colors.transparent,
        onTap: widget.onTap,
        child: Container(
          decoration: const BoxDecoration(
            shape: BoxShape.circle,
            color: CustomColors.onPrimary,
            boxShadow: [
              BoxShadow(
                color: CustomColors.arrowShadowColor,
                offset: Offset(0, 0),
                blurRadius: 2,
              ),
            ],
          ),
          child: Transform.rotate(
            angle: pi,
            child: const SvgIcon(
              Resources.send,
              size: 32,
              color: CustomColors.primary,
            ),
          ),
        )
      ),
    );
  }
}
