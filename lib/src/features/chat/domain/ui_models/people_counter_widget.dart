import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class PeopleCounterWidget extends StatelessWidget {
  final String talkersCount;
  final void Function() onCountTap;

  const PeopleCounterWidget({required this.talkersCount, required this.onCountTap});
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: BorderRadius.zero,
        child: Padding(
            padding: const EdgeInsets.symmetric(
              horizontal: 4,
            ),
            child: InkWell(
              onTap: onCountTap,
              splashColor: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  border: const Border.fromBorderSide(BorderSide(color: CustomColors.primary)),
                  borderRadius: BorderRadius.circular(100),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const SvgIcon(
                        Resources.people,
                        height: 11,
                        color: CustomColors.primary,
                      ),
                      const SizedBox(width: 4),
                      SizedBox(
                        width: _countTextWidth(context) + 1,
                        child: Text(
                          talkersCount,
                          style: TextStyles.subtitle1(
                            fontSize: 14,
                            color: CustomColors.primary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            )));
  }

  double _countTextWidth(BuildContext context) {
    String textSample = '';
    for (int i = 0; i < talkersCount.length; i++) {
      textSample = textSample + '9';
    }
    final TextPainter textPainter = TextPainter(
      text: TextSpan(
        text: textSample,
        style: TextStyles.subtitle1(
          fontSize: 14,
          color: CustomColors.primary,
        ),
      ),
      textDirection: TextDirection.ltr,
      textWidthBasis: TextWidthBasis.longestLine,
    )..layout(maxWidth: MediaQuery.of(context).size.width);

    return textPainter.width;
  }
}
