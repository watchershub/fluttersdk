import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_option.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/poll_item_widget.dart';

class QuizAnswerWidget extends StatelessWidget {
  final Poll poll;
  final List<PollOption> optionsChosen;

  const QuizAnswerWidget({required this.poll, required this.optionsChosen});

  @override
  Widget build(BuildContext context) {
    final isAnswerRight = optionsChosen.any((e) => e.isRight);

    final backgroundColor = poll.isQuiz
        ? isAnswerRight
            ? CustomColors.pollAnswerBackground
            : CustomColors.wrongAnswer
        : CustomColors.pollAnswerBackground;

    return Align(
      alignment: Alignment.centerRight,
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 5),
        constraints: BoxConstraints(maxWidth: 310.fw),
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(12),
        ),
        margin: const EdgeInsets.all(4),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "Вы проголосовали",
                    style: TextStyles.title(
                      fontWeight: FontWeight.w400,
                      fontSize: 12,
                      color: CustomColors.quizAnswerHint,
                    ),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  Text(
                    poll.text,
                    textWidthBasis: TextWidthBasis.longestLine,
                    style: TextStyles.title(fontSize: 13, color: CustomColors.quizAnswerQustion),
                  ),
                  const SizedBox(
                    height: 4,
                  ),
                  ...optionsChosen
                      .map<Widget>(
                        (e) => Padding(
                          padding: const EdgeInsets.only(top: 6),
                          child: PollItemWidget(
                            rightPadding: 3,
                            isAnswerMessage: true,
                            nameStyle: TextStyles.title(
                              fontSize: 15,
                              fontWeight: FontWeight.w500,
                              color: CustomColors.onPrimary,
                            ),
                            alreadyVoted: true,
                            pic: e.pic,
                            title: e.text,
                            backgroundColor: backgroundColor,
                            isQuiz: poll.isQuiz,
                            isRight: e.isRight,
                            isMultiple: poll.isMultiple,
                            isSelected: !poll.isQuiz,
                            isAnswerRight: isAnswerRight,
                          ),
                        ),
                      )
                      .toList()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
