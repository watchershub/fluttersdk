import 'package:connectivity/connectivity.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/create_delete_reaction_dto.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/slomode_dto.dart';
import 'package:watchers_widget/src/features/chat/data/request/reject_hand_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/report_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/send_message/send_message_requst_mixin.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_ban_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_message_visible_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_messages_visible_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_mute_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_pinned_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/domain/models/emotion.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/models/preview.dart';

import '../../common/models/room.dart';
import '../data/request/delete_message_request.dart';
import '../data/request/edit_message_request.dart';
import 'models/sticker.dart';

abstract class IChatRepository {
  Stream<Emotion> get $myEmotions;

  Future<List<Message>> getMessages({
    String? limit,
    String? lastId,
  });

  Future<Message?> getMessageById({
    required String messageId,
  });

  Future<Message?> getPinnedMessage();

  Future<Room?> getRoom();

  Future<void> joinRoom({
    String? title,
    required String externalRoomId,
    required Function(String, dynamic) eventHandler,
  });

  void sendMessage({required SendMessageRequestMixin sendMessageRequest});

  void toggleHand();

  void rejectHand(RejectHandRequest rejectHandRequest);

  void setRole(SetRoleRequest setRoleRequest);

  void setMute(SetMuteRequest setMuteRequest);

  void editMessage({required EditMessageRequest editMessageRequest});

  void deleteMessage({required DeleteMessageRequest deleteMessageRequest});

  Future<Message> checkMessage({
    required Message message,
  });

  void sendEmotion({
    required Emotion emotion,
  });

  void setMessageVisible({required SetMessageVisibleRequest setMessageVisibleRequest});

  void setMessagesVisible({required SetMessagesVisibleRequest setMessagesVisibleRequest});

  void setBan({required SetBanRequest setBanRequest});

  void setPinnedMessage({required SetPinnedMessageRequest setPinnedMessageRequest});

  Future<void> reportMessage({required ReportMessageRequest reportMessageRequest});

  Future<Preview> getPreview({required String url});

  Future<List<Sticker>> getStickers();

  void createReaction({required ReactionDto reactionDto});

  void deleteReaction({required ReactionDto reactionDto});

  void setSlowmode({required SlowmodeDto slowmodeDto});

  Future<Message?> getUserLastMessage({
    required String userId,
  });

  void close();

  Stream<ConnectivityResult> get onConnectivityChanged$;
}
