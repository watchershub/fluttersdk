import 'dart:convert';

import 'package:equatable/equatable.dart';

class Sticker extends Equatable{
  final int id;
  final int position;
  final String pic;
  final DateTime? deletedAt;

  const Sticker({
    required this.id,
    required this.position,
    required this.pic,
    this.deletedAt,
  });


  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'position' : position,
      'pic' : pic,
      'deletedAt' : deletedAt,
    };
  }

  factory Sticker.fromMap(Map<String, dynamic> map) {
    return Sticker(
      id: map['id']?.toInt() ?? 0,
      position: map['id']?.toInt() ?? 0,
      pic: map['pic'] ?? '',
      deletedAt: map['deletedAt'] != null ? DateTime.parse(map['deletedAt']) : null,
    );
  }

  String toJson() => json.encode(toMap());

  factory Sticker.fromJson(String source) => Sticker.fromMap(json.decode(source));

  @override
  List<Object?> get props => [
    id,
    position,
    pic,
    deletedAt,
  ];
}
