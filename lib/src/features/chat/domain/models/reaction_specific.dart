import 'package:equatable/equatable.dart';
import 'package:watchers_widget/src/features/chat/domain/models/reaction.dart';

class ReactionSpecific extends Equatable {
  final int reactionCount;
  final bool hasMine;
  final Reaction reaction;

  const ReactionSpecific({
    required this.reactionCount,
    required this.hasMine,
    required this.reaction,
  });

  ReactionSpecific copyWith({
    int? reactionCount,
    bool? hasMine,
    Reaction? reaction,
  }) {
    return ReactionSpecific(
        reactionCount: reactionCount ?? this.reactionCount,
        hasMine: hasMine ?? this.hasMine,
        reaction: reaction ?? this.reaction,
    );
  }

  @override
  List<Object?> get props => [
        reactionCount,
        hasMine,
        reaction,
      ];
}
