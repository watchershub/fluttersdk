import 'dart:convert';

import 'package:equatable/equatable.dart';

class Reaction extends Equatable{
  final String emotion;
  final int messageId;
  final int talkerId;

  const Reaction({
    required this.emotion,
    required this.messageId,
    required this.talkerId,
  });


  Map<String, dynamic> toMap() {
    return {
      'emotion': emotion,
      'messageId' : messageId,
      'talkerId' : talkerId,
    };
  }

  factory Reaction.fromMap(Map<String, dynamic> map) {
    return Reaction(
      emotion: map['emotion'] ?? '',
      messageId: map['messageId']!=null? int.parse(map['messageId']!.toString()) : 0,
      talkerId: map['talkerId']?.toInt() ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory Reaction.fromJson(String source) => Reaction.fromMap(json.decode(source));

  @override
  List<Object?> get props => [
    emotion,
    messageId,
    talkerId,
  ];
}
