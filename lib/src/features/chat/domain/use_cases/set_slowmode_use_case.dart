import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/slomode_dto.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';

class SetSlowmodeUseCase {
  final IChatRepository _chatRepository;

  const SetSlowmodeUseCase({
    required IChatRepository chatRepository,
  }) : _chatRepository = chatRepository;

  Future<Result<void, Exception>> call({
    required SlowmodeDto slowmodeDto,
  }) async {
    try {
      return Success(_chatRepository.setSlowmode(slowmodeDto: slowmodeDto));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}