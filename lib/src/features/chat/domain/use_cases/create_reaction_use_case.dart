import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/create_delete_reaction_dto.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';

class CreateReactionUseCase {
  final IChatRepository _chatRepository;

  const CreateReactionUseCase({
    required IChatRepository chatRepository,
  }) : _chatRepository = chatRepository;

  Future<Result<void, Exception>> call({
    required ReactionDto reactionDto,
  }) async {
    try {
      return Success(_chatRepository.createReaction(reactionDto: reactionDto));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
