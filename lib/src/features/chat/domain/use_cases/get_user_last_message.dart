import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';

import 'package:watchers_widget/src/features/common/models/message.dart';

class GetUserLastMessageUseCase {
  final IChatRepository _chatRepository;

  const GetUserLastMessageUseCase({
    required IChatRepository chatRepository,
  }) : _chatRepository = chatRepository;

  Future<Result<Message?, Exception>> call({
    required String userId,
  }) async {
    try {
      final message = await _chatRepository.getUserLastMessage(userId: userId);
      return Success(message);
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
