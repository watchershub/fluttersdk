import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';
import 'package:watchers_widget/src/features/common/models/preview.dart';

class GetPreviewUseCase {
  final IChatRepository _chatRepository;
  const GetPreviewUseCase({
    required IChatRepository chatRepository,
  }) : _chatRepository = chatRepository;

  Future<Result<Preview, Exception>> call({required String url}) async {
    try {
      final preview = await _chatRepository.getPreview(url: url);
      return Success(preview);
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
