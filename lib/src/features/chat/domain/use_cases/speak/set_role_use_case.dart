import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';

class SetRoleUseCase {
  final IChatRepository _chatRepository;

  const SetRoleUseCase({
    required IChatRepository chatRepository,
  }) : _chatRepository = chatRepository;

  Future<Result<void, Exception>> call(SetRoleRequest setRoleRequest) async {
    try {
      return Success(_chatRepository.setRole(setRoleRequest));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
