import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/domain/i_agora_repository.dart';

class SetLocalAudioUseCase {
  final IAgoraRepository _agoraRepository;

  const SetLocalAudioUseCase({
    required IAgoraRepository agoraRepository,
  }) : _agoraRepository = agoraRepository;

  Future<Result<void, Exception>> call({required bool audioState}) async {
    try {
      return Success(_agoraRepository.setLocalAudio(audioState: audioState));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
