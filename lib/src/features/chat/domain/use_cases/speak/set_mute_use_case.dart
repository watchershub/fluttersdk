import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_mute_request.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';

class SetMuteUseCase {
  final IChatRepository _chatRepository;

  const SetMuteUseCase({
    required IChatRepository chatRepository,
  }) : _chatRepository = chatRepository;

  Future<Result<void, Exception>> call(SetMuteRequest setMuteRequest) async {
    try {
      return Success(_chatRepository.setMute(setMuteRequest));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
