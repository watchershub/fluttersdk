import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/domain/i_agora_repository.dart';

class LeaveVoiceRoomUseCase {
  final IAgoraRepository _agoraRepository;

  const LeaveVoiceRoomUseCase({
    required IAgoraRepository agoraRepository,
  }) : _agoraRepository = agoraRepository;

  Future<Result<void, Exception>> call() async {
    try {
      return Success(_agoraRepository.leaveRoom());
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}