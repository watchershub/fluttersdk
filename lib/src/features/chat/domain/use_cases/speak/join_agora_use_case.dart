import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/domain/i_agora_repository.dart';

class JoinAgoraUseCase {
  final IAgoraRepository _agoraRepository;

  const JoinAgoraUseCase({
    required IAgoraRepository agoraRepository,
  }) : _agoraRepository = agoraRepository;

  Future<Result<void, Exception>> call(
      {required String appId,
      required String agoraToken,
      required int talkerId,
      required String channelId,
      required bool enableLocalAudio}) async {
    try {
      return Success(_agoraRepository.joinRoom(
          appId: appId, agoraToken: agoraToken, talkerId: talkerId, channelId: channelId, enableLocalAudio: enableLocalAudio));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
