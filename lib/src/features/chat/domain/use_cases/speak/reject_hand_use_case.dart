import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/data/request/reject_hand_request.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';

class RejectHandUseCase {
  final IChatRepository _chatRepository;

  const RejectHandUseCase({
    required IChatRepository chatRepository,
  }) : _chatRepository = chatRepository;

  Future<Result<void, Exception>> call(RejectHandRequest rejectHandRequest) async {
    try {
      return Success(_chatRepository.rejectHand(rejectHandRequest));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
