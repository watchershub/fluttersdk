import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';
import 'package:watchers_widget/src/features/chat/domain/models/sticker.dart';

class GetStickersUseCase {
  final IChatRepository _chatRepository;

  const GetStickersUseCase({
    required IChatRepository chatRepository,
  }) : _chatRepository = chatRepository;

  Future<Result<List<Sticker>, Exception>> call() async {
    try {
      final stickers = await _chatRepository.getStickers();
      return Success(stickers);
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
