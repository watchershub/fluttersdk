import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';

part 'message_input_type.freezed.dart';

@freezed
class MessageInputType with _$MessageInputType {
  const MessageInputType._();

  const factory MessageInputType.reply({
    required Message message,
  }) = ReplyType;

  const factory MessageInputType.edit({
    required Message message,
  }) = EditType;

  const factory MessageInputType.link({
    required String linkText,
}) = LinkType;

  String get titlePrefix => map(
        reply: (value) => 'Ответить',
        edit: (value) => 'Редактирование',
        link: (value) => 'Ссылка',
      );

  String get iconPath => map(
        reply: (value) => Resources.reply,
        edit: (value) => Resources.edit,
        link: (value) => Resources.link,
      );

  bool get isReply => this is ReplyType;
  bool get isEdit => this is EditType;
  bool get isLink => this is LinkType;
}
