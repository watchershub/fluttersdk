// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'message_input_type.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$MessageInputType {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Message message) reply,
    required TResult Function(Message message) edit,
    required TResult Function(String linkText) link,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Message message)? reply,
    TResult Function(Message message)? edit,
    TResult Function(String linkText)? link,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Message message)? reply,
    TResult Function(Message message)? edit,
    TResult Function(String linkText)? link,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReplyType value) reply,
    required TResult Function(EditType value) edit,
    required TResult Function(LinkType value) link,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReplyType value)? reply,
    TResult Function(EditType value)? edit,
    TResult Function(LinkType value)? link,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReplyType value)? reply,
    TResult Function(EditType value)? edit,
    TResult Function(LinkType value)? link,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MessageInputTypeCopyWith<$Res> {
  factory $MessageInputTypeCopyWith(
          MessageInputType value, $Res Function(MessageInputType) then) =
      _$MessageInputTypeCopyWithImpl<$Res>;
}

/// @nodoc
class _$MessageInputTypeCopyWithImpl<$Res>
    implements $MessageInputTypeCopyWith<$Res> {
  _$MessageInputTypeCopyWithImpl(this._value, this._then);

  final MessageInputType _value;
  // ignore: unused_field
  final $Res Function(MessageInputType) _then;
}

/// @nodoc
abstract class _$$ReplyTypeCopyWith<$Res> {
  factory _$$ReplyTypeCopyWith(
          _$ReplyType value, $Res Function(_$ReplyType) then) =
      __$$ReplyTypeCopyWithImpl<$Res>;
  $Res call({Message message});
}

/// @nodoc
class __$$ReplyTypeCopyWithImpl<$Res>
    extends _$MessageInputTypeCopyWithImpl<$Res>
    implements _$$ReplyTypeCopyWith<$Res> {
  __$$ReplyTypeCopyWithImpl(
      _$ReplyType _value, $Res Function(_$ReplyType) _then)
      : super(_value, (v) => _then(v as _$ReplyType));

  @override
  _$ReplyType get _value => super._value as _$ReplyType;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$ReplyType(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
    ));
  }
}

/// @nodoc

class _$ReplyType extends ReplyType {
  const _$ReplyType({required this.message}) : super._();

  @override
  final Message message;

  @override
  String toString() {
    return 'MessageInputType.reply(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ReplyType &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$ReplyTypeCopyWith<_$ReplyType> get copyWith =>
      __$$ReplyTypeCopyWithImpl<_$ReplyType>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Message message) reply,
    required TResult Function(Message message) edit,
    required TResult Function(String linkText) link,
  }) {
    return reply(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Message message)? reply,
    TResult Function(Message message)? edit,
    TResult Function(String linkText)? link,
  }) {
    return reply?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Message message)? reply,
    TResult Function(Message message)? edit,
    TResult Function(String linkText)? link,
    required TResult orElse(),
  }) {
    if (reply != null) {
      return reply(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReplyType value) reply,
    required TResult Function(EditType value) edit,
    required TResult Function(LinkType value) link,
  }) {
    return reply(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReplyType value)? reply,
    TResult Function(EditType value)? edit,
    TResult Function(LinkType value)? link,
  }) {
    return reply?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReplyType value)? reply,
    TResult Function(EditType value)? edit,
    TResult Function(LinkType value)? link,
    required TResult orElse(),
  }) {
    if (reply != null) {
      return reply(this);
    }
    return orElse();
  }
}

abstract class ReplyType extends MessageInputType {
  const factory ReplyType({required final Message message}) = _$ReplyType;
  const ReplyType._() : super._();

  Message get message;
  @JsonKey(ignore: true)
  _$$ReplyTypeCopyWith<_$ReplyType> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$EditTypeCopyWith<$Res> {
  factory _$$EditTypeCopyWith(
          _$EditType value, $Res Function(_$EditType) then) =
      __$$EditTypeCopyWithImpl<$Res>;
  $Res call({Message message});
}

/// @nodoc
class __$$EditTypeCopyWithImpl<$Res>
    extends _$MessageInputTypeCopyWithImpl<$Res>
    implements _$$EditTypeCopyWith<$Res> {
  __$$EditTypeCopyWithImpl(_$EditType _value, $Res Function(_$EditType) _then)
      : super(_value, (v) => _then(v as _$EditType));

  @override
  _$EditType get _value => super._value as _$EditType;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$EditType(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
    ));
  }
}

/// @nodoc

class _$EditType extends EditType {
  const _$EditType({required this.message}) : super._();

  @override
  final Message message;

  @override
  String toString() {
    return 'MessageInputType.edit(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$EditType &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$EditTypeCopyWith<_$EditType> get copyWith =>
      __$$EditTypeCopyWithImpl<_$EditType>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Message message) reply,
    required TResult Function(Message message) edit,
    required TResult Function(String linkText) link,
  }) {
    return edit(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Message message)? reply,
    TResult Function(Message message)? edit,
    TResult Function(String linkText)? link,
  }) {
    return edit?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Message message)? reply,
    TResult Function(Message message)? edit,
    TResult Function(String linkText)? link,
    required TResult orElse(),
  }) {
    if (edit != null) {
      return edit(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReplyType value) reply,
    required TResult Function(EditType value) edit,
    required TResult Function(LinkType value) link,
  }) {
    return edit(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReplyType value)? reply,
    TResult Function(EditType value)? edit,
    TResult Function(LinkType value)? link,
  }) {
    return edit?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReplyType value)? reply,
    TResult Function(EditType value)? edit,
    TResult Function(LinkType value)? link,
    required TResult orElse(),
  }) {
    if (edit != null) {
      return edit(this);
    }
    return orElse();
  }
}

abstract class EditType extends MessageInputType {
  const factory EditType({required final Message message}) = _$EditType;
  const EditType._() : super._();

  Message get message;
  @JsonKey(ignore: true)
  _$$EditTypeCopyWith<_$EditType> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LinkTypeCopyWith<$Res> {
  factory _$$LinkTypeCopyWith(
          _$LinkType value, $Res Function(_$LinkType) then) =
      __$$LinkTypeCopyWithImpl<$Res>;
  $Res call({String linkText});
}

/// @nodoc
class __$$LinkTypeCopyWithImpl<$Res>
    extends _$MessageInputTypeCopyWithImpl<$Res>
    implements _$$LinkTypeCopyWith<$Res> {
  __$$LinkTypeCopyWithImpl(_$LinkType _value, $Res Function(_$LinkType) _then)
      : super(_value, (v) => _then(v as _$LinkType));

  @override
  _$LinkType get _value => super._value as _$LinkType;

  @override
  $Res call({
    Object? linkText = freezed,
  }) {
    return _then(_$LinkType(
      linkText: linkText == freezed
          ? _value.linkText
          : linkText // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$LinkType extends LinkType {
  const _$LinkType({required this.linkText}) : super._();

  @override
  final String linkText;

  @override
  String toString() {
    return 'MessageInputType.link(linkText: $linkText)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LinkType &&
            const DeepCollectionEquality().equals(other.linkText, linkText));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(linkText));

  @JsonKey(ignore: true)
  @override
  _$$LinkTypeCopyWith<_$LinkType> get copyWith =>
      __$$LinkTypeCopyWithImpl<_$LinkType>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(Message message) reply,
    required TResult Function(Message message) edit,
    required TResult Function(String linkText) link,
  }) {
    return link(linkText);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(Message message)? reply,
    TResult Function(Message message)? edit,
    TResult Function(String linkText)? link,
  }) {
    return link?.call(linkText);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(Message message)? reply,
    TResult Function(Message message)? edit,
    TResult Function(String linkText)? link,
    required TResult orElse(),
  }) {
    if (link != null) {
      return link(linkText);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(ReplyType value) reply,
    required TResult Function(EditType value) edit,
    required TResult Function(LinkType value) link,
  }) {
    return link(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(ReplyType value)? reply,
    TResult Function(EditType value)? edit,
    TResult Function(LinkType value)? link,
  }) {
    return link?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(ReplyType value)? reply,
    TResult Function(EditType value)? edit,
    TResult Function(LinkType value)? link,
    required TResult orElse(),
  }) {
    if (link != null) {
      return link(this);
    }
    return orElse();
  }
}

abstract class LinkType extends MessageInputType {
  const factory LinkType({required final String linkText}) = _$LinkType;
  const LinkType._() : super._();

  String get linkText;
  @JsonKey(ignore: true)
  _$$LinkTypeCopyWith<_$LinkType> get copyWith =>
      throw _privateConstructorUsedError;
}
