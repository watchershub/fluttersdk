import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:watchers_widget/src/core/base/bloc_injectable_state.dart';
import 'package:watchers_widget/src/core/base/bloc_whens.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/utils/transitions.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/screens/change_banned_name_screen.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/chat_body_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/upcoming_event/disabled_room_screen.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/upcoming_event/upcoming_event_screen.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/user_banned_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/loading_widget.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/logic/onboarding_bloc.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/logic/polls_bloc.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen();

  static Route route() => Transitions.buildFadeTransition(const ChatScreen());

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends BlocInjectableState<ChatScreen, ChatBloc, ChatEvent, ChatState> {
  bool _hasFocus = false;

  @override
  void initState() {
    bloc.focusNode.addListener(_updateShowOverlayStatus);

    super.initState();
  }

  @override
  void dispose() {
    bloc.focusNode.removeListener(_updateShowOverlayStatus);
    super.dispose();
  }

  void _updateShowOverlayStatus() {
    setState(() {
      _hasFocus = bloc.focusNode.hasFocus;
    });
  }

  bool _isFirstBuild = true;

  @override
  bool listenWhen(ChatState previous, ChatState current) {
    return previous is ChatStateLoaded &&
            previous.isChatEmpty &&
            current is ChatStateLoaded &&
            !current.isChatEmpty ||
        whenExactStateUpdated<ChatStateLoaded>(
          previous: previous,
          current: current,
          test: (previous, current) => previous.noneConnection && !current.noneConnection,
        );
  }

  @override
  void listener(BuildContext context, ChatState state) {
    if (state is ChatStateLoaded) {
      _isFirstBuild = _isFirstBuild || !state.isChatEmpty;
    }
  }

  @override
  Widget builder(BuildContext context, ChatState state) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: CustomColors.chatBackground,
      body: state.map(
        loading: (_) => const LoadingWidget(),
        loaded: (state) {
          if (state.startTime != null) {
            return UpcomingEventScreen(startTime: state.startTime!);
          }

          if (state.room.status == 'DISABLED') {
            return const DisabledRoomScreen();
          }

          if (state.context == null) {
            bloc.add(ChatEvent.setContext(context: context));
          }

          if (_isFirstBuild) {
            WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
              bloc.jumpToBottom().then((_) {
                setState(() {
                  _isFirstBuild = false;
                });
              });
            });
          }

          return Visibility(
            visible: !_isFirstBuild,
            maintainAnimation: true,
            maintainSize: true,
            maintainState: true,
            child: SafeArea(
              child: Stack(
                alignment: Alignment.center,
                children: [
                  PollProvider(
                    child: ChatScreenBodyWidget(
                      state: state,
                      bloc: bloc,
                      showOverlay: _hasFocus,
                    ),
                  ),
                  if (state.ban != null) ...[
                    Positioned.fill(
                      child: Container(
                        color: CustomColors.transparentBarrer,
                      ),
                    ),
                    if (state.ban!.reason != 6)
                      FutureBuilder(
                          future: bloc.getChatRules(),
                          builder: (context, AsyncSnapshot<String> chatRulesString) {
                            if (chatRulesString.connectionState == ConnectionState.done &&
                                chatRulesString.hasData) {
                              return UserBannedWidget(
                                ban: state.ban!,
                                chatRules: chatRulesString.data!,
                              );
                            }
                            return Container();
                          }),
                    if (state.ban!.reason == 6)
                      ChangeBannedNameScreen(
                          chatBloc: bloc, onboardingBloc: context.read<OnboardingBloc>()),
                  ],
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}

class PollProvider extends StatefulWidget {
  final Widget child;

  const PollProvider({
    required this.child,
  });

  @override
  State<PollProvider> createState() => _PollProviderState();
}

class _PollProviderState
    extends BlocInjectableState<PollProvider, PollsBloc, PollsEvent, PollsState> {
  @override
  Widget builder(BuildContext context, PollsState state) {
    return widget.child;
  }
}
