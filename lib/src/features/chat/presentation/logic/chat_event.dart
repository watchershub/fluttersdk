part of 'chat_bloc.dart';

@freezed
class ChatEvent with _$ChatEvent {
  const factory ChatEvent.init({
    required String externalRoomId,
  }) = _Init;

  const factory ChatEvent.sendMessage({
    required BuildContext context,
    required String text,
  }) = _SendMessage;

  const factory ChatEvent.finishLoading({
    Message? pinnedMessage,
    required List<Message> messages,
    required List<Talker> talkers,
    required Talker talker,
    required String externalRoomId,
    required List<int> initiatorIds,
    required List<int> targetIds,
    required Room room,
    Ban? ban,
    required List<Sticker> stickers,
    required bool isSlowmodeInterrupted,
    int? initialElapsedDelay,
  }) = _FinishLoading;

  const factory ChatEvent.fetchChat({
    required String externalRoomId,
    required List<Message> messages,
    required List<Talker> talkers,
  }) = _FetchChat;

  const factory ChatEvent.showEmotion({
    required Emotion emotion,
    required bool isMyEmotion,
  }) = _ShowEmotion;
  const factory ChatEvent.mentionMessage({
    required Message message,
  }) = _MentionMessage;

  const factory ChatEvent.copyMessage({
    required BuildContext context,
    required Message message,
  }) = _CopyMessage;

  const factory ChatEvent.editMessage({
    required Message message,
  }) = _EditMessage;

  const factory ChatEvent.closeOverhang() = _CloseOverhang;

  const factory ChatEvent.deleteMessage({
    required Message message,
    required BuildContext context,
  }) = _DeleteMessage;

  const factory ChatEvent.updateLoaded(
    CopyWithLoaded copyWithLoaded,
  ) = _UpdateLoaded;

  const factory ChatEvent.reportMessage({
    required BuildContext context,
    required Message message,
  }) = _ReportMessage;

  const factory ChatEvent.blockUser({
    required BuildContext context,
    required Message message,
  }) = _BlockUser;

  const factory ChatEvent.changeMessageVisibility({
    required Message message,
    required bool isVisible,
    required BuildContext context,
  }) = _ChangeMessageVisibility;

  const factory ChatEvent.changeMessagesVisibility({
    required Talker talker,
    required bool isVisible,
    required BuildContext context,
  }) = _ChangeMessagesVisibility;

  const factory ChatEvent.setBan({
    required Talker talker,
    required bool isBanned,
    required BuildContext context,
  }) = _SetBan;

  const factory ChatEvent.updateTalker(Talker talker) = _UpdateTalker;

  const factory ChatEvent.loadMoreMessages({
    required ChatStateLoaded loadedState,
    Message? scrollToMessage,
    int? limit,
  }) = LoadMoreMessages;

  const factory ChatEvent.sendPendingMessages({
    required ChatStateLoaded loadedState,
  }) = SendPendingMessages;

  const factory ChatEvent.setMessageRead({
    required Message message,
  }) = _SetMessageRead;

  const factory ChatEvent.scrollToMessage({
    required Message message,
    ListScrollPosition? position,
  }) = _ScrollToMessage;

  const factory ChatEvent.connectivityChanged(
    ConnectivityResult connectivityResult,
  ) = _ConnectivityChanged;

  const factory ChatEvent.setPinnedMessage({
    required BuildContext context,
    required Message message,
    required bool isPinned,
  }) = ChatEventSetPinnedMessage;

  const factory ChatEvent.updatePinnedMessage({
    required Message? pinnedMessage,
    required List<Message> messages,
  }) = _UpdatePinnedMessage;

  const factory ChatEvent.changeSelectedEmotion({
    required Emotion emotion,
  }) = ChangeSelectedEmotion;

  const factory ChatEvent.setEmotionPannelVisibility({
    required bool isVisible,
  }) = SetEmotionPannelVisibility;

  const factory ChatEvent.setLargeMessage({required bool largeMessage}) = _SetLargeMessage;

  const factory ChatEvent.sendSelectedEmotion() = SendSelectedEmotion;

  const factory ChatEvent.setSwearingMessage({
    required bool isSwearingMessage,
  }) = _SetSwearingMessage;

  const factory ChatEvent.setContext({
    required BuildContext context,
  }) = _SetContext;

  const factory ChatEvent.addPreviewToCache({
    required Preview preview,
    required String url,
  }) = ChatEventAddPreviewToCache;

  const factory ChatEvent.showLoading() = _ShowLoading;

  const factory ChatEvent.toggleHand() = _ToggleHand;

  const factory ChatEvent.rejectHand({
    required RejectHandRequest rejectHandRequest,
  }) = _RejectHand;

  const factory ChatEvent.setRole({
    required SetRoleRequest setRoleRequest,
    required BuildContext context,
    required Talker talker,
  }) = _SetRole;

  const factory ChatEvent.setMute({
    required SetMuteRequest setMuteRequest,
  }) = _SetMute;

  const factory ChatEvent.setMicStatus({
    required PermissionStatus micStatus,
  }) = _SetMicStatus;

  const factory ChatEvent.setStickerMode({
    required bool stickerMode,
  }) = _SetStickerMode;

  const factory ChatEvent.sendSticker({
    required int stickerId,
  }) = _SendSticker;

  const factory ChatEvent.createReaction({
    required ReactionDto reactionDto,
  }) = _CreateReaction;

  const factory ChatEvent.deleteReaction({
    required ReactionDto reactionDto,
  }) = _DeleteReaction;

  const factory ChatEvent.setCurrentTrigger({
    required TooltipTrigger currentTrigger,
  }) = _SetCurrentTrigger;
}
