import 'dart:async';
import 'dart:ui';

import 'package:amplitude_flutter/amplitude.dart';
import 'package:bloc_concurrency/bloc_concurrency.dart' as bloc_concurrency;
import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:watchers_widget/src/app/di/locator.dart';
import 'package:watchers_widget/src/app/watchers_params.dart';
import 'package:watchers_widget/src/core/base/bloc_whens.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/strings.dart';
import 'package:watchers_widget/src/core/log/i_log.dart';
import 'package:watchers_widget/src/core/packages/flutter_list_view/lib/flutter_list_view.dart';
import 'package:watchers_widget/src/core/packages/flutter_list_view/lib/src/flutter_list_view_element.dart';
import 'package:watchers_widget/src/core/packages/linkify/src/linkify.dart';
import 'package:watchers_widget/src/core/packages/scroll_to_index/scroll_to_index.dart';
import 'package:watchers_widget/src/core/utils/bloc_event_transformers.dart';
import 'package:watchers_widget/src/core/utils/concurrency.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/analitics_dto.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/create_delete_reaction_dto.dart';
import 'package:watchers_widget/src/features/chat/data/request/delete_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/edit_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/reject_hand_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/report_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/send_message/send_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/send_message/send_sticker_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_ban_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_message_visible_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_messages_visible_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_mute_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_pinned_message_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/domain/models/emotion.dart';
import 'package:watchers_widget/src/features/chat/domain/models/reaction.dart';
import 'package:watchers_widget/src/features/chat/domain/models/sticker.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/check_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/create_reaction_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/delete_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/delete_reaction_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/edit_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/emotion/get_all_emotions_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/emotion/get_emotion_by_name_scenario.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_message_array_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_pinned_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_preview_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_stickers_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_user_last_message.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/join_room_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/on_connectivity_changed_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/report_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/send_emotion_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/send_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_ban_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_message_visible_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_messages_visible_request.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_pinned_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_slowmode_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/join_agora_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/leave_voice_room_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/reject_hand_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/set_local_audio_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/set_mute_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/set_role_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/toggle_hand_use_case.dart';
import 'package:watchers_widget/src/features/chat/presentation/models/message_input_type.dart';
import 'package:watchers_widget/src/features/chat/presentation/screens/choose_ban_reason_screen.dart';
import 'package:watchers_widget/src/features/chat/presentation/screens/report_type_screen.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/chat_toast_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/report_type_widget.dart';
import 'package:watchers_widget/src/features/common/data/apis/stat/dtos/stat_calc_time_event_dto.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/block/add_block_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/block/get_blocks_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/settings/get_chat_rules_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/stat/calc_time_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/talker/get_room_and_talkers_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/user/get_local_user_use_case.dart';
import 'package:watchers_widget/src/features/common/models/ban.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/models/preview.dart';
import 'package:watchers_widget/src/features/common/models/room.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/common/models/user.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/confirm_dialog.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/multi_confirm_dialog.dart';
import 'package:watchers_widget/src/features/sound_check/presentation/sound_check_dialog.dart';
import 'package:watchers_widget/src/features/tooltips/domain/models/tooltip_trigger.dart';

import '../../../polls-and-quiz/domain/models/poll.dart';

part 'chat_bloc.freezed.dart';
part 'chat_event.dart';
part 'chat_state.dart';

typedef CopyWithLoaded = ChatStateLoaded Function(ChatStateLoaded);

class ChatBloc extends Bloc<ChatEvent, ChatState> {
  final GetMessageArrayUseCase _getMessageArrayUseCase;
  final GetPinnedMessageUseCase _getPinnedMessageUseCase;
  final JoinRoomUseCase _joinRoomUseCase;
  final SendMessageUseCase _sendMessageUseCase;
  final CheckMessageUseCase _checkMessageUseCase;
  final SendEmotionUseCase _sendEmotionUseCase;
  final GetAllEmotionsUseCase _getAllEmotionsUseCase;
  final GetEmotionByNameScenario _getEmotionByNameScenario;
  final GetRoomAndTalkersUseCase _getRoomAndTalkersUseCase;
  final EditMessageUseCase _editMessageUseCase;
  final DeleteMessageUseCase _deleteMessageUseCase;
  final ReportMessageUseCase _reportMessageUseCase;
  final AddBlockUseCase _addBlockUseCase;
  final SetMessageVisibleUseCase _setMessageVisibleUseCase;
  final SetMessagesVisibleUseCase _setMessagesVisibleUseCase;
  final SetBanUseCase _setBanUseCase;
  final GetBlocksUseCase _getBlocksUseCase;
  final GetConnectivityChangeStreamUseCase _getConnectivityChangeStreamUseCase;
  final SetPinnedMessageUseCase _setPinnedMessageUseCase;
  final LazyFutureAntiSwearing _antiSwearingFuture;
  final WatchersParamsProvider _paramsProvider;
  final GetPreviewUseCase _getPreviewUseCase;
  final ToggleHandUseCase _toggleHandUseCase;
  final RejectHandUseCase _rejectHandUseCase;
  final SetRoleUseCase _setRoleUseCase;
  final SetMuteUseCase _setMuteUseCase;
  final JoinAgoraUseCase _joinAgoraUseCase;
  final SetLocalAudioUseCase _setLocalAudioUseCase;
  final CalcTimeUseCase _calcTimeUseCase;
  final Stopwatch _benchMark;
  final GetStickersUseCase _getStickersUseCase;
  final CreateReactionUseCase _createReactionUseCase;
  final DeleteReactionUseCase _deleteReactionUseCase;
  final GetChatRulesUseCase _getChatRulesUseCase;
  final GetLocalUserUseCase _getLocalUserUseCase;
  final SetSlowmodeUseCase _setSlowmodeUseCase;
  final LeaveVoiceRoomUseCase _leaveVoiceRoomUseCase;
  final GetUserLastMessageUseCase _getUserLastMessageUseCase;
  final Amplitude _amplitudeAnalitic;

  final List<StreamSubscription> _subscriptions = [];

  ChatBloc(
    this._benchMark,
    this._calcTimeUseCase,
    this._setMuteUseCase,
    this._setRoleUseCase,
    this._rejectHandUseCase,
    this._toggleHandUseCase,
    this._getPreviewUseCase,
    this._addBlockUseCase,
    this._checkMessageUseCase,
    this._deleteMessageUseCase,
    this._editMessageUseCase,
    this._getAllEmotionsUseCase,
    this._getConnectivityChangeStreamUseCase,
    this._getEmotionByNameScenario,
    this._getMessageArrayUseCase,
    this._getBlocksUseCase,
    this._getPinnedMessageUseCase,
    this._setMessageVisibleUseCase,
    this._reportMessageUseCase,
    this._paramsProvider,
    this._getRoomAndTalkersUseCase,
    this._joinRoomUseCase,
    this._sendEmotionUseCase,
    this._sendMessageUseCase,
    this._setBanUseCase,
    this._setMessagesVisibleUseCase,
    this._setPinnedMessageUseCase,
    this._joinAgoraUseCase,
    this._setLocalAudioUseCase,
    this._antiSwearingFuture,
    this._getStickersUseCase,
    this._createReactionUseCase,
    this._deleteReactionUseCase,
    this._getChatRulesUseCase,
    this._getLocalUserUseCase,
    this._setSlowmodeUseCase,
    this._leaveVoiceRoomUseCase,
    this._getUserLastMessageUseCase,
    this._amplitudeAnalitic,
  ) : super(ChatState.loading()) {
    on<ChatEvent>(
      (event, emit) => event.mapOrNull(
        init: (event) => _onInit(event, emit),
        mentionMessage: (event) => _mentionMessage(event, emit),
        copyMessage: (event) => _copyMessage(event, emit),
        editMessage: (event) => _onEditMessage(event, emit),
        closeOverhang: (event) => _onCloseOverhang(event, emit),
        deleteMessage: (event) => _onDeleteMessage(event, emit),
        updateLoaded: (event) => _onUpdateLoaded(event, emit),
        reportMessage: (event) => _onReportMessage(event, emit),
        blockUser: (event) => _onBlockUser(event, emit),
        scrollToMessage: (event) => _onScrollToMessage(event, emit),
        setMessageRead: (event) => _onSetMessageRead(event, emit),
        setContext: (event) => _onSetContext(event, emit),
        addPreviewToCache: (event) => _onAddPreviewToCache(event, emit),
        setMicStatus: (event) => _onSetMicStatus(event, emit),
        setStickerMode: (event) => _onSetStickerMode(event, emit),
        sendSticker: (event) => _onSendSticker(event, emit),
        createReaction: (event) => _onCreateReaction(event, emit),
        deleteReaction: (event) => _onDeleteReaction(event, emit),
        setCurrentTrigger: (event) => _onSetCurrentTrigger(event, emit),
      ),
    );

    on<_SendMessage>(
      _sendMessage,
      transformer: droppable(),
    );

    on<_ChangeMessageVisibility>(_onChangeMessageVisibility);
    on<_ChangeMessagesVisibility>(_onChangeMessagesVisibility);
    on<_FinishLoading>(_finishLoading);
    on<_SetBan>(_setBan);
    on<_ConnectivityChanged>(_onConnectivityChanged);
    on<ChatEventSetPinnedMessage>(_onSetPinnedMessage);
    on<_UpdatePinnedMessage>(_onUpdatePinnedMessage);
    on<ChangeSelectedEmotion>(_changeSelectedEmotion);
    on<SetEmotionPannelVisibility>(_setEmotionPannelVisibility);
    on<SendSelectedEmotion>(
      _sendSelectedEmotion,
      transformer: BlocEventTransformers.throttle(
        const Duration(milliseconds: 400),
      ),
    );
    on<_ShowEmotion>(
      _showEmotion,
      transformer: BlocEventTransformers.throttle(
        const Duration(milliseconds: 400),
      ),
    );
    on<_SetSwearingMessage>(_onSetSwearingMessage);
    on<_ShowLoading>(_onShowLoading);
    on<_ToggleHand>(_onToggleHand);
    on<_RejectHand>(_rejectHand);
    on<_SetRole>(_setRole);
    on<_SetMute>(_setMute);

    on<LoadMoreMessages>(
      _loadMoreMessages,
      transformer: bloc_concurrency.droppable(),
    );

    _subscriptions.addAll([
      _getConnectivityChangeStreamUseCase.call().listen(
            (connectivityResult) => add(_ConnectivityChanged(connectivityResult)),
          ),
    ]);

    _scrollController.addListener(() {
      if (_scrollController.position.userScrollDirection == ScrollDirection.reverse) {
        FocusManager.instance.primaryFocus?.unfocus();
      }

      final curState = state;
      if (curState is! ChatStateLoaded) return;

      if (_scrollController.position.maxScrollExtent - _scrollController.offset < 3500 &&
          !curState.isLoadedAllMessages &&
          !curState.havePendingMessages) {
        loadMoreMessages(
          LoadMoreMessages(
            loadedState: curState,
            limit: 100,
          ),
        );
      }
    });

    _amplitudeAnalitic.setUserId(_paramsProvider.params.userId);

    _init(_paramsProvider.params.roomId);
  }

  final TextEditingController _textEditingcontroller = TextEditingController();
  TextEditingController get textEditingController => _textEditingcontroller;

  final FocusNode _focusNode = FocusNode();
  FocusNode get focusNode => _focusNode;

  final AutoScrollController _autoScrollController = AutoScrollController();
  AutoScrollController get autoScrollController => _autoScrollController;

  final FlutterListViewController _scrollController = FlutterListViewController();
  FlutterListViewController get scrollController => _scrollController;

  FlutterListViewController? get scrollControllerOrNull =>
      _scrollController.hasClients ? _scrollController : null;

  Timer _interruptionTimer({int? interruptionTime}) {
    interruptionStopwatch
      ..reset()
      ..start();

    return Timer(
        Duration(
            milliseconds: interruptionTime ?? (state as ChatStateLoaded).room.slowmodeDelayMS!),
        () {
      try {
        add(ChatEvent.updateLoaded(
            (p0) => p0.copyWith(isSlowmodeInterrupted: false, initialElapsedDelay: null)));
      } catch (e) {
        print('wrong state $e');
      }

      interruptionStopwatch
        ..stop()
        ..reset();
    });
  }

  final _interruptionStopwatch = Stopwatch();
  Stopwatch get interruptionStopwatch => _interruptionStopwatch;

  final GlobalKey _chatKey = GlobalKey();
  GlobalKey get chatKey => _chatKey;

  final FToast _fToast = FToast();

  @override
  void onTransition(Transition<ChatEvent, ChatState> transition) {
    /// Если изменилось количество участников обновляем [ChatStateLoaded.showCounter]
    if (whenExactStateUpdated<ChatStateLoaded>(
      previous: transition.currentState,
      current: transition.nextState,
      test: (previous, current) => previous.talkers.length != current.talkers.length,
    )) {
      final nextState = transition.nextState as ChatStateLoaded;
      add(ChatEvent.updateLoaded(
        (_) => nextState.copyWith(
          showCounter: nextState.updateShowCounter(),
        ),
      ));
    }

    doWhenStateUpdated<ChatStateLoaded>(
      previous: transition.currentState,
      current: transition.nextState,
      test: (previous, current) => previous.talker.isBanned != current.talker.isBanned,
      whenTrue: (previous, current) {
        if (current.talker.isBanned) {
          _showToast(
            text: 'Вас заблокировали. Вы не можете писать сообщения в чат',
            iconPath: Resources.user_blocked,
          );
        } else {
          _showToast(
            text: 'Вас разблокировали. Вы можете писать сообщения в чат',
            iconPath: Resources.warning,
          );
        }
      },
    );

    doWhenStateUpdated<ChatStateLoaded>(
      previous: transition.currentState,
      current: transition.nextState,
      test: (previous, current) => previous.noneConnection != current.noneConnection,
      whenTrue: (previous, current) {
        if (current.noneConnection) {
          _showToast(
            text: 'Отсутствует интернет-соединение',
            iconPath: Resources.no_internet,
          );
        }
      },
    );

    doWhenStateUpdated<ChatStateLoaded>(
      previous: transition.currentState,
      current: transition.nextState,
      test: (previous, current) => previous.isSwearingMessage != current.isSwearingMessage,
      whenTrue: (previous, current) {
        if (current.isSwearingMessage) {
          _showToast(
            text: 'Попробуйте переформулировать',
            iconPath: Resources.disabled,
          );
        }
      },
    );

    super.onTransition(transition);
  }

  Future<void> jumpToBottom({
    bool animateRescrolling = false,
  }) async {
    if (!_scrollController.hasClients) return;

    double curValue = 0;
    double prevValue = curValue - 1;

    while (prevValue != curValue) {
      _scrollController.jumpTo(curValue);

      prevValue = curValue;
      await SchedulerBinding.instance.endOfFrame;
      curValue = 0;
    }

    // Если использовать jumpToIndex, то addListener не работает в ScrollDownWidget и надо флаг переносить в state
    // _scrollController.sliverController.jumpToIndex(0);
  }

  void initShowToast(BuildContext context) {
    _fToast.init(context);
  }

  Future<Preview?> getPreview({required String url}) async {
    final result = await _getPreviewUseCase.call(url: url);
    if (result.isError) {
      return null;
    }
    return result.successValue;
  }

  VoidCallback _showToast({
    required String text,
    required String iconPath,
    bool isClosable = true,
  }) {
    _fToast.removeCustomToast();
    _fToast.showToast(
      positionedToastBuilder: (context, child) {
        return Positioned(
          left: 16,
          bottom: 60,
          right: 16,
          child: child,
        );
      },
      toastDuration: isClosable ? const Duration(milliseconds: 3000) : const Duration(days: 7),
      //gravity: ToastGravity.BOTTOM,
      child: ChatToastWidget(
        onClose: isClosable ? () => _fToast.removeCustomToast() : null,
        iconPath: iconPath,
        text: text,
      ),
    );

    return () => _fToast.removeCustomToast();
  }

  bool _onPaused = false;
  ChatStateLoaded? _pendingState;

  void _emitLoaded({
    required Emitter<ChatState> emit,
    required CopyWithLoaded copyWithLoaded,
  }) {
    final currentState = state;
    if (currentState is! ChatStateLoaded) return;

    if (!_onPaused) {
      return emit(copyWithLoaded(currentState));
    }

    if (_pendingState == null) {
      _pendingState = copyWithLoaded(currentState);
      return;
    }

    _pendingState = copyWithLoaded(_pendingState!);
  }

  void _init(String externalRoomId) => add(ChatEvent.init(externalRoomId: externalRoomId));
  Future<void> _onInit(_Init event, Emitter<ChatState> emit) async {
    emit(ChatState.loading());

    // Слушаем состояние приложения (активн/инактив)
    SystemChannels.lifecycle.setMessageHandler((msg) async {
      debugPrint('SystemChannels> $msg');

      if (msg == AppLifecycleState.paused.toString()) {
        _onPaused = true;
        return;
      }

      if (msg == AppLifecycleState.resumed.toString()) {
        _onPaused = false;
        if (_pendingState != null) {
          final stateToShow = _pendingState!.copyWith();
          add(ChatEvent.updateLoaded((_) => stateToShow));
        }
        _pendingState = null;
      }
    });

    _focusNode.addListener(() {
      if (state.isNotA<ChatStateLoaded>()) return;
      final loadedState = state.as<ChatStateLoaded>();

      if (_focusNode.hasFocus && loadedState.stickerMode == true) {
        add(ChatEvent.updateLoaded((_) => loadedState.copyWith(
              stickerMode: false,
            )));
      }
    });

    _textEditingcontroller.addListener(() async {
      if (state is ChatStateLoaded) {
        final loadedState = state as ChatStateLoaded;
        final isSwearing =
            (await _antiSwearingFuture()).containsSwearing(_textEditingcontroller.text);
        _setSwearingMessage(_SetSwearingMessage(
          isSwearingMessage: isSwearing,
        ));
        if (_textEditingcontroller.text.isNotEmpty) {
          final linkable = linkify(_textEditingcontroller.text).firstWhere(
              (element) => element is LinkableElement,
              orElse: () => LinkableElement('', ''));
          if (loadedState.messageInputType == null || loadedState.messageInputType!.isLink) {
            add(ChatEvent.updateLoaded((_) => loadedState.copyWith(
                  messageInputType: linkable.text.isNotEmpty
                      ? MessageInputType.link(linkText: linkable.text)
                      : null,
                  isSwearingMessage: isSwearing,
                )));
          }
        }
      }
    });

    await _joinRoomUseCase.call(
      title: _paramsProvider.params.title,
      externalRoomId: event.externalRoomId,
      eventHandler: (socketEvent, data) async {
        log.debug('Recieved <<< event: $socketEvent, data: $data');

        if (socketEvent == 'successfullyJoined') {
          add(const ChatEvent.showLoading());

          final stickers = await _getStickersUseCase.call();
          if (stickers.isError) return;

          final blockResult = await _getBlocksUseCase.call();
          if (blockResult.isError) return;
          final initiatorids = blockResult.successValue.initiator.map((e) => e.id).toList();
          final targetIds = blockResult.successValue.target.map((e) => e.id).toList();

          final tupleResult = await waitConcurrently3(
            _getMessageArrayUseCase.call(),
            _getRoomAndTalkersUseCase.call(event.externalRoomId),
            _getPinnedMessageUseCase.call(),
          );

          if (tupleResult.item1.isError || tupleResult.item2.isError || tupleResult.item3.isError) {
            return;
          }

          final room = tupleResult.item2.successValue.first.room;
          if (room.isSpeak) {
            Future.delayed(const Duration(seconds: 10), () {
              add(const ChatEvent.setCurrentTrigger(currentTrigger: TooltipTrigger.requestMic));
            });
          }

          final messages = tupleResult.item1.successValue
              .map((e) => _setMessageVisibility(e, initiatorids, targetIds))
              .toList()
              .reversed
              .toList();
          //Slowmode restriction after reconnecting to room;l
          bool slowmodeInterrupted = false;
          int initialElapsedDelay = 0;
          if (room.slowmodeDelayMS != null && !Talker.fromMap(data['talker']).isModerOrAdmin) {
            if (messages.indexWhere((element) =>
                    DateTime.now().millisecondsSinceEpoch -
                        DateTime.parse(element.serverDateTime).millisecondsSinceEpoch >
                    room.slowmodeDelayMS!) !=
                -1) // Если в пачке нет сообщений старше слоумода нету смысла смотреть,идем в апи
            {
              if (messages.indexWhere((element) => element.isMyMessage == true) ==
                  -1) //если нет моего ,при этом предыдущая проверка сказала ,что в пачке есть старше слоумода значит не даем ограничение на отправку
              {
                //ставим - прерывание слоумодом отсутствует
                slowmodeInterrupted = false;
              } else //иначе делаем проверку
              {
                //Идем по сообщениям от последнего
                //Если встретили чужое больше слоумода не встретив наше не даем ограничений
                //Встретив наше ,проверяем ,если старше не даем огран ,если меньше даем
                //Сделал так,потому что indexOf - по сути тот же цикл, и так он будет крутиться до нашего ,а по вышеописанной схеме можно прерваться раньше
                for (int i = 0; i < messages.length; i++) {
                  final currentMessage = messages[messages.length - 1 - i];
                  final currentInterruptionTime = DateTime.now().millisecondsSinceEpoch -
                      DateTime.parse(currentMessage.serverDateTime).millisecondsSinceEpoch;

                  if (currentMessage.isMyMessage == true) {
                    if (currentInterruptionTime < room.slowmodeDelayMS!) {
                      slowmodeInterrupted = true;
                      initialElapsedDelay = currentInterruptionTime;
                      _interruptionTimer(
                          interruptionTime: room.slowmodeDelayMS! - currentInterruptionTime);
                      break;
                    } else {
                      slowmodeInterrupted = false;
                    }
                  } else {
                    if (currentInterruptionTime < room.slowmodeDelayMS!) {
                      slowmodeInterrupted = true;
                      initialElapsedDelay = currentInterruptionTime;
                      _interruptionTimer(
                          interruptionTime: room.slowmodeDelayMS! - currentInterruptionTime);
                      break;
                    }
                  }
                }
              }
            } else {
              // Идем в АПИ
              final lastUserMessageResultInRoom = await _getUserLastMessageUseCase.call(
                  userId: Talker.fromMap(data['talker']).user.id.toString());
              if (lastUserMessageResultInRoom.isSuccess &&
                  lastUserMessageResultInRoom.successValue != null) {
                final lastMessageInRoom = lastUserMessageResultInRoom.successValue!;
                final currentInterruptionTime = DateTime.now().millisecondsSinceEpoch -
                    DateTime.parse(lastMessageInRoom.serverDateTime).millisecondsSinceEpoch;
                if (currentInterruptionTime < room.slowmodeDelayMS!) {
                  slowmodeInterrupted = true;
                  initialElapsedDelay = currentInterruptionTime;
                  _interruptionTimer(
                      interruptionTime: room.slowmodeDelayMS! - currentInterruptionTime);
                } else {
                  slowmodeInterrupted = false;
                }
              } else {
                slowmodeInterrupted = false;
              }
            }
          } else {
            // нет слоумода - нет ограничений
            slowmodeInterrupted = false;
          }

          final talkers = tupleResult.item2.successValue.map((e) => e.talker).toList();

          for (final message in messages) {
            final talkerIndex = talkers.indexWhere((talker) => talker.id == message.talker.id);
            if (talkerIndex != -1) {
              talkers[talkerIndex] = talkers[talkerIndex].copyWith(haveMessages: true);
            }
          }

          final ban = (data['ban'] == null) ? null : Ban.fromMap(data['ban']);

          add(_FinishLoading(
            messages: messages,
            talkers: talkers,
            room: room,
            pinnedMessage: tupleResult.item3.successValue,
            talker: Talker.fromMap(data['talker']),
            externalRoomId: event.externalRoomId,
            initiatorIds: initiatorids,
            targetIds: targetIds,
            ban: ban,
            stickers: stickers.successValue,
            isSlowmodeInterrupted: slowmodeInterrupted,
            initialElapsedDelay: initialElapsedDelay,
          ));

          return;
        }

        // Todo(dartloli): проверить лучше ли чекать тут интернет чем в connectivity
        // if (socketEvent == 'connect_error') {
        //   return;
        // }

        if (socketEvent == 'userJoinedAgora') {
          await _joinAgoraUseCase.call(
              appId: data['appId'],
              agoraToken: data['token'],
              talkerId: data['talker']['id'],
              channelId: data['channelName'],
              enableLocalAudio: !data['talker']['isMuted']);
        }

        if (state.isNotA<ChatStateLoaded>()) return;
        final loadedState = _pendingState ?? state.as<ChatStateLoaded>();

        /// Если опрос удалили (отредачили) то и сообщение с голосованием удаляется
        if (socketEvent == 'pollDeleted') {
          final poll = Poll.fromMap(data['poll']);
          add(ChatEvent.updateLoaded((_) => loadedState.copyWith(
              messages: loadedState.messages
                  .where((message) => message.poll == null || message.poll!.id != poll.id)
                  .toList())));
        }

        if (socketEvent == 'message') {
          final Message newMessage =
              (await _checkMessageUseCase(message: Message.fromMap(data['message']))).successValue!;

          if (newMessage.poll != null &&
              newMessage.talker.id != loadedState.talker.id &&
              !newMessage.isPollResults) {
            return;
          }

          final checkedMessage = _setMessageVisibility(
            newMessage,
            loadedState.initiatorIds,
            loadedState.targetIds,
          );

          final vMessage = checkedMessage.talker.id == loadedState.talker.id
              ? checkedMessage.copyWith(isVisible: true)
              : checkedMessage;

          final messages = List<Message>.from(loadedState.messages)..insert(0, vMessage);

          final talkers = _tryMarkHaveMessages(loadedState.talkers, newMessage);

          if (scrollController.hasClients && 0 == scrollController.offset ||
              checkedMessage.isMyMessage == true ||
              messages.length == 1 ||
              !checkedMessage.isVisible) {
            final bool shouldSlowmodeInterrupt = checkedMessage.isMyMessage == true &&
                !loadedState.talker.isModerOrAdmin &&
                loadedState.room.isSlowmode;

            if (shouldSlowmodeInterrupt) {
              _interruptionTimer();
            }

            add(ChatEvent.updateLoaded((state) => loadedState.copyWith(
                  messages: messages,
                  talkers: talkers,
                  isSlowmodeInterrupted:
                      (shouldSlowmodeInterrupt) ? true : state.isSlowmodeInterrupted,
                  initialElapsedDelay: null,
                )));
          } else {
            final oldUnreadParams = loadedState.unreadParams;

            final unreadParams = UnreadParams(
              unreadMentions: checkedMessage.isMentionMe == true
                  ? oldUnreadParams.unreadMentions + 1
                  : oldUnreadParams.unreadMentions,
              firstUnreadMessage: oldUnreadParams.firstUnreadMessage ?? checkedMessage,
              unreadMessages: [...oldUnreadParams.unreadMessages, checkedMessage],
            );

            add(ChatEvent.updateLoaded((_) => loadedState.copyWith(
                  unreadParams: unreadParams,
                  messages: messages,
                  talkers: talkers,
                )));
          }
          return;
        }

        if (socketEvent == 'userJoined') {
          final Talker newTalker = Talker.fromMap(data['talker']);
          final talkers = <Talker>{...loadedState.talkers, newTalker};

          add(ChatEvent.updateLoaded(
              (loadedState) => loadedState.copyWith(talkers: talkers.toList())));
          return;
        }

        if (socketEvent == 'userLeft') {
          final talkers = Set<Talker>.from(loadedState.talkers);

          final int leftTalkerId = data['id'];
          talkers.removeWhere((element) => element.id == leftTalkerId);

          add(ChatEvent.updateLoaded(
              (loadedState) => loadedState.copyWith(talkers: talkers.toList())));
          return;
        }

        if (socketEvent == 'messageEdited') {
          // Todo(dartloli): make message factory (to unify this logic)
          final Message rawEditedMessage = Message.fromMap(data['message']);
          final Message editedMessage = await _checkMessageUseCase
              .call(message: rawEditedMessage)
              .then((value) => value.successValue!);

          final editedMessageIndex =
              loadedState.messages.indexWhere((message) => message.id == editedMessage.id);

          final messages = List<Message>.from(loadedState.messages);
          messages[editedMessageIndex] = editedMessage;

          for (int i = 0; i < messages.length; i++) {
            if (messages[i].mentionMessage?.id == editedMessage.id) {
              messages[i] = messages[i].copyWith(mentionMessage: editedMessage);
            }
          }

          final shouldUpdatePin = loadedState.pinnedMessage?.id == editedMessage.id;

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
                pinnedMessage: shouldUpdatePin ? editedMessage : loadedState.pinnedMessage,
              )));
          return;
        }

        if (socketEvent == 'messageDeleted') {
          final messages = List<Message>.from(loadedState.messages);

          for (int i = 0; i < messages.length; i++) {
            if (messages[i].mentionMessage?.id.toString() == data['messageId'].toString()) {
              messages[i] = messages[i].copyWith(showMention: false);
            }
          }

          messages.removeWhere((message) => message.id.toString() == data['messageId'].toString());

          final unreadParams = _updateUnreadParamsOrNull(int.parse(data['messageId'].toString()),
              messageIsRemoved: true);

          final shouldDeletePin = loadedState.pinnedMessage?.id.toString() == data['messageId'];

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
                unreadParams: unreadParams ?? loadedState.unreadParams,
                pinnedMessage: shouldDeletePin ? null : loadedState.pinnedMessage,
              )));
          return;
        }

        if (socketEvent == 'messageVisibleSet') {
          final messages = List<Message>.from(loadedState.messages);
          final updatedMessage = Message.fromMap(data['message']);

          if (loadedState.talker.id == updatedMessage.talker.id) {
            return;
          } else {
            log.debug('wrong talker');
          }

          for (int i = 0; i < messages.length; i++) {
            if ((messages[i].mentionMessage?.id == updatedMessage.id) &&
                (!loadedState.talker.isModer)) {
              messages[i] = messages[i].copyWith(showMention: updatedMessage.isVisible);
            }
          }

          final updatedIndex = messages.indexWhere((message) => message.id == updatedMessage.id);
          if (updatedIndex == -1) return;

          messages[updatedIndex] = updatedMessage;

          final unreadParams = _updateUnreadParamsOrNull(updatedMessage.id, messageIsRemoved: true);

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
                unreadParams: unreadParams ?? loadedState.unreadParams,
              )));
          // Show info for moders
          if (loadedState.talker.isModerOrAdmin) {
            if (updatedMessage.isVisible) {
              _showToast(
                text: 'Сообщениe показано',
                iconPath: Resources.plus,
              );
              return;
            }
            _showToast(
              text: 'Сообщение скрыто',
              iconPath: Resources.hide_message,
            );
          }

          return;
        }

        if (socketEvent == 'messagesVisibleSet') {
          final messages = List<Message>.from(loadedState.messages);

          final int userId = int.parse(data['userId'].toString());
          final bool isVisible = data['isVisible'];
          if (loadedState.talker.user.id == userId) {
            return;
          } else {
            log.debug('wrong talker');
          }

          UnreadParams? unreadParams;

          // Update messages
          for (int i = 0; i < messages.length; i++) {
            final message = messages[i];
            if (message.creatorId == userId) {
              messages[i] = message.copyWith(
                isVisible: isVisible,
                talker: message.talker.copyWith(isSupressed: !isVisible),
              );
              unreadParams = _updateUnreadParamsOrNull(message.id, messageIsRemoved: true);
            }
            if ((message.mentionMessage?.creatorId == userId) && (!loadedState.talker.isModer)) {
              messages[i] = message.copyWith(
                showMention: isVisible,
              );
            }
          }

          // Update talkers
          final talkerIndex = loadedState.talkers.indexWhere((e) => e.user.id == userId);

          final talkers = List<Talker>.from(loadedState.talkers);
          if (talkerIndex != -1) {
            talkers[talkerIndex] = talkers[talkerIndex].copyWith(isSupressed: !isVisible);
          }

          // Update talker
          Talker updatedTalker = loadedState.talker.copyWith();
          if (loadedState.talker.user.id == userId) {
            updatedTalker = updatedTalker.copyWith(isSupressed: !isVisible);
          }

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
                talkers: talkers,
                talker: updatedTalker,
                unreadParams: unreadParams ?? loadedState.unreadParams,
              )));

          if (loadedState.talker.isModerOrAdmin) {
            if (isVisible) {
              _showToast(
                text: 'Сообщения показаны',
                iconPath: Resources.plus,
              );
              return;
            }

            _showToast(
              text: 'Сообщения скрыты',
              iconPath: Resources.hide_message,
            );
          }
        }

        if (socketEvent == 'userBanSet') {
          final messages = List<Message>.from(loadedState.messages);
          final updatedUser = User.fromMap(data['user']);

          // Update messages
          for (int i = 0; i < messages.length; i++) {
            final message = messages[i];
            if (message.talker.id == updatedUser.id) {
              messages[i] = message.copyWith(
                talker: message.talker.copyWith(user: updatedUser),
              );
            }
          }

          // Update talkers
          final talkerIndex = loadedState.talkers.indexWhere(
            (e) => e.user.id == updatedUser.id,
          );

          final talkers = List<Talker>.from(loadedState.talkers);
          if (talkerIndex != -1) {
            talkers[talkerIndex] = talkers[talkerIndex].copyWith(user: updatedUser);
          }
          final ban = (data['ban'] == null) ? null : Ban.fromMap(data['ban']);

          add(ChatEvent.updateLoaded(
            (loadedState) => loadedState.copyWith(
                messages: messages,
                talkers: talkers,

                // Update talker
                talker: updatedUser.id == loadedState.talker.user.id
                    ? loadedState.talker.copyWith(user: updatedUser)
                    : loadedState.talker,
                // Update ban
                ban: updatedUser.id == loadedState.talker.user.id ? ban : loadedState.ban),
          ));
        }

        if (socketEvent == 'banSet' || socketEvent == 'talkerBanSet') {
          final messages = List<Message>.from(loadedState.messages);
          final updatedTalker = Talker.fromMap(data['talker']);

          // Update messages
          for (int i = 0; i < messages.length; i++) {
            final message = messages[i];
            if (message.talker.id == updatedTalker.id) {
              messages[i] = message.copyWith(
                talker: updatedTalker,
              );
            }
          }

          // Update talkers
          final talkerIndex = loadedState.talkers.indexWhere(
            (e) => e.user.id == updatedTalker.user.id,
          );
          final talkers = List<Talker>.from(loadedState.talkers);
          if (talkerIndex != -1) {
            talkers[talkerIndex] = updatedTalker;
          }

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
              messages: messages,
              talkers: talkers,

              // Update talker
              talker: updatedTalker.user.id == loadedState.talker.user.id
                  ? updatedTalker
                  : loadedState.talker,
              // Update ban
              ban: updatedTalker.user.id == loadedState.talker.user.id
                  ? data['ban'] == null
                      ? null
                      : Ban.fromMap(data['ban'])
                  : loadedState.ban)));
          // Show info for moders
          if (loadedState.talker.isModerOrAdmin) {
            if (updatedTalker.isBanned) {
              _showToast(
                text: 'Пользователь заблокирован',
                iconPath: Resources.block,
              );
              return;
            }

            _showToast(
              text: 'Пользователь разблокирован',
              iconPath: Resources.speaker,
            );
          }
        }

        if (socketEvent == 'blockSet') {
          final bool isBlocked = data['isBlocked'];
          final int initiatorId = data['initiatorId'];
          final int targetId = data['targetId'];

          final messages = List<Message>.from(loadedState.messages);

          // Если мы сейчас модер, то у нас нет работы с ЧС
          if (loadedState.talker.isModer) return;

          final myId = loadedState.talker.user.id;

          UnreadParams? _unreadParams;
          List<Message>? currentStaticUnreadMessage = [...loadedState.unreadParams.unreadMessages];

          for (int i = 0; i < messages.length; i++) {
            final message = messages[i];

            // Скрыть сообщения цели у инициатора
            if (myId == initiatorId && message.creatorId == targetId) {
              messages[i] = message.copyWith(isVisible: !isBlocked);
              if (currentStaticUnreadMessage?.indexWhere((element) => element.id == message.id) !=
                  -1) {
                _unreadParams = _updateUnreadParamsOrNull(message.id,
                    messageIsRemoved: true, staticUnreadMessages: currentStaticUnreadMessage);
                currentStaticUnreadMessage = _unreadParams?.unreadMessages;
              }
            }
            if (myId == initiatorId && message.mentionMessage?.creatorId == targetId) {
              messages[i] = message.copyWith(showMention: !isBlocked);
            }

            // Скрыть сообщения инициатора у цели
            if (myId == targetId && message.creatorId == initiatorId) {
              messages[i] = message.copyWith(isVisible: !isBlocked);
              if (currentStaticUnreadMessage?.indexWhere((element) => element.id == message.id) !=
                  -1) {
                _unreadParams = _updateUnreadParamsOrNull(message.id,
                    messageIsRemoved: true, staticUnreadMessages: currentStaticUnreadMessage);
                currentStaticUnreadMessage = _unreadParams?.unreadMessages;
              }
            }
            if (myId == targetId && message.mentionMessage?.creatorId == initiatorId) {
              messages[i] = message.copyWith(
                showMention: !isBlocked,
              );
            }
          }

          final initiatorIds = List<int>.from(loadedState.initiatorIds);
          final targetIds = List<int>.from(loadedState.targetIds);

          if (initiatorId == myId) {
            if (isBlocked) {
              initiatorIds.add(targetId);
            } else {
              initiatorIds.remove(targetId);
            }
          }

          if (targetId == myId) {
            if (isBlocked) {
              targetIds.add(initiatorId);
            } else {
              targetIds.remove(initiatorId);
            }
          }

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
                initiatorIds: initiatorIds,
                targetIds: targetIds,
                unreadParams: _unreadParams ?? loadedState.unreadParams,
                currentTip: myId == initiatorId && isBlocked
                    ? TooltipTrigger.firstBlock
                    : loadedState.currentTip,
              )));
        }

        if (socketEvent == 'messagePinned') {
          // Открепляем
          if (data['message'] == null) {
            final updatedMessages = List<Message>.from(loadedState.messages);
            final index = updatedMessages.indexWhere((m) => m.isPinned);

            if (index != -1) {
              updatedMessages[index] = updatedMessages[index].copyWith(isPinned: false);
            }

            _updatePinnedMessage(_UpdatePinnedMessage(
              pinnedMessage: null,
              messages: updatedMessages,
            ));
            return;
          }

          // Закрепляем
          final Message pinnedMessage = Message.fromMap(data['message']);
          final updatedMessages = List<Message>.from(loadedState.messages);
          final index = updatedMessages.indexWhere((m) => m.id == pinnedMessage.id);
          final currentPinnedIndex =
              updatedMessages.indexWhere((m) => m.id == loadedState.pinnedMessage?.id);
          if (index != -1) {
            updatedMessages[index] = pinnedMessage
              ..isMyMessage = updatedMessages[index].isMyMessage;
            if (currentPinnedIndex != -1) {
              updatedMessages[currentPinnedIndex] =
                  updatedMessages[currentPinnedIndex].copyWith(isPinned: false);
            }
          }

          _updatePinnedMessage(_UpdatePinnedMessage(
            pinnedMessage: pinnedMessage,
            messages: updatedMessages,
          ));
        }
        if (socketEvent == 'userUpdated') {
          final user = User.fromMap(data['user']);
          final messages = List<Message>.from(loadedState.messages);

          // Update messages
          for (int i = 0; i < messages.length; i++) {
            final message = messages[i];
            if (message.talker.user.id == user.id) {
              messages[i] = message.copyWith(
                talker: message.talker.copyWith(user: user),
              );
            }
            if (message.mentionMessage?.talker.user.id == user.id) {
              messages[i] = message.copyWith(
                mentionMessage:
                    message.mentionMessage?.copyWith(talker: message.talker.copyWith(user: user)),
              );
            }
          }

          // Update talkers
          final talkerIndex = loadedState.talkers.indexWhere(
            (e) => e.user.id == user.id,
          );
          final talkers = List<Talker>.from(loadedState.talkers);
          if (talkerIndex != -1) {
            talkers[talkerIndex] = talkers[talkerIndex].copyWith(user: user);
          }

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
                talkers: talkers,

                // Update talker
                talker: user.id == loadedState.talker.user.id
                    ? loadedState.talker.copyWith(user: user)
                    : loadedState.talker,
              )));
        }

        if (socketEvent == 'talkerModerSet') {
          final talker = Talker.fromMap(data['talker']);
          final messages = List<Message>.from(loadedState.messages);

          // Update messages
          for (int i = 0; i < messages.length; i++) {
            final message = messages[i];
            if (message.talker.id == talker.id) {
              messages[i] = message.copyWith(
                talker: talker,
              );
            }
          }

          // Update talkers
          final talkerIndex = loadedState.talkers.indexWhere(
            (e) => e.id == talker.id,
          );
          final talkers = List<Talker>.from(loadedState.talkers);
          if (talkerIndex != -1) {
            talkers[talkerIndex] = talker;
          }

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
                talkers: talkers,

                // Update talker
                talker: talker.id == loadedState.talker.id ? talker : loadedState.talker,
              )));
          if (talker.id == loadedState.talker.id) {
            _showToast(
              text: talker.isModer ? 'Вы модератор!' : 'Вы больше не модератор',
              iconPath: Resources.warning,
            );
          }
        }

        if (socketEvent == 'roomStatusSet') {
          final room = Room.fromMap(data['room']);
          add(_UpdateLoaded((_) => loadedState.copyWith(room: room)));
          return;
        }

        if (socketEvent == 'roomSpeakSet') {
          final room = Room.fromMap(data['room']);
          add(_UpdateLoaded((_) => loadedState.copyWith(room: room)));
          return;
        }

        if (socketEvent == 'handToggled') {
          final talker = Talker.fromMap(data['talker']);
          if (talker.hand) {
            add(const ChatEvent.setCurrentTrigger(currentTrigger: TooltipTrigger.hasHands));
          }
          if (data['byModer'] == true) {
            _showToast(
                text: 'Запрос отозван администратором', iconPath: Resources.hand, isClosable: true);
          }
          add(_UpdateLoaded((loadedState) {
            // Update talkers
            final talkerIndex = loadedState.talkers.indexWhere(
              (e) => e.id == talker.id,
            );
            final talkers = List<Talker>.from(loadedState.talkers);
            if (talkerIndex != -1) {
              talkers[talkerIndex] = talker;
            }
            return loadedState.copyWith(
              talkers: talkers,
              // Update talker
              talker: talker.id == loadedState.talker.id ? talker : loadedState.talker,
            );
          }));
          return;
        }

        if (socketEvent == 'roleSet') {
          // role: SPEAKER or role: GUEST
          // если это ты то у тебя вместо руки будет выкл/вкл микро
          // если чужой талкер, то просто обнови его в списке толкеров
          final talker = Talker.fromMap(data['talker']);
          final messages = List<Message>.from(loadedState.messages);

          // Update messages
          for (int i = 0; i < messages.length; i++) {
            final message = messages[i];
            if (message.talker.id == talker.id) {
              messages[i] = message.copyWith(
                talker: talker,
              );
            }
          }
          add(_UpdateLoaded((loadedState) {
            // Update talkers
            final talkerIndex = loadedState.talkers.indexWhere(
              (e) => e.id == talker.id,
            );
            final talkers = List<Talker>.from(loadedState.talkers);
            if (talkerIndex != -1) {
              talkers[talkerIndex] = talker;
            }

            if (loadedState.talker.id == talker.id) {
              _showToast(
                text: talker.role == 'SPEAKER'
                    ? 'Вы спикер! Скажите что-нибудь'
                    : "Администратор удалил вас из спикеров",
                iconPath:
                    talker.role == 'SPEAKER' ? Resources.mic_on : Resources.remove_from_speakers,
              );
            }

            return loadedState.copyWith(
              talkers: talkers,
              messages: messages,
              // Update talker
              talker: talker.id == loadedState.talker.id ? talker : loadedState.talker,
            );
          }));
          return;
        }

        if (socketEvent == 'muteSet') {
          // isMuted: true
          // Todo(dartloli): все обновления толкеров в стейте вынести в общий метод
          final talker = Talker.fromMap(data['talker']);
          final messages = List<Message>.from(loadedState.messages);

          // Update messages
          for (int i = 0; i < messages.length; i++) {
            final message = messages[i];
            if (message.talker.id == talker.id) {
              messages[i] = message.copyWith(
                talker: talker,
              );
            }
          }

          add(_UpdateLoaded((loadedState) {
            // Update talkers
            final talkerIndex = loadedState.talkers.indexWhere(
              (e) => e.id == talker.id,
            );
            final talkers = List<Talker>.from(loadedState.talkers);
            if (talkerIndex != -1) {
              talkers[talkerIndex] = talker;
            }

            if (loadedState.talker.id == talker.id) {
              _showToast(
                text: talker.isMuted ? 'Ваш микрофон заглушен' : "Ваш микрофон включен",
                iconPath: talker.isMuted ? Resources.mic_off : Resources.mic_on,
              );
            }

            return loadedState.copyWith(
              talkers: talkers,
              messages: messages,
              // Update talker
              talker: talker.id == loadedState.talker.id ? talker : loadedState.talker,
            );
          }));
        }
        if (socketEvent == 'reactionCreated') {
          final reaction = Reaction.fromMap(data['reaction']);
          final messages = List<Message>.from(loadedState.messages);

          final updatedIndex = messages.indexWhere((message) => message.id == reaction.messageId);
          if (updatedIndex == -1) return;

          messages[updatedIndex] = messages[updatedIndex]
              .copyWith(reactions: [...?messages[updatedIndex].reactions, reaction]);

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
              )));
        }
        if (socketEvent == 'reactionDeleted') {
          final reaction = Reaction.fromMap(data['reaction']);
          final messages = List<Message>.from(loadedState.messages);

          final updatedIndex = messages.indexWhere((message) => message.id == reaction.messageId);
          if (updatedIndex == -1) return;

          final reactions = [...?messages[updatedIndex].reactions];
          reactions.remove(reaction);
          messages[updatedIndex] = messages[updatedIndex].copyWith(reactions: reactions);

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                messages: messages,
              )));
        }

        if (socketEvent == 'roomSlowmodeSet') {
          final room = Room.fromMap(data['room']);

          add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(
                room: room,
                isSlowmodeInterrupted: room.isSlowmode ? loadedState.isSlowmodeInterrupted : false,
              )));
        }
      },
    );
  }

  /// [ChatEvent.finishLoading]
  void finishLoading(ChatEvent event) => add(event);
  void _finishLoading(_FinishLoading event, Emitter<ChatState> emit) {
    final allEmotions = _getAllEmotionsUseCase.call();

    emit(ChatState.loaded(
      pinnedMessage: event.pinnedMessage,
      messages: event.messages,
      talkers: event.talkers,
      allEmotions: allEmotions,
      selectedEmotion: allEmotions.first,
      externalRoomId: event.externalRoomId,
      talker: event.talker,
      initiatorIds: event.initiatorIds,
      targetIds: event.targetIds,
      noneConnection: false,
      room: event.room,
      ban: event.ban,
      stickerPack: event.stickers,
      isSlowmodeInterrupted: event.isSlowmodeInterrupted,
      initialElapsedDelay: event.initialElapsedDelay,
    ));
  }

  List<Message> _pendingMessages = [];

  void loadMoreMessages(ChatEvent event) => add(event);

  Future<void> _loadMoreMessages(LoadMoreMessages event, Emitter<ChatState> emit) async {
    final loadedState = event.loadedState;

    if (loadedState.havePendingMessages || loadedState.isLoadedAllMessages) return;

    final lastMessageId = loadedState.messages.last.id.toString();

    final messagesResult =
        await _getMessageArrayUseCase.call(lastId: lastMessageId, limit: event.limit.toString());
    if (messagesResult.isError) return;

    _pendingMessages = messagesResult.successValue.reversed.toList();

    emit(loadedState.copyWith(
      havePendingMessages: _pendingMessages.isNotEmpty,
      isLoadedAllMessages: _pendingMessages.isEmpty,
    ));

    final messages = loadedState.messages + _pendingMessages;

    final talkers = [...loadedState.talkers];

    for (final message in messages) {
      final talkerIndex = talkers.indexWhere((talker) => talker.id == message.talker.id);
      if (talkerIndex != -1) {
        talkers[talkerIndex] = talkers[talkerIndex].copyWith(haveMessages: true);
      }
    }

    emit(loadedState.copyWith(
      messages: messages,
      havePendingMessages: false,
      isLoadedAllMessages: _pendingMessages.isEmpty,
      talkers: talkers,
    ));

    if (event.scrollToMessage != null) {
      Future.delayed(const Duration(milliseconds: 100), () {
        add(ChatEvent.scrollToMessage(message: event.scrollToMessage!));
      });
    }
  }

  List<Talker> _tryMarkHaveMessages(List<Talker> talkers, Message message) {
    final result = List<Talker>.from(talkers);
    final talkerIndex = talkers.indexWhere((talker) => talker.id == message.talker.id);
    if (talkerIndex != -1) {
      result[talkerIndex] = talkers[talkerIndex].copyWith(haveMessages: true);
    }
    return result;
  }

  void _onShowLoading(_ShowLoading event, Emitter<ChatState> emit) {
    emit(ChatState.loading());
  }

  Future<void> _onScrollToMessage(_ScrollToMessage event, Emitter<ChatState> emit) async {
    if (state.isNotA<ChatStateLoaded>()) return;
    final loadedState = state.as<ChatStateLoaded>();

    final int mentionIndex =
        loadedState.messages.indexWhere((element) => element.id == event.message.id);

    if (mentionIndex != -1) {
      _scrollController.sliverController
          .animateToIndex(
            mentionIndex,
            position: event.position ?? ListScrollPosition.middle,
            duration: const Duration(microseconds: 100),
            curve: Curves.easeIn,
          )
          .whenComplete(() => Future.delayed(const Duration(microseconds: 100), () {
                autoScrollController.highlight(mentionIndex,
                    highlightDuration: const Duration(seconds: 3));
              }));
    } else {
      loadMoreMessages(
        LoadMoreMessages(
          scrollToMessage: event.message,
          limit: loadedState.messages.last.id - event.message.id + 10,
          loadedState: loadedState,
        ),
      );
    }
  }

  UnreadParams? _updateUnreadParamsOrNull(int messageId,
      {bool messageIsRemoved = false, List<Message>? staticUnreadMessages}) {
    if (state.isNotA<ChatStateLoaded>()) return null;
    final loadedState = state.as<ChatStateLoaded>();

    final unreadMessages = staticUnreadMessages ?? loadedState.unreadParams.unreadMessages;

    final readMessageIndex = unreadMessages.indexWhere((element) => element.id == messageId);

    if (readMessageIndex == -1) return null;

    final List<Message> currentUnreadMessages = [];
    int unreadMentionsCount = 0;
    for (int i = 0; i < unreadMessages.length; i++) {
      if (i > readMessageIndex || (messageIsRemoved && readMessageIndex != i)) {
        currentUnreadMessages.add(unreadMessages[i]);
        if (unreadMessages[i].isMentionMe == true) {
          unreadMentionsCount++;
        }
      }
    }

    return UnreadParams(
      firstUnreadMessage: currentUnreadMessages.isEmpty
          ? null
          : (readMessageIndex == 0) && messageIsRemoved
              ? currentUnreadMessages.first
              : loadedState.unreadParams.firstUnreadMessage,
      unreadMentions: unreadMentionsCount,
      unreadMessages: currentUnreadMessages,
    );
  }

  Future<void> _onSetMessageRead(_SetMessageRead event, Emitter<ChatState> emit) async {
    if (state.isNotA<ChatStateLoaded>()) return;

    final unreadParams = _updateUnreadParamsOrNull(event.message.id);

    if (unreadParams == null) return;

    add(ChatEvent.updateLoaded((loadedState) => loadedState.copyWith(unreadParams: unreadParams)));
  }

  Future<void> _showEmotion(_ShowEmotion event, Emitter<ChatState> emit) async {
    if (state.isNotA<ChatStateLoaded>()) return;
    final loadedState = state.as<ChatStateLoaded>();

    if (event.isMyEmotion) {
      sendAmplitudeEvent(
          eventName: 'Social_ButtonClick_EmojiSend', optionalParams: {'emoji': event.emotion.name});
      _sendEmotionUseCase.call(
        emotion: event.emotion,
        externalRoomId: loadedState.externalRoomId,
      );
    }
  }

  Future<void> _sendMessage(_SendMessage event, Emitter<ChatState> emit) async {
    if (state.isNotA<ChatStateLoaded>()) return;
    final loaded = state.as<ChatStateLoaded>();
    if (event.text.length > 500) {
      return;
    }

    // Edit message
    final messageInputType = loaded.messageInputType;
    if (messageInputType != null) {
      if (messageInputType.isEdit) {
        sendAmplitudeEvent(eventName: 'Social_ButtonClick_MessageEdit');
        _editMessageUseCase.call(
          editMessageRequest: EditMessageRequest(
            text: event.text,
            messageId: (messageInputType as EditType).message.id.toString(),
            mentionMessageId: (messageInputType as EditType).message.mentionMessage?.id.toString(),
          ),
        );

        // Close overhang
        add(const ChatEvent.closeOverhang());
        return;
      }
    }
    // Close overhang
    emit(loaded.copyWith(messageInputType: null));
    _textEditingcontroller.clear();

    // Send message
    if (loaded.messageInputType != null && loaded.messageInputType!.isReply) {
      sendAmplitudeEvent(eventName: 'Social_ButtonClick_Reply');
    }
    _sendMessageUseCase.call(
      sendMessageRequest: SendMessageRequest(
        text: event.text,
        mentionMessageId: loaded.messageInputType?.mapOrNull(
          reply: (value) => value.message.id,
        ),
        hasPreview: loaded.messageInputType?.mapOrNull(
          link: (value) => true,
        ),
      ),
    );
  }

  void _copyMessage(_CopyMessage event, Emitter<ChatState> emit) {
    if (event.message.advertisement == null) {
      Clipboard.setData(ClipboardData(text: event.message.text));
    } else {
      final title = event.message.advertisement?.title;
      final text = event.message.advertisement?.text;
      Clipboard.setData(ClipboardData(text: (title != null ? (title + '. ') : '') + (text ?? '')));
    }

    _showToast(
      text: 'Текст сообщения скопирован',
      iconPath: Resources.copy,
    );
  }

  void _mentionMessage(_MentionMessage event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;
    emit(state
        .as<ChatStateLoaded>()
        .copyWith(messageInputType: MessageInputType.reply(message: event.message)));
    _focusNode.requestFocus();
  }

  void _onEditMessage(_EditMessage event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;
    emit(state
        .as<ChatStateLoaded>()
        .copyWith(messageInputType: MessageInputType.edit(message: event.message)));
    _textEditingcontroller.text = event.message.text;
    _focusNode.requestFocus();
  }

  void _onCloseOverhang(_CloseOverhang event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;
    if ((state as ChatStateLoaded).messageInputType!.isEdit ||
        (state as ChatStateLoaded).messageInputType!.isReply) {
      _textEditingcontroller.clear();
    }
    emit(state.as<ChatStateLoaded>().copyWith(messageInputType: null));
  }

  Future<void> _onDeleteMessage(_DeleteMessage event, Emitter<ChatState> emit) async {
    await showDialog(
      useRootNavigator: false,
      context: event.context,
      builder: (context) => ConfirmDialog(
        titleText: 'Удалить сообщение?',
        confirmButtonText: 'Удалить',
        onConfirm: () async {
          await _deleteMessageUseCase.call(
            deleteMessageRequest: DeleteMessageRequest(
              messageId: event.message.id.toString(),
            ),
          );
          Navigator.pop(context);

          _showToast(
            text: 'Сообщение удалено',
            iconPath: Resources.remove,
          );
        },
        onCancel: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  void _onUpdateLoaded(_UpdateLoaded event, Emitter<ChatState> emit) {
    _emitLoaded(emit: emit, copyWithLoaded: event.copyWithLoaded);
  }

  Future<void> _onReportMessage(_ReportMessage event, Emitter<ChatState> emit) async {
    Navigator.of(
      event.context,
      rootNavigator: false,
    ).push(
      ReportTypeScreen.route(
        userName: event.message.talker.user.name,
        onConfirm: (reportType) {
          sendAmplitudeEvent(
              eventName: "Social_ButtonClick_ComplainConfirm",
              optionalParams: {'complain_type': reportType.reportReason});
          _reportMessageUseCase.call(
            reportMessageRequest: ReportMessageRequest(
              messageId: event.message.id,
              reason: reportType.reportReason,
            ),
          );

          _showToast(
            text: 'Мы получили вашу жалобу',
            iconPath: Resources.report_badge_simple,
          );
        },
        onSelect: (ReportType reportType) {
          sendAmplitudeEvent(
              eventName: "Social_ButtonClick_Complain",
              optionalParams: {'complain_type': reportType.reportReason});
        },
        onCancel: (ReportType reportType) {
          sendAmplitudeEvent(
              eventName: "Social_ButtonClick_ComplainCancle",
              optionalParams: {'complain_type': reportType.reportReason});
        },
      ),
    );
  }

  Future<void> _onBlockUser(_BlockUser event, Emitter<ChatState> emit) async {
    await showDialog(
      useRootNavigator: false,
      context: event.context,
      builder: (context) => ConfirmDialog(
        titleText: 'Заблокировать ${event.message.talker.user.name}?',
        confirmButtonText: 'Заблокировать',
        subtitleText: 'Пользователь не сможет с вами общаться',
        onConfirm: () async {
          sendAmplitudeEvent(eventName: 'Social_ButtonClick_BanConfirm');
          Navigator.pop(context);
          await _addBlockUseCase.call(targetId: event.message.creatorId);

          _showToast(
            text: 'Пользователь заблокирован',
            iconPath: Resources.block,
          );
        },
        onCancel: () {
          sendAmplitudeEvent(eventName: 'Social_ButtonClick_BanCancel');
          Navigator.pop(context);
        },
      ),
    );
  }

  Future<void> _onChangeMessageVisibility(
    _ChangeMessageVisibility event,
    Emitter<ChatState> emit,
  ) async {
    Future<void> singleAction() async => _setMessageVisibleUseCase.call(
          setMessageVisibleRequest: SetMessageVisibleRequest(
            messageId: event.message.id,
            isVisible: event.isVisible,
          ),
        );
    Future<void> multiAction() async => _setMessagesVisibleUseCase.call(
          setMessagesVisibleRequest: SetMessagesVisibleRequest(
            userId: event.message.talker.user.id.toString(),
            isVisible: event.isVisible,
          ),
        );

    if (event.isVisible) {
      await showDialog(
        useRootNavigator: false,
        context: event.context,
        builder: (context) => MultiConfirmDialog(
          isPositiveDialog: true,
          titleText: 'Показать сообщение от ${event.message.talker.user.name}?',
          confirmButtonTexts: [
            "Показать выбранное",
            "Показать все",
          ],
          //subtitleText: 'Другие пользователи больше не смогут видеть сообщение от этого пользователя',
          onConfirms: [
            () async {
              Navigator.pop(context);

              await singleAction();
            },
            () async {
              Navigator.pop(context);

              await multiAction();
            },
          ],
          onCancel: () {
            Navigator.pop(context);
          },
        ),
      );
    } else {
      await showDialog(
        useRootNavigator: false,
        context: event.context,
        builder: (context) => MultiConfirmDialog(
          titleText: 'Скрыть сообщение от ${event.message.talker.user.name}?',
          confirmButtonTexts: [
            "Скрыть выбранное",
            "Скрыть все",
          ],
          subtitleText:
              'Другие пользователи больше не смогут видеть сообщение от этого пользователя',
          onConfirms: [
            () async {
              Navigator.pop(context);

              await singleAction();
            },
            () async {
              Navigator.pop(context);

              await multiAction();
            },
          ],
          onCancel: () {
            Navigator.pop(context);
          },
        ),
      );
    }
  }

  /// [ChatEvent.changeMessagesVisibility]
  void changeMessagesVisibility(ChatEvent event) => add(event);
  Future<void> _onChangeMessagesVisibility(
    _ChangeMessagesVisibility event,
    Emitter<ChatState> emit,
  ) async {
    Future<void> action() async => _setMessagesVisibleUseCase.call(
          setMessagesVisibleRequest: SetMessagesVisibleRequest(
            userId: event.talker.user.id.toString(),
            isVisible: event.isVisible,
          ),
        );

    if (event.isVisible) {
      await action();

      return;
    }

    await showDialog(
      useRootNavigator: false,
      context: event.context,
      builder: (context) => ConfirmDialog(
        titleText: 'Скрыть сообщения от ${event.talker.user.name}?',
        confirmButtonText: 'Скрыть',
        subtitleText: 'Другие пользователи больше не смогут видеть сообщения от этого пользователя',
        onConfirm: () async {
          Navigator.pop(context);

          await action();
        },
        onCancel: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  /// [ChatEvent.setBan]
  void setBan(ChatEvent event) => add(event);
  Future<void> _setBan(_SetBan event, Emitter<ChatState> emit) async {
    Future<void> action({int? banReason}) async => _setBanUseCase.call(
          setBanRequest: SetBanRequest(
            userId: event.talker.user.id,
            isBanned: event.isBanned,
            banReason: banReason,
          ),
        );

    if (!event.isBanned) {
      await action();

      return;
    }

    await showDialog(
        useRootNavigator: false,
        context: event.context,
        builder: (context) => ChooseBanReasonScreen(
              onItemChoosen: (banReasonIndex) async {
                await showDialog(
                  useRootNavigator: false,
                  context: event.context,
                  builder: (context) => ConfirmDialog(
                    titleText: 'Заблокировать ${event.talker.user.name}?',
                    confirmButtonText: 'Заблокировать',
                    subtitleText:
                        'Пользователь будет заблокирован за ${Strings.banReasons[banReasonIndex]}',
                    onConfirm: () async {
                      Navigator.of(context)
                        ..pop()
                        ..pop();

                      await action(banReason: banReasonIndex);
                    },
                    onCancel: () {
                      Navigator.of(context)..pop();
                    },
                  ),
                );
              },
            ));
  }

  /// Скрываем сообщения
  Message _setMessageVisibility(
    Message message,
    List<int> initiatorIds,
    List<int> targetIds,
  ) {
    // Скрываем сообщение, если автор нас заблокировал
    for (final targetId in targetIds) {
      if (message.creatorId == targetId) {
        return message.copyWith(
            isVisible: false,
            showMention: message.mentionMessage?.creatorId == targetId
                ? false
                : message.mentionMessage?.isVisible);
      }
      if (message.mentionMessage?.creatorId == targetId) {
        return message.copyWith(showMention: false);
      }
    }

    // Скрываем сообщение, если мы заблокировали автора
    for (final initiatorId in initiatorIds) {
      if (message.creatorId == initiatorId) {
        return message.copyWith(
            isVisible: false,
            showMention: message.mentionMessage?.creatorId == initiatorId
                ? false
                : message.mentionMessage?.isVisible);
      }
      if (message.mentionMessage?.creatorId == initiatorId) {
        return message.copyWith(showMention: false);
      }
    }

    return message;
  }

  Future<void> soundCheck(BuildContext context) async {
    await showDialog(
      useRootNavigator: false,
      context: context,
      builder: (context) => const SoundCheckDialog(),
    );
  }

  @override
  Future<void> close() async {
    for (final sub in _subscriptions) {
      sub.cancel();
    }
    await _leaveVoiceRoomUseCase();
    return super.close();
  }

  /// [ChatEvent.connectivityChanged]
  Future<void> _onConnectivityChanged(_ConnectivityChanged event, Emitter<ChatState> emit) async {
    await state.mapOrNull(
      loaded: (state) async {
        if (event.connectivityResult != ConnectivityResult.none) return;

        emit(state.copyWith(
          noneConnection: true,
        ));
      },
    );
  }

  /// [ChatEvent.setPinnedMessage]
  void setPinnedMessage(ChatEventSetPinnedMessage event) => add(event);
  Future<void> _onSetPinnedMessage(ChatEventSetPinnedMessage event, Emitter<ChatState> emit) async {
    final actionName = event.isPinned ? 'Закрепить' : 'Открепить';
    final snackbarActionName = event.isPinned ? 'закреплено' : 'откреплено';
    final leadingIconPath = event.isPinned ? Resources.pin : Resources.unpin;

    await showDialog(
      useRootNavigator: false,
      context: event.context,
      builder: (context) => ConfirmDialog(
        titleText: '$actionName сообщение?',
        confirmButtonText: actionName,
        onConfirm: () async {
          state.mapOrNull(
            loaded: (state) => _setPinnedMessageUseCase.call(
              setPinnedMessageRequest: SetPinnedMessageRequest(
                messageId: event.isPinned ? event.message.id.toString() : null,
              ),
            ),
          );
          Navigator.pop(context);

          _showToast(
            text: 'Сообщение $snackbarActionName',
            iconPath: leadingIconPath,
          );
        },
        onCancel: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  /// [ChatEvent.updatePinnedMessage]
  void _updatePinnedMessage(_UpdatePinnedMessage event) => add(event);
  void _onUpdatePinnedMessage(_UpdatePinnedMessage event, Emitter<ChatState> emit) {
    state.mapOrNull(
      loaded: (state) => emit(state.copyWith(
        pinnedMessage: event.pinnedMessage,
        messages: event.messages,
      )),
    );
  }

  void changeSelectedEmotion(ChangeSelectedEmotion event) => add(event);
  void _changeSelectedEmotion(ChangeSelectedEmotion event, Emitter<ChatState> emit) {
    state.mapOrNull(loaded: (state) {
      emit(state.copyWith(selectedEmotion: event.emotion));
      add(ChatEvent.showEmotion(emotion: event.emotion, isMyEmotion: true));
    });
  }

  void setEmotionPannelVisibility(SetEmotionPannelVisibility event) => add(event);
  void _setEmotionPannelVisibility(SetEmotionPannelVisibility event, Emitter<ChatState> emit) {
    if (event.isVisible) {
      sendAmplitudeEvent(eventName: 'Social_ButtonClick_EmojiPanel');
    } else {
      sendAmplitudeEvent(eventName: 'Social_ButtonClick_EmojiPanelClose');
    }

    state.mapOrNull(
      loaded: (state) {
        emit(state.copyWith(showEmojiPannel: event.isVisible));
      },
    );
  }

  void sendSelectedEmotion(SendSelectedEmotion event) => add(event);
  void _sendSelectedEmotion(SendSelectedEmotion event, Emitter<ChatState> emit) {
    state.mapOrNull(
      loaded: (state) {
        add(ChatEvent.showEmotion(emotion: state.selectedEmotion, isMyEmotion: true));
      },
    );
  }

  Future<void> _onSetContext(_SetContext event, Emitter<ChatState> emit) async {
    initShowToast(event.context);
    await sendUserEntryTime();
    state.mapOrNull(
      loaded: (state) => emit(state.copyWith(context: event.context)),
    );
  }

  void addPreviewToCache(ChatEventAddPreviewToCache event) => add(event);

  void _onAddPreviewToCache(ChatEventAddPreviewToCache event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;
    final loadedState = state as ChatStateLoaded;
    final previewCache = {...loadedState.previewCache, event.url: event.preview};
    state.mapOrNull(
      loaded: (state) => emit(state.copyWith(previewCache: previewCache)),
    );
  }

  void _onSetMicStatus(_SetMicStatus event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;

    state.mapOrNull(
      loaded: (state) => emit(state.copyWith(micStatus: event.micStatus)),
    );
  }

  void _setSwearingMessage(_SetSwearingMessage event) => add(event);

  void _onSetSwearingMessage(_SetSwearingMessage event, Emitter<ChatState> emit) {
    state.mapOrNull(loaded: (state) {
      return emit(state.copyWith(isSwearingMessage: event.isSwearingMessage));
    });
  }

  /// [ChatEvent.raiseHand]
  void toggleHand(ChatEvent event) => add(event);

  void _onToggleHand(_ToggleHand event, Emitter<ChatState> emit) {
    _toggleHandUseCase.call();

    state.mapOrNull(loaded: (loadedState) {
      if (!loadedState.talker.hand) {
        _showToast(
          text: 'Запрос отправлен',
          iconPath: Resources.hand,
        );
      }
    });
  }

  /// [ChatEvent.rejectHand]
  void rejectHand(ChatEvent event) => add(event);

  void _rejectHand(_RejectHand event, Emitter<ChatState> emit) {
    _rejectHandUseCase.call(event.rejectHandRequest);
  }

  /// [ChatEvent.rejectHand]
  void setRole(ChatEvent event) => add(event);

  Future<void> _setRole(_SetRole event, Emitter<ChatState> emit) async {
    final userName = event.talker.user.name;

    if (event.setRoleRequest.role == TalkerRole.guest) {
      await showDialog(
        useRootNavigator: false,
        context: event.context,
        builder: (context) => ConfirmDialog(
          titleText: 'Убрать $userName из спикеров?',
          confirmButtonText: 'Убрать',
          onConfirm: () async {
            Navigator.pop(context);

            await _setRoleUseCase.call(event.setRoleRequest);

            _showToast(
              text: 'Пользователь удален из спикеров',
              iconPath: Resources.remove_from_speakers,
            );
          },
          onCancel: () {
            Navigator.pop(context);
          },
        ),
      );
      return;
    }

    if (event.setRoleRequest.role == TalkerRole.speaker) {
      await _setRoleUseCase.call(event.setRoleRequest);

      _showToast(
        iconPath: Resources.speaker,
        text: '$userName теперь спикер',
      );

      return;
    }
  }

  /// [ChatEvent.setMute]
  void setMute(ChatEvent event) => add(event);

  Future<void> _setMute(_SetMute event, Emitter<ChatState> emit) async {
    if (state.isNotA<ChatStateLoaded>()) return;
    final loadedState = state as ChatStateLoaded;

    _setMuteUseCase.call(event.setMuteRequest);

    if (event.setMuteRequest.userId == loadedState.talker.user.id) {
      await _setLocalAudioUseCase.call(audioState: !event.setMuteRequest.isMuted);
    } else {
      _showToast(
        text: event.setMuteRequest.isMuted ? 'Микрофон заглушен' : "Микрофон включен",
        iconPath: event.setMuteRequest.isMuted ? Resources.mic_off : Resources.mic_on,
      );
    }
  }

  Future<void> sendUserEntryTime() async {
    if (state.isNotA<ChatStateLoaded>()) return;
    final loadedState = state as ChatStateLoaded;

    _benchMark.stop();
    final String entryTime = (_benchMark.elapsed.inMilliseconds / 1000).toString() + '000';

    final entryTimeFormatted = entryTime.substring(0, entryTime.lastIndexOf('.') + 4);

    await _calcTimeUseCase.call(
        calcTimeDto: CalcTimeRequest(
            roomId: loadedState.room.externalRoomId,
            userId: loadedState.talker.user.id,
            value: entryTimeFormatted,
            key: 'JGMHFqonZuwKeoly+bzNnw=='));
  }

  void _onSetStickerMode(_SetStickerMode event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;

    if (event.stickerMode && _focusNode.hasFocus) {
      _focusNode.unfocus();
    } else {
      if (!event.stickerMode) {
        _focusNode.requestFocus();
      }
    }
    state.mapOrNull(
      loaded: (state) => emit(state.copyWith(stickerMode: event.stickerMode)),
    );
  }

  void _onSendSticker(_SendSticker event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;

    _sendMessageUseCase.call(
      sendMessageRequest: SendStickerMessageRequest(
        stickerId: event.stickerId,
      ),
    );

    state.mapOrNull(
      loaded: (state) => emit(state.copyWith(stickerMode: false)),
    );
  }

  void _onCreateReaction(_CreateReaction event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;

    _createReactionUseCase.call(
      reactionDto: event.reactionDto,
    );
  }

  void _onDeleteReaction(_DeleteReaction event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;

    _deleteReactionUseCase.call(
      reactionDto: event.reactionDto,
    );
  }

  Future<String> getChatRules() async {
    final result = await _getChatRulesUseCase();
    if (result.isError) {
      return '';
    }
    return result.successValue.rulesHtml;
  }

  void _onSetCurrentTrigger(_SetCurrentTrigger event, Emitter<ChatState> emit) {
    if (state.isNotA<ChatStateLoaded>()) return;

    _emitLoaded(
        emit: emit, copyWithLoaded: (state) => state.copyWith(currentTip: event.currentTrigger));
  }

  void sendAmplitudeEvent({required String eventName, Map<String, dynamic>? optionalParams}) {
    if (state is ChatStateLoaded) {
      final loaded = state as ChatStateLoaded;

      final timeStampFormatted = DateTime.now().toIso8601String();

      Map<String, dynamic> eventProperties = {
        'user_id': _paramsProvider.params.userId,
        'screen_size': '${window.physicalSize.width}x${window.physicalSize.height}',
        'timeStamp': timeStampFormatted
      };

      eventProperties.addAll(loaded.analiticsDto.toMap());
      if (optionalParams != null) {
        eventProperties.addAll(optionalParams);
      }
      _amplitudeAnalitic.logEvent(eventName, eventProperties: eventProperties);
      _amplitudeAnalitic.uploadEvents();
    }
    return;
  }
}
