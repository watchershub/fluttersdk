part of 'chat_bloc.dart';

@freezed
class ChatState with _$ChatState {
  ChatState._();

  factory ChatState.loading() = LoadingState;

  factory ChatState.loaded({
    @Default([]) List<Sticker> stickerPack,
    @Default(false) bool stickerMode,
    BuildContext? context,
    Ban? ban,
    @Default(null) PermissionStatus? micStatus,
    @Default({}) Map<String, Preview> previewCache,
    @Default(false) bool largeMessage,
    @Default(false) bool isSwearingMessage,
    Message? pinnedMessage,
    @Default(false) bool isLoadedAllMessages,
    @Default(UnreadParams()) UnreadParams unreadParams,
    required String externalRoomId,
    required List<Talker> talkers,
    required List<Message> messages,
    required List<Emotion> allEmotions,
    required Talker talker,
    MessageInputType? messageInputType,
    required List<int> initiatorIds,
    required List<int> targetIds,
    @Default(false) bool noneConnection,
    @Default(false) bool showEmojiPannel,
    required Emotion selectedEmotion,
    @Default(false) bool showCounter,
    @Default(false) bool havePendingMessages,
    @Default(false) bool isSlowmodeInterrupted,
    @Default(TooltipTrigger.longTapEmoji) TooltipTrigger currentTip,
    int? initialElapsedDelay,
    required Room room,
  }) = ChatStateLoaded;

  bool get isChatEmpty => maybeMap(
        orElse: () => false,

        // Либо чат действительно пустой, либо вы гость и все сообщения скрыты модераторами
        loaded: (state) =>
            state.messages.where((message) => message.isVisible).isEmpty && !state.talker.isModer ||
            state.messages.isEmpty,
      );
}

extension ChatStateLoadedX on ChatStateLoaded {
  // ignore: avoid_positional_boolean_parameters
  bool updateShowCounter() {
    if (talkers.length >= 55) return true;
    if (talkers.length <= 45) return false;

    return showCounter;
  }

  DateTime? get startTime => room.isUpcomingEvent ? room.startTime : null;

  List<Talker> get talkersWithHands =>
      talkers.where((talker) => talker.hand && talker.role != TalkerRole.speaker.toStr).toList();

  List<Talker> get speakers =>
      talkers.where((talker) => talker.role == TalkerRole.speaker.toStr).toList();

  AnaliticsDto get analiticsDto => AnaliticsDto(
        event_id: room.externalRoomId,
        user_role: talker.role,
        user_type: talker.user.vipStatus.toUpperCase(),
      );
}

class UnreadParams {
  final Message? firstUnreadMessage;
  final List<Message> unreadMessages;
  final int unreadMentions;

  const UnreadParams({
    this.firstUnreadMessage,
    this.unreadMessages = const [],
    this.unreadMentions = 0,
  });
}
