// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'chat_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$ChatEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatEventCopyWith<$Res> {
  factory $ChatEventCopyWith(ChatEvent value, $Res Function(ChatEvent) then) =
      _$ChatEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$ChatEventCopyWithImpl<$Res> implements $ChatEventCopyWith<$Res> {
  _$ChatEventCopyWithImpl(this._value, this._then);

  final ChatEvent _value;
  // ignore: unused_field
  final $Res Function(ChatEvent) _then;
}

/// @nodoc
abstract class _$$_InitCopyWith<$Res> {
  factory _$$_InitCopyWith(_$_Init value, $Res Function(_$_Init) then) =
      __$$_InitCopyWithImpl<$Res>;
  $Res call({String externalRoomId});
}

/// @nodoc
class __$$_InitCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_InitCopyWith<$Res> {
  __$$_InitCopyWithImpl(_$_Init _value, $Res Function(_$_Init) _then)
      : super(_value, (v) => _then(v as _$_Init));

  @override
  _$_Init get _value => super._value as _$_Init;

  @override
  $Res call({
    Object? externalRoomId = freezed,
  }) {
    return _then(_$_Init(
      externalRoomId: externalRoomId == freezed
          ? _value.externalRoomId
          : externalRoomId // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_Init implements _Init {
  const _$_Init({required this.externalRoomId});

  @override
  final String externalRoomId;

  @override
  String toString() {
    return 'ChatEvent.init(externalRoomId: $externalRoomId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Init &&
            const DeepCollectionEquality()
                .equals(other.externalRoomId, externalRoomId));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(externalRoomId));

  @JsonKey(ignore: true)
  @override
  _$$_InitCopyWith<_$_Init> get copyWith =>
      __$$_InitCopyWithImpl<_$_Init>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return init(externalRoomId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return init?.call(externalRoomId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(externalRoomId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements ChatEvent {
  const factory _Init({required final String externalRoomId}) = _$_Init;

  String get externalRoomId;
  @JsonKey(ignore: true)
  _$$_InitCopyWith<_$_Init> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SendMessageCopyWith<$Res> {
  factory _$$_SendMessageCopyWith(
          _$_SendMessage value, $Res Function(_$_SendMessage) then) =
      __$$_SendMessageCopyWithImpl<$Res>;
  $Res call({BuildContext context, String text});
}

/// @nodoc
class __$$_SendMessageCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SendMessageCopyWith<$Res> {
  __$$_SendMessageCopyWithImpl(
      _$_SendMessage _value, $Res Function(_$_SendMessage) _then)
      : super(_value, (v) => _then(v as _$_SendMessage));

  @override
  _$_SendMessage get _value => super._value as _$_SendMessage;

  @override
  $Res call({
    Object? context = freezed,
    Object? text = freezed,
  }) {
    return _then(_$_SendMessage(
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
      text: text == freezed
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_SendMessage implements _SendMessage {
  const _$_SendMessage({required this.context, required this.text});

  @override
  final BuildContext context;
  @override
  final String text;

  @override
  String toString() {
    return 'ChatEvent.sendMessage(context: $context, text: $text)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendMessage &&
            const DeepCollectionEquality().equals(other.context, context) &&
            const DeepCollectionEquality().equals(other.text, text));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(context),
      const DeepCollectionEquality().hash(text));

  @JsonKey(ignore: true)
  @override
  _$$_SendMessageCopyWith<_$_SendMessage> get copyWith =>
      __$$_SendMessageCopyWithImpl<_$_SendMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return sendMessage(context, text);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return sendMessage?.call(context, text);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (sendMessage != null) {
      return sendMessage(context, text);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return sendMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return sendMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (sendMessage != null) {
      return sendMessage(this);
    }
    return orElse();
  }
}

abstract class _SendMessage implements ChatEvent {
  const factory _SendMessage(
      {required final BuildContext context,
      required final String text}) = _$_SendMessage;

  BuildContext get context;
  String get text;
  @JsonKey(ignore: true)
  _$$_SendMessageCopyWith<_$_SendMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_FinishLoadingCopyWith<$Res> {
  factory _$$_FinishLoadingCopyWith(
          _$_FinishLoading value, $Res Function(_$_FinishLoading) then) =
      __$$_FinishLoadingCopyWithImpl<$Res>;
  $Res call(
      {Message? pinnedMessage,
      List<Message> messages,
      List<Talker> talkers,
      Talker talker,
      String externalRoomId,
      List<int> initiatorIds,
      List<int> targetIds,
      Room room,
      Ban? ban,
      List<Sticker> stickers,
      bool isSlowmodeInterrupted,
      int? initialElapsedDelay});
}

/// @nodoc
class __$$_FinishLoadingCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_FinishLoadingCopyWith<$Res> {
  __$$_FinishLoadingCopyWithImpl(
      _$_FinishLoading _value, $Res Function(_$_FinishLoading) _then)
      : super(_value, (v) => _then(v as _$_FinishLoading));

  @override
  _$_FinishLoading get _value => super._value as _$_FinishLoading;

  @override
  $Res call({
    Object? pinnedMessage = freezed,
    Object? messages = freezed,
    Object? talkers = freezed,
    Object? talker = freezed,
    Object? externalRoomId = freezed,
    Object? initiatorIds = freezed,
    Object? targetIds = freezed,
    Object? room = freezed,
    Object? ban = freezed,
    Object? stickers = freezed,
    Object? isSlowmodeInterrupted = freezed,
    Object? initialElapsedDelay = freezed,
  }) {
    return _then(_$_FinishLoading(
      pinnedMessage: pinnedMessage == freezed
          ? _value.pinnedMessage
          : pinnedMessage // ignore: cast_nullable_to_non_nullable
              as Message?,
      messages: messages == freezed
          ? _value._messages
          : messages // ignore: cast_nullable_to_non_nullable
              as List<Message>,
      talkers: talkers == freezed
          ? _value._talkers
          : talkers // ignore: cast_nullable_to_non_nullable
              as List<Talker>,
      talker: talker == freezed
          ? _value.talker
          : talker // ignore: cast_nullable_to_non_nullable
              as Talker,
      externalRoomId: externalRoomId == freezed
          ? _value.externalRoomId
          : externalRoomId // ignore: cast_nullable_to_non_nullable
              as String,
      initiatorIds: initiatorIds == freezed
          ? _value._initiatorIds
          : initiatorIds // ignore: cast_nullable_to_non_nullable
              as List<int>,
      targetIds: targetIds == freezed
          ? _value._targetIds
          : targetIds // ignore: cast_nullable_to_non_nullable
              as List<int>,
      room: room == freezed
          ? _value.room
          : room // ignore: cast_nullable_to_non_nullable
              as Room,
      ban: ban == freezed
          ? _value.ban
          : ban // ignore: cast_nullable_to_non_nullable
              as Ban?,
      stickers: stickers == freezed
          ? _value._stickers
          : stickers // ignore: cast_nullable_to_non_nullable
              as List<Sticker>,
      isSlowmodeInterrupted: isSlowmodeInterrupted == freezed
          ? _value.isSlowmodeInterrupted
          : isSlowmodeInterrupted // ignore: cast_nullable_to_non_nullable
              as bool,
      initialElapsedDelay: initialElapsedDelay == freezed
          ? _value.initialElapsedDelay
          : initialElapsedDelay // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$_FinishLoading implements _FinishLoading {
  const _$_FinishLoading(
      {this.pinnedMessage,
      required final List<Message> messages,
      required final List<Talker> talkers,
      required this.talker,
      required this.externalRoomId,
      required final List<int> initiatorIds,
      required final List<int> targetIds,
      required this.room,
      this.ban,
      required final List<Sticker> stickers,
      required this.isSlowmodeInterrupted,
      this.initialElapsedDelay})
      : _messages = messages,
        _talkers = talkers,
        _initiatorIds = initiatorIds,
        _targetIds = targetIds,
        _stickers = stickers;

  @override
  final Message? pinnedMessage;
  final List<Message> _messages;
  @override
  List<Message> get messages {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_messages);
  }

  final List<Talker> _talkers;
  @override
  List<Talker> get talkers {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_talkers);
  }

  @override
  final Talker talker;
  @override
  final String externalRoomId;
  final List<int> _initiatorIds;
  @override
  List<int> get initiatorIds {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_initiatorIds);
  }

  final List<int> _targetIds;
  @override
  List<int> get targetIds {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_targetIds);
  }

  @override
  final Room room;
  @override
  final Ban? ban;
  final List<Sticker> _stickers;
  @override
  List<Sticker> get stickers {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_stickers);
  }

  @override
  final bool isSlowmodeInterrupted;
  @override
  final int? initialElapsedDelay;

  @override
  String toString() {
    return 'ChatEvent.finishLoading(pinnedMessage: $pinnedMessage, messages: $messages, talkers: $talkers, talker: $talker, externalRoomId: $externalRoomId, initiatorIds: $initiatorIds, targetIds: $targetIds, room: $room, ban: $ban, stickers: $stickers, isSlowmodeInterrupted: $isSlowmodeInterrupted, initialElapsedDelay: $initialElapsedDelay)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FinishLoading &&
            const DeepCollectionEquality()
                .equals(other.pinnedMessage, pinnedMessage) &&
            const DeepCollectionEquality().equals(other._messages, _messages) &&
            const DeepCollectionEquality().equals(other._talkers, _talkers) &&
            const DeepCollectionEquality().equals(other.talker, talker) &&
            const DeepCollectionEquality()
                .equals(other.externalRoomId, externalRoomId) &&
            const DeepCollectionEquality()
                .equals(other._initiatorIds, _initiatorIds) &&
            const DeepCollectionEquality()
                .equals(other._targetIds, _targetIds) &&
            const DeepCollectionEquality().equals(other.room, room) &&
            const DeepCollectionEquality().equals(other.ban, ban) &&
            const DeepCollectionEquality().equals(other._stickers, _stickers) &&
            const DeepCollectionEquality()
                .equals(other.isSlowmodeInterrupted, isSlowmodeInterrupted) &&
            const DeepCollectionEquality()
                .equals(other.initialElapsedDelay, initialElapsedDelay));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(pinnedMessage),
      const DeepCollectionEquality().hash(_messages),
      const DeepCollectionEquality().hash(_talkers),
      const DeepCollectionEquality().hash(talker),
      const DeepCollectionEquality().hash(externalRoomId),
      const DeepCollectionEquality().hash(_initiatorIds),
      const DeepCollectionEquality().hash(_targetIds),
      const DeepCollectionEquality().hash(room),
      const DeepCollectionEquality().hash(ban),
      const DeepCollectionEquality().hash(_stickers),
      const DeepCollectionEquality().hash(isSlowmodeInterrupted),
      const DeepCollectionEquality().hash(initialElapsedDelay));

  @JsonKey(ignore: true)
  @override
  _$$_FinishLoadingCopyWith<_$_FinishLoading> get copyWith =>
      __$$_FinishLoadingCopyWithImpl<_$_FinishLoading>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return finishLoading(
        pinnedMessage,
        messages,
        talkers,
        talker,
        externalRoomId,
        initiatorIds,
        targetIds,
        room,
        ban,
        stickers,
        isSlowmodeInterrupted,
        initialElapsedDelay);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return finishLoading?.call(
        pinnedMessage,
        messages,
        talkers,
        talker,
        externalRoomId,
        initiatorIds,
        targetIds,
        room,
        ban,
        stickers,
        isSlowmodeInterrupted,
        initialElapsedDelay);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (finishLoading != null) {
      return finishLoading(
          pinnedMessage,
          messages,
          talkers,
          talker,
          externalRoomId,
          initiatorIds,
          targetIds,
          room,
          ban,
          stickers,
          isSlowmodeInterrupted,
          initialElapsedDelay);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return finishLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return finishLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (finishLoading != null) {
      return finishLoading(this);
    }
    return orElse();
  }
}

abstract class _FinishLoading implements ChatEvent {
  const factory _FinishLoading(
      {final Message? pinnedMessage,
      required final List<Message> messages,
      required final List<Talker> talkers,
      required final Talker talker,
      required final String externalRoomId,
      required final List<int> initiatorIds,
      required final List<int> targetIds,
      required final Room room,
      final Ban? ban,
      required final List<Sticker> stickers,
      required final bool isSlowmodeInterrupted,
      final int? initialElapsedDelay}) = _$_FinishLoading;

  Message? get pinnedMessage;
  List<Message> get messages;
  List<Talker> get talkers;
  Talker get talker;
  String get externalRoomId;
  List<int> get initiatorIds;
  List<int> get targetIds;
  Room get room;
  Ban? get ban;
  List<Sticker> get stickers;
  bool get isSlowmodeInterrupted;
  int? get initialElapsedDelay;
  @JsonKey(ignore: true)
  _$$_FinishLoadingCopyWith<_$_FinishLoading> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_FetchChatCopyWith<$Res> {
  factory _$$_FetchChatCopyWith(
          _$_FetchChat value, $Res Function(_$_FetchChat) then) =
      __$$_FetchChatCopyWithImpl<$Res>;
  $Res call(
      {String externalRoomId, List<Message> messages, List<Talker> talkers});
}

/// @nodoc
class __$$_FetchChatCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_FetchChatCopyWith<$Res> {
  __$$_FetchChatCopyWithImpl(
      _$_FetchChat _value, $Res Function(_$_FetchChat) _then)
      : super(_value, (v) => _then(v as _$_FetchChat));

  @override
  _$_FetchChat get _value => super._value as _$_FetchChat;

  @override
  $Res call({
    Object? externalRoomId = freezed,
    Object? messages = freezed,
    Object? talkers = freezed,
  }) {
    return _then(_$_FetchChat(
      externalRoomId: externalRoomId == freezed
          ? _value.externalRoomId
          : externalRoomId // ignore: cast_nullable_to_non_nullable
              as String,
      messages: messages == freezed
          ? _value._messages
          : messages // ignore: cast_nullable_to_non_nullable
              as List<Message>,
      talkers: talkers == freezed
          ? _value._talkers
          : talkers // ignore: cast_nullable_to_non_nullable
              as List<Talker>,
    ));
  }
}

/// @nodoc

class _$_FetchChat implements _FetchChat {
  const _$_FetchChat(
      {required this.externalRoomId,
      required final List<Message> messages,
      required final List<Talker> talkers})
      : _messages = messages,
        _talkers = talkers;

  @override
  final String externalRoomId;
  final List<Message> _messages;
  @override
  List<Message> get messages {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_messages);
  }

  final List<Talker> _talkers;
  @override
  List<Talker> get talkers {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_talkers);
  }

  @override
  String toString() {
    return 'ChatEvent.fetchChat(externalRoomId: $externalRoomId, messages: $messages, talkers: $talkers)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_FetchChat &&
            const DeepCollectionEquality()
                .equals(other.externalRoomId, externalRoomId) &&
            const DeepCollectionEquality().equals(other._messages, _messages) &&
            const DeepCollectionEquality().equals(other._talkers, _talkers));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(externalRoomId),
      const DeepCollectionEquality().hash(_messages),
      const DeepCollectionEquality().hash(_talkers));

  @JsonKey(ignore: true)
  @override
  _$$_FetchChatCopyWith<_$_FetchChat> get copyWith =>
      __$$_FetchChatCopyWithImpl<_$_FetchChat>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return fetchChat(externalRoomId, messages, talkers);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return fetchChat?.call(externalRoomId, messages, talkers);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (fetchChat != null) {
      return fetchChat(externalRoomId, messages, talkers);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return fetchChat(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return fetchChat?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (fetchChat != null) {
      return fetchChat(this);
    }
    return orElse();
  }
}

abstract class _FetchChat implements ChatEvent {
  const factory _FetchChat(
      {required final String externalRoomId,
      required final List<Message> messages,
      required final List<Talker> talkers}) = _$_FetchChat;

  String get externalRoomId;
  List<Message> get messages;
  List<Talker> get talkers;
  @JsonKey(ignore: true)
  _$$_FetchChatCopyWith<_$_FetchChat> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ShowEmotionCopyWith<$Res> {
  factory _$$_ShowEmotionCopyWith(
          _$_ShowEmotion value, $Res Function(_$_ShowEmotion) then) =
      __$$_ShowEmotionCopyWithImpl<$Res>;
  $Res call({Emotion emotion, bool isMyEmotion});
}

/// @nodoc
class __$$_ShowEmotionCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_ShowEmotionCopyWith<$Res> {
  __$$_ShowEmotionCopyWithImpl(
      _$_ShowEmotion _value, $Res Function(_$_ShowEmotion) _then)
      : super(_value, (v) => _then(v as _$_ShowEmotion));

  @override
  _$_ShowEmotion get _value => super._value as _$_ShowEmotion;

  @override
  $Res call({
    Object? emotion = freezed,
    Object? isMyEmotion = freezed,
  }) {
    return _then(_$_ShowEmotion(
      emotion: emotion == freezed
          ? _value.emotion
          : emotion // ignore: cast_nullable_to_non_nullable
              as Emotion,
      isMyEmotion: isMyEmotion == freezed
          ? _value.isMyEmotion
          : isMyEmotion // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_ShowEmotion implements _ShowEmotion {
  const _$_ShowEmotion({required this.emotion, required this.isMyEmotion});

  @override
  final Emotion emotion;
  @override
  final bool isMyEmotion;

  @override
  String toString() {
    return 'ChatEvent.showEmotion(emotion: $emotion, isMyEmotion: $isMyEmotion)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ShowEmotion &&
            const DeepCollectionEquality().equals(other.emotion, emotion) &&
            const DeepCollectionEquality()
                .equals(other.isMyEmotion, isMyEmotion));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(emotion),
      const DeepCollectionEquality().hash(isMyEmotion));

  @JsonKey(ignore: true)
  @override
  _$$_ShowEmotionCopyWith<_$_ShowEmotion> get copyWith =>
      __$$_ShowEmotionCopyWithImpl<_$_ShowEmotion>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return showEmotion(emotion, isMyEmotion);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return showEmotion?.call(emotion, isMyEmotion);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (showEmotion != null) {
      return showEmotion(emotion, isMyEmotion);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return showEmotion(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return showEmotion?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (showEmotion != null) {
      return showEmotion(this);
    }
    return orElse();
  }
}

abstract class _ShowEmotion implements ChatEvent {
  const factory _ShowEmotion(
      {required final Emotion emotion,
      required final bool isMyEmotion}) = _$_ShowEmotion;

  Emotion get emotion;
  bool get isMyEmotion;
  @JsonKey(ignore: true)
  _$$_ShowEmotionCopyWith<_$_ShowEmotion> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_MentionMessageCopyWith<$Res> {
  factory _$$_MentionMessageCopyWith(
          _$_MentionMessage value, $Res Function(_$_MentionMessage) then) =
      __$$_MentionMessageCopyWithImpl<$Res>;
  $Res call({Message message});
}

/// @nodoc
class __$$_MentionMessageCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_MentionMessageCopyWith<$Res> {
  __$$_MentionMessageCopyWithImpl(
      _$_MentionMessage _value, $Res Function(_$_MentionMessage) _then)
      : super(_value, (v) => _then(v as _$_MentionMessage));

  @override
  _$_MentionMessage get _value => super._value as _$_MentionMessage;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$_MentionMessage(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
    ));
  }
}

/// @nodoc

class _$_MentionMessage implements _MentionMessage {
  const _$_MentionMessage({required this.message});

  @override
  final Message message;

  @override
  String toString() {
    return 'ChatEvent.mentionMessage(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_MentionMessage &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$_MentionMessageCopyWith<_$_MentionMessage> get copyWith =>
      __$$_MentionMessageCopyWithImpl<_$_MentionMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return mentionMessage(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return mentionMessage?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (mentionMessage != null) {
      return mentionMessage(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return mentionMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return mentionMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (mentionMessage != null) {
      return mentionMessage(this);
    }
    return orElse();
  }
}

abstract class _MentionMessage implements ChatEvent {
  const factory _MentionMessage({required final Message message}) =
      _$_MentionMessage;

  Message get message;
  @JsonKey(ignore: true)
  _$$_MentionMessageCopyWith<_$_MentionMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_CopyMessageCopyWith<$Res> {
  factory _$$_CopyMessageCopyWith(
          _$_CopyMessage value, $Res Function(_$_CopyMessage) then) =
      __$$_CopyMessageCopyWithImpl<$Res>;
  $Res call({BuildContext context, Message message});
}

/// @nodoc
class __$$_CopyMessageCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_CopyMessageCopyWith<$Res> {
  __$$_CopyMessageCopyWithImpl(
      _$_CopyMessage _value, $Res Function(_$_CopyMessage) _then)
      : super(_value, (v) => _then(v as _$_CopyMessage));

  @override
  _$_CopyMessage get _value => super._value as _$_CopyMessage;

  @override
  $Res call({
    Object? context = freezed,
    Object? message = freezed,
  }) {
    return _then(_$_CopyMessage(
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
    ));
  }
}

/// @nodoc

class _$_CopyMessage implements _CopyMessage {
  const _$_CopyMessage({required this.context, required this.message});

  @override
  final BuildContext context;
  @override
  final Message message;

  @override
  String toString() {
    return 'ChatEvent.copyMessage(context: $context, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CopyMessage &&
            const DeepCollectionEquality().equals(other.context, context) &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(context),
      const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$_CopyMessageCopyWith<_$_CopyMessage> get copyWith =>
      __$$_CopyMessageCopyWithImpl<_$_CopyMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return copyMessage(context, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return copyMessage?.call(context, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (copyMessage != null) {
      return copyMessage(context, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return copyMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return copyMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (copyMessage != null) {
      return copyMessage(this);
    }
    return orElse();
  }
}

abstract class _CopyMessage implements ChatEvent {
  const factory _CopyMessage(
      {required final BuildContext context,
      required final Message message}) = _$_CopyMessage;

  BuildContext get context;
  Message get message;
  @JsonKey(ignore: true)
  _$$_CopyMessageCopyWith<_$_CopyMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_EditMessageCopyWith<$Res> {
  factory _$$_EditMessageCopyWith(
          _$_EditMessage value, $Res Function(_$_EditMessage) then) =
      __$$_EditMessageCopyWithImpl<$Res>;
  $Res call({Message message});
}

/// @nodoc
class __$$_EditMessageCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_EditMessageCopyWith<$Res> {
  __$$_EditMessageCopyWithImpl(
      _$_EditMessage _value, $Res Function(_$_EditMessage) _then)
      : super(_value, (v) => _then(v as _$_EditMessage));

  @override
  _$_EditMessage get _value => super._value as _$_EditMessage;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$_EditMessage(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
    ));
  }
}

/// @nodoc

class _$_EditMessage implements _EditMessage {
  const _$_EditMessage({required this.message});

  @override
  final Message message;

  @override
  String toString() {
    return 'ChatEvent.editMessage(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_EditMessage &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$_EditMessageCopyWith<_$_EditMessage> get copyWith =>
      __$$_EditMessageCopyWithImpl<_$_EditMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return editMessage(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return editMessage?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (editMessage != null) {
      return editMessage(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return editMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return editMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (editMessage != null) {
      return editMessage(this);
    }
    return orElse();
  }
}

abstract class _EditMessage implements ChatEvent {
  const factory _EditMessage({required final Message message}) = _$_EditMessage;

  Message get message;
  @JsonKey(ignore: true)
  _$$_EditMessageCopyWith<_$_EditMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_CloseOverhangCopyWith<$Res> {
  factory _$$_CloseOverhangCopyWith(
          _$_CloseOverhang value, $Res Function(_$_CloseOverhang) then) =
      __$$_CloseOverhangCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CloseOverhangCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_CloseOverhangCopyWith<$Res> {
  __$$_CloseOverhangCopyWithImpl(
      _$_CloseOverhang _value, $Res Function(_$_CloseOverhang) _then)
      : super(_value, (v) => _then(v as _$_CloseOverhang));

  @override
  _$_CloseOverhang get _value => super._value as _$_CloseOverhang;
}

/// @nodoc

class _$_CloseOverhang implements _CloseOverhang {
  const _$_CloseOverhang();

  @override
  String toString() {
    return 'ChatEvent.closeOverhang()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_CloseOverhang);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return closeOverhang();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return closeOverhang?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (closeOverhang != null) {
      return closeOverhang();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return closeOverhang(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return closeOverhang?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (closeOverhang != null) {
      return closeOverhang(this);
    }
    return orElse();
  }
}

abstract class _CloseOverhang implements ChatEvent {
  const factory _CloseOverhang() = _$_CloseOverhang;
}

/// @nodoc
abstract class _$$_DeleteMessageCopyWith<$Res> {
  factory _$$_DeleteMessageCopyWith(
          _$_DeleteMessage value, $Res Function(_$_DeleteMessage) then) =
      __$$_DeleteMessageCopyWithImpl<$Res>;
  $Res call({Message message, BuildContext context});
}

/// @nodoc
class __$$_DeleteMessageCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_DeleteMessageCopyWith<$Res> {
  __$$_DeleteMessageCopyWithImpl(
      _$_DeleteMessage _value, $Res Function(_$_DeleteMessage) _then)
      : super(_value, (v) => _then(v as _$_DeleteMessage));

  @override
  _$_DeleteMessage get _value => super._value as _$_DeleteMessage;

  @override
  $Res call({
    Object? message = freezed,
    Object? context = freezed,
  }) {
    return _then(_$_DeleteMessage(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
    ));
  }
}

/// @nodoc

class _$_DeleteMessage implements _DeleteMessage {
  const _$_DeleteMessage({required this.message, required this.context});

  @override
  final Message message;
  @override
  final BuildContext context;

  @override
  String toString() {
    return 'ChatEvent.deleteMessage(message: $message, context: $context)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DeleteMessage &&
            const DeepCollectionEquality().equals(other.message, message) &&
            const DeepCollectionEquality().equals(other.context, context));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(message),
      const DeepCollectionEquality().hash(context));

  @JsonKey(ignore: true)
  @override
  _$$_DeleteMessageCopyWith<_$_DeleteMessage> get copyWith =>
      __$$_DeleteMessageCopyWithImpl<_$_DeleteMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return deleteMessage(message, context);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return deleteMessage?.call(message, context);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (deleteMessage != null) {
      return deleteMessage(message, context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return deleteMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return deleteMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (deleteMessage != null) {
      return deleteMessage(this);
    }
    return orElse();
  }
}

abstract class _DeleteMessage implements ChatEvent {
  const factory _DeleteMessage(
      {required final Message message,
      required final BuildContext context}) = _$_DeleteMessage;

  Message get message;
  BuildContext get context;
  @JsonKey(ignore: true)
  _$$_DeleteMessageCopyWith<_$_DeleteMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdateLoadedCopyWith<$Res> {
  factory _$$_UpdateLoadedCopyWith(
          _$_UpdateLoaded value, $Res Function(_$_UpdateLoaded) then) =
      __$$_UpdateLoadedCopyWithImpl<$Res>;
  $Res call({CopyWithLoaded copyWithLoaded});
}

/// @nodoc
class __$$_UpdateLoadedCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_UpdateLoadedCopyWith<$Res> {
  __$$_UpdateLoadedCopyWithImpl(
      _$_UpdateLoaded _value, $Res Function(_$_UpdateLoaded) _then)
      : super(_value, (v) => _then(v as _$_UpdateLoaded));

  @override
  _$_UpdateLoaded get _value => super._value as _$_UpdateLoaded;

  @override
  $Res call({
    Object? copyWithLoaded = freezed,
  }) {
    return _then(_$_UpdateLoaded(
      copyWithLoaded == freezed
          ? _value.copyWithLoaded
          : copyWithLoaded // ignore: cast_nullable_to_non_nullable
              as CopyWithLoaded,
    ));
  }
}

/// @nodoc

class _$_UpdateLoaded implements _UpdateLoaded {
  const _$_UpdateLoaded(this.copyWithLoaded);

  @override
  final CopyWithLoaded copyWithLoaded;

  @override
  String toString() {
    return 'ChatEvent.updateLoaded(copyWithLoaded: $copyWithLoaded)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateLoaded &&
            (identical(other.copyWithLoaded, copyWithLoaded) ||
                other.copyWithLoaded == copyWithLoaded));
  }

  @override
  int get hashCode => Object.hash(runtimeType, copyWithLoaded);

  @JsonKey(ignore: true)
  @override
  _$$_UpdateLoadedCopyWith<_$_UpdateLoaded> get copyWith =>
      __$$_UpdateLoadedCopyWithImpl<_$_UpdateLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return updateLoaded(copyWithLoaded);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return updateLoaded?.call(copyWithLoaded);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (updateLoaded != null) {
      return updateLoaded(copyWithLoaded);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return updateLoaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return updateLoaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (updateLoaded != null) {
      return updateLoaded(this);
    }
    return orElse();
  }
}

abstract class _UpdateLoaded implements ChatEvent {
  const factory _UpdateLoaded(final CopyWithLoaded copyWithLoaded) =
      _$_UpdateLoaded;

  CopyWithLoaded get copyWithLoaded;
  @JsonKey(ignore: true)
  _$$_UpdateLoadedCopyWith<_$_UpdateLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ReportMessageCopyWith<$Res> {
  factory _$$_ReportMessageCopyWith(
          _$_ReportMessage value, $Res Function(_$_ReportMessage) then) =
      __$$_ReportMessageCopyWithImpl<$Res>;
  $Res call({BuildContext context, Message message});
}

/// @nodoc
class __$$_ReportMessageCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_ReportMessageCopyWith<$Res> {
  __$$_ReportMessageCopyWithImpl(
      _$_ReportMessage _value, $Res Function(_$_ReportMessage) _then)
      : super(_value, (v) => _then(v as _$_ReportMessage));

  @override
  _$_ReportMessage get _value => super._value as _$_ReportMessage;

  @override
  $Res call({
    Object? context = freezed,
    Object? message = freezed,
  }) {
    return _then(_$_ReportMessage(
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
    ));
  }
}

/// @nodoc

class _$_ReportMessage implements _ReportMessage {
  const _$_ReportMessage({required this.context, required this.message});

  @override
  final BuildContext context;
  @override
  final Message message;

  @override
  String toString() {
    return 'ChatEvent.reportMessage(context: $context, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ReportMessage &&
            const DeepCollectionEquality().equals(other.context, context) &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(context),
      const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$_ReportMessageCopyWith<_$_ReportMessage> get copyWith =>
      __$$_ReportMessageCopyWithImpl<_$_ReportMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return reportMessage(context, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return reportMessage?.call(context, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (reportMessage != null) {
      return reportMessage(context, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return reportMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return reportMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (reportMessage != null) {
      return reportMessage(this);
    }
    return orElse();
  }
}

abstract class _ReportMessage implements ChatEvent {
  const factory _ReportMessage(
      {required final BuildContext context,
      required final Message message}) = _$_ReportMessage;

  BuildContext get context;
  Message get message;
  @JsonKey(ignore: true)
  _$$_ReportMessageCopyWith<_$_ReportMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_BlockUserCopyWith<$Res> {
  factory _$$_BlockUserCopyWith(
          _$_BlockUser value, $Res Function(_$_BlockUser) then) =
      __$$_BlockUserCopyWithImpl<$Res>;
  $Res call({BuildContext context, Message message});
}

/// @nodoc
class __$$_BlockUserCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_BlockUserCopyWith<$Res> {
  __$$_BlockUserCopyWithImpl(
      _$_BlockUser _value, $Res Function(_$_BlockUser) _then)
      : super(_value, (v) => _then(v as _$_BlockUser));

  @override
  _$_BlockUser get _value => super._value as _$_BlockUser;

  @override
  $Res call({
    Object? context = freezed,
    Object? message = freezed,
  }) {
    return _then(_$_BlockUser(
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
    ));
  }
}

/// @nodoc

class _$_BlockUser implements _BlockUser {
  const _$_BlockUser({required this.context, required this.message});

  @override
  final BuildContext context;
  @override
  final Message message;

  @override
  String toString() {
    return 'ChatEvent.blockUser(context: $context, message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BlockUser &&
            const DeepCollectionEquality().equals(other.context, context) &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(context),
      const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$_BlockUserCopyWith<_$_BlockUser> get copyWith =>
      __$$_BlockUserCopyWithImpl<_$_BlockUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return blockUser(context, message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return blockUser?.call(context, message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (blockUser != null) {
      return blockUser(context, message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return blockUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return blockUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (blockUser != null) {
      return blockUser(this);
    }
    return orElse();
  }
}

abstract class _BlockUser implements ChatEvent {
  const factory _BlockUser(
      {required final BuildContext context,
      required final Message message}) = _$_BlockUser;

  BuildContext get context;
  Message get message;
  @JsonKey(ignore: true)
  _$$_BlockUserCopyWith<_$_BlockUser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ChangeMessageVisibilityCopyWith<$Res> {
  factory _$$_ChangeMessageVisibilityCopyWith(_$_ChangeMessageVisibility value,
          $Res Function(_$_ChangeMessageVisibility) then) =
      __$$_ChangeMessageVisibilityCopyWithImpl<$Res>;
  $Res call({Message message, bool isVisible, BuildContext context});
}

/// @nodoc
class __$$_ChangeMessageVisibilityCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_ChangeMessageVisibilityCopyWith<$Res> {
  __$$_ChangeMessageVisibilityCopyWithImpl(_$_ChangeMessageVisibility _value,
      $Res Function(_$_ChangeMessageVisibility) _then)
      : super(_value, (v) => _then(v as _$_ChangeMessageVisibility));

  @override
  _$_ChangeMessageVisibility get _value =>
      super._value as _$_ChangeMessageVisibility;

  @override
  $Res call({
    Object? message = freezed,
    Object? isVisible = freezed,
    Object? context = freezed,
  }) {
    return _then(_$_ChangeMessageVisibility(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
      isVisible: isVisible == freezed
          ? _value.isVisible
          : isVisible // ignore: cast_nullable_to_non_nullable
              as bool,
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
    ));
  }
}

/// @nodoc

class _$_ChangeMessageVisibility implements _ChangeMessageVisibility {
  const _$_ChangeMessageVisibility(
      {required this.message, required this.isVisible, required this.context});

  @override
  final Message message;
  @override
  final bool isVisible;
  @override
  final BuildContext context;

  @override
  String toString() {
    return 'ChatEvent.changeMessageVisibility(message: $message, isVisible: $isVisible, context: $context)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChangeMessageVisibility &&
            const DeepCollectionEquality().equals(other.message, message) &&
            const DeepCollectionEquality().equals(other.isVisible, isVisible) &&
            const DeepCollectionEquality().equals(other.context, context));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(message),
      const DeepCollectionEquality().hash(isVisible),
      const DeepCollectionEquality().hash(context));

  @JsonKey(ignore: true)
  @override
  _$$_ChangeMessageVisibilityCopyWith<_$_ChangeMessageVisibility>
      get copyWith =>
          __$$_ChangeMessageVisibilityCopyWithImpl<_$_ChangeMessageVisibility>(
              this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return changeMessageVisibility(message, isVisible, context);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return changeMessageVisibility?.call(message, isVisible, context);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (changeMessageVisibility != null) {
      return changeMessageVisibility(message, isVisible, context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return changeMessageVisibility(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return changeMessageVisibility?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (changeMessageVisibility != null) {
      return changeMessageVisibility(this);
    }
    return orElse();
  }
}

abstract class _ChangeMessageVisibility implements ChatEvent {
  const factory _ChangeMessageVisibility(
      {required final Message message,
      required final bool isVisible,
      required final BuildContext context}) = _$_ChangeMessageVisibility;

  Message get message;
  bool get isVisible;
  BuildContext get context;
  @JsonKey(ignore: true)
  _$$_ChangeMessageVisibilityCopyWith<_$_ChangeMessageVisibility>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ChangeMessagesVisibilityCopyWith<$Res> {
  factory _$$_ChangeMessagesVisibilityCopyWith(
          _$_ChangeMessagesVisibility value,
          $Res Function(_$_ChangeMessagesVisibility) then) =
      __$$_ChangeMessagesVisibilityCopyWithImpl<$Res>;
  $Res call({Talker talker, bool isVisible, BuildContext context});
}

/// @nodoc
class __$$_ChangeMessagesVisibilityCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_ChangeMessagesVisibilityCopyWith<$Res> {
  __$$_ChangeMessagesVisibilityCopyWithImpl(_$_ChangeMessagesVisibility _value,
      $Res Function(_$_ChangeMessagesVisibility) _then)
      : super(_value, (v) => _then(v as _$_ChangeMessagesVisibility));

  @override
  _$_ChangeMessagesVisibility get _value =>
      super._value as _$_ChangeMessagesVisibility;

  @override
  $Res call({
    Object? talker = freezed,
    Object? isVisible = freezed,
    Object? context = freezed,
  }) {
    return _then(_$_ChangeMessagesVisibility(
      talker: talker == freezed
          ? _value.talker
          : talker // ignore: cast_nullable_to_non_nullable
              as Talker,
      isVisible: isVisible == freezed
          ? _value.isVisible
          : isVisible // ignore: cast_nullable_to_non_nullable
              as bool,
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
    ));
  }
}

/// @nodoc

class _$_ChangeMessagesVisibility implements _ChangeMessagesVisibility {
  const _$_ChangeMessagesVisibility(
      {required this.talker, required this.isVisible, required this.context});

  @override
  final Talker talker;
  @override
  final bool isVisible;
  @override
  final BuildContext context;

  @override
  String toString() {
    return 'ChatEvent.changeMessagesVisibility(talker: $talker, isVisible: $isVisible, context: $context)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChangeMessagesVisibility &&
            const DeepCollectionEquality().equals(other.talker, talker) &&
            const DeepCollectionEquality().equals(other.isVisible, isVisible) &&
            const DeepCollectionEquality().equals(other.context, context));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(talker),
      const DeepCollectionEquality().hash(isVisible),
      const DeepCollectionEquality().hash(context));

  @JsonKey(ignore: true)
  @override
  _$$_ChangeMessagesVisibilityCopyWith<_$_ChangeMessagesVisibility>
      get copyWith => __$$_ChangeMessagesVisibilityCopyWithImpl<
          _$_ChangeMessagesVisibility>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return changeMessagesVisibility(talker, isVisible, context);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return changeMessagesVisibility?.call(talker, isVisible, context);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (changeMessagesVisibility != null) {
      return changeMessagesVisibility(talker, isVisible, context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return changeMessagesVisibility(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return changeMessagesVisibility?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (changeMessagesVisibility != null) {
      return changeMessagesVisibility(this);
    }
    return orElse();
  }
}

abstract class _ChangeMessagesVisibility implements ChatEvent {
  const factory _ChangeMessagesVisibility(
      {required final Talker talker,
      required final bool isVisible,
      required final BuildContext context}) = _$_ChangeMessagesVisibility;

  Talker get talker;
  bool get isVisible;
  BuildContext get context;
  @JsonKey(ignore: true)
  _$$_ChangeMessagesVisibilityCopyWith<_$_ChangeMessagesVisibility>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetBanCopyWith<$Res> {
  factory _$$_SetBanCopyWith(_$_SetBan value, $Res Function(_$_SetBan) then) =
      __$$_SetBanCopyWithImpl<$Res>;
  $Res call({Talker talker, bool isBanned, BuildContext context});
}

/// @nodoc
class __$$_SetBanCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetBanCopyWith<$Res> {
  __$$_SetBanCopyWithImpl(_$_SetBan _value, $Res Function(_$_SetBan) _then)
      : super(_value, (v) => _then(v as _$_SetBan));

  @override
  _$_SetBan get _value => super._value as _$_SetBan;

  @override
  $Res call({
    Object? talker = freezed,
    Object? isBanned = freezed,
    Object? context = freezed,
  }) {
    return _then(_$_SetBan(
      talker: talker == freezed
          ? _value.talker
          : talker // ignore: cast_nullable_to_non_nullable
              as Talker,
      isBanned: isBanned == freezed
          ? _value.isBanned
          : isBanned // ignore: cast_nullable_to_non_nullable
              as bool,
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
    ));
  }
}

/// @nodoc

class _$_SetBan implements _SetBan {
  const _$_SetBan(
      {required this.talker, required this.isBanned, required this.context});

  @override
  final Talker talker;
  @override
  final bool isBanned;
  @override
  final BuildContext context;

  @override
  String toString() {
    return 'ChatEvent.setBan(talker: $talker, isBanned: $isBanned, context: $context)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetBan &&
            const DeepCollectionEquality().equals(other.talker, talker) &&
            const DeepCollectionEquality().equals(other.isBanned, isBanned) &&
            const DeepCollectionEquality().equals(other.context, context));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(talker),
      const DeepCollectionEquality().hash(isBanned),
      const DeepCollectionEquality().hash(context));

  @JsonKey(ignore: true)
  @override
  _$$_SetBanCopyWith<_$_SetBan> get copyWith =>
      __$$_SetBanCopyWithImpl<_$_SetBan>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setBan(talker, isBanned, context);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setBan?.call(talker, isBanned, context);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setBan != null) {
      return setBan(talker, isBanned, context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setBan(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setBan?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setBan != null) {
      return setBan(this);
    }
    return orElse();
  }
}

abstract class _SetBan implements ChatEvent {
  const factory _SetBan(
      {required final Talker talker,
      required final bool isBanned,
      required final BuildContext context}) = _$_SetBan;

  Talker get talker;
  bool get isBanned;
  BuildContext get context;
  @JsonKey(ignore: true)
  _$$_SetBanCopyWith<_$_SetBan> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdateTalkerCopyWith<$Res> {
  factory _$$_UpdateTalkerCopyWith(
          _$_UpdateTalker value, $Res Function(_$_UpdateTalker) then) =
      __$$_UpdateTalkerCopyWithImpl<$Res>;
  $Res call({Talker talker});
}

/// @nodoc
class __$$_UpdateTalkerCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_UpdateTalkerCopyWith<$Res> {
  __$$_UpdateTalkerCopyWithImpl(
      _$_UpdateTalker _value, $Res Function(_$_UpdateTalker) _then)
      : super(_value, (v) => _then(v as _$_UpdateTalker));

  @override
  _$_UpdateTalker get _value => super._value as _$_UpdateTalker;

  @override
  $Res call({
    Object? talker = freezed,
  }) {
    return _then(_$_UpdateTalker(
      talker == freezed
          ? _value.talker
          : talker // ignore: cast_nullable_to_non_nullable
              as Talker,
    ));
  }
}

/// @nodoc

class _$_UpdateTalker implements _UpdateTalker {
  const _$_UpdateTalker(this.talker);

  @override
  final Talker talker;

  @override
  String toString() {
    return 'ChatEvent.updateTalker(talker: $talker)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdateTalker &&
            const DeepCollectionEquality().equals(other.talker, talker));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(talker));

  @JsonKey(ignore: true)
  @override
  _$$_UpdateTalkerCopyWith<_$_UpdateTalker> get copyWith =>
      __$$_UpdateTalkerCopyWithImpl<_$_UpdateTalker>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return updateTalker(talker);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return updateTalker?.call(talker);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (updateTalker != null) {
      return updateTalker(talker);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return updateTalker(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return updateTalker?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (updateTalker != null) {
      return updateTalker(this);
    }
    return orElse();
  }
}

abstract class _UpdateTalker implements ChatEvent {
  const factory _UpdateTalker(final Talker talker) = _$_UpdateTalker;

  Talker get talker;
  @JsonKey(ignore: true)
  _$$_UpdateTalkerCopyWith<_$_UpdateTalker> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$LoadMoreMessagesCopyWith<$Res> {
  factory _$$LoadMoreMessagesCopyWith(
          _$LoadMoreMessages value, $Res Function(_$LoadMoreMessages) then) =
      __$$LoadMoreMessagesCopyWithImpl<$Res>;
  $Res call(
      {ChatStateLoaded loadedState, Message? scrollToMessage, int? limit});
}

/// @nodoc
class __$$LoadMoreMessagesCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$LoadMoreMessagesCopyWith<$Res> {
  __$$LoadMoreMessagesCopyWithImpl(
      _$LoadMoreMessages _value, $Res Function(_$LoadMoreMessages) _then)
      : super(_value, (v) => _then(v as _$LoadMoreMessages));

  @override
  _$LoadMoreMessages get _value => super._value as _$LoadMoreMessages;

  @override
  $Res call({
    Object? loadedState = freezed,
    Object? scrollToMessage = freezed,
    Object? limit = freezed,
  }) {
    return _then(_$LoadMoreMessages(
      loadedState: loadedState == freezed
          ? _value.loadedState
          : loadedState // ignore: cast_nullable_to_non_nullable
              as ChatStateLoaded,
      scrollToMessage: scrollToMessage == freezed
          ? _value.scrollToMessage
          : scrollToMessage // ignore: cast_nullable_to_non_nullable
              as Message?,
      limit: limit == freezed
          ? _value.limit
          : limit // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc

class _$LoadMoreMessages implements LoadMoreMessages {
  const _$LoadMoreMessages(
      {required this.loadedState, this.scrollToMessage, this.limit});

  @override
  final ChatStateLoaded loadedState;
  @override
  final Message? scrollToMessage;
  @override
  final int? limit;

  @override
  String toString() {
    return 'ChatEvent.loadMoreMessages(loadedState: $loadedState, scrollToMessage: $scrollToMessage, limit: $limit)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$LoadMoreMessages &&
            const DeepCollectionEquality()
                .equals(other.loadedState, loadedState) &&
            const DeepCollectionEquality()
                .equals(other.scrollToMessage, scrollToMessage) &&
            const DeepCollectionEquality().equals(other.limit, limit));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(loadedState),
      const DeepCollectionEquality().hash(scrollToMessage),
      const DeepCollectionEquality().hash(limit));

  @JsonKey(ignore: true)
  @override
  _$$LoadMoreMessagesCopyWith<_$LoadMoreMessages> get copyWith =>
      __$$LoadMoreMessagesCopyWithImpl<_$LoadMoreMessages>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return loadMoreMessages(loadedState, this.scrollToMessage, limit);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return loadMoreMessages?.call(loadedState, this.scrollToMessage, limit);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (loadMoreMessages != null) {
      return loadMoreMessages(loadedState, this.scrollToMessage, limit);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return loadMoreMessages(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return loadMoreMessages?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (loadMoreMessages != null) {
      return loadMoreMessages(this);
    }
    return orElse();
  }
}

abstract class LoadMoreMessages implements ChatEvent {
  const factory LoadMoreMessages(
      {required final ChatStateLoaded loadedState,
      final Message? scrollToMessage,
      final int? limit}) = _$LoadMoreMessages;

  ChatStateLoaded get loadedState;
  Message? get scrollToMessage;
  int? get limit;
  @JsonKey(ignore: true)
  _$$LoadMoreMessagesCopyWith<_$LoadMoreMessages> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SendPendingMessagesCopyWith<$Res> {
  factory _$$SendPendingMessagesCopyWith(_$SendPendingMessages value,
          $Res Function(_$SendPendingMessages) then) =
      __$$SendPendingMessagesCopyWithImpl<$Res>;
  $Res call({ChatStateLoaded loadedState});
}

/// @nodoc
class __$$SendPendingMessagesCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$SendPendingMessagesCopyWith<$Res> {
  __$$SendPendingMessagesCopyWithImpl(
      _$SendPendingMessages _value, $Res Function(_$SendPendingMessages) _then)
      : super(_value, (v) => _then(v as _$SendPendingMessages));

  @override
  _$SendPendingMessages get _value => super._value as _$SendPendingMessages;

  @override
  $Res call({
    Object? loadedState = freezed,
  }) {
    return _then(_$SendPendingMessages(
      loadedState: loadedState == freezed
          ? _value.loadedState
          : loadedState // ignore: cast_nullable_to_non_nullable
              as ChatStateLoaded,
    ));
  }
}

/// @nodoc

class _$SendPendingMessages implements SendPendingMessages {
  const _$SendPendingMessages({required this.loadedState});

  @override
  final ChatStateLoaded loadedState;

  @override
  String toString() {
    return 'ChatEvent.sendPendingMessages(loadedState: $loadedState)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SendPendingMessages &&
            const DeepCollectionEquality()
                .equals(other.loadedState, loadedState));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(loadedState));

  @JsonKey(ignore: true)
  @override
  _$$SendPendingMessagesCopyWith<_$SendPendingMessages> get copyWith =>
      __$$SendPendingMessagesCopyWithImpl<_$SendPendingMessages>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return sendPendingMessages(loadedState);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return sendPendingMessages?.call(loadedState);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (sendPendingMessages != null) {
      return sendPendingMessages(loadedState);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return sendPendingMessages(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return sendPendingMessages?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (sendPendingMessages != null) {
      return sendPendingMessages(this);
    }
    return orElse();
  }
}

abstract class SendPendingMessages implements ChatEvent {
  const factory SendPendingMessages(
      {required final ChatStateLoaded loadedState}) = _$SendPendingMessages;

  ChatStateLoaded get loadedState;
  @JsonKey(ignore: true)
  _$$SendPendingMessagesCopyWith<_$SendPendingMessages> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetMessageReadCopyWith<$Res> {
  factory _$$_SetMessageReadCopyWith(
          _$_SetMessageRead value, $Res Function(_$_SetMessageRead) then) =
      __$$_SetMessageReadCopyWithImpl<$Res>;
  $Res call({Message message});
}

/// @nodoc
class __$$_SetMessageReadCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetMessageReadCopyWith<$Res> {
  __$$_SetMessageReadCopyWithImpl(
      _$_SetMessageRead _value, $Res Function(_$_SetMessageRead) _then)
      : super(_value, (v) => _then(v as _$_SetMessageRead));

  @override
  _$_SetMessageRead get _value => super._value as _$_SetMessageRead;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$_SetMessageRead(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
    ));
  }
}

/// @nodoc

class _$_SetMessageRead implements _SetMessageRead {
  const _$_SetMessageRead({required this.message});

  @override
  final Message message;

  @override
  String toString() {
    return 'ChatEvent.setMessageRead(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetMessageRead &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$_SetMessageReadCopyWith<_$_SetMessageRead> get copyWith =>
      __$$_SetMessageReadCopyWithImpl<_$_SetMessageRead>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setMessageRead(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setMessageRead?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setMessageRead != null) {
      return setMessageRead(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setMessageRead(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setMessageRead?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setMessageRead != null) {
      return setMessageRead(this);
    }
    return orElse();
  }
}

abstract class _SetMessageRead implements ChatEvent {
  const factory _SetMessageRead({required final Message message}) =
      _$_SetMessageRead;

  Message get message;
  @JsonKey(ignore: true)
  _$$_SetMessageReadCopyWith<_$_SetMessageRead> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ScrollToMessageCopyWith<$Res> {
  factory _$$_ScrollToMessageCopyWith(
          _$_ScrollToMessage value, $Res Function(_$_ScrollToMessage) then) =
      __$$_ScrollToMessageCopyWithImpl<$Res>;
  $Res call({Message message, ListScrollPosition? position});
}

/// @nodoc
class __$$_ScrollToMessageCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_ScrollToMessageCopyWith<$Res> {
  __$$_ScrollToMessageCopyWithImpl(
      _$_ScrollToMessage _value, $Res Function(_$_ScrollToMessage) _then)
      : super(_value, (v) => _then(v as _$_ScrollToMessage));

  @override
  _$_ScrollToMessage get _value => super._value as _$_ScrollToMessage;

  @override
  $Res call({
    Object? message = freezed,
    Object? position = freezed,
  }) {
    return _then(_$_ScrollToMessage(
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
      position: position == freezed
          ? _value.position
          : position // ignore: cast_nullable_to_non_nullable
              as ListScrollPosition?,
    ));
  }
}

/// @nodoc

class _$_ScrollToMessage implements _ScrollToMessage {
  const _$_ScrollToMessage({required this.message, this.position});

  @override
  final Message message;
  @override
  final ListScrollPosition? position;

  @override
  String toString() {
    return 'ChatEvent.scrollToMessage(message: $message, position: $position)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ScrollToMessage &&
            const DeepCollectionEquality().equals(other.message, message) &&
            const DeepCollectionEquality().equals(other.position, position));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(message),
      const DeepCollectionEquality().hash(position));

  @JsonKey(ignore: true)
  @override
  _$$_ScrollToMessageCopyWith<_$_ScrollToMessage> get copyWith =>
      __$$_ScrollToMessageCopyWithImpl<_$_ScrollToMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return scrollToMessage(message, position);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return scrollToMessage?.call(message, position);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (scrollToMessage != null) {
      return scrollToMessage(message, position);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return scrollToMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return scrollToMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (scrollToMessage != null) {
      return scrollToMessage(this);
    }
    return orElse();
  }
}

abstract class _ScrollToMessage implements ChatEvent {
  const factory _ScrollToMessage(
      {required final Message message,
      final ListScrollPosition? position}) = _$_ScrollToMessage;

  Message get message;
  ListScrollPosition? get position;
  @JsonKey(ignore: true)
  _$$_ScrollToMessageCopyWith<_$_ScrollToMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ConnectivityChangedCopyWith<$Res> {
  factory _$$_ConnectivityChangedCopyWith(_$_ConnectivityChanged value,
          $Res Function(_$_ConnectivityChanged) then) =
      __$$_ConnectivityChangedCopyWithImpl<$Res>;
  $Res call({ConnectivityResult connectivityResult});
}

/// @nodoc
class __$$_ConnectivityChangedCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_ConnectivityChangedCopyWith<$Res> {
  __$$_ConnectivityChangedCopyWithImpl(_$_ConnectivityChanged _value,
      $Res Function(_$_ConnectivityChanged) _then)
      : super(_value, (v) => _then(v as _$_ConnectivityChanged));

  @override
  _$_ConnectivityChanged get _value => super._value as _$_ConnectivityChanged;

  @override
  $Res call({
    Object? connectivityResult = freezed,
  }) {
    return _then(_$_ConnectivityChanged(
      connectivityResult == freezed
          ? _value.connectivityResult
          : connectivityResult // ignore: cast_nullable_to_non_nullable
              as ConnectivityResult,
    ));
  }
}

/// @nodoc

class _$_ConnectivityChanged implements _ConnectivityChanged {
  const _$_ConnectivityChanged(this.connectivityResult);

  @override
  final ConnectivityResult connectivityResult;

  @override
  String toString() {
    return 'ChatEvent.connectivityChanged(connectivityResult: $connectivityResult)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ConnectivityChanged &&
            const DeepCollectionEquality()
                .equals(other.connectivityResult, connectivityResult));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(connectivityResult));

  @JsonKey(ignore: true)
  @override
  _$$_ConnectivityChangedCopyWith<_$_ConnectivityChanged> get copyWith =>
      __$$_ConnectivityChangedCopyWithImpl<_$_ConnectivityChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return connectivityChanged(connectivityResult);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return connectivityChanged?.call(connectivityResult);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (connectivityChanged != null) {
      return connectivityChanged(connectivityResult);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return connectivityChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return connectivityChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (connectivityChanged != null) {
      return connectivityChanged(this);
    }
    return orElse();
  }
}

abstract class _ConnectivityChanged implements ChatEvent {
  const factory _ConnectivityChanged(
      final ConnectivityResult connectivityResult) = _$_ConnectivityChanged;

  ConnectivityResult get connectivityResult;
  @JsonKey(ignore: true)
  _$$_ConnectivityChangedCopyWith<_$_ConnectivityChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChatEventSetPinnedMessageCopyWith<$Res> {
  factory _$$ChatEventSetPinnedMessageCopyWith(
          _$ChatEventSetPinnedMessage value,
          $Res Function(_$ChatEventSetPinnedMessage) then) =
      __$$ChatEventSetPinnedMessageCopyWithImpl<$Res>;
  $Res call({BuildContext context, Message message, bool isPinned});
}

/// @nodoc
class __$$ChatEventSetPinnedMessageCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$ChatEventSetPinnedMessageCopyWith<$Res> {
  __$$ChatEventSetPinnedMessageCopyWithImpl(_$ChatEventSetPinnedMessage _value,
      $Res Function(_$ChatEventSetPinnedMessage) _then)
      : super(_value, (v) => _then(v as _$ChatEventSetPinnedMessage));

  @override
  _$ChatEventSetPinnedMessage get _value =>
      super._value as _$ChatEventSetPinnedMessage;

  @override
  $Res call({
    Object? context = freezed,
    Object? message = freezed,
    Object? isPinned = freezed,
  }) {
    return _then(_$ChatEventSetPinnedMessage(
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
      message: message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as Message,
      isPinned: isPinned == freezed
          ? _value.isPinned
          : isPinned // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$ChatEventSetPinnedMessage implements ChatEventSetPinnedMessage {
  const _$ChatEventSetPinnedMessage(
      {required this.context, required this.message, required this.isPinned});

  @override
  final BuildContext context;
  @override
  final Message message;
  @override
  final bool isPinned;

  @override
  String toString() {
    return 'ChatEvent.setPinnedMessage(context: $context, message: $message, isPinned: $isPinned)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatEventSetPinnedMessage &&
            const DeepCollectionEquality().equals(other.context, context) &&
            const DeepCollectionEquality().equals(other.message, message) &&
            const DeepCollectionEquality().equals(other.isPinned, isPinned));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(context),
      const DeepCollectionEquality().hash(message),
      const DeepCollectionEquality().hash(isPinned));

  @JsonKey(ignore: true)
  @override
  _$$ChatEventSetPinnedMessageCopyWith<_$ChatEventSetPinnedMessage>
      get copyWith => __$$ChatEventSetPinnedMessageCopyWithImpl<
          _$ChatEventSetPinnedMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setPinnedMessage(context, message, isPinned);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setPinnedMessage?.call(context, message, isPinned);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setPinnedMessage != null) {
      return setPinnedMessage(context, message, isPinned);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setPinnedMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setPinnedMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setPinnedMessage != null) {
      return setPinnedMessage(this);
    }
    return orElse();
  }
}

abstract class ChatEventSetPinnedMessage implements ChatEvent {
  const factory ChatEventSetPinnedMessage(
      {required final BuildContext context,
      required final Message message,
      required final bool isPinned}) = _$ChatEventSetPinnedMessage;

  BuildContext get context;
  Message get message;
  bool get isPinned;
  @JsonKey(ignore: true)
  _$$ChatEventSetPinnedMessageCopyWith<_$ChatEventSetPinnedMessage>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UpdatePinnedMessageCopyWith<$Res> {
  factory _$$_UpdatePinnedMessageCopyWith(_$_UpdatePinnedMessage value,
          $Res Function(_$_UpdatePinnedMessage) then) =
      __$$_UpdatePinnedMessageCopyWithImpl<$Res>;
  $Res call({Message? pinnedMessage, List<Message> messages});
}

/// @nodoc
class __$$_UpdatePinnedMessageCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_UpdatePinnedMessageCopyWith<$Res> {
  __$$_UpdatePinnedMessageCopyWithImpl(_$_UpdatePinnedMessage _value,
      $Res Function(_$_UpdatePinnedMessage) _then)
      : super(_value, (v) => _then(v as _$_UpdatePinnedMessage));

  @override
  _$_UpdatePinnedMessage get _value => super._value as _$_UpdatePinnedMessage;

  @override
  $Res call({
    Object? pinnedMessage = freezed,
    Object? messages = freezed,
  }) {
    return _then(_$_UpdatePinnedMessage(
      pinnedMessage: pinnedMessage == freezed
          ? _value.pinnedMessage
          : pinnedMessage // ignore: cast_nullable_to_non_nullable
              as Message?,
      messages: messages == freezed
          ? _value._messages
          : messages // ignore: cast_nullable_to_non_nullable
              as List<Message>,
    ));
  }
}

/// @nodoc

class _$_UpdatePinnedMessage implements _UpdatePinnedMessage {
  const _$_UpdatePinnedMessage(
      {required this.pinnedMessage, required final List<Message> messages})
      : _messages = messages;

  @override
  final Message? pinnedMessage;
  final List<Message> _messages;
  @override
  List<Message> get messages {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_messages);
  }

  @override
  String toString() {
    return 'ChatEvent.updatePinnedMessage(pinnedMessage: $pinnedMessage, messages: $messages)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UpdatePinnedMessage &&
            const DeepCollectionEquality()
                .equals(other.pinnedMessage, pinnedMessage) &&
            const DeepCollectionEquality().equals(other._messages, _messages));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(pinnedMessage),
      const DeepCollectionEquality().hash(_messages));

  @JsonKey(ignore: true)
  @override
  _$$_UpdatePinnedMessageCopyWith<_$_UpdatePinnedMessage> get copyWith =>
      __$$_UpdatePinnedMessageCopyWithImpl<_$_UpdatePinnedMessage>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return updatePinnedMessage(pinnedMessage, messages);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return updatePinnedMessage?.call(pinnedMessage, messages);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (updatePinnedMessage != null) {
      return updatePinnedMessage(pinnedMessage, messages);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return updatePinnedMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return updatePinnedMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (updatePinnedMessage != null) {
      return updatePinnedMessage(this);
    }
    return orElse();
  }
}

abstract class _UpdatePinnedMessage implements ChatEvent {
  const factory _UpdatePinnedMessage(
      {required final Message? pinnedMessage,
      required final List<Message> messages}) = _$_UpdatePinnedMessage;

  Message? get pinnedMessage;
  List<Message> get messages;
  @JsonKey(ignore: true)
  _$$_UpdatePinnedMessageCopyWith<_$_UpdatePinnedMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChangeSelectedEmotionCopyWith<$Res> {
  factory _$$ChangeSelectedEmotionCopyWith(_$ChangeSelectedEmotion value,
          $Res Function(_$ChangeSelectedEmotion) then) =
      __$$ChangeSelectedEmotionCopyWithImpl<$Res>;
  $Res call({Emotion emotion});
}

/// @nodoc
class __$$ChangeSelectedEmotionCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$ChangeSelectedEmotionCopyWith<$Res> {
  __$$ChangeSelectedEmotionCopyWithImpl(_$ChangeSelectedEmotion _value,
      $Res Function(_$ChangeSelectedEmotion) _then)
      : super(_value, (v) => _then(v as _$ChangeSelectedEmotion));

  @override
  _$ChangeSelectedEmotion get _value => super._value as _$ChangeSelectedEmotion;

  @override
  $Res call({
    Object? emotion = freezed,
  }) {
    return _then(_$ChangeSelectedEmotion(
      emotion: emotion == freezed
          ? _value.emotion
          : emotion // ignore: cast_nullable_to_non_nullable
              as Emotion,
    ));
  }
}

/// @nodoc

class _$ChangeSelectedEmotion implements ChangeSelectedEmotion {
  const _$ChangeSelectedEmotion({required this.emotion});

  @override
  final Emotion emotion;

  @override
  String toString() {
    return 'ChatEvent.changeSelectedEmotion(emotion: $emotion)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChangeSelectedEmotion &&
            const DeepCollectionEquality().equals(other.emotion, emotion));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(emotion));

  @JsonKey(ignore: true)
  @override
  _$$ChangeSelectedEmotionCopyWith<_$ChangeSelectedEmotion> get copyWith =>
      __$$ChangeSelectedEmotionCopyWithImpl<_$ChangeSelectedEmotion>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return changeSelectedEmotion(emotion);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return changeSelectedEmotion?.call(emotion);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (changeSelectedEmotion != null) {
      return changeSelectedEmotion(emotion);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return changeSelectedEmotion(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return changeSelectedEmotion?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (changeSelectedEmotion != null) {
      return changeSelectedEmotion(this);
    }
    return orElse();
  }
}

abstract class ChangeSelectedEmotion implements ChatEvent {
  const factory ChangeSelectedEmotion({required final Emotion emotion}) =
      _$ChangeSelectedEmotion;

  Emotion get emotion;
  @JsonKey(ignore: true)
  _$$ChangeSelectedEmotionCopyWith<_$ChangeSelectedEmotion> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SetEmotionPannelVisibilityCopyWith<$Res> {
  factory _$$SetEmotionPannelVisibilityCopyWith(
          _$SetEmotionPannelVisibility value,
          $Res Function(_$SetEmotionPannelVisibility) then) =
      __$$SetEmotionPannelVisibilityCopyWithImpl<$Res>;
  $Res call({bool isVisible});
}

/// @nodoc
class __$$SetEmotionPannelVisibilityCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$SetEmotionPannelVisibilityCopyWith<$Res> {
  __$$SetEmotionPannelVisibilityCopyWithImpl(
      _$SetEmotionPannelVisibility _value,
      $Res Function(_$SetEmotionPannelVisibility) _then)
      : super(_value, (v) => _then(v as _$SetEmotionPannelVisibility));

  @override
  _$SetEmotionPannelVisibility get _value =>
      super._value as _$SetEmotionPannelVisibility;

  @override
  $Res call({
    Object? isVisible = freezed,
  }) {
    return _then(_$SetEmotionPannelVisibility(
      isVisible: isVisible == freezed
          ? _value.isVisible
          : isVisible // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$SetEmotionPannelVisibility implements SetEmotionPannelVisibility {
  const _$SetEmotionPannelVisibility({required this.isVisible});

  @override
  final bool isVisible;

  @override
  String toString() {
    return 'ChatEvent.setEmotionPannelVisibility(isVisible: $isVisible)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SetEmotionPannelVisibility &&
            const DeepCollectionEquality().equals(other.isVisible, isVisible));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(isVisible));

  @JsonKey(ignore: true)
  @override
  _$$SetEmotionPannelVisibilityCopyWith<_$SetEmotionPannelVisibility>
      get copyWith => __$$SetEmotionPannelVisibilityCopyWithImpl<
          _$SetEmotionPannelVisibility>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setEmotionPannelVisibility(isVisible);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setEmotionPannelVisibility?.call(isVisible);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setEmotionPannelVisibility != null) {
      return setEmotionPannelVisibility(isVisible);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setEmotionPannelVisibility(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setEmotionPannelVisibility?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setEmotionPannelVisibility != null) {
      return setEmotionPannelVisibility(this);
    }
    return orElse();
  }
}

abstract class SetEmotionPannelVisibility implements ChatEvent {
  const factory SetEmotionPannelVisibility({required final bool isVisible}) =
      _$SetEmotionPannelVisibility;

  bool get isVisible;
  @JsonKey(ignore: true)
  _$$SetEmotionPannelVisibilityCopyWith<_$SetEmotionPannelVisibility>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetLargeMessageCopyWith<$Res> {
  factory _$$_SetLargeMessageCopyWith(
          _$_SetLargeMessage value, $Res Function(_$_SetLargeMessage) then) =
      __$$_SetLargeMessageCopyWithImpl<$Res>;
  $Res call({bool largeMessage});
}

/// @nodoc
class __$$_SetLargeMessageCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetLargeMessageCopyWith<$Res> {
  __$$_SetLargeMessageCopyWithImpl(
      _$_SetLargeMessage _value, $Res Function(_$_SetLargeMessage) _then)
      : super(_value, (v) => _then(v as _$_SetLargeMessage));

  @override
  _$_SetLargeMessage get _value => super._value as _$_SetLargeMessage;

  @override
  $Res call({
    Object? largeMessage = freezed,
  }) {
    return _then(_$_SetLargeMessage(
      largeMessage: largeMessage == freezed
          ? _value.largeMessage
          : largeMessage // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_SetLargeMessage implements _SetLargeMessage {
  const _$_SetLargeMessage({required this.largeMessage});

  @override
  final bool largeMessage;

  @override
  String toString() {
    return 'ChatEvent.setLargeMessage(largeMessage: $largeMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetLargeMessage &&
            const DeepCollectionEquality()
                .equals(other.largeMessage, largeMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(largeMessage));

  @JsonKey(ignore: true)
  @override
  _$$_SetLargeMessageCopyWith<_$_SetLargeMessage> get copyWith =>
      __$$_SetLargeMessageCopyWithImpl<_$_SetLargeMessage>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setLargeMessage(largeMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setLargeMessage?.call(largeMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setLargeMessage != null) {
      return setLargeMessage(largeMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setLargeMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setLargeMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setLargeMessage != null) {
      return setLargeMessage(this);
    }
    return orElse();
  }
}

abstract class _SetLargeMessage implements ChatEvent {
  const factory _SetLargeMessage({required final bool largeMessage}) =
      _$_SetLargeMessage;

  bool get largeMessage;
  @JsonKey(ignore: true)
  _$$_SetLargeMessageCopyWith<_$_SetLargeMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SendSelectedEmotionCopyWith<$Res> {
  factory _$$SendSelectedEmotionCopyWith(_$SendSelectedEmotion value,
          $Res Function(_$SendSelectedEmotion) then) =
      __$$SendSelectedEmotionCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SendSelectedEmotionCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$SendSelectedEmotionCopyWith<$Res> {
  __$$SendSelectedEmotionCopyWithImpl(
      _$SendSelectedEmotion _value, $Res Function(_$SendSelectedEmotion) _then)
      : super(_value, (v) => _then(v as _$SendSelectedEmotion));

  @override
  _$SendSelectedEmotion get _value => super._value as _$SendSelectedEmotion;
}

/// @nodoc

class _$SendSelectedEmotion implements SendSelectedEmotion {
  const _$SendSelectedEmotion();

  @override
  String toString() {
    return 'ChatEvent.sendSelectedEmotion()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SendSelectedEmotion);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return sendSelectedEmotion();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return sendSelectedEmotion?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (sendSelectedEmotion != null) {
      return sendSelectedEmotion();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return sendSelectedEmotion(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return sendSelectedEmotion?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (sendSelectedEmotion != null) {
      return sendSelectedEmotion(this);
    }
    return orElse();
  }
}

abstract class SendSelectedEmotion implements ChatEvent {
  const factory SendSelectedEmotion() = _$SendSelectedEmotion;
}

/// @nodoc
abstract class _$$_SetSwearingMessageCopyWith<$Res> {
  factory _$$_SetSwearingMessageCopyWith(_$_SetSwearingMessage value,
          $Res Function(_$_SetSwearingMessage) then) =
      __$$_SetSwearingMessageCopyWithImpl<$Res>;
  $Res call({bool isSwearingMessage});
}

/// @nodoc
class __$$_SetSwearingMessageCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetSwearingMessageCopyWith<$Res> {
  __$$_SetSwearingMessageCopyWithImpl(
      _$_SetSwearingMessage _value, $Res Function(_$_SetSwearingMessage) _then)
      : super(_value, (v) => _then(v as _$_SetSwearingMessage));

  @override
  _$_SetSwearingMessage get _value => super._value as _$_SetSwearingMessage;

  @override
  $Res call({
    Object? isSwearingMessage = freezed,
  }) {
    return _then(_$_SetSwearingMessage(
      isSwearingMessage: isSwearingMessage == freezed
          ? _value.isSwearingMessage
          : isSwearingMessage // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_SetSwearingMessage implements _SetSwearingMessage {
  const _$_SetSwearingMessage({required this.isSwearingMessage});

  @override
  final bool isSwearingMessage;

  @override
  String toString() {
    return 'ChatEvent.setSwearingMessage(isSwearingMessage: $isSwearingMessage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetSwearingMessage &&
            const DeepCollectionEquality()
                .equals(other.isSwearingMessage, isSwearingMessage));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(isSwearingMessage));

  @JsonKey(ignore: true)
  @override
  _$$_SetSwearingMessageCopyWith<_$_SetSwearingMessage> get copyWith =>
      __$$_SetSwearingMessageCopyWithImpl<_$_SetSwearingMessage>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setSwearingMessage(isSwearingMessage);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setSwearingMessage?.call(isSwearingMessage);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setSwearingMessage != null) {
      return setSwearingMessage(isSwearingMessage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setSwearingMessage(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setSwearingMessage?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setSwearingMessage != null) {
      return setSwearingMessage(this);
    }
    return orElse();
  }
}

abstract class _SetSwearingMessage implements ChatEvent {
  const factory _SetSwearingMessage({required final bool isSwearingMessage}) =
      _$_SetSwearingMessage;

  bool get isSwearingMessage;
  @JsonKey(ignore: true)
  _$$_SetSwearingMessageCopyWith<_$_SetSwearingMessage> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetContextCopyWith<$Res> {
  factory _$$_SetContextCopyWith(
          _$_SetContext value, $Res Function(_$_SetContext) then) =
      __$$_SetContextCopyWithImpl<$Res>;
  $Res call({BuildContext context});
}

/// @nodoc
class __$$_SetContextCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetContextCopyWith<$Res> {
  __$$_SetContextCopyWithImpl(
      _$_SetContext _value, $Res Function(_$_SetContext) _then)
      : super(_value, (v) => _then(v as _$_SetContext));

  @override
  _$_SetContext get _value => super._value as _$_SetContext;

  @override
  $Res call({
    Object? context = freezed,
  }) {
    return _then(_$_SetContext(
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
    ));
  }
}

/// @nodoc

class _$_SetContext implements _SetContext {
  const _$_SetContext({required this.context});

  @override
  final BuildContext context;

  @override
  String toString() {
    return 'ChatEvent.setContext(context: $context)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetContext &&
            const DeepCollectionEquality().equals(other.context, context));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(context));

  @JsonKey(ignore: true)
  @override
  _$$_SetContextCopyWith<_$_SetContext> get copyWith =>
      __$$_SetContextCopyWithImpl<_$_SetContext>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setContext(context);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setContext?.call(context);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setContext != null) {
      return setContext(context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setContext(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setContext?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setContext != null) {
      return setContext(this);
    }
    return orElse();
  }
}

abstract class _SetContext implements ChatEvent {
  const factory _SetContext({required final BuildContext context}) =
      _$_SetContext;

  BuildContext get context;
  @JsonKey(ignore: true)
  _$$_SetContextCopyWith<_$_SetContext> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ChatEventAddPreviewToCacheCopyWith<$Res> {
  factory _$$ChatEventAddPreviewToCacheCopyWith(
          _$ChatEventAddPreviewToCache value,
          $Res Function(_$ChatEventAddPreviewToCache) then) =
      __$$ChatEventAddPreviewToCacheCopyWithImpl<$Res>;
  $Res call({Preview preview, String url});
}

/// @nodoc
class __$$ChatEventAddPreviewToCacheCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$ChatEventAddPreviewToCacheCopyWith<$Res> {
  __$$ChatEventAddPreviewToCacheCopyWithImpl(
      _$ChatEventAddPreviewToCache _value,
      $Res Function(_$ChatEventAddPreviewToCache) _then)
      : super(_value, (v) => _then(v as _$ChatEventAddPreviewToCache));

  @override
  _$ChatEventAddPreviewToCache get _value =>
      super._value as _$ChatEventAddPreviewToCache;

  @override
  $Res call({
    Object? preview = freezed,
    Object? url = freezed,
  }) {
    return _then(_$ChatEventAddPreviewToCache(
      preview: preview == freezed
          ? _value.preview
          : preview // ignore: cast_nullable_to_non_nullable
              as Preview,
      url: url == freezed
          ? _value.url
          : url // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$ChatEventAddPreviewToCache implements ChatEventAddPreviewToCache {
  const _$ChatEventAddPreviewToCache(
      {required this.preview, required this.url});

  @override
  final Preview preview;
  @override
  final String url;

  @override
  String toString() {
    return 'ChatEvent.addPreviewToCache(preview: $preview, url: $url)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatEventAddPreviewToCache &&
            const DeepCollectionEquality().equals(other.preview, preview) &&
            const DeepCollectionEquality().equals(other.url, url));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(preview),
      const DeepCollectionEquality().hash(url));

  @JsonKey(ignore: true)
  @override
  _$$ChatEventAddPreviewToCacheCopyWith<_$ChatEventAddPreviewToCache>
      get copyWith => __$$ChatEventAddPreviewToCacheCopyWithImpl<
          _$ChatEventAddPreviewToCache>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return addPreviewToCache(preview, url);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return addPreviewToCache?.call(preview, url);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (addPreviewToCache != null) {
      return addPreviewToCache(preview, url);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return addPreviewToCache(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return addPreviewToCache?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (addPreviewToCache != null) {
      return addPreviewToCache(this);
    }
    return orElse();
  }
}

abstract class ChatEventAddPreviewToCache implements ChatEvent {
  const factory ChatEventAddPreviewToCache(
      {required final Preview preview,
      required final String url}) = _$ChatEventAddPreviewToCache;

  Preview get preview;
  String get url;
  @JsonKey(ignore: true)
  _$$ChatEventAddPreviewToCacheCopyWith<_$ChatEventAddPreviewToCache>
      get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ShowLoadingCopyWith<$Res> {
  factory _$$_ShowLoadingCopyWith(
          _$_ShowLoading value, $Res Function(_$_ShowLoading) then) =
      __$$_ShowLoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ShowLoadingCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_ShowLoadingCopyWith<$Res> {
  __$$_ShowLoadingCopyWithImpl(
      _$_ShowLoading _value, $Res Function(_$_ShowLoading) _then)
      : super(_value, (v) => _then(v as _$_ShowLoading));

  @override
  _$_ShowLoading get _value => super._value as _$_ShowLoading;
}

/// @nodoc

class _$_ShowLoading implements _ShowLoading {
  const _$_ShowLoading();

  @override
  String toString() {
    return 'ChatEvent.showLoading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ShowLoading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return showLoading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return showLoading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (showLoading != null) {
      return showLoading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return showLoading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return showLoading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (showLoading != null) {
      return showLoading(this);
    }
    return orElse();
  }
}

abstract class _ShowLoading implements ChatEvent {
  const factory _ShowLoading() = _$_ShowLoading;
}

/// @nodoc
abstract class _$$_ToggleHandCopyWith<$Res> {
  factory _$$_ToggleHandCopyWith(
          _$_ToggleHand value, $Res Function(_$_ToggleHand) then) =
      __$$_ToggleHandCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ToggleHandCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_ToggleHandCopyWith<$Res> {
  __$$_ToggleHandCopyWithImpl(
      _$_ToggleHand _value, $Res Function(_$_ToggleHand) _then)
      : super(_value, (v) => _then(v as _$_ToggleHand));

  @override
  _$_ToggleHand get _value => super._value as _$_ToggleHand;
}

/// @nodoc

class _$_ToggleHand implements _ToggleHand {
  const _$_ToggleHand();

  @override
  String toString() {
    return 'ChatEvent.toggleHand()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ToggleHand);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return toggleHand();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return toggleHand?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (toggleHand != null) {
      return toggleHand();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return toggleHand(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return toggleHand?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (toggleHand != null) {
      return toggleHand(this);
    }
    return orElse();
  }
}

abstract class _ToggleHand implements ChatEvent {
  const factory _ToggleHand() = _$_ToggleHand;
}

/// @nodoc
abstract class _$$_RejectHandCopyWith<$Res> {
  factory _$$_RejectHandCopyWith(
          _$_RejectHand value, $Res Function(_$_RejectHand) then) =
      __$$_RejectHandCopyWithImpl<$Res>;
  $Res call({RejectHandRequest rejectHandRequest});
}

/// @nodoc
class __$$_RejectHandCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_RejectHandCopyWith<$Res> {
  __$$_RejectHandCopyWithImpl(
      _$_RejectHand _value, $Res Function(_$_RejectHand) _then)
      : super(_value, (v) => _then(v as _$_RejectHand));

  @override
  _$_RejectHand get _value => super._value as _$_RejectHand;

  @override
  $Res call({
    Object? rejectHandRequest = freezed,
  }) {
    return _then(_$_RejectHand(
      rejectHandRequest: rejectHandRequest == freezed
          ? _value.rejectHandRequest
          : rejectHandRequest // ignore: cast_nullable_to_non_nullable
              as RejectHandRequest,
    ));
  }
}

/// @nodoc

class _$_RejectHand implements _RejectHand {
  const _$_RejectHand({required this.rejectHandRequest});

  @override
  final RejectHandRequest rejectHandRequest;

  @override
  String toString() {
    return 'ChatEvent.rejectHand(rejectHandRequest: $rejectHandRequest)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RejectHand &&
            const DeepCollectionEquality()
                .equals(other.rejectHandRequest, rejectHandRequest));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(rejectHandRequest));

  @JsonKey(ignore: true)
  @override
  _$$_RejectHandCopyWith<_$_RejectHand> get copyWith =>
      __$$_RejectHandCopyWithImpl<_$_RejectHand>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return rejectHand(rejectHandRequest);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return rejectHand?.call(rejectHandRequest);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (rejectHand != null) {
      return rejectHand(rejectHandRequest);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return rejectHand(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return rejectHand?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (rejectHand != null) {
      return rejectHand(this);
    }
    return orElse();
  }
}

abstract class _RejectHand implements ChatEvent {
  const factory _RejectHand(
      {required final RejectHandRequest rejectHandRequest}) = _$_RejectHand;

  RejectHandRequest get rejectHandRequest;
  @JsonKey(ignore: true)
  _$$_RejectHandCopyWith<_$_RejectHand> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetRoleCopyWith<$Res> {
  factory _$$_SetRoleCopyWith(
          _$_SetRole value, $Res Function(_$_SetRole) then) =
      __$$_SetRoleCopyWithImpl<$Res>;
  $Res call(
      {SetRoleRequest setRoleRequest, BuildContext context, Talker talker});
}

/// @nodoc
class __$$_SetRoleCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetRoleCopyWith<$Res> {
  __$$_SetRoleCopyWithImpl(_$_SetRole _value, $Res Function(_$_SetRole) _then)
      : super(_value, (v) => _then(v as _$_SetRole));

  @override
  _$_SetRole get _value => super._value as _$_SetRole;

  @override
  $Res call({
    Object? setRoleRequest = freezed,
    Object? context = freezed,
    Object? talker = freezed,
  }) {
    return _then(_$_SetRole(
      setRoleRequest: setRoleRequest == freezed
          ? _value.setRoleRequest
          : setRoleRequest // ignore: cast_nullable_to_non_nullable
              as SetRoleRequest,
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
      talker: talker == freezed
          ? _value.talker
          : talker // ignore: cast_nullable_to_non_nullable
              as Talker,
    ));
  }
}

/// @nodoc

class _$_SetRole implements _SetRole {
  const _$_SetRole(
      {required this.setRoleRequest,
      required this.context,
      required this.talker});

  @override
  final SetRoleRequest setRoleRequest;
  @override
  final BuildContext context;
  @override
  final Talker talker;

  @override
  String toString() {
    return 'ChatEvent.setRole(setRoleRequest: $setRoleRequest, context: $context, talker: $talker)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetRole &&
            const DeepCollectionEquality()
                .equals(other.setRoleRequest, setRoleRequest) &&
            const DeepCollectionEquality().equals(other.context, context) &&
            const DeepCollectionEquality().equals(other.talker, talker));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(setRoleRequest),
      const DeepCollectionEquality().hash(context),
      const DeepCollectionEquality().hash(talker));

  @JsonKey(ignore: true)
  @override
  _$$_SetRoleCopyWith<_$_SetRole> get copyWith =>
      __$$_SetRoleCopyWithImpl<_$_SetRole>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setRole(setRoleRequest, context, talker);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setRole?.call(setRoleRequest, context, talker);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setRole != null) {
      return setRole(setRoleRequest, context, talker);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setRole(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setRole?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setRole != null) {
      return setRole(this);
    }
    return orElse();
  }
}

abstract class _SetRole implements ChatEvent {
  const factory _SetRole(
      {required final SetRoleRequest setRoleRequest,
      required final BuildContext context,
      required final Talker talker}) = _$_SetRole;

  SetRoleRequest get setRoleRequest;
  BuildContext get context;
  Talker get talker;
  @JsonKey(ignore: true)
  _$$_SetRoleCopyWith<_$_SetRole> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetMuteCopyWith<$Res> {
  factory _$$_SetMuteCopyWith(
          _$_SetMute value, $Res Function(_$_SetMute) then) =
      __$$_SetMuteCopyWithImpl<$Res>;
  $Res call({SetMuteRequest setMuteRequest});
}

/// @nodoc
class __$$_SetMuteCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetMuteCopyWith<$Res> {
  __$$_SetMuteCopyWithImpl(_$_SetMute _value, $Res Function(_$_SetMute) _then)
      : super(_value, (v) => _then(v as _$_SetMute));

  @override
  _$_SetMute get _value => super._value as _$_SetMute;

  @override
  $Res call({
    Object? setMuteRequest = freezed,
  }) {
    return _then(_$_SetMute(
      setMuteRequest: setMuteRequest == freezed
          ? _value.setMuteRequest
          : setMuteRequest // ignore: cast_nullable_to_non_nullable
              as SetMuteRequest,
    ));
  }
}

/// @nodoc

class _$_SetMute implements _SetMute {
  const _$_SetMute({required this.setMuteRequest});

  @override
  final SetMuteRequest setMuteRequest;

  @override
  String toString() {
    return 'ChatEvent.setMute(setMuteRequest: $setMuteRequest)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetMute &&
            const DeepCollectionEquality()
                .equals(other.setMuteRequest, setMuteRequest));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(setMuteRequest));

  @JsonKey(ignore: true)
  @override
  _$$_SetMuteCopyWith<_$_SetMute> get copyWith =>
      __$$_SetMuteCopyWithImpl<_$_SetMute>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setMute(setMuteRequest);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setMute?.call(setMuteRequest);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setMute != null) {
      return setMute(setMuteRequest);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setMute(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setMute?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setMute != null) {
      return setMute(this);
    }
    return orElse();
  }
}

abstract class _SetMute implements ChatEvent {
  const factory _SetMute({required final SetMuteRequest setMuteRequest}) =
      _$_SetMute;

  SetMuteRequest get setMuteRequest;
  @JsonKey(ignore: true)
  _$$_SetMuteCopyWith<_$_SetMute> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetMicStatusCopyWith<$Res> {
  factory _$$_SetMicStatusCopyWith(
          _$_SetMicStatus value, $Res Function(_$_SetMicStatus) then) =
      __$$_SetMicStatusCopyWithImpl<$Res>;
  $Res call({PermissionStatus micStatus});
}

/// @nodoc
class __$$_SetMicStatusCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetMicStatusCopyWith<$Res> {
  __$$_SetMicStatusCopyWithImpl(
      _$_SetMicStatus _value, $Res Function(_$_SetMicStatus) _then)
      : super(_value, (v) => _then(v as _$_SetMicStatus));

  @override
  _$_SetMicStatus get _value => super._value as _$_SetMicStatus;

  @override
  $Res call({
    Object? micStatus = freezed,
  }) {
    return _then(_$_SetMicStatus(
      micStatus: micStatus == freezed
          ? _value.micStatus
          : micStatus // ignore: cast_nullable_to_non_nullable
              as PermissionStatus,
    ));
  }
}

/// @nodoc

class _$_SetMicStatus implements _SetMicStatus {
  const _$_SetMicStatus({required this.micStatus});

  @override
  final PermissionStatus micStatus;

  @override
  String toString() {
    return 'ChatEvent.setMicStatus(micStatus: $micStatus)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetMicStatus &&
            const DeepCollectionEquality().equals(other.micStatus, micStatus));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(micStatus));

  @JsonKey(ignore: true)
  @override
  _$$_SetMicStatusCopyWith<_$_SetMicStatus> get copyWith =>
      __$$_SetMicStatusCopyWithImpl<_$_SetMicStatus>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setMicStatus(micStatus);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setMicStatus?.call(micStatus);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setMicStatus != null) {
      return setMicStatus(micStatus);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setMicStatus(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setMicStatus?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setMicStatus != null) {
      return setMicStatus(this);
    }
    return orElse();
  }
}

abstract class _SetMicStatus implements ChatEvent {
  const factory _SetMicStatus({required final PermissionStatus micStatus}) =
      _$_SetMicStatus;

  PermissionStatus get micStatus;
  @JsonKey(ignore: true)
  _$$_SetMicStatusCopyWith<_$_SetMicStatus> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetStickerModeCopyWith<$Res> {
  factory _$$_SetStickerModeCopyWith(
          _$_SetStickerMode value, $Res Function(_$_SetStickerMode) then) =
      __$$_SetStickerModeCopyWithImpl<$Res>;
  $Res call({bool stickerMode});
}

/// @nodoc
class __$$_SetStickerModeCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetStickerModeCopyWith<$Res> {
  __$$_SetStickerModeCopyWithImpl(
      _$_SetStickerMode _value, $Res Function(_$_SetStickerMode) _then)
      : super(_value, (v) => _then(v as _$_SetStickerMode));

  @override
  _$_SetStickerMode get _value => super._value as _$_SetStickerMode;

  @override
  $Res call({
    Object? stickerMode = freezed,
  }) {
    return _then(_$_SetStickerMode(
      stickerMode: stickerMode == freezed
          ? _value.stickerMode
          : stickerMode // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_SetStickerMode implements _SetStickerMode {
  const _$_SetStickerMode({required this.stickerMode});

  @override
  final bool stickerMode;

  @override
  String toString() {
    return 'ChatEvent.setStickerMode(stickerMode: $stickerMode)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetStickerMode &&
            const DeepCollectionEquality()
                .equals(other.stickerMode, stickerMode));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(stickerMode));

  @JsonKey(ignore: true)
  @override
  _$$_SetStickerModeCopyWith<_$_SetStickerMode> get copyWith =>
      __$$_SetStickerModeCopyWithImpl<_$_SetStickerMode>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setStickerMode(stickerMode);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setStickerMode?.call(stickerMode);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setStickerMode != null) {
      return setStickerMode(stickerMode);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setStickerMode(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setStickerMode?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setStickerMode != null) {
      return setStickerMode(this);
    }
    return orElse();
  }
}

abstract class _SetStickerMode implements ChatEvent {
  const factory _SetStickerMode({required final bool stickerMode}) =
      _$_SetStickerMode;

  bool get stickerMode;
  @JsonKey(ignore: true)
  _$$_SetStickerModeCopyWith<_$_SetStickerMode> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SendStickerCopyWith<$Res> {
  factory _$$_SendStickerCopyWith(
          _$_SendSticker value, $Res Function(_$_SendSticker) then) =
      __$$_SendStickerCopyWithImpl<$Res>;
  $Res call({int stickerId});
}

/// @nodoc
class __$$_SendStickerCopyWithImpl<$Res> extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SendStickerCopyWith<$Res> {
  __$$_SendStickerCopyWithImpl(
      _$_SendSticker _value, $Res Function(_$_SendSticker) _then)
      : super(_value, (v) => _then(v as _$_SendSticker));

  @override
  _$_SendSticker get _value => super._value as _$_SendSticker;

  @override
  $Res call({
    Object? stickerId = freezed,
  }) {
    return _then(_$_SendSticker(
      stickerId: stickerId == freezed
          ? _value.stickerId
          : stickerId // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SendSticker implements _SendSticker {
  const _$_SendSticker({required this.stickerId});

  @override
  final int stickerId;

  @override
  String toString() {
    return 'ChatEvent.sendSticker(stickerId: $stickerId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SendSticker &&
            const DeepCollectionEquality().equals(other.stickerId, stickerId));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(stickerId));

  @JsonKey(ignore: true)
  @override
  _$$_SendStickerCopyWith<_$_SendSticker> get copyWith =>
      __$$_SendStickerCopyWithImpl<_$_SendSticker>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return sendSticker(stickerId);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return sendSticker?.call(stickerId);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (sendSticker != null) {
      return sendSticker(stickerId);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return sendSticker(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return sendSticker?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (sendSticker != null) {
      return sendSticker(this);
    }
    return orElse();
  }
}

abstract class _SendSticker implements ChatEvent {
  const factory _SendSticker({required final int stickerId}) = _$_SendSticker;

  int get stickerId;
  @JsonKey(ignore: true)
  _$$_SendStickerCopyWith<_$_SendSticker> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_CreateReactionCopyWith<$Res> {
  factory _$$_CreateReactionCopyWith(
          _$_CreateReaction value, $Res Function(_$_CreateReaction) then) =
      __$$_CreateReactionCopyWithImpl<$Res>;
  $Res call({ReactionDto reactionDto});
}

/// @nodoc
class __$$_CreateReactionCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_CreateReactionCopyWith<$Res> {
  __$$_CreateReactionCopyWithImpl(
      _$_CreateReaction _value, $Res Function(_$_CreateReaction) _then)
      : super(_value, (v) => _then(v as _$_CreateReaction));

  @override
  _$_CreateReaction get _value => super._value as _$_CreateReaction;

  @override
  $Res call({
    Object? reactionDto = freezed,
  }) {
    return _then(_$_CreateReaction(
      reactionDto: reactionDto == freezed
          ? _value.reactionDto
          : reactionDto // ignore: cast_nullable_to_non_nullable
              as ReactionDto,
    ));
  }
}

/// @nodoc

class _$_CreateReaction implements _CreateReaction {
  const _$_CreateReaction({required this.reactionDto});

  @override
  final ReactionDto reactionDto;

  @override
  String toString() {
    return 'ChatEvent.createReaction(reactionDto: $reactionDto)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CreateReaction &&
            const DeepCollectionEquality()
                .equals(other.reactionDto, reactionDto));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(reactionDto));

  @JsonKey(ignore: true)
  @override
  _$$_CreateReactionCopyWith<_$_CreateReaction> get copyWith =>
      __$$_CreateReactionCopyWithImpl<_$_CreateReaction>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return createReaction(reactionDto);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return createReaction?.call(reactionDto);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (createReaction != null) {
      return createReaction(reactionDto);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return createReaction(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return createReaction?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (createReaction != null) {
      return createReaction(this);
    }
    return orElse();
  }
}

abstract class _CreateReaction implements ChatEvent {
  const factory _CreateReaction({required final ReactionDto reactionDto}) =
      _$_CreateReaction;

  ReactionDto get reactionDto;
  @JsonKey(ignore: true)
  _$$_CreateReactionCopyWith<_$_CreateReaction> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_DeleteReactionCopyWith<$Res> {
  factory _$$_DeleteReactionCopyWith(
          _$_DeleteReaction value, $Res Function(_$_DeleteReaction) then) =
      __$$_DeleteReactionCopyWithImpl<$Res>;
  $Res call({ReactionDto reactionDto});
}

/// @nodoc
class __$$_DeleteReactionCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_DeleteReactionCopyWith<$Res> {
  __$$_DeleteReactionCopyWithImpl(
      _$_DeleteReaction _value, $Res Function(_$_DeleteReaction) _then)
      : super(_value, (v) => _then(v as _$_DeleteReaction));

  @override
  _$_DeleteReaction get _value => super._value as _$_DeleteReaction;

  @override
  $Res call({
    Object? reactionDto = freezed,
  }) {
    return _then(_$_DeleteReaction(
      reactionDto: reactionDto == freezed
          ? _value.reactionDto
          : reactionDto // ignore: cast_nullable_to_non_nullable
              as ReactionDto,
    ));
  }
}

/// @nodoc

class _$_DeleteReaction implements _DeleteReaction {
  const _$_DeleteReaction({required this.reactionDto});

  @override
  final ReactionDto reactionDto;

  @override
  String toString() {
    return 'ChatEvent.deleteReaction(reactionDto: $reactionDto)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DeleteReaction &&
            const DeepCollectionEquality()
                .equals(other.reactionDto, reactionDto));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(reactionDto));

  @JsonKey(ignore: true)
  @override
  _$$_DeleteReactionCopyWith<_$_DeleteReaction> get copyWith =>
      __$$_DeleteReactionCopyWithImpl<_$_DeleteReaction>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return deleteReaction(reactionDto);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return deleteReaction?.call(reactionDto);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (deleteReaction != null) {
      return deleteReaction(reactionDto);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return deleteReaction(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return deleteReaction?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (deleteReaction != null) {
      return deleteReaction(this);
    }
    return orElse();
  }
}

abstract class _DeleteReaction implements ChatEvent {
  const factory _DeleteReaction({required final ReactionDto reactionDto}) =
      _$_DeleteReaction;

  ReactionDto get reactionDto;
  @JsonKey(ignore: true)
  _$$_DeleteReactionCopyWith<_$_DeleteReaction> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SetCurrentTriggerCopyWith<$Res> {
  factory _$$_SetCurrentTriggerCopyWith(_$_SetCurrentTrigger value,
          $Res Function(_$_SetCurrentTrigger) then) =
      __$$_SetCurrentTriggerCopyWithImpl<$Res>;
  $Res call({TooltipTrigger currentTrigger});
}

/// @nodoc
class __$$_SetCurrentTriggerCopyWithImpl<$Res>
    extends _$ChatEventCopyWithImpl<$Res>
    implements _$$_SetCurrentTriggerCopyWith<$Res> {
  __$$_SetCurrentTriggerCopyWithImpl(
      _$_SetCurrentTrigger _value, $Res Function(_$_SetCurrentTrigger) _then)
      : super(_value, (v) => _then(v as _$_SetCurrentTrigger));

  @override
  _$_SetCurrentTrigger get _value => super._value as _$_SetCurrentTrigger;

  @override
  $Res call({
    Object? currentTrigger = freezed,
  }) {
    return _then(_$_SetCurrentTrigger(
      currentTrigger: currentTrigger == freezed
          ? _value.currentTrigger
          : currentTrigger // ignore: cast_nullable_to_non_nullable
              as TooltipTrigger,
    ));
  }
}

/// @nodoc

class _$_SetCurrentTrigger implements _SetCurrentTrigger {
  const _$_SetCurrentTrigger({required this.currentTrigger});

  @override
  final TooltipTrigger currentTrigger;

  @override
  String toString() {
    return 'ChatEvent.setCurrentTrigger(currentTrigger: $currentTrigger)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetCurrentTrigger &&
            const DeepCollectionEquality()
                .equals(other.currentTrigger, currentTrigger));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(currentTrigger));

  @JsonKey(ignore: true)
  @override
  _$$_SetCurrentTriggerCopyWith<_$_SetCurrentTrigger> get copyWith =>
      __$$_SetCurrentTriggerCopyWithImpl<_$_SetCurrentTrigger>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function(String externalRoomId) init,
    required TResult Function(BuildContext context, String text) sendMessage,
    required TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)
        finishLoading,
    required TResult Function(
            String externalRoomId, List<Message> messages, List<Talker> talkers)
        fetchChat,
    required TResult Function(Emotion emotion, bool isMyEmotion) showEmotion,
    required TResult Function(Message message) mentionMessage,
    required TResult Function(BuildContext context, Message message)
        copyMessage,
    required TResult Function(Message message) editMessage,
    required TResult Function() closeOverhang,
    required TResult Function(Message message, BuildContext context)
        deleteMessage,
    required TResult Function(CopyWithLoaded copyWithLoaded) updateLoaded,
    required TResult Function(BuildContext context, Message message)
        reportMessage,
    required TResult Function(BuildContext context, Message message) blockUser,
    required TResult Function(
            Message message, bool isVisible, BuildContext context)
        changeMessageVisibility,
    required TResult Function(
            Talker talker, bool isVisible, BuildContext context)
        changeMessagesVisibility,
    required TResult Function(
            Talker talker, bool isBanned, BuildContext context)
        setBan,
    required TResult Function(Talker talker) updateTalker,
    required TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)
        loadMoreMessages,
    required TResult Function(ChatStateLoaded loadedState) sendPendingMessages,
    required TResult Function(Message message) setMessageRead,
    required TResult Function(Message message, ListScrollPosition? position)
        scrollToMessage,
    required TResult Function(ConnectivityResult connectivityResult)
        connectivityChanged,
    required TResult Function(
            BuildContext context, Message message, bool isPinned)
        setPinnedMessage,
    required TResult Function(Message? pinnedMessage, List<Message> messages)
        updatePinnedMessage,
    required TResult Function(Emotion emotion) changeSelectedEmotion,
    required TResult Function(bool isVisible) setEmotionPannelVisibility,
    required TResult Function(bool largeMessage) setLargeMessage,
    required TResult Function() sendSelectedEmotion,
    required TResult Function(bool isSwearingMessage) setSwearingMessage,
    required TResult Function(BuildContext context) setContext,
    required TResult Function(Preview preview, String url) addPreviewToCache,
    required TResult Function() showLoading,
    required TResult Function() toggleHand,
    required TResult Function(RejectHandRequest rejectHandRequest) rejectHand,
    required TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)
        setRole,
    required TResult Function(SetMuteRequest setMuteRequest) setMute,
    required TResult Function(PermissionStatus micStatus) setMicStatus,
    required TResult Function(bool stickerMode) setStickerMode,
    required TResult Function(int stickerId) sendSticker,
    required TResult Function(ReactionDto reactionDto) createReaction,
    required TResult Function(ReactionDto reactionDto) deleteReaction,
    required TResult Function(TooltipTrigger currentTrigger) setCurrentTrigger,
  }) {
    return setCurrentTrigger(currentTrigger);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
  }) {
    return setCurrentTrigger?.call(currentTrigger);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function(String externalRoomId)? init,
    TResult Function(BuildContext context, String text)? sendMessage,
    TResult Function(
            Message? pinnedMessage,
            List<Message> messages,
            List<Talker> talkers,
            Talker talker,
            String externalRoomId,
            List<int> initiatorIds,
            List<int> targetIds,
            Room room,
            Ban? ban,
            List<Sticker> stickers,
            bool isSlowmodeInterrupted,
            int? initialElapsedDelay)?
        finishLoading,
    TResult Function(String externalRoomId, List<Message> messages,
            List<Talker> talkers)?
        fetchChat,
    TResult Function(Emotion emotion, bool isMyEmotion)? showEmotion,
    TResult Function(Message message)? mentionMessage,
    TResult Function(BuildContext context, Message message)? copyMessage,
    TResult Function(Message message)? editMessage,
    TResult Function()? closeOverhang,
    TResult Function(Message message, BuildContext context)? deleteMessage,
    TResult Function(CopyWithLoaded copyWithLoaded)? updateLoaded,
    TResult Function(BuildContext context, Message message)? reportMessage,
    TResult Function(BuildContext context, Message message)? blockUser,
    TResult Function(Message message, bool isVisible, BuildContext context)?
        changeMessageVisibility,
    TResult Function(Talker talker, bool isVisible, BuildContext context)?
        changeMessagesVisibility,
    TResult Function(Talker talker, bool isBanned, BuildContext context)?
        setBan,
    TResult Function(Talker talker)? updateTalker,
    TResult Function(
            ChatStateLoaded loadedState, Message? scrollToMessage, int? limit)?
        loadMoreMessages,
    TResult Function(ChatStateLoaded loadedState)? sendPendingMessages,
    TResult Function(Message message)? setMessageRead,
    TResult Function(Message message, ListScrollPosition? position)?
        scrollToMessage,
    TResult Function(ConnectivityResult connectivityResult)?
        connectivityChanged,
    TResult Function(BuildContext context, Message message, bool isPinned)?
        setPinnedMessage,
    TResult Function(Message? pinnedMessage, List<Message> messages)?
        updatePinnedMessage,
    TResult Function(Emotion emotion)? changeSelectedEmotion,
    TResult Function(bool isVisible)? setEmotionPannelVisibility,
    TResult Function(bool largeMessage)? setLargeMessage,
    TResult Function()? sendSelectedEmotion,
    TResult Function(bool isSwearingMessage)? setSwearingMessage,
    TResult Function(BuildContext context)? setContext,
    TResult Function(Preview preview, String url)? addPreviewToCache,
    TResult Function()? showLoading,
    TResult Function()? toggleHand,
    TResult Function(RejectHandRequest rejectHandRequest)? rejectHand,
    TResult Function(
            SetRoleRequest setRoleRequest, BuildContext context, Talker talker)?
        setRole,
    TResult Function(SetMuteRequest setMuteRequest)? setMute,
    TResult Function(PermissionStatus micStatus)? setMicStatus,
    TResult Function(bool stickerMode)? setStickerMode,
    TResult Function(int stickerId)? sendSticker,
    TResult Function(ReactionDto reactionDto)? createReaction,
    TResult Function(ReactionDto reactionDto)? deleteReaction,
    TResult Function(TooltipTrigger currentTrigger)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setCurrentTrigger != null) {
      return setCurrentTrigger(currentTrigger);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_SendMessage value) sendMessage,
    required TResult Function(_FinishLoading value) finishLoading,
    required TResult Function(_FetchChat value) fetchChat,
    required TResult Function(_ShowEmotion value) showEmotion,
    required TResult Function(_MentionMessage value) mentionMessage,
    required TResult Function(_CopyMessage value) copyMessage,
    required TResult Function(_EditMessage value) editMessage,
    required TResult Function(_CloseOverhang value) closeOverhang,
    required TResult Function(_DeleteMessage value) deleteMessage,
    required TResult Function(_UpdateLoaded value) updateLoaded,
    required TResult Function(_ReportMessage value) reportMessage,
    required TResult Function(_BlockUser value) blockUser,
    required TResult Function(_ChangeMessageVisibility value)
        changeMessageVisibility,
    required TResult Function(_ChangeMessagesVisibility value)
        changeMessagesVisibility,
    required TResult Function(_SetBan value) setBan,
    required TResult Function(_UpdateTalker value) updateTalker,
    required TResult Function(LoadMoreMessages value) loadMoreMessages,
    required TResult Function(SendPendingMessages value) sendPendingMessages,
    required TResult Function(_SetMessageRead value) setMessageRead,
    required TResult Function(_ScrollToMessage value) scrollToMessage,
    required TResult Function(_ConnectivityChanged value) connectivityChanged,
    required TResult Function(ChatEventSetPinnedMessage value) setPinnedMessage,
    required TResult Function(_UpdatePinnedMessage value) updatePinnedMessage,
    required TResult Function(ChangeSelectedEmotion value)
        changeSelectedEmotion,
    required TResult Function(SetEmotionPannelVisibility value)
        setEmotionPannelVisibility,
    required TResult Function(_SetLargeMessage value) setLargeMessage,
    required TResult Function(SendSelectedEmotion value) sendSelectedEmotion,
    required TResult Function(_SetSwearingMessage value) setSwearingMessage,
    required TResult Function(_SetContext value) setContext,
    required TResult Function(ChatEventAddPreviewToCache value)
        addPreviewToCache,
    required TResult Function(_ShowLoading value) showLoading,
    required TResult Function(_ToggleHand value) toggleHand,
    required TResult Function(_RejectHand value) rejectHand,
    required TResult Function(_SetRole value) setRole,
    required TResult Function(_SetMute value) setMute,
    required TResult Function(_SetMicStatus value) setMicStatus,
    required TResult Function(_SetStickerMode value) setStickerMode,
    required TResult Function(_SendSticker value) sendSticker,
    required TResult Function(_CreateReaction value) createReaction,
    required TResult Function(_DeleteReaction value) deleteReaction,
    required TResult Function(_SetCurrentTrigger value) setCurrentTrigger,
  }) {
    return setCurrentTrigger(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
  }) {
    return setCurrentTrigger?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_SendMessage value)? sendMessage,
    TResult Function(_FinishLoading value)? finishLoading,
    TResult Function(_FetchChat value)? fetchChat,
    TResult Function(_ShowEmotion value)? showEmotion,
    TResult Function(_MentionMessage value)? mentionMessage,
    TResult Function(_CopyMessage value)? copyMessage,
    TResult Function(_EditMessage value)? editMessage,
    TResult Function(_CloseOverhang value)? closeOverhang,
    TResult Function(_DeleteMessage value)? deleteMessage,
    TResult Function(_UpdateLoaded value)? updateLoaded,
    TResult Function(_ReportMessage value)? reportMessage,
    TResult Function(_BlockUser value)? blockUser,
    TResult Function(_ChangeMessageVisibility value)? changeMessageVisibility,
    TResult Function(_ChangeMessagesVisibility value)? changeMessagesVisibility,
    TResult Function(_SetBan value)? setBan,
    TResult Function(_UpdateTalker value)? updateTalker,
    TResult Function(LoadMoreMessages value)? loadMoreMessages,
    TResult Function(SendPendingMessages value)? sendPendingMessages,
    TResult Function(_SetMessageRead value)? setMessageRead,
    TResult Function(_ScrollToMessage value)? scrollToMessage,
    TResult Function(_ConnectivityChanged value)? connectivityChanged,
    TResult Function(ChatEventSetPinnedMessage value)? setPinnedMessage,
    TResult Function(_UpdatePinnedMessage value)? updatePinnedMessage,
    TResult Function(ChangeSelectedEmotion value)? changeSelectedEmotion,
    TResult Function(SetEmotionPannelVisibility value)?
        setEmotionPannelVisibility,
    TResult Function(_SetLargeMessage value)? setLargeMessage,
    TResult Function(SendSelectedEmotion value)? sendSelectedEmotion,
    TResult Function(_SetSwearingMessage value)? setSwearingMessage,
    TResult Function(_SetContext value)? setContext,
    TResult Function(ChatEventAddPreviewToCache value)? addPreviewToCache,
    TResult Function(_ShowLoading value)? showLoading,
    TResult Function(_ToggleHand value)? toggleHand,
    TResult Function(_RejectHand value)? rejectHand,
    TResult Function(_SetRole value)? setRole,
    TResult Function(_SetMute value)? setMute,
    TResult Function(_SetMicStatus value)? setMicStatus,
    TResult Function(_SetStickerMode value)? setStickerMode,
    TResult Function(_SendSticker value)? sendSticker,
    TResult Function(_CreateReaction value)? createReaction,
    TResult Function(_DeleteReaction value)? deleteReaction,
    TResult Function(_SetCurrentTrigger value)? setCurrentTrigger,
    required TResult orElse(),
  }) {
    if (setCurrentTrigger != null) {
      return setCurrentTrigger(this);
    }
    return orElse();
  }
}

abstract class _SetCurrentTrigger implements ChatEvent {
  const factory _SetCurrentTrigger(
      {required final TooltipTrigger currentTrigger}) = _$_SetCurrentTrigger;

  TooltipTrigger get currentTrigger;
  @JsonKey(ignore: true)
  _$$_SetCurrentTriggerCopyWith<_$_SetCurrentTrigger> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$ChatState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)
        loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)?
        loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)?
        loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingState value) loading,
    required TResult Function(ChatStateLoaded value) loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LoadingState value)? loading,
    TResult Function(ChatStateLoaded value)? loaded,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingState value)? loading,
    TResult Function(ChatStateLoaded value)? loaded,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatStateCopyWith<$Res> {
  factory $ChatStateCopyWith(ChatState value, $Res Function(ChatState) then) =
      _$ChatStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$ChatStateCopyWithImpl<$Res> implements $ChatStateCopyWith<$Res> {
  _$ChatStateCopyWithImpl(this._value, this._then);

  final ChatState _value;
  // ignore: unused_field
  final $Res Function(ChatState) _then;
}

/// @nodoc
abstract class _$$LoadingStateCopyWith<$Res> {
  factory _$$LoadingStateCopyWith(
          _$LoadingState value, $Res Function(_$LoadingState) then) =
      __$$LoadingStateCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingStateCopyWithImpl<$Res> extends _$ChatStateCopyWithImpl<$Res>
    implements _$$LoadingStateCopyWith<$Res> {
  __$$LoadingStateCopyWithImpl(
      _$LoadingState _value, $Res Function(_$LoadingState) _then)
      : super(_value, (v) => _then(v as _$LoadingState));

  @override
  _$LoadingState get _value => super._value as _$LoadingState;
}

/// @nodoc

class _$LoadingState extends LoadingState {
  _$LoadingState() : super._();

  @override
  String toString() {
    return 'ChatState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$LoadingState);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)
        loaded,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)?
        loaded,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)?
        loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingState value) loading,
    required TResult Function(ChatStateLoaded value) loaded,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LoadingState value)? loading,
    TResult Function(ChatStateLoaded value)? loaded,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingState value)? loading,
    TResult Function(ChatStateLoaded value)? loaded,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class LoadingState extends ChatState {
  factory LoadingState() = _$LoadingState;
  LoadingState._() : super._();
}

/// @nodoc
abstract class _$$ChatStateLoadedCopyWith<$Res> {
  factory _$$ChatStateLoadedCopyWith(
          _$ChatStateLoaded value, $Res Function(_$ChatStateLoaded) then) =
      __$$ChatStateLoadedCopyWithImpl<$Res>;
  $Res call(
      {List<Sticker> stickerPack,
      bool stickerMode,
      BuildContext? context,
      Ban? ban,
      PermissionStatus? micStatus,
      Map<String, Preview> previewCache,
      bool largeMessage,
      bool isSwearingMessage,
      Message? pinnedMessage,
      bool isLoadedAllMessages,
      UnreadParams unreadParams,
      String externalRoomId,
      List<Talker> talkers,
      List<Message> messages,
      List<Emotion> allEmotions,
      Talker talker,
      MessageInputType? messageInputType,
      List<int> initiatorIds,
      List<int> targetIds,
      bool noneConnection,
      bool showEmojiPannel,
      Emotion selectedEmotion,
      bool showCounter,
      bool havePendingMessages,
      bool isSlowmodeInterrupted,
      TooltipTrigger currentTip,
      int? initialElapsedDelay,
      Room room});

  $MessageInputTypeCopyWith<$Res>? get messageInputType;
}

/// @nodoc
class __$$ChatStateLoadedCopyWithImpl<$Res>
    extends _$ChatStateCopyWithImpl<$Res>
    implements _$$ChatStateLoadedCopyWith<$Res> {
  __$$ChatStateLoadedCopyWithImpl(
      _$ChatStateLoaded _value, $Res Function(_$ChatStateLoaded) _then)
      : super(_value, (v) => _then(v as _$ChatStateLoaded));

  @override
  _$ChatStateLoaded get _value => super._value as _$ChatStateLoaded;

  @override
  $Res call({
    Object? stickerPack = freezed,
    Object? stickerMode = freezed,
    Object? context = freezed,
    Object? ban = freezed,
    Object? micStatus = freezed,
    Object? previewCache = freezed,
    Object? largeMessage = freezed,
    Object? isSwearingMessage = freezed,
    Object? pinnedMessage = freezed,
    Object? isLoadedAllMessages = freezed,
    Object? unreadParams = freezed,
    Object? externalRoomId = freezed,
    Object? talkers = freezed,
    Object? messages = freezed,
    Object? allEmotions = freezed,
    Object? talker = freezed,
    Object? messageInputType = freezed,
    Object? initiatorIds = freezed,
    Object? targetIds = freezed,
    Object? noneConnection = freezed,
    Object? showEmojiPannel = freezed,
    Object? selectedEmotion = freezed,
    Object? showCounter = freezed,
    Object? havePendingMessages = freezed,
    Object? isSlowmodeInterrupted = freezed,
    Object? currentTip = freezed,
    Object? initialElapsedDelay = freezed,
    Object? room = freezed,
  }) {
    return _then(_$ChatStateLoaded(
      stickerPack: stickerPack == freezed
          ? _value._stickerPack
          : stickerPack // ignore: cast_nullable_to_non_nullable
              as List<Sticker>,
      stickerMode: stickerMode == freezed
          ? _value.stickerMode
          : stickerMode // ignore: cast_nullable_to_non_nullable
              as bool,
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext?,
      ban: ban == freezed
          ? _value.ban
          : ban // ignore: cast_nullable_to_non_nullable
              as Ban?,
      micStatus: micStatus == freezed
          ? _value.micStatus
          : micStatus // ignore: cast_nullable_to_non_nullable
              as PermissionStatus?,
      previewCache: previewCache == freezed
          ? _value._previewCache
          : previewCache // ignore: cast_nullable_to_non_nullable
              as Map<String, Preview>,
      largeMessage: largeMessage == freezed
          ? _value.largeMessage
          : largeMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      isSwearingMessage: isSwearingMessage == freezed
          ? _value.isSwearingMessage
          : isSwearingMessage // ignore: cast_nullable_to_non_nullable
              as bool,
      pinnedMessage: pinnedMessage == freezed
          ? _value.pinnedMessage
          : pinnedMessage // ignore: cast_nullable_to_non_nullable
              as Message?,
      isLoadedAllMessages: isLoadedAllMessages == freezed
          ? _value.isLoadedAllMessages
          : isLoadedAllMessages // ignore: cast_nullable_to_non_nullable
              as bool,
      unreadParams: unreadParams == freezed
          ? _value.unreadParams
          : unreadParams // ignore: cast_nullable_to_non_nullable
              as UnreadParams,
      externalRoomId: externalRoomId == freezed
          ? _value.externalRoomId
          : externalRoomId // ignore: cast_nullable_to_non_nullable
              as String,
      talkers: talkers == freezed
          ? _value._talkers
          : talkers // ignore: cast_nullable_to_non_nullable
              as List<Talker>,
      messages: messages == freezed
          ? _value._messages
          : messages // ignore: cast_nullable_to_non_nullable
              as List<Message>,
      allEmotions: allEmotions == freezed
          ? _value._allEmotions
          : allEmotions // ignore: cast_nullable_to_non_nullable
              as List<Emotion>,
      talker: talker == freezed
          ? _value.talker
          : talker // ignore: cast_nullable_to_non_nullable
              as Talker,
      messageInputType: messageInputType == freezed
          ? _value.messageInputType
          : messageInputType // ignore: cast_nullable_to_non_nullable
              as MessageInputType?,
      initiatorIds: initiatorIds == freezed
          ? _value._initiatorIds
          : initiatorIds // ignore: cast_nullable_to_non_nullable
              as List<int>,
      targetIds: targetIds == freezed
          ? _value._targetIds
          : targetIds // ignore: cast_nullable_to_non_nullable
              as List<int>,
      noneConnection: noneConnection == freezed
          ? _value.noneConnection
          : noneConnection // ignore: cast_nullable_to_non_nullable
              as bool,
      showEmojiPannel: showEmojiPannel == freezed
          ? _value.showEmojiPannel
          : showEmojiPannel // ignore: cast_nullable_to_non_nullable
              as bool,
      selectedEmotion: selectedEmotion == freezed
          ? _value.selectedEmotion
          : selectedEmotion // ignore: cast_nullable_to_non_nullable
              as Emotion,
      showCounter: showCounter == freezed
          ? _value.showCounter
          : showCounter // ignore: cast_nullable_to_non_nullable
              as bool,
      havePendingMessages: havePendingMessages == freezed
          ? _value.havePendingMessages
          : havePendingMessages // ignore: cast_nullable_to_non_nullable
              as bool,
      isSlowmodeInterrupted: isSlowmodeInterrupted == freezed
          ? _value.isSlowmodeInterrupted
          : isSlowmodeInterrupted // ignore: cast_nullable_to_non_nullable
              as bool,
      currentTip: currentTip == freezed
          ? _value.currentTip
          : currentTip // ignore: cast_nullable_to_non_nullable
              as TooltipTrigger,
      initialElapsedDelay: initialElapsedDelay == freezed
          ? _value.initialElapsedDelay
          : initialElapsedDelay // ignore: cast_nullable_to_non_nullable
              as int?,
      room: room == freezed
          ? _value.room
          : room // ignore: cast_nullable_to_non_nullable
              as Room,
    ));
  }

  @override
  $MessageInputTypeCopyWith<$Res>? get messageInputType {
    if (_value.messageInputType == null) {
      return null;
    }

    return $MessageInputTypeCopyWith<$Res>(_value.messageInputType!, (value) {
      return _then(_value.copyWith(messageInputType: value));
    });
  }
}

/// @nodoc

class _$ChatStateLoaded extends ChatStateLoaded {
  _$ChatStateLoaded(
      {final List<Sticker> stickerPack = const [],
      this.stickerMode = false,
      this.context,
      this.ban,
      this.micStatus = null,
      final Map<String, Preview> previewCache = const {},
      this.largeMessage = false,
      this.isSwearingMessage = false,
      this.pinnedMessage,
      this.isLoadedAllMessages = false,
      this.unreadParams = const UnreadParams(),
      required this.externalRoomId,
      required final List<Talker> talkers,
      required final List<Message> messages,
      required final List<Emotion> allEmotions,
      required this.talker,
      this.messageInputType,
      required final List<int> initiatorIds,
      required final List<int> targetIds,
      this.noneConnection = false,
      this.showEmojiPannel = false,
      required this.selectedEmotion,
      this.showCounter = false,
      this.havePendingMessages = false,
      this.isSlowmodeInterrupted = false,
      this.currentTip = TooltipTrigger.longTapEmoji,
      this.initialElapsedDelay,
      required this.room})
      : _stickerPack = stickerPack,
        _previewCache = previewCache,
        _talkers = talkers,
        _messages = messages,
        _allEmotions = allEmotions,
        _initiatorIds = initiatorIds,
        _targetIds = targetIds,
        super._();

  final List<Sticker> _stickerPack;
  @override
  @JsonKey()
  List<Sticker> get stickerPack {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_stickerPack);
  }

  @override
  @JsonKey()
  final bool stickerMode;
  @override
  final BuildContext? context;
  @override
  final Ban? ban;
  @override
  @JsonKey()
  final PermissionStatus? micStatus;
  final Map<String, Preview> _previewCache;
  @override
  @JsonKey()
  Map<String, Preview> get previewCache {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_previewCache);
  }

  @override
  @JsonKey()
  final bool largeMessage;
  @override
  @JsonKey()
  final bool isSwearingMessage;
  @override
  final Message? pinnedMessage;
  @override
  @JsonKey()
  final bool isLoadedAllMessages;
  @override
  @JsonKey()
  final UnreadParams unreadParams;
  @override
  final String externalRoomId;
  final List<Talker> _talkers;
  @override
  List<Talker> get talkers {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_talkers);
  }

  final List<Message> _messages;
  @override
  List<Message> get messages {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_messages);
  }

  final List<Emotion> _allEmotions;
  @override
  List<Emotion> get allEmotions {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_allEmotions);
  }

  @override
  final Talker talker;
  @override
  final MessageInputType? messageInputType;
  final List<int> _initiatorIds;
  @override
  List<int> get initiatorIds {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_initiatorIds);
  }

  final List<int> _targetIds;
  @override
  List<int> get targetIds {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_targetIds);
  }

  @override
  @JsonKey()
  final bool noneConnection;
  @override
  @JsonKey()
  final bool showEmojiPannel;
  @override
  final Emotion selectedEmotion;
  @override
  @JsonKey()
  final bool showCounter;
  @override
  @JsonKey()
  final bool havePendingMessages;
  @override
  @JsonKey()
  final bool isSlowmodeInterrupted;
  @override
  @JsonKey()
  final TooltipTrigger currentTip;
  @override
  final int? initialElapsedDelay;
  @override
  final Room room;

  @override
  String toString() {
    return 'ChatState.loaded(stickerPack: $stickerPack, stickerMode: $stickerMode, context: $context, ban: $ban, micStatus: $micStatus, previewCache: $previewCache, largeMessage: $largeMessage, isSwearingMessage: $isSwearingMessage, pinnedMessage: $pinnedMessage, isLoadedAllMessages: $isLoadedAllMessages, unreadParams: $unreadParams, externalRoomId: $externalRoomId, talkers: $talkers, messages: $messages, allEmotions: $allEmotions, talker: $talker, messageInputType: $messageInputType, initiatorIds: $initiatorIds, targetIds: $targetIds, noneConnection: $noneConnection, showEmojiPannel: $showEmojiPannel, selectedEmotion: $selectedEmotion, showCounter: $showCounter, havePendingMessages: $havePendingMessages, isSlowmodeInterrupted: $isSlowmodeInterrupted, currentTip: $currentTip, initialElapsedDelay: $initialElapsedDelay, room: $room)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ChatStateLoaded &&
            const DeepCollectionEquality()
                .equals(other._stickerPack, _stickerPack) &&
            const DeepCollectionEquality()
                .equals(other.stickerMode, stickerMode) &&
            const DeepCollectionEquality().equals(other.context, context) &&
            const DeepCollectionEquality().equals(other.ban, ban) &&
            const DeepCollectionEquality().equals(other.micStatus, micStatus) &&
            const DeepCollectionEquality()
                .equals(other._previewCache, _previewCache) &&
            const DeepCollectionEquality()
                .equals(other.largeMessage, largeMessage) &&
            const DeepCollectionEquality()
                .equals(other.isSwearingMessage, isSwearingMessage) &&
            const DeepCollectionEquality()
                .equals(other.pinnedMessage, pinnedMessage) &&
            const DeepCollectionEquality()
                .equals(other.isLoadedAllMessages, isLoadedAllMessages) &&
            const DeepCollectionEquality()
                .equals(other.unreadParams, unreadParams) &&
            const DeepCollectionEquality()
                .equals(other.externalRoomId, externalRoomId) &&
            const DeepCollectionEquality().equals(other._talkers, _talkers) &&
            const DeepCollectionEquality().equals(other._messages, _messages) &&
            const DeepCollectionEquality()
                .equals(other._allEmotions, _allEmotions) &&
            const DeepCollectionEquality().equals(other.talker, talker) &&
            const DeepCollectionEquality()
                .equals(other.messageInputType, messageInputType) &&
            const DeepCollectionEquality()
                .equals(other._initiatorIds, _initiatorIds) &&
            const DeepCollectionEquality()
                .equals(other._targetIds, _targetIds) &&
            const DeepCollectionEquality()
                .equals(other.noneConnection, noneConnection) &&
            const DeepCollectionEquality()
                .equals(other.showEmojiPannel, showEmojiPannel) &&
            const DeepCollectionEquality()
                .equals(other.selectedEmotion, selectedEmotion) &&
            const DeepCollectionEquality()
                .equals(other.showCounter, showCounter) &&
            const DeepCollectionEquality()
                .equals(other.havePendingMessages, havePendingMessages) &&
            const DeepCollectionEquality()
                .equals(other.isSlowmodeInterrupted, isSlowmodeInterrupted) &&
            const DeepCollectionEquality()
                .equals(other.currentTip, currentTip) &&
            const DeepCollectionEquality()
                .equals(other.initialElapsedDelay, initialElapsedDelay) &&
            const DeepCollectionEquality().equals(other.room, room));
  }

  @override
  int get hashCode => Object.hashAll([
        runtimeType,
        const DeepCollectionEquality().hash(_stickerPack),
        const DeepCollectionEquality().hash(stickerMode),
        const DeepCollectionEquality().hash(context),
        const DeepCollectionEquality().hash(ban),
        const DeepCollectionEquality().hash(micStatus),
        const DeepCollectionEquality().hash(_previewCache),
        const DeepCollectionEquality().hash(largeMessage),
        const DeepCollectionEquality().hash(isSwearingMessage),
        const DeepCollectionEquality().hash(pinnedMessage),
        const DeepCollectionEquality().hash(isLoadedAllMessages),
        const DeepCollectionEquality().hash(unreadParams),
        const DeepCollectionEquality().hash(externalRoomId),
        const DeepCollectionEquality().hash(_talkers),
        const DeepCollectionEquality().hash(_messages),
        const DeepCollectionEquality().hash(_allEmotions),
        const DeepCollectionEquality().hash(talker),
        const DeepCollectionEquality().hash(messageInputType),
        const DeepCollectionEquality().hash(_initiatorIds),
        const DeepCollectionEquality().hash(_targetIds),
        const DeepCollectionEquality().hash(noneConnection),
        const DeepCollectionEquality().hash(showEmojiPannel),
        const DeepCollectionEquality().hash(selectedEmotion),
        const DeepCollectionEquality().hash(showCounter),
        const DeepCollectionEquality().hash(havePendingMessages),
        const DeepCollectionEquality().hash(isSlowmodeInterrupted),
        const DeepCollectionEquality().hash(currentTip),
        const DeepCollectionEquality().hash(initialElapsedDelay),
        const DeepCollectionEquality().hash(room)
      ]);

  @JsonKey(ignore: true)
  @override
  _$$ChatStateLoadedCopyWith<_$ChatStateLoaded> get copyWith =>
      __$$ChatStateLoadedCopyWithImpl<_$ChatStateLoaded>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)
        loaded,
  }) {
    return loaded(
        stickerPack,
        stickerMode,
        context,
        ban,
        micStatus,
        previewCache,
        largeMessage,
        isSwearingMessage,
        pinnedMessage,
        isLoadedAllMessages,
        unreadParams,
        externalRoomId,
        talkers,
        messages,
        allEmotions,
        talker,
        messageInputType,
        initiatorIds,
        targetIds,
        noneConnection,
        showEmojiPannel,
        selectedEmotion,
        showCounter,
        havePendingMessages,
        isSlowmodeInterrupted,
        currentTip,
        initialElapsedDelay,
        room);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)?
        loaded,
  }) {
    return loaded?.call(
        stickerPack,
        stickerMode,
        context,
        ban,
        micStatus,
        previewCache,
        largeMessage,
        isSwearingMessage,
        pinnedMessage,
        isLoadedAllMessages,
        unreadParams,
        externalRoomId,
        talkers,
        messages,
        allEmotions,
        talker,
        messageInputType,
        initiatorIds,
        targetIds,
        noneConnection,
        showEmojiPannel,
        selectedEmotion,
        showCounter,
        havePendingMessages,
        isSlowmodeInterrupted,
        currentTip,
        initialElapsedDelay,
        room);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(
            List<Sticker> stickerPack,
            bool stickerMode,
            BuildContext? context,
            Ban? ban,
            PermissionStatus? micStatus,
            Map<String, Preview> previewCache,
            bool largeMessage,
            bool isSwearingMessage,
            Message? pinnedMessage,
            bool isLoadedAllMessages,
            UnreadParams unreadParams,
            String externalRoomId,
            List<Talker> talkers,
            List<Message> messages,
            List<Emotion> allEmotions,
            Talker talker,
            MessageInputType? messageInputType,
            List<int> initiatorIds,
            List<int> targetIds,
            bool noneConnection,
            bool showEmojiPannel,
            Emotion selectedEmotion,
            bool showCounter,
            bool havePendingMessages,
            bool isSlowmodeInterrupted,
            TooltipTrigger currentTip,
            int? initialElapsedDelay,
            Room room)?
        loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(
          stickerPack,
          stickerMode,
          context,
          ban,
          micStatus,
          previewCache,
          largeMessage,
          isSwearingMessage,
          pinnedMessage,
          isLoadedAllMessages,
          unreadParams,
          externalRoomId,
          talkers,
          messages,
          allEmotions,
          talker,
          messageInputType,
          initiatorIds,
          targetIds,
          noneConnection,
          showEmojiPannel,
          selectedEmotion,
          showCounter,
          havePendingMessages,
          isSlowmodeInterrupted,
          currentTip,
          initialElapsedDelay,
          room);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(LoadingState value) loading,
    required TResult Function(ChatStateLoaded value) loaded,
  }) {
    return loaded(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(LoadingState value)? loading,
    TResult Function(ChatStateLoaded value)? loaded,
  }) {
    return loaded?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(LoadingState value)? loading,
    TResult Function(ChatStateLoaded value)? loaded,
    required TResult orElse(),
  }) {
    if (loaded != null) {
      return loaded(this);
    }
    return orElse();
  }
}

abstract class ChatStateLoaded extends ChatState {
  factory ChatStateLoaded(
      {final List<Sticker> stickerPack,
      final bool stickerMode,
      final BuildContext? context,
      final Ban? ban,
      final PermissionStatus? micStatus,
      final Map<String, Preview> previewCache,
      final bool largeMessage,
      final bool isSwearingMessage,
      final Message? pinnedMessage,
      final bool isLoadedAllMessages,
      final UnreadParams unreadParams,
      required final String externalRoomId,
      required final List<Talker> talkers,
      required final List<Message> messages,
      required final List<Emotion> allEmotions,
      required final Talker talker,
      final MessageInputType? messageInputType,
      required final List<int> initiatorIds,
      required final List<int> targetIds,
      final bool noneConnection,
      final bool showEmojiPannel,
      required final Emotion selectedEmotion,
      final bool showCounter,
      final bool havePendingMessages,
      final bool isSlowmodeInterrupted,
      final TooltipTrigger currentTip,
      final int? initialElapsedDelay,
      required final Room room}) = _$ChatStateLoaded;
  ChatStateLoaded._() : super._();

  List<Sticker> get stickerPack;
  bool get stickerMode;
  BuildContext? get context;
  Ban? get ban;
  PermissionStatus? get micStatus;
  Map<String, Preview> get previewCache;
  bool get largeMessage;
  bool get isSwearingMessage;
  Message? get pinnedMessage;
  bool get isLoadedAllMessages;
  UnreadParams get unreadParams;
  String get externalRoomId;
  List<Talker> get talkers;
  List<Message> get messages;
  List<Emotion> get allEmotions;
  Talker get talker;
  MessageInputType? get messageInputType;
  List<int> get initiatorIds;
  List<int> get targetIds;
  bool get noneConnection;
  bool get showEmojiPannel;
  Emotion get selectedEmotion;
  bool get showCounter;
  bool get havePendingMessages;
  bool get isSlowmodeInterrupted;
  TooltipTrigger get currentTip;
  int? get initialElapsedDelay;
  Room get room;
  @JsonKey(ignore: true)
  _$$ChatStateLoadedCopyWith<_$ChatStateLoaded> get copyWith =>
      throw _privateConstructorUsedError;
}
