import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:watchers_widget/src/app/di/locator.dart';
import 'package:watchers_widget/src/core/constants/constants.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/strings.dart';

import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/common/models/ban.dart';
import 'package:watchers_widget/src/features/onboarding/domain/licence.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/logic/onboarding_bloc.dart';
import 'package:watchers_widget/src/features/settings/presentation/screens/chat_rules_screen.dart';

class UserBannedWidget extends StatelessWidget {
  const UserBannedWidget({required this.ban,required this.chatRules});

  final Ban ban;
  final String chatRules;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 38.fw),
      child: Container(
        constraints: const BoxConstraints(maxWidth: 299),
        decoration: BoxDecoration(
          color: CustomColors.modalBackground,
          borderRadius: BorderRadius.circular(8),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 18.fw, vertical: 28.fh),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Администратор заблокировал вас за ${Strings.banReasons[ban.reason]}',
                style: TextStyles.title(fontWeight: FontWeight.w600, fontSize: 16),
                textAlign: TextAlign.center,
              ),

              if (ban.expires!=null)
                ...[
                  SizedBox(height: 7.fh),
                  Text(
                    'Вы заблокированы до ${buildDateFormat('dd MMMM, hh:mm').format(ban.expires!)}',
                    style: TextStyles.title(fontWeight: FontWeight.w400, fontSize: 13),
                    textAlign: TextAlign.center,
                  ),
                ],
              SizedBox(height: 30.fh),
              InkWell(
                onTap: (){
                  Navigator.of(context).push(ChatRulesScreen.route(chatRulesHtml: chatRules));
                },
                splashColor: Colors.transparent,
                child:Text(
                  'Правила чата',
                  style: TextStyles.title(fontWeight: FontWeight.w600, fontSize: 16,color:CustomColors.chatRuleButtonColor),
                  textAlign: TextAlign.center,
                ),
              ),
              SizedBox(height: 22.fh),
              InkWell(
                onTap: () async {
                  try {
                    await launchUrl(Uri(
                      scheme: 'mailto',
                      path: Constants.partnerEmail,
                      queryParameters: {
                        'subject': 'Бан',
                      },
                    ));  // Works like a charm...
                  } catch (e) {
                    print('Caught an error in Send us an e-mail!');
                    print('e is: ${e.toString()}');
                  }
                },
                splashColor: Colors.transparent,
                child:Text(
                  'Написать нам',
                  style: TextStyles.title(fontWeight: FontWeight.w600, fontSize: 16,color: CustomColors.wrightUsButtonColor),
                  textAlign: TextAlign.center,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
