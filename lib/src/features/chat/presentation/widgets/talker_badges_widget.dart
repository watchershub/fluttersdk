import 'package:flutter/material.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/vip_badge.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/common/widgets/badges/admin_badge.dart';
import 'package:watchers_widget/src/features/common/widgets/badges/invited_guest_badge.dart';

class TalkerBadgesWidget extends StatelessWidget {
  final Talker talker;

  const TalkerBadgesWidget({
    required this.talker,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: _badges(),
    );
  }

  List<Widget> _badges() {
    if (talker.user.isInvitedGuest) {
      return [
        const SizedBox(width: 3),
        const InvitedGuestBadge(),
      ];
    }

    if (talker.isModerOrAdmin) {
      return [
        const SizedBox(width: 3),
        const AdminBadge(),
      ];
    }

    if (talker.user.statusName != null) {
      return [
        const SizedBox(width: 3),
        VipBadgeWidget(statusName: talker.user.statusName!),
      ];
    }

    return [];
  }
}
