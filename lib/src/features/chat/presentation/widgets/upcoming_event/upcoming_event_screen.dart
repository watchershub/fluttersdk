import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/current_remaining_time.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/extensions/date_time_x.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/common/widgets/loading_widget.dart';

class UpcomingEventScreen extends StatefulWidget {
  final DateTime startTime;

  const UpcomingEventScreen({
    required this.startTime,
  });

  @override
  State<UpcomingEventScreen> createState() => _UpcomingEventScreenState();
}

class _UpcomingEventScreenState extends State<UpcomingEventScreen> {
  bool _isTimeExpired = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.fw),
      child: Stack(
        children: [
          if (_isTimeExpired) const Center(child: LoadingWidget()),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      _isTimeExpired ? 'Трансляция скоро начнется' : 'Запланированное событие',
                      style: TextStyles.title(),
                    ),
                  ],
                ),
              ),
              if (!_isTimeExpired) ...[
                const SizedBox(height: 50),
                Text(
                  'Начало ${widget.startTime.startDateFormatted}',
                  style: TextStyles.title(),
                ),
                const SizedBox(height: 16),
                CountdownTimer(
                  endTime: widget.startTime.millisecondsSinceEpoch,
                  widgetBuilder: _builderCountdownTimer,
                  onEnd: () {
                    setState(() {
                      _isTimeExpired = true;
                    });
                  },
                ),
              ],
            ],
          ),
        ],
      ),
    );
  }

  Widget _builderCountdownTimer(
    BuildContext context,
    CurrentRemainingTime? time,
  ) {
    if (time == null) return const SizedBox.shrink();

    String? days;
    if (time.days != null) {
      days = _getNumberAddZero(time.days!);
    }
    final hours = _getNumberAddZero(time.hours ?? 0);
    final min = _getNumberAddZero(time.min ?? 0);
    final sec = _getNumberAddZero(time.sec ?? 0);

    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        color: CustomColors.secondary,
      ),
      padding: EdgeInsets.fromLTRB(0.fw, 24, 0.fw, 32),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'Чат к трансляции откроется через:',
            style: TextStyles.secondary(fontSize: 13),
          ),
          const SizedBox(height: 12),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (days != null) ...[
                _buildTimeBox(days, 'день'),
                _buildTimeSeparator,
              ],
              _buildTimeBox(hours, 'часы'),
              _buildTimeSeparator,
              _buildTimeBox(min, 'минуты'),
              if (days == null) ...[
                _buildTimeSeparator,
                _buildTimeBox(sec, 'секунды'),
              ],
            ],
          ),
        ],
      ),
    );
  }

  /// 1 -> 01
  String _getNumberAddZero(int number) {
    if (number < 10) {
      return "0" + number.toString();
    }
    return number.toString();
  }

  Widget _buildTimeBox(String value, String measureName) => Container(
        width: 67,
        height: 49,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(12),
          color: CustomColors.inputBorder,
        ),
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(
                value,
                style: TextStyles.title(fontWeight: FontWeight.w600, fontSize: 16),
              ),
              Text(
                measureName,
                style: TextStyles.secondary(fontSize: 9),
              ),
            ],
          ),
        ),
      );

  Widget get _buildTimeSeparator => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8.0),
        child: Text(
          ':',
          style: TextStyles.secondary(fontSize: 16),
        ),
      );
}
