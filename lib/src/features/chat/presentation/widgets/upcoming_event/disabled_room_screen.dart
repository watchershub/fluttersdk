import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/common/widgets/loading_widget.dart';

class DisabledRoomScreen extends StatefulWidget {
  const DisabledRoomScreen();

  @override
  State<DisabledRoomScreen> createState() => _DisabledRoomScreenState();
}

class _DisabledRoomScreenState extends State<DisabledRoomScreen> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16.fw),
      child: Stack(
        children: [
          const Expanded(child: Center(child: LoadingWidget())),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Трансляция временно недоступна',
                      style: TextStyles.title(),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
