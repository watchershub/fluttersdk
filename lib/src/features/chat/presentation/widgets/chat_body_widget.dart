import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:watchers_widget/src/core/animations/animated_visibility.dart';
import 'package:watchers_widget/src/core/base/bloc_whens.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/packages/flutter_list_view/lib/src/flutter_list_view_element.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/core/utils/multi_hit_stack.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_mute_request.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/new_messages_flag_widget.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/people_counter_widget.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/pinned_message_view.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/scroll_down_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/animation_layer/animation_layer_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/be_first_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/emotion_panel.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message/chat_messages_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message_input/message_input_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/logic/onboarding_bloc.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/polls_widget.dart';
import 'package:watchers_widget/src/features/settings/presentation/settings_screen.dart';
import 'package:watchers_widget/src/features/talkers/presentation/talkers_screen.dart';
import 'package:watchers_widget/src/features/tooltips/domain/models/tooltip_trigger.dart';
import 'package:watchers_widget/src/features/tooltips/no_internet_tooltip.dart';
import 'package:watchers_widget/src/features/tooltips/slow_mode_info_tooltip.dart';
import 'package:watchers_widget/src/features/tooltips/tooltip_widget.dart';

import 'message_input/pannel_button.dart';
import 'message_input/sticker_picker_widget.dart';

class ChatScreenBodyWidget extends StatefulWidget {
  const ChatScreenBodyWidget({
    required this.bloc,
    required this.state,
    required this.showOverlay,
  });

  final ChatBloc bloc;
  final ChatStateLoaded state;
  final bool showOverlay;

  @override
  State<ChatScreenBodyWidget> createState() => _ChatScreenBodyWidgetState();
}

class _ChatScreenBodyWidgetState extends State<ChatScreenBodyWidget> {
  double offsetFromBottom = 0;

  @override
  Widget build(BuildContext context) {
    final state = widget.state;
    final bloc = widget.bloc;
    Widget toOnboardingWrapper({required Widget child}) {
      return Listener(
          behavior: HitTestBehavior.translucent,
          onPointerDown: (_) {
            if (!state.talker.user.isOnboard) {
              context.read<OnboardingBloc>().teleportToOnboarding(
                  OnboardingEventTeleportToOnboardingEvent(
                      onboardStage: OnboardStage.values[state.talker.user.onboardStage],
                      user: state.talker.user));
            }
          },
          child: IgnorePointer(
            ignoring: !state.talker.user.isOnboard,
            child: child,
          ));
    }

    return BlocListener<ChatBloc, ChatState>(
      listenWhen: (previous, current) => whenExactStateUpdated<ChatStateLoaded>(
          previous: previous,
          current: current,
          test: (previous, current) {
            // Поменялось первое сообщение (прилетело новое из сокета)
            final test = previous.messages.isNotEmpty &&
                current.messages.isNotEmpty &&
                previous.messages.first.id != current.messages.first.id;

            return test;
          }),
      listener: (context, state) {
        // Если мы внизу чата до добавления нового сообщения вниз, то доскроливаем
        if (bloc.scrollController.offset == 0.0) {
          WidgetsBinding.instance.addPostFrameCallback((_) async {
            bloc.scrollController.jumpTo(0);
          });
        }
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                if (widget.state.pinnedMessage != null)
                  AnimatedVisibility(
                    visible: !widget.showOverlay,
                    child: PinnedMessageView(
                      talker: widget.state.talker,
                      pinnedMessage: widget.state.pinnedMessage!,
                      onMessageTap: () {
                        bloc.setEmotionPannelVisibility(
                            const SetEmotionPannelVisibility(isVisible: false));

                        widget.bloc.add(ChatEvent.scrollToMessage(
                          message: widget.state.pinnedMessage!,
                        ));
                      },
                      onButtonTap: () {
                        widget.bloc.setPinnedMessage(ChatEventSetPinnedMessage(
                          context: context,
                          message: widget.state.pinnedMessage!,
                          isPinned: false,
                        ));
                      },
                    ),
                  ),
                Expanded(
                  child: GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () {
                      FocusManager.instance.primaryFocus?.unfocus();
                    },
                    child: MultiHitStack(
                      alignment:
                          widget.state.isChatEmpty ? Alignment.center : Alignment.bottomCenter,
                      children: [
                        if (widget.state.isChatEmpty)
                          const SingleChildScrollView(
                            physics: ClampingScrollPhysics(),
                            child: BeFirstWidget(),
                          )
                        else
                          const ChatMessagesWidget(),

                        // Close emoji pannel
                        if (state.showEmojiPannel)
                          Positioned.fill(
                            child: GestureDetector(
                              onTap: () {
                                bloc.setEmotionPannelVisibility(
                                    const SetEmotionPannelVisibility(isVisible: false));
                              },
                              child: Container(
                                color: Colors.transparent,
                              ),
                            ),
                          ),

                        // Опросы
                        Positioned(
                            key: const ValueKey('polls'),
                            top: 44,
                            right: 14,
                            child: AnimatedVisibility(
                              visible: !widget.showOverlay,
                              child: toOnboardingWrapper(child: PollsWidget(
                                onTap: () {
                                  bloc.setEmotionPannelVisibility(
                                      const SetEmotionPannelVisibility(isVisible: false));
                                },
                              )),
                            )),

                        // Positioned(
                        //   left: 12,
                        //   top: 12,
                        //   child: InkWell(
                        //       onTap: () {
                        //         if (state.room.isSpeak) {
                        //           bloc.soundCheck(context);
                        //         } else {
                        //           Navigator.of(context).pop();
                        //         }
                        //       },
                        //       child: const UniversalPicture(
                        //         Resources.leave,
                        //         24,
                        //         24,
                        //         BoxFit.scaleDown,
                        //       )),
                        // ),

                        Positioned(
                          right: 12,
                          top: 12,
                          child: AnimatedVisibility(
                            visible: !widget.showOverlay,
                            child: Row(
                              children: [
                                // Нет интернета
                                if (state.noneConnection)
                                  const NoInternetTooltip(
                                    tooltipText: 'Отсутствует интернет-соединение',
                                    preferredDirection: AxisDirection.down,
                                    child: SvgIcon(
                                      Resources.no_internet,
                                      color: CustomColors.noInternetIcon,
                                    ),
                                  ),
                                const SizedBox(width: 16),

                                if (state.room.isSlowmode && state.talker.isModerOrAdmin) ...[
                                  SlowmodeInfoTooltip(
                                    tooltipText:
                                        'Slow mode включен. Пользователи могут отправлять сообщения с промежутком ${Resources.slowmodeDelayMap[state.room.slowmodeDelayMS!]}, но на вас это не распространяется.',
                                    preferredDirection: AxisDirection.down,
                                    child: const SvgIcon(
                                      Resources.slowmodeIcon,
                                      //color: CustomColors.noInternetIcon,
                                    ),
                                  ),
                                  const SizedBox(width: 16),
                                ],

                                // Участники
                                if (state.talker.isModerOrAdmin || state.showCounter)
                                  PeopleCounterWidget(
                                    talkersCount: state.talkers.length.toString(),
                                    onCountTap: () {
                                      bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_UserList');
                                      Navigator.of(context).push(
                                        TalkersScreen.route(
                                          bloc: bloc,
                                          onToggleUserBan: (talker, value) {
                                            return bloc.setBan(
                                              ChatEvent.setBan(
                                                talker: talker,
                                                isBanned: value,
                                                context: context,
                                              ),
                                            );
                                          },
                                          onToggleMessagesVisibility: (talker, isVisible) {
                                            return bloc.changeMessagesVisibility(
                                              ChatEvent.changeMessagesVisibility(
                                                talker: talker,
                                                isVisible: isVisible,
                                                context: context,
                                              ),
                                            );
                                          },
                                        ),
                                      );
                                      bloc.setEmotionPannelVisibility(
                                          const SetEmotionPannelVisibility(isVisible: false));
                                    },
                                  ),
                                toOnboardingWrapper(
                                    child: state.currentTip == TooltipTrigger.firstBlock
                                        ? TooltipWidget(
                                            tooltipTrigger: TooltipTrigger.firstBlock,
                                            preferredDirection: AxisDirection.down,
                                            delayBeforeShow: 0,
                                            showTime: 4,
                                            offset: 10,
                                            child: PannelButton(
                                              buttonSize: 13,
                                              iconSize: 16,
                                              iconPath: Resources.settings,
                                              iconColor: CustomColors.talkersCounterIconColor,
                                              onTap: () {
                                                bloc.setEmotionPannelVisibility(
                                                    const SetEmotionPannelVisibility(
                                                        isVisible: false));
                                                Navigator.of(context).push(SettingsScreen.route(
                                                    talker: state.talker,
                                                    externalRoomId: state.externalRoomId));
                                              },
                                            ))
                                        : PannelButton(
                                            buttonSize: 13,
                                            iconSize: 16,
                                            iconPath: Resources.settings,
                                            iconColor: CustomColors.talkersCounterIconColor,
                                            onTap: () {
                                              bloc.setEmotionPannelVisibility(
                                                  const SetEmotionPannelVisibility(
                                                      isVisible: false));
                                              Navigator.of(context).push(SettingsScreen.route(
                                                  talker: state.talker,
                                                  externalRoomId: state.externalRoomId));
                                            },
                                          )),
                              ],
                            ),
                          ),
                        ),

                        // Стрелка вниз или каунтер непрочитанных
                        if (state.unreadParams.unreadMessages.isNotEmpty)
                          NewMessagesFlag(
                              onArrowDownTap: (tapCount) {
                                bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_ScrollToUnread');
                                if (tapCount == 0) {
                                  bloc.add(ChatEvent.scrollToMessage(
                                      position: ListScrollPosition.end,
                                      message: state.unreadParams.unreadMessages.first));
                                } else {
                                  widget.bloc.scrollController.animateTo(0,
                                      duration: const Duration(milliseconds: 100),
                                      curve: Curves.ease);
                                }
                              },
                              messagesCount: state.unreadParams.unreadMessages.length,
                              onCommonUnreadTap: () {
                                bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_ScrollToUnread');

                                bloc.add(ChatEvent.scrollToMessage(
                                    position: ListScrollPosition.end,
                                    message: state.unreadParams.unreadMessages.firstWhere(
                                      (element) => element.isMentionMe == false,
                                    )));
                              },
                              onMentionUnreadTap: () {
                                bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_ScrollToUnread');

                                bloc.add(ChatEvent.scrollToMessage(
                                    position: ListScrollPosition.end,
                                    message: state.unreadParams.unreadMessages
                                        .firstWhere((element) => element.isMentionMe == true)));
                              },
                              mentionUnreadMessagesCount: state.unreadParams.unreadMentions,
                              additionalBottomPadding: state.showEmojiPannel ? 52 : 0)
                        else
                          ScrollDownWidget(
                            onTap: () {
                              bloc.jumpToBottom(animateRescrolling: true);
                            },
                            scrollController: bloc.scrollController,
                            additionalArrowBottomPadding: state.showEmojiPannel ? 52 : 0,
                          ),
                        //Добавить три виджета с подсказками
                        // if (state.talker.user.hintStruct != null &&
                        //     state.talker.user.hintStruct!.shouldShowTips) ...[
                        //   ...state.talker.user.hintStruct!.tips
                        //       .map((e) => e.alreadyShown == false
                        //           ? Positioned(
                        //               bottom: 8,
                        //               right: 8,
                        //               child: UniversalToolTip(
                        //                 toolTipText: e.text,
                        //                 onTimeOutCLose: () {
                        //                   bloc.add(ChatEvent.setTipShown(tipName: e.name));
                        //                 },
                        //               ),
                        //             )
                        //           : Container())
                        //       .toList(),
                        // ],
                        // Выбор эмоджи
                        Positioned(
                          bottom: 8,
                          right: 8,
                          child: EmotionPannel(
                            isVisible: state.showEmojiPannel,
                            emotions: state.allEmotions,
                            onEmotionSelected: (emoji) {
                              bloc.changeSelectedEmotion(ChangeSelectedEmotion(emotion: emoji));
                            },
                          ),
                        ),

                        // Летящие эмоджи/голоса и прочее
                        const AnimationLayerWidget(),
                      ],
                    ),
                  ),
                ),

                // Todo(dartloli): пора делаеть его dependent widget
                if (state.room.status == "ENDED")
                  Container(
                    constraints: const BoxConstraints(maxHeight: 58),
                    decoration: const BoxDecoration(
                      color: CustomColors.modalBackground,
                    ),
                    child: Center(
                      child: Text(
                        'Трансляция завершена',
                        style: TextStyles.title(
                          fontWeight: FontWeight.w700,
                          fontSize: 17,
                          color: CustomColors.danger,
                        ),
                      ),
                    ),
                  )
                else
                  toOnboardingWrapper(
                      child: MessageInputWidget(
                    haveRaisedHands: state.talkersWithHands.isNotEmpty,
                    chatBloc: bloc,
                    talker: state.talker,
                    toggleHand: () {
                      widget.bloc.toggleHand(const ChatEvent.toggleHand());
                    },
                    isHandRaised: widget.state.talker.hand,
                    isSpeak: widget.state.room.isSpeak,
                    isDisabledSending:
                        widget.state.isSwearingMessage || widget.state.noneConnection,
                    enabled: !widget.state.talker.isBanned,
                    emotions: widget.state.allEmotions,
                    onLongPressEmotion: () {
                      widget.bloc.setEmotionPannelVisibility(
                          const SetEmotionPannelVisibility(isVisible: true));
                    },
                    onSendEmotion: () {
                      widget.bloc.setEmotionPannelVisibility(
                          const SetEmotionPannelVisibility(isVisible: false));
                      widget.bloc.sendSelectedEmotion(const SendSelectedEmotion());
                    },
                    selectedEmotion: widget.state.selectedEmotion,
                    onSettingsTap: () {
                      bloc.setEmotionPannelVisibility(
                          const SetEmotionPannelVisibility(isVisible: false));
                      Navigator.of(context).push(SettingsScreen.route(
                          talker: state.talker, externalRoomId: state.externalRoomId));
                    },
                    focusNode: widget.bloc.focusNode,
                    controller: widget.bloc.textEditingController,
                    onSendTap: () {
                      widget.bloc.add(ChatEvent.sendMessage(
                          text: widget.bloc.textEditingController.text.trim(), context: context));

                      if (!widget.bloc.scrollController.hasClients) return;
                      widget.bloc.scrollController.jumpTo(0);
                    },
                    hideEmotionPannel: () {
                      bloc.setEmotionPannelVisibility(
                          const SetEmotionPannelVisibility(isVisible: false));
                    },
                    onMicOnTap: () {
                      bloc.add(ChatEvent.setMute(
                          setMuteRequest:
                              SetMuteRequest(userId: widget.state.talker.user.id, isMuted: true)));
                    },
                    onMicOffTap: () async {
                      if (widget.state.micStatus != PermissionStatus.granted) {
                        if (widget.state.micStatus != null &&
                            widget.state.micStatus!.isPermanentlyDenied) {
                          openAppSettings();
                        } else {
                          await Permission.microphone.request();
                        }
                        bloc.add(
                            ChatEvent.setMicStatus(micStatus: await Permission.microphone.status));
                        if (await Permission.microphone.status.isGranted) {
                          bloc.add(ChatEvent.setMute(
                              setMuteRequest: SetMuteRequest(
                                  userId: widget.state.talker.user.id, isMuted: false)));
                        }
                      } else {
                        bloc.add(ChatEvent.setMute(
                            setMuteRequest: SetMuteRequest(
                                userId: widget.state.talker.user.id, isMuted: false)));
                      }
                    },
                  )),
                if (state.stickerMode) ...[
                  const Divider(
                    height: 1,
                    color: CustomColors.divider,
                  ),
                  const StickerPickerWidget(),
                  const Divider(
                    height: 1,
                    color: CustomColors.divider,
                  ),
                ]
              ],
            ),
          ),
        ],
      ),
    );
  }
}
