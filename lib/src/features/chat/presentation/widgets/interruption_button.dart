import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_countdown_timer/countdown_timer_controller.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/features/tooltips/send_interruption_tooltip.dart';

class InterruptionButton extends StatefulWidget {
  final int interruptionTime;
  final String intervalText;
  final void Function() onTimeEnd;

  const InterruptionButton({
    required this.interruptionTime,
    required this.onTimeEnd,
    required this.intervalText,
  });

  @override
  _InterruptionButtonState createState() => _InterruptionButtonState();
}

class _InterruptionButtonState extends State<InterruptionButton> {
  late CountdownTimerController controller;
  late int endTime;

  @override
  void initState() {
    endTime = DateTime.now().millisecondsSinceEpoch + widget.interruptionTime;
    controller = CountdownTimerController(endTime: endTime, onEnd: widget.onTimeEnd);

    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SendInterruptionTooltip(
        tooltipText: 'Вы можете отправлять сообщения в интервале ${widget.intervalText}',
        preferredDirection: AxisDirection.up,
        offset: 40,
        child: Center(
          child: CountdownTimer(
            endWidget: Container(),
            controller: controller,
            onEnd: widget.onTimeEnd,
            endTime: endTime,
            widgetBuilder: (context, remainingTime) {
              if (remainingTime!=null) {
                String timeText = '';
                String minutesString = '';
                String secondsString = '';
                if (remainingTime.min !=null){
                  minutesString = "${remainingTime.min.toString()}:";
                }

                if (remainingTime.sec !=null){
                  if (remainingTime.min!=null && remainingTime.sec!<10){
                    secondsString = '0${remainingTime.sec}';
                  } else {
                    secondsString = remainingTime.sec.toString();
                  }
                }

                timeText = minutesString + secondsString;

                return CircleAvatar(
                    radius: 19,
                    backgroundColor: CustomColors.secondary,
                    child: Text(
                      timeText,
                      style: TextStyles.title(
                          fontSize: 12,
                          fontWeight: FontWeight.w600,
                          color: CustomColors.contributionTextColor),
                    ));
              }
              return Container();
            },
            textStyle: TextStyles.title(
                fontSize: 12,
                fontWeight: FontWeight.w600,
                color: CustomColors.contributionTextColor),
          ),
        ));
  }
}
