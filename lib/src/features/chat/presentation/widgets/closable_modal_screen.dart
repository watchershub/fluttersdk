import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class ClosableModalScreen extends StatelessWidget {
  final Widget leadingIcon;
  final Widget body;
  final String titleText;
  final String subtitleText;

  const ClosableModalScreen({
    required this.leadingIcon,
    required this.titleText,
    required this.subtitleText,
    required this.body,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        Expanded(
          child: ClipRRect(
            borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(16),
              topRight: Radius.circular(16),
            ),
            child: Container(
              width: 100.w,
              color: CustomColors.modalBackground,
              child: Stack(
                children: [
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.fromLTRB(16, 16, 12, 16),
                        child: Row(
                          children: [
                            leadingIcon,
                            SizedBox(width: 10.fw),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  titleText,
                                  style: TextStyles.title(),
                                ),
                                Text(
                                  subtitleText,
                                  style: TextStyles.secondary(),
                                ),
                              ],
                            ),
                            const Spacer(),
                            Padding(
                              padding: const EdgeInsets.only(bottom: 16),
                              child: Padding(
                                padding: const EdgeInsets.all(4.0),
                                child: InkWell(
                                  onTap: () => Navigator.maybePop(context),
                                  child: const SvgIcon(Resources.close, color: CustomColors.danger),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      const Divider(
                        color: CustomColors.divider,
                        thickness: 1,
                      ),
                      body,
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
