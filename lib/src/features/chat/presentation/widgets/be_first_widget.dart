import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';

class BeFirstWidget extends StatelessWidget {
  const BeFirstWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(height: 50.fh),
            Container(
                constraints: const BoxConstraints(maxWidth: 258),
                child: Column(
                  children: [
                    Row(
                      children: [
                        _buildShimmer(
                          child: Container(
                            height: 32,
                            width: 32,
                            decoration: const BoxDecoration(
                                color: CustomColors.gray800, shape: BoxShape.circle),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Container(
                          padding: const EdgeInsets.all(8),
                          decoration: BoxDecoration(
                              border: Border.all(color: CustomColors.secondary),
                              color: CustomColors.beFirstContainerBackground,
                              borderRadius: BorderRadius.circular(8)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _buildShimmer(
                                child: Container(
                                  height: 8,
                                  width: 49,
                                  decoration: BoxDecoration(
                                      color: CustomColors.gray800,
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              _buildShimmer(
                                child: Container(
                                  height: 8,
                                  width: 141,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: CustomColors.gray800,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    const SizedBox(
                      height: 8,
                    ),
                    Row(
                      children: [
                        _buildShimmer(
                          child: Container(
                            height: 32,
                            width: 32,
                            decoration: const BoxDecoration(
                                color: CustomColors.gray800, shape: BoxShape.circle),
                          ),
                        ),
                        const SizedBox(
                          width: 8,
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 8, top: 8, bottom: 8, right: 29),
                          decoration: BoxDecoration(
                              border: Border.all(color: CustomColors.secondary),
                              color: CustomColors.beFirstContainerBackground,
                              borderRadius: BorderRadius.circular(8)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              _buildShimmer(
                                child: Container(
                                  height: 8,
                                  width: 49,
                                  decoration: BoxDecoration(
                                      color: CustomColors.gray800,
                                      borderRadius: BorderRadius.circular(8)),
                                ),
                              ),
                              const SizedBox(
                                height: 8,
                              ),
                              _buildShimmer(
                                child: Container(
                                  height: 8,
                                  width: 179,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(8),
                                    color: CustomColors.gray800,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                )),
            SizedBox(height: 24.fh),
            Text(
              'Поделись мыслями!',
              style: TextStyles.title(fontWeight: FontWeight.w700, fontSize: 15),
            ),
            SizedBox(height: 30.fh),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.fw),
              child: Text(
                'Начни обсуждать событие с другими игроками',
                style: TextStyles.title(fontSize: 15),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ));
  }

  Widget _buildShimmer({required Widget child}) {
    return Shimmer.fromColors(
        direction: ShimmerDirection.ltr,
        highlightColor: CustomColors.shimmerAnimationColor,
        baseColor: CustomColors.gray800,
        child: child);
  }
}
