import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/talker_badges_widget.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class TalkerItemWidget extends StatelessWidget {
  final Talker talker;
  final List<Widget> actions;

  const TalkerItemWidget({
    required this.talker,
    this.actions = const [],
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              ClipOval(
                child: CircleAvatar(
                  radius: 20,
                  child: UniversalPicture(
                    talker.user.pic,
                  ),
                ),
              ),
              const SizedBox(
                width: 12,
              ),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                        child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Flexible(
                          child: Text(
                            talker.user.name,
                            style: TextStyles.title(fontSize: 15),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                        TalkerBadgesWidget(talker: talker),
                      ],
                    )),
                    ...actions,
                  ],
                ),
              ),
            ],
          ),
        ),
        const Padding(
          padding: EdgeInsets.only(left: 53),
          child: Align(
            alignment: Alignment.bottomCenter,
            child: Divider(
              height: 1,
              thickness: 1,
              color: CustomColors.divider,
            ),
          ),
        )
      ],
    );
  }
}
