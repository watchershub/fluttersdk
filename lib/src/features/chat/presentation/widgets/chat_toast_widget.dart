import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class ChatToastWidget extends StatelessWidget {
  final String iconPath;
  final String text;

  final void Function()? onClose;

  const ChatToastWidget({
    required this.iconPath,
    required this.text,
    this.onClose,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(bottom: 8),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8.0),
          child: BackdropFilter(
            filter: ImageFilter.blur(
              sigmaX: CustomColors.toastBlurSigma,
              sigmaY: CustomColors.toastBlurSigma,
            ),
            child: Container(
              decoration: BoxDecoration(
                color: CustomColors.toastBackground,
                borderRadius: BorderRadius.circular(8),
              ),
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SvgIcon(
                      iconPath,
                      color: CustomColors.toastIcon,
                      size: 20,
                    ),
                    SizedBox(width: 12.fw),
                    Expanded(
                      child: Text(
                        text,
                        style: TextStyles.subhead.copyWith(color: CustomColors.toastTextColor),
                      ),
                    ),
                    SizedBox(width: 8.fw),
                    if (onClose != null)
                      GestureDetector(
                        onTap: onClose,
                        child: const Icon(
                          Icons.close,
                          color: CustomColors.toastCloseIcon,
                        ),
                      ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
