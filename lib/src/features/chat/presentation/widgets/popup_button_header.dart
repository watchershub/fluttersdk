import 'package:flutter/material.dart';
import 'package:separated_row/separated_row.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';

class PopupMenuButtonHeader extends StatelessWidget {
  final List<Widget> buttons;

  const PopupMenuButtonHeader({
    required this.buttons,
  });

  @override
  Widget build(BuildContext context) {
    return Material(
        color: Colors.transparent,
        child: SizedBox(
            height: 48,
            child: SeparatedRow(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              separatorBuilder: (BuildContext context, int index) {
                if (index != buttons.length - 1) {
                  return const VerticalDivider(
                    indent: 12,
                    thickness: 1,
                    color: CustomColors.divider,
                  );
                }
                return const SizedBox.shrink();
              },
              children: buttons.map((e) {
                return e;
              }).toList(),
            )));
  }
}
