import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/dependent_stateless_widget.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/create_delete_reaction_dto.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_mute_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/custom_popup_menu.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/emotion_panel.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/popup_button_header.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/popup_menu_button.dart'
    hide PopupMenuItemType;
import 'package:watchers_widget/src/features/chat/presentation/widgets/popup_menu_item.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';

class MessagePopupMenu extends BlocDependentStatelessWidget<ChatBloc, ChatEvent, ChatState> {
  final Message message;
  final bool isSentByUser;
  final bool isModer;
  final Widget child;
  final bool? isOnAdv;

  MessagePopupMenu({
    required this.message,
    required this.isSentByUser,
    required this.isModer,
    required this.child,
    this.isOnAdv,
  });

  final CustomPopupMenuController _controller = CustomPopupMenuController();

  @override
  bool buildWhen(ChatState previous, ChatState current) => false;

  @override
  Widget builder(BuildContext context, _, ChatBloc bloc) {
    return CustomPopupMenu(
      isLTR: !isSentByUser,
      position: PreferredPosition.top,
      controller: _controller,
      pressType: PressType.longPress,
      menuBuilder: () {
        return Column(
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 8),
              child: EmotionPannel(
                isVisible: true,
                emotions: (bloc.state as ChatStateLoaded).allEmotions,
                onEmotionSelected: (emoji) {
                  _onMenuTap(() {
                    if (message.reactions?.indexWhere((element) =>
                                (element.talkerId == (bloc.state as ChatStateLoaded).talker.id) &&
                                element.emotion == emoji.name) !=
                            -1 &&
                        message.reactions != null &&
                        message.reactions!.isNotEmpty) {
                      bloc.add(ChatEvent.deleteReaction(
                          reactionDto:
                              ReactionDto(emotion: emoji.name, messageId: message.id.toString())));
                    } else {
                      bloc.add(ChatEvent.createReaction(
                          reactionDto:
                              ReactionDto(emotion: emoji.name, messageId: message.id.toString())));
                    }
                  });
                },
              ),
            ),
            ClipRRect(
                borderRadius: BorderRadius.circular(12),
                child: Container(
                  width: 75.w,
                  color: CustomColors.modalBackground,
                  child: ListView(
                    physics: const NeverScrollableScrollPhysics(),
                    padding: EdgeInsets.zero,
                    shrinkWrap: true,
                    children: isSentByUser
                        ? _selfMessageMenu(bloc, context)
                        : _otherMessageMenu(bloc, context, isOnAdv),
                  ),
                )),
          ],
        );
      },
      child: child,
    );
  }

  void _onMenuTap(void Function() onTap) {
    onTap();
    _controller.hideMenu();
  }

  List<Widget> _otherMessageMenu(ChatBloc bloc, BuildContext context, bool? isOnAdv) => isOnAdv ==
          true
      ? [
          if (isModer)
            ..._buildPinnedMessageMenu(isPinned: message.isPinned, bloc: bloc, context: context),
          PopupMenuItemWidget(
            titleText: 'Копировать',
            iconPath: Resources.copy,
            onTap: () {
              _onMenuTap(() {
                bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_MessageCopy');
                bloc.add(ChatEvent.copyMessage(
                  message: message,
                  context: context,
                ));
              });
            },
          ),
        ]
      : [
          PopupMenuButtonHeader(
            buttons: [
              ..._headerCommonMenu(bloc, context),
              if (isModer) ..._headerModerMenu(bloc, context),
            ],
          ),
          _buildDivider(),
          if (isModer) ...[
            ..._moderMenu(bloc, context),
          ] else ...[
            ..._userMenu(bloc, context),
          ],
        ];

  List<Widget> _selfMessageMenu(ChatBloc bloc, BuildContext context) => [
        PopupMenuButtonHeader(buttons: [
          ..._headerCommonMenu(bloc, context),
        ]),
        _buildDivider(),
        PopupMenuItemWidget(
          titleText: 'Изменить',
          iconPath: Resources.edit,
          onTap: () {
            _onMenuTap(() {
              bloc.add(ChatEvent.editMessage(message: message));
            });
          },
        ),
        _buildDivider(),
        if (isModer)
          ..._buildPinnedMessageMenu(isPinned: message.isPinned, bloc: bloc, context: context),
        PopupMenuItemWidget(
          titleText: 'Удалить',
          iconPath: Resources.remove,
          popupMenuItemType: PopupMenuItemType.destructive,
          onTap: () {
            _onMenuTap(() {
              bloc.add(ChatEvent.deleteMessage(message: message, context: context));
            });
          },
        ),
      ];

  Widget _buildDivider() {
    return const Divider(
      indent: 12,
      height: 1,
      thickness: 1,
      color: CustomColors.divider,
    );
  }

  List<Widget> _buildPinnedMessageMenu({
    required bool isPinned,
    required ChatBloc bloc,
    required BuildContext context,
  }) =>
      [
        if (message.isPinned)
          PopupMenuItemWidget(
            titleText: 'Открепить',
            iconPath: Resources.unpin,
            onTap: () {
              _onMenuTap(() {
                bloc.setPinnedMessage(ChatEventSetPinnedMessage(
                  context: context,
                  message: message,
                  isPinned: false,
                ));
              });
            },
          )
        else
          PopupMenuItemWidget(
            titleText: 'Закрепить',
            iconPath: Resources.pin,
            onTap: () {
              _onMenuTap(() {
                bloc.setPinnedMessage(ChatEventSetPinnedMessage(
                  context: context,
                  message: message,
                  isPinned: true,
                ));
              });
            },
          ),
        _buildDivider(),
      ];

  Widget _buildPinnedButtonItem({
    required bool isPinned,
    required ChatBloc bloc,
    required BuildContext context,
  }) =>
      (message.isPinned)
          ? PopupHeaderButton(
              iconPath: Resources.unpin,
              onTap: () {
                _onMenuTap(() {
                  bloc.setPinnedMessage(ChatEventSetPinnedMessage(
                    context: context,
                    message: message,
                    isPinned: false,
                  ));
                });
              },
            )
          : PopupHeaderButton(
              iconPath: Resources.pin,
              onTap: () {
                _onMenuTap(() {
                  bloc.setPinnedMessage(ChatEventSetPinnedMessage(
                    context: context,
                    message: message,
                    isPinned: true,
                  ));
                });
              },
            );

  List<Widget> _userMenu(ChatBloc bloc, BuildContext context) => [
        if (!message.talker.isModerOrAdmin) ...[
          PopupMenuItemWidget(
            titleText: 'Заблокировать',
            iconPath: Resources.report,
            popupMenuItemType: PopupMenuItemType.destructive,
            onTap: () {
              _onMenuTap(() {
                bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_Ban');
                bloc.add(ChatEvent.blockUser(message: message, context: context));
              });
            },
          ),
          _buildDivider(),
        ],
        PopupMenuItemWidget(
          titleText: 'Пожаловаться на сообщение',
          iconPath: Resources.report_message,
          //popupMenuItemType: PopupMenuItemType.destructive,
          onTap: () {
            _onMenuTap(() {
              bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_Complain');
              bloc.add(ChatEvent.reportMessage(
                message: message,
                context: context,
              ));
            });
          },
        ),
        _buildDivider(),
        PopupMenuItemWidget(
          titleText: 'Пожаловаться на пользователя',
          iconPath: Resources.report_user,
          //popupMenuItemType: PopupMenuItemType.destructive,
          onTap: () {
            _onMenuTap(() {
              bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_Complain');
              bloc.add(ChatEvent.reportMessage(
                message: message,
                context: context,
              ));
            });
          },
        ),
      ];

  List<Widget> _moderMenu(ChatBloc bloc, BuildContext context) => [
        if (!(bloc.state as ChatStateLoaded).room.isSpeak)
          ..._buildPinnedMessageMenu(isPinned: message.isPinned, bloc: bloc, context: context)
        else ...[
          if (message.talker.role == "SPEAKER") ...[
            PopupMenuItemWidget(
              titleText: 'Убрать из спикеров',
              iconPath: Resources.remove_from_speakers,
              iconColor: CustomColors.danger,
              onTap: () {
                _onMenuTap(() {
                  bloc.add(ChatEvent.setRole(
                    setRoleRequest:
                        SetRoleRequest(userId: message.talker.user.id, role: TalkerRole.guest),
                    talker: message.talker,
                    context: context,
                  ));
                });
              },
            ),
            if (!message.talker.isModerOrAdmin) ...[
              _buildDivider(),
              if (!message.talker.isMuted)
                PopupMenuItemWidget(
                  titleText: 'Выключить микрофон',
                  iconPath: Resources.mic_off,
                  iconColor: CustomColors.danger,
                  onTap: () {
                    _onMenuTap(() {
                      bloc.add(ChatEvent.setMute(
                          setMuteRequest:
                              SetMuteRequest(userId: message.talker.user.id, isMuted: true)));
                    });
                  },
                )
              else
                PopupMenuItemWidget(
                  titleText: 'Включить микрофон',
                  iconPath: Resources.mic_on,
                  onTap: () {
                    _onMenuTap(() {
                      bloc.add(ChatEvent.setMute(
                          setMuteRequest:
                              SetMuteRequest(userId: message.talker.user.id, isMuted: false)));
                    });
                  },
                ),
            ]
          ] else
            PopupMenuItemWidget(
              titleText: 'Сделать спикером',
              iconPath: Resources.speaker,
              popupMenuItemType: PopupMenuItemType.positive,
              onTap: () {
                _onMenuTap(() {
                  bloc.add(ChatEvent.setRole(
                    setRoleRequest:
                        SetRoleRequest(userId: message.talker.user.id, role: TalkerRole.speaker),
                    talker: message.talker,
                    context: context,
                  ));
                });
              },
            ),
          _buildDivider(),
        ],
        if (message.isVisible)
          PopupMenuItemWidget(
            titleText: 'Скрыть сообщение',
            iconPath: Resources.hide_message,
            iconColor: CustomColors.danger,
            onTap: () {
              _onMenuTap(() {
                bloc.add(ChatEvent.changeMessageVisibility(
                  message: message,
                  isVisible: false,
                  context: context,
                ));
              });
            },
          )
        else
          PopupMenuItemWidget(
            titleText: 'Показать сообщение',
            iconPath: Resources.show_message,
            popupMenuItemType: PopupMenuItemType.positive,
            onTap: () {
              _onMenuTap(() {
                bloc.add(ChatEvent.changeMessageVisibility(
                  message: message,
                  isVisible: true,
                  context: context,
                ));
              });
            },
          ),
        // _buildDivider(),
        // if (message.talker.isSupressed)
        //   PopupMenuItemWidget(
        //     titleText: 'Показать сообщения',
        //     iconPath: Resources.show_message,
        //     popupMenuItemType: PopupMenuItemType.positive,
        //     onTap: () {
        //       _onMenuTap(() {
        //         bloc.add(ChatEvent.changeMessagesVisibility(
        //           context: context,
        //           talker: message.talker,
        //           isVisible: true,
        //         ));
        //       });
        //     },
        //   )
        // else
        //   PopupMenuItemWidget(
        //     titleText: 'Скрыть сообщения',
        //     iconPath: Resources.hide_message,
        //     iconColor: CustomColors.danger,
        //     onTap: () {
        //       _onMenuTap(() {
        //         bloc.add(ChatEvent.changeMessagesVisibility(
        //           talker: message.talker,
        //           isVisible: false,
        //           context: context,
        //         ));
        //       });
        //     },
        //   ),
        if (!message.talker.isModerOrAdmin) ...[
          const Divider(
            indent: 12,
            height: 1,
            color: CustomColors.divider,
          ),
          if (message.talker.isBanned)
            PopupMenuItemWidget(
              titleText: 'Разблокировать',
              iconPath: Resources.show_message,
              popupMenuItemType: PopupMenuItemType.positive,
              onTap: () {
                _onMenuTap(() {
                  bloc.setBan(ChatEvent.setBan(
                    talker: message.talker,
                    isBanned: false,
                    context: context,
                  ));
                });
              },
            )
          else
            PopupMenuItemWidget(
              titleText: 'Заблокировать',
              iconPath: Resources.report,
              popupMenuItemType: PopupMenuItemType.destructive,
              onTap: () {
                _onMenuTap(() {
                  bloc.setBan(ChatEvent.setBan(
                    talker: message.talker,
                    isBanned: true,
                    context: context,
                  ));
                });
              },
            )
        ]
      ];
  List<Widget> _headerModerMenu(ChatBloc bloc, BuildContext context) => [
        if ((bloc.state as ChatStateLoaded).room.isSpeak)
          _buildPinnedButtonItem(isPinned: message.isPinned, bloc: bloc, context: context),
      ];
  List<Widget> _headerCommonMenu(ChatBloc bloc, BuildContext context) => [
        PopupHeaderButton(
          iconPath: Resources.reply,
          onTap: () {
            _onMenuTap(() {
              bloc.add(ChatEvent.mentionMessage(message: message));
            });
          },
        ),
        PopupHeaderButton(
          iconPath: Resources.copy,
          onTap: () {
            _onMenuTap(() {
              bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_MessageCopy');

              bloc.add(ChatEvent.copyMessage(
                message: message,
                context: context,
              ));
            });
          },
        ),
      ];
}
