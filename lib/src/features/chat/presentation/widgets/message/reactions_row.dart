import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/dependent_stateless_widget.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/create_delete_reaction_dto.dart';
import 'package:watchers_widget/src/features/chat/domain/models/reaction.dart';
import 'package:watchers_widget/src/features/chat/domain/models/reaction_specific.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class ReactionsRow extends BlocDependentStatelessWidget<ChatBloc, ChatEvent, ChatState> {
  final List<Reaction> reactions;

  const ReactionsRow({required this.reactions,});

  @override
  Widget builder(BuildContext context, ChatState state, ChatBloc bloc) {
    return Container(
        constraints: const BoxConstraints(
          maxWidth: 240,
        ),
        child: Wrap(children: _computeSpecifics(reactions: reactions,bloc: bloc)));
  }

  List<Widget> _computeSpecifics({required List<Reaction> reactions,required ChatBloc bloc}) {
    Map<String, ReactionSpecific> computed = {};
    for (int i = 0; i < reactions.length; i++) {
      if (computed[reactions[i].emotion] != null) {
        final specific = computed[reactions[i].emotion];
        computed[reactions[i].emotion] = specific!.copyWith(
            reactionCount: specific.reactionCount + 1,
            hasMine: reactions[i].talkerId == (bloc.state as ChatStateLoaded).talker.id ? true : specific.hasMine);
      } else {
        computed.addAll({
          reactions[i].emotion:
              ReactionSpecific(reactionCount: 1, hasMine: reactions[i].talkerId == (bloc.state as ChatStateLoaded).talker.id ,reaction: reactions[i])
        });
      }
    }
    return computed.entries
        .map<Widget>((e) =>
        InkWell(
          splashColor: Colors.transparent,
            onTap: (){
              if (e.value.hasMine){
                bloc.add(ChatEvent.deleteReaction(reactionDto: ReactionDto(emotion: e.key,messageId: e.value.reaction.messageId.toString())));
              } else {
                bloc.add(ChatEvent.createReaction(reactionDto: ReactionDto(emotion: e.key,messageId: e.value.reaction.messageId.toString())));
              }
            },
            child: Container(
              margin: const EdgeInsets.only(bottom: 4,right: 4),
              padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 5),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: e.value.hasMine
                    ? CustomColors.myReactionColor
                    : CustomColors.reactionBackground,
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  UniversalPicture(
                      Resources.reactionToAssetName(e.key), 15, 15, BoxFit.scaleDown
                  ),
                  const SizedBox(
                    width: 2,
                  ),
                  Text(
                    e.value.reactionCount.toString(), style: TextStyles.title(
                      fontSize: 10,
                      fontWeight: FontWeight.w400,
                      color: e.value.hasMine
                          ? CustomColors.myReactionCountText
                          : CustomColors.reactionCountText,
                    ),
                  )
                ],
              ),
            )
        )
    ).toList();
  }
}
