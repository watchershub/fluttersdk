import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:watchers_widget/src/core/base/bloc_dependent_state.dart';
import 'package:watchers_widget/src/core/base/bloc_whens.dart';
import 'package:watchers_widget/src/core/extensions/date_time_x.dart';
import 'package:watchers_widget/src/core/packages/flutter_list_view/lib/flutter_list_view.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/quiz_answer_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message/chat_message_widget.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/logic/onboarding_bloc.dart';

class ChatMessagesWidget extends StatefulWidget {
  const ChatMessagesWidget();

  @override
  State<ChatMessagesWidget> createState() => _ChatMessagesWidgetState();
}

class _ChatMessagesWidgetState
    extends BlocDependentState<ChatMessagesWidget, ChatBloc, ChatEvent, ChatState> {
  @override
  bool buildWhen(ChatState previous, ChatState current) => whenExactStateUpdated<ChatStateLoaded>(
        previous: previous,
        current: current,
        test: (previous, current) =>
            previous.messages != current.messages ||
            previous.isLoadedAllMessages != current.isLoadedAllMessages ||
            previous.talker != current.talker,
      );

  final _messagesOnScreen = SplayTreeMap<int, Message>();

  bool _isLastVisibleMessage(Message message) {
    if (_messagesOnScreen.isEmpty) return false;

    return _messagesOnScreen[_messagesOnScreen.lastKey()] == message;
  }

  @override
  Widget builder(BuildContext context, ChatState state) {
    if (state is! ChatStateLoaded) return const SizedBox.shrink();
    final List<Message> availableMessages = [];
    for (int k = 0; k < state.messages.length; k++) {
      if (state.talker.isModerOrAdmin) {
        availableMessages.add(state.messages[k]);
      } else {
        if (state.messages[k].isVisible) {
          availableMessages.add(state.messages[k]);
        }
      }
    }
    return FlutterListView(
      key: bloc.chatKey,
      physics: const ClampingScrollPhysics(),
      reverse: true,
      controller: bloc.scrollController,
      delegate: FlutterListViewDelegate(
        (BuildContext context, int index) => _buildMessage(state, index, availableMessages),
        childCount: availableMessages.length,
        onItemKey: (index) => availableMessages[index].id.toString(),
        keepPosition: true,
        keepPositionOffset: 0,
        initIndex: 0,
        initOffset: 0.0,
        firstItemAlign: FirstItemAlign.start,
      ),
    );
  }

  Widget _buildMessage(
    ChatStateLoaded state,
    int index,
    List<Message> availableMessages,
  ) {
    final message = availableMessages[index];

    if (message.poll != null && !message.isPollResults) {
      return QuizAnswerWidget(
        optionsChosen: message.optionsChosen!,
        poll: message.poll!,
      );
    }

    final DateTime currentMessageDateTime = DateTime.parse(message.serverDateTime).toLocal();

    final DateTime prevDateTime = index < (availableMessages.length - 1)
        ? DateTime.parse(availableMessages[index + 1].serverDateTime).toLocal()
        : DateTime(1990);

    final DateTime nextDateTime = index > 0
        ? DateTime.parse(availableMessages[index - 1].serverDateTime).toLocal()
        : DateTime(3990);

    final bool prevIsDifferentDay = currentMessageDateTime.differFrom(prevDateTime);
    final bool nextIsDifferentDay = currentMessageDateTime.differFrom(nextDateTime);

    final bool iAmAdminOrModer = state.talker.role == 'ADMIN' || state.talker.isModer;

    bool notShowAvatar = (index != 0) &&
        (availableMessages[index - 1].talker.id == message.talker.id) &&
        !nextIsDifferentDay;

    bool notShowNickName = (index != availableMessages.length - 1) &&
        (availableMessages[index + 1].talker.id == message.talker.id) &&
        !prevIsDifferentDay;

    if (!iAmAdminOrModer) {
      notShowAvatar = notShowAvatar && availableMessages[index - 1].isVisible;
      notShowNickName = notShowNickName && availableMessages[index + 1].isVisible;
    }

    final showDateAbove = prevIsDifferentDay;
    Widget toOnboardingWrapper({required Widget child}) {
      return Listener(
          behavior: HitTestBehavior.translucent,
          onPointerDown: (_) {
            if (!state.talker.user.isOnboard) {
              context.read<OnboardingBloc>().teleportToOnboarding(
                  OnboardingEventTeleportToOnboardingEvent(
                      onboardStage: OnboardStage.values[state.talker.user.onboardStage],
                      user: state.talker.user));
            }
          },
          child: IgnorePointer(
            ignoring: !state.talker.user.isOnboard,
            child: child,
          ));
    }

    return toOnboardingWrapper(
        child: ChatMessageWidget(
      pollForPollResults: message.isPollResults ? message.poll : null,
      isNextAdv: index < availableMessages.length - 1
          ? availableMessages[index + 1].advertisement != null
          : true,
      isPrevAdv: index > 0 ? availableMessages[index - 1].advertisement != null : true,
      index: index,
      message: message,
      showDateAbove: showDateAbove,
      showAvatar: !notShowAvatar,
      showNickName: !notShowNickName,
      onAddToViewport: () {
        _messagesOnScreen[message.id] = message;
      },
      onRemoveFromViewport: () {
        _messagesOnScreen.removeWhere((key, value) => key == message.id);
      },
      isLastVisible: () => _isLastVisibleMessage(message),
    ));
  }
}
