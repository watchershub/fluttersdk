import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/bloc_whens.dart';
import 'package:watchers_widget/src/core/base/dependent_stateless_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/common/widgets/loading_widget.dart';

class LoaderForMessages extends BlocDependentStatelessWidget<ChatBloc, ChatEvent, ChatState> {
  const LoaderForMessages();

  @override
  bool buildWhen(ChatState previous, ChatState current) => whenExactStateUpdated<ChatStateLoaded>(
        previous: previous,
        current: current,
        test: (previous, current) =>
            previous.havePendingMessages != current.havePendingMessages ||
            previous.isLoadedAllMessages != current.isLoadedAllMessages,
      );

  @override
  Widget builder(BuildContext context, ChatState state, ChatBloc bloc) {
    if (state is! ChatStateLoaded) return const SizedBox.shrink();

    if (!state.isLoadedAllMessages && state.havePendingMessages) {
      return const SizedBox(width: 100, height: 50, child: LoadingWidget());
    }
    return SizedBox(width: 100, height: state.isLoadedAllMessages ? 0 : 0);
  }
}
