import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/widgets/poll_item_widget.dart';

class PollResultsMessage extends StatelessWidget {
  final Poll poll;
  final bool isMyMessage;

  const PollResultsMessage({
    required this.poll,
    required this.isMyMessage,
  });

  @override
  Widget build(BuildContext context) {
    final backgroundColor = isMyMessage
        ? CustomColors.pollResultsBackgroundMyMessage
        : CustomColors.pollResultsBackground;

    final titleColor =
        isMyMessage ? CustomColors.pollResultsTitleMyMessage : CustomColors.pollResultsTitle;

    final subtitleColor =
        isMyMessage ? CustomColors.pollResultsSubtitleMyMessage : CustomColors.pollResultsSubtitle;

    return Container(
      constraints: BoxConstraints(maxWidth: 205.fw),
      decoration: BoxDecoration(
        color: backgroundColor,
      ),
      padding: EdgeInsets.only(top: isMyMessage ? 0 : 8),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Flexible(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  poll.text,
                  style: TextStyle(
                    fontSize: 15,
                    color: titleColor,
                  ),
                  textAlign: TextAlign.left,
                ),
                const SizedBox(height: 2),
                Text(
                  'Результаты голосования',
                  style: TextStyle(
                    fontSize: 12,
                    color: subtitleColor,
                  ),
                  textAlign: TextAlign.left,
                ),
                ...poll.options.map<Widget>(
                  (e) {
                    final percent = e.votesLength != null && poll.votesSum != 0
                        ? e.votesLength! / poll.votesSum
                        : null;

                    return Padding(
                      padding: const EdgeInsets.only(top: 6),
                      child: PollItemWidget(
                        percent: percent,
                        rightPadding: 3,
                        isAnswerMessage: true,
                        nameStyle: TextStyles.title(
                          fontSize: 15,
                          fontWeight: FontWeight.w500,
                          color: titleColor,
                        ),
                        alreadyVoted: true,
                        pic: e.pic,
                        title: e.text,
                        backgroundColor: backgroundColor,
                        isQuiz: poll.isQuiz,
                        isRight: e.isRight,
                        isMultiple: poll.isMultiple,
                        isSelected: !poll.isQuiz,
                        isAnswerRight: false,
                        progressColor: subtitleColor,
                        percentColor: subtitleColor,
                        isPollResults: true,
                      ),
                    );
                  },
                ).toList(),
                const SizedBox(height: 8),
                Text(
                  _buildVotesSumString(),
                  //_buildVotesSumString(),
                  style: TextStyles.title(color: CustomColors.textTertiary,fontSize: 10,fontWeight: FontWeight.w400),
                  textAlign: TextAlign.left,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
  String _buildVotesSumString(){
    late String postString;
    final votesLengthString = poll.votesSum.toString();
    final int lastNumber = poll.votesSum % 10;
    if (lastNumber == 1){
      postString = 'голос';
    } else {
      if (lastNumber >0 && lastNumber<5){
        postString = 'голоса';
      } else {
        postString = 'голосов';
      }
    }
    return votesLengthString + ' ' + postString;
  }
}
