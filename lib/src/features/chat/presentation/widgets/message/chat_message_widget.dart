import 'package:flutter/material.dart';
import 'package:visibility_detector/visibility_detector.dart';
import 'package:watchers_widget/src/core/base/bloc_dependent_state.dart';
import 'package:watchers_widget/src/core/base/bloc_whens.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/packages/scroll_to_index/scroll_to_index.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/advertisement_message.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/date_header_widget.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/message_view.dart';
import 'package:watchers_widget/src/features/chat/domain/ui_models/unread_header_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll.dart';

class ChatMessageWidget extends StatefulWidget {
  final int index;
  final Message message;
  final bool showDateAbove;
  final bool showNickName;
  final bool showAvatar;
  final void Function() onAddToViewport;
  final void Function() onRemoveFromViewport;
  final bool Function() isLastVisible;
  final bool isNextAdv;
  final bool isPrevAdv;
  final Poll? pollForPollResults;

  const ChatMessageWidget({
    required this.index,
    required this.message,
    required this.showDateAbove,
    required this.showAvatar,
    required this.showNickName,
    required this.onAddToViewport,
    required this.onRemoveFromViewport,
    required this.isLastVisible,
    required this.isNextAdv,
    required this.isPrevAdv,
    required this.pollForPollResults,
  });

  @override
  State<ChatMessageWidget> createState() => _ChatMessageWidgetState();
}

class _ChatMessageWidgetState
    extends BlocDependentState<ChatMessageWidget, ChatBloc, ChatEvent, ChatState> {
  final GlobalKey _globalKey = GlobalKey();

  @override
  bool buildWhen(ChatState previous, ChatState current) => whenExactStateUpdated<ChatStateLoaded>(
        previous: previous,
        current: current,
        test: (previous, current) =>
            previous.unreadParams != current.unreadParams ||
            previous.talker.isModer != current.talker.isModer ||
            previous.isLoadedAllMessages != current.isLoadedAllMessages ||
            previous.havePendingMessages != current.havePendingMessages,
      );

  @override
  Widget builder(BuildContext context, ChatState state) {
    if (state is! ChatStateLoaded) return const SizedBox.shrink();

    return Container(
      key: _globalKey,
      child: VisibilityDetector(
        key: ValueKey(widget.index),
        onVisibilityChanged: (visibilityInfo) {
          if (!mounted) return;

          if (visibilityInfo.visibleFraction == 0) {
            widget.onRemoveFromViewport();
          }

          if (visibilityInfo.visibleFraction > 0) {
            widget.onAddToViewport();
          }

          if ((state.unreadParams.unreadMessages
                      .indexWhere((element) => element.id == widget.message.id) !=
                  -1) &&
              (visibilityInfo.visibleFraction == 1)) {
            bloc.add(ChatEvent.setMessageRead(message: widget.message));
          }
        },
        child: Column(
          children: [
            if (widget.showDateAbove)
              DateHeaderWidget(
                date: DateTime.parse(widget.message.serverDateTime),
              ),
            if (state.unreadParams.firstUnreadMessage?.id == widget.message.id)
              UnreadHeaderWidget(),
            AutoScrollTag(
              highlightColor: CustomColors.primary.withOpacity(0.1),
              key: ValueKey(widget.index),
              controller: bloc.autoScrollController,
              index: widget.index,
              child: widget.message.advertisement != null
                  ? AdvertisementMessage(
                      extendLowerPadding: !widget.isNextAdv,
                      extendUpperPadding: !widget.isPrevAdv,
                      isModer: state.talker.isModerOrAdmin,
                      message: widget.message,
                    )
                  : MessageView(
                      showNickName: widget.showNickName,
                      showAvatar: widget.showAvatar,
                      onMentionTap: () {
                        bloc.add(ChatEvent.scrollToMessage(
                          message: widget.message.mentionMessage!,
                        ));
                      },
                      isModer: state.talker.isModer,
                      message: widget.message,
                      pollForPollResults: widget.pollForPollResults,
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
