import 'package:flutter/cupertino.dart';
import 'package:watchers_widget/src/features/chat/domain/models/sticker.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class StickerView extends StatelessWidget {
  final Sticker sticker;
  final bool? resizeToMini;

  const StickerView({required this.sticker, this.resizeToMini});

  @override
  Widget build(BuildContext context) {
    return Container(
        constraints: BoxConstraints(
            maxWidth: resizeToMini == true ? 32 : 71, maxHeight: resizeToMini == true ? 32 : 71),
        child: UniversalPicture(sticker.pic, resizeToMini == true ? 32 : 71,
            resizeToMini == true ? 32 : 71, BoxFit.fill));
  }
}
