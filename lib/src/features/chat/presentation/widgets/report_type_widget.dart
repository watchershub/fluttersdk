import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/closable_modal_screen.dart';

enum ReportType { spam, violence, provocations, personalDetails, flood, fraudBegging, other }

extension ReportTypeX on ReportType {
  T map<T>({
    required T Function() spam,
    required T Function() violence,
    required T Function() provocations,
    required T Function() personalDetails,
    required T Function() flood,
    required T Function() fraudBegging,
    required T Function() other,
  }) {
    switch (this) {
      case ReportType.spam:
        return spam();
      case ReportType.violence:
        return violence();
      case ReportType.provocations:
        return provocations();
      case ReportType.personalDetails:
        return personalDetails();
      case ReportType.flood:
        return flood();
      case ReportType.fraudBegging:
        return fraudBegging();
      case ReportType.other:
        return other();
    }
  }

  String get title => map(
        violence: () => 'Оскорбления, угрозы, ругань',
        provocations: () => 'Провокации',
        personalDetails: () => 'Персональные данные',
        flood: () => 'Флуд',
        spam: () => 'Спам',
        fraudBegging: () => 'Мошенничество, попрошайничество',
        other: () => 'Другое',
      );

  String get reportReason => map(
        violence: () => 'VIOLENCE',
        provocations: () => 'PROVOCATIONS',
        personalDetails: () => 'PERSONAL_DETAILS',
        flood: () => 'FLOOD',
        spam: () => 'SPAM',
        fraudBegging: () => 'FRAUD/BEGGING',
        other: () => 'OTHER',
      );
}

class ReportTypeWidget extends StatelessWidget {
  final Function(ReportType reportType) onSelected;

  const ReportTypeWidget({
    required this.onSelected,
  });

  @override
  Widget build(BuildContext context) {
    return ClosableModalScreen(
      leadingIcon: const SvgIcon(
        Resources.report_badge,
        size: 48,
      ),
      titleText: 'Сообщить о нарушении',
      subtitleText: 'Жалобы отправляются анонимно',
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 36),
        child: ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          itemCount: ReportType.values.length,
          itemBuilder: (context, index) {
            final reportType = ReportType.values[index];

            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                TextButton(
                  onPressed: () {
                    Navigator.of(context).maybePop().then((value) => onSelected(reportType));
                  },
                  child: Row(
                    children: [
                      Text(
                        reportType.title,
                        style: TextStyles.errorTextStyle(fontSize: 17),
                        textAlign: TextAlign.left,
                      ),
                    ],
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }
}
