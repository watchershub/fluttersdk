import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';

class EmojiWidget extends StatefulWidget {
  final bool isTransparentBackground;
  final String emotionPath;
  final bool isShort;

  const EmojiWidget({
    super.key,
    required this.emotionPath,
    this.isTransparentBackground = false,
    this.isShort = false,
  });

  @override
  State<EmojiWidget> createState() => _EmojiWidgetState();
}

class _EmojiWidgetState extends State<EmojiWidget> {
  ImageProvider<Object>? _imageProvider;

  @override
  void dispose() {
    if (widget.isShort) {
      _imageProvider?.evict();
    }

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final image = Image.asset(
      'assets/emoji/${widget.emotionPath}${widget.isShort ? '_short' : ''}.gif',
      width: 50,
      package: 'watchers_widget',
    );

    _imageProvider = image.image;

    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: widget.isTransparentBackground
          ? image
          : CircleAvatar(
              backgroundColor: CustomColors.emojiBackground,
              child: image,
            ),
    );
  }
}
