part of 'animation_layer_cubit.dart';

@freezed
class AnimationLayerState with _$AnimationLayerState {
  const factory AnimationLayerState({
    required Map<String, Widget> emojis,
    required Map<String, Widget> votes,
    @Default(false) bool mirrorEmotion,
  }) = _AnimationLayerState;

  factory AnimationLayerState.initial() => const AnimationLayerState(
        emojis: {},
        votes: {},
      );
}
