import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:uuid/uuid.dart';
import 'package:watchers_widget/src/features/chat/data/chat_api.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';
import 'package:watchers_widget/src/features/chat/domain/models/emotion.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/emotion/get_emotion_by_name_scenario.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/emoji_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/animations/emoji_animation.dart';
import 'package:watchers_widget/src/features/common/widgets/animations/tweens/vote_animation_builder.dart';
import 'package:watchers_widget/src/features/common/widgets/animations/vote_animation.dart';
import 'package:watchers_widget/src/features/common/widgets/super_ellips_card.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_option.dart';

part 'animation_layer_state.dart';
part 'animation_layer_cubit.freezed.dart';

class AnimationLayerCubit extends Cubit<AnimationLayerState> {
  final ChatApi _chatApi;
  final IChatRepository _chatRepository;
  final GetEmotionByNameScenario _getEmotionByNameScenario;

  AnimationLayerCubit(
    this._chatApi,
    this._getEmotionByNameScenario,
    this._chatRepository,
  ) : super(AnimationLayerState.initial()) {
    _init();
  }

  final List<StreamSubscription> _streamSubscriptions = [];

  final Map<String, Widget> _emojis = {};

  final Map<String, Widget> _votes = {};

  int _voteAnimationCounter = 0;

  final List<VoteAnimationBuilder> _voteAnimationBuilders = [
    VoteAnim1(),
    VoteAnim2(),
    VoteAnim3(),
    VoteAnim4(),
    VoteAnim5()
  ];

  void _init() {
    _streamSubscriptions.addAll([
      // Подписка на сокет
      _chatApi.socketIsolateController.stream.listen((e) {
        final event = e['event'];
        final data = e['data'];

        if (event == 'voteCreated') {
          final pollOption = PollOption.fromMap(data['pollOption']);
          _showVoteAnimation(pollOption.pic);
          return;
        }

        if (event == 'emotion') {
          final emotionName = data['emotion']['name'];
          _showEmojiAnimation(_getEmotionByNameScenario.call(emotionName));
          return;
        }
      }),

      // Подписка на отправленные емоджи
      _chatRepository.$myEmotions.listen((emotion) {
        _showEmojiAnimation(emotion);
      }),
    ]);
  }

  Future<void> _showEmojiAnimation(Emotion emotion) async {
    final animationId = const Uuid().v4();
    _emojis.addAll({
      animationId: Padding(
        key: ValueKey(animationId),
        padding: const EdgeInsets.only(right: 12),
        child: Align(
          alignment: Alignment.bottomRight,
          child: EmojiAnimation(
            animationId: animationId,
            onCompleted: (animationId) {
              _emojis.remove(animationId);
            },
            mirror: !state.mirrorEmotion,
            child: EmojiWidget(
              emotionPath: emotion.path,
              isTransparentBackground: true,
            ),
          ),
        ),
      ),
    });

    emit(state.copyWith(
      emojis: Map<String, Widget>.from(_emojis),
      mirrorEmotion: !state.mirrorEmotion,
    ));
  }

  Future<void> _showVoteAnimation(String pic) async {
    if (pic.isEmpty) return;

    final animationId = const Uuid().v4();

    final animationBuilder =
        _voteAnimationBuilders[_voteAnimationCounter % _voteAnimationBuilders.length];

    final delayBeforeStart = Duration(milliseconds: 200 + (_voteAnimationCounter * 50));

    _votes.addAll({
      animationId: Positioned(
        key: ValueKey(animationId),
        top: 80,
        right: 16,
        child: VoteAnimation(
          voteAnimationBuilder: animationBuilder,
          animationId: animationId,
          onCompleted: (animationId) {
            _votes.remove(animationId);
          },
          child: SuperEllipseCard(
            child: UniversalPicture(pic, 20, 20, BoxFit.cover),
          ),
        ),
      ),
    });

    _voteAnimationCounter = (_voteAnimationCounter + 1) % 20;

    await Future.delayed(delayBeforeStart, () {
      emit(state.copyWith(
        votes: Map<String, Widget>.from(_votes),
      ));
    });
  }

  @override
  Future<void> close() {
    for (final sub in _streamSubscriptions) {
      sub.cancel();
    }
    return super.close();
  }
}
