// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'animation_layer_cubit.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$AnimationLayerState {
  Map<String, Widget> get emojis => throw _privateConstructorUsedError;
  Map<String, Widget> get votes => throw _privateConstructorUsedError;
  bool get mirrorEmotion => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $AnimationLayerStateCopyWith<AnimationLayerState> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $AnimationLayerStateCopyWith<$Res> {
  factory $AnimationLayerStateCopyWith(
          AnimationLayerState value, $Res Function(AnimationLayerState) then) =
      _$AnimationLayerStateCopyWithImpl<$Res>;
  $Res call(
      {Map<String, Widget> emojis,
      Map<String, Widget> votes,
      bool mirrorEmotion});
}

/// @nodoc
class _$AnimationLayerStateCopyWithImpl<$Res>
    implements $AnimationLayerStateCopyWith<$Res> {
  _$AnimationLayerStateCopyWithImpl(this._value, this._then);

  final AnimationLayerState _value;
  // ignore: unused_field
  final $Res Function(AnimationLayerState) _then;

  @override
  $Res call({
    Object? emojis = freezed,
    Object? votes = freezed,
    Object? mirrorEmotion = freezed,
  }) {
    return _then(_value.copyWith(
      emojis: emojis == freezed
          ? _value.emojis
          : emojis // ignore: cast_nullable_to_non_nullable
              as Map<String, Widget>,
      votes: votes == freezed
          ? _value.votes
          : votes // ignore: cast_nullable_to_non_nullable
              as Map<String, Widget>,
      mirrorEmotion: mirrorEmotion == freezed
          ? _value.mirrorEmotion
          : mirrorEmotion // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc
abstract class _$$_AnimationLayerStateCopyWith<$Res>
    implements $AnimationLayerStateCopyWith<$Res> {
  factory _$$_AnimationLayerStateCopyWith(_$_AnimationLayerState value,
          $Res Function(_$_AnimationLayerState) then) =
      __$$_AnimationLayerStateCopyWithImpl<$Res>;
  @override
  $Res call(
      {Map<String, Widget> emojis,
      Map<String, Widget> votes,
      bool mirrorEmotion});
}

/// @nodoc
class __$$_AnimationLayerStateCopyWithImpl<$Res>
    extends _$AnimationLayerStateCopyWithImpl<$Res>
    implements _$$_AnimationLayerStateCopyWith<$Res> {
  __$$_AnimationLayerStateCopyWithImpl(_$_AnimationLayerState _value,
      $Res Function(_$_AnimationLayerState) _then)
      : super(_value, (v) => _then(v as _$_AnimationLayerState));

  @override
  _$_AnimationLayerState get _value => super._value as _$_AnimationLayerState;

  @override
  $Res call({
    Object? emojis = freezed,
    Object? votes = freezed,
    Object? mirrorEmotion = freezed,
  }) {
    return _then(_$_AnimationLayerState(
      emojis: emojis == freezed
          ? _value._emojis
          : emojis // ignore: cast_nullable_to_non_nullable
              as Map<String, Widget>,
      votes: votes == freezed
          ? _value._votes
          : votes // ignore: cast_nullable_to_non_nullable
              as Map<String, Widget>,
      mirrorEmotion: mirrorEmotion == freezed
          ? _value.mirrorEmotion
          : mirrorEmotion // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_AnimationLayerState implements _AnimationLayerState {
  const _$_AnimationLayerState(
      {required final Map<String, Widget> emojis,
      required final Map<String, Widget> votes,
      this.mirrorEmotion = false})
      : _emojis = emojis,
        _votes = votes;

  final Map<String, Widget> _emojis;
  @override
  Map<String, Widget> get emojis {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_emojis);
  }

  final Map<String, Widget> _votes;
  @override
  Map<String, Widget> get votes {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableMapView(_votes);
  }

  @override
  @JsonKey()
  final bool mirrorEmotion;

  @override
  String toString() {
    return 'AnimationLayerState(emojis: $emojis, votes: $votes, mirrorEmotion: $mirrorEmotion)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AnimationLayerState &&
            const DeepCollectionEquality().equals(other._emojis, _emojis) &&
            const DeepCollectionEquality().equals(other._votes, _votes) &&
            const DeepCollectionEquality()
                .equals(other.mirrorEmotion, mirrorEmotion));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_emojis),
      const DeepCollectionEquality().hash(_votes),
      const DeepCollectionEquality().hash(mirrorEmotion));

  @JsonKey(ignore: true)
  @override
  _$$_AnimationLayerStateCopyWith<_$_AnimationLayerState> get copyWith =>
      __$$_AnimationLayerStateCopyWithImpl<_$_AnimationLayerState>(
          this, _$identity);
}

abstract class _AnimationLayerState implements AnimationLayerState {
  const factory _AnimationLayerState(
      {required final Map<String, Widget> emojis,
      required final Map<String, Widget> votes,
      final bool mirrorEmotion}) = _$_AnimationLayerState;

  @override
  Map<String, Widget> get emojis;
  @override
  Map<String, Widget> get votes;
  @override
  bool get mirrorEmotion;
  @override
  @JsonKey(ignore: true)
  _$$_AnimationLayerStateCopyWith<_$_AnimationLayerState> get copyWith =>
      throw _privateConstructorUsedError;
}
