import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/injectable_state.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/animation_layer/logic/animation_layer_cubit.dart';

class AnimationLayerWidget extends StatefulWidget {
  const AnimationLayerWidget();

  @override
  State<AnimationLayerWidget> createState() => _AnimationLayerWidgetState();
}

class _AnimationLayerWidgetState
    extends InjectableState<AnimationLayerWidget, AnimationLayerCubit, AnimationLayerState> {
  @override
  Widget builder(BuildContext context, AnimationLayerState state) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        ...state.emojis.values,
        ...state.votes.values,
      ],
    );
  }
}
