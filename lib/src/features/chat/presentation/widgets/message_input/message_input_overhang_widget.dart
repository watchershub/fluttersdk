import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/bloc_whens.dart';
import 'package:watchers_widget/src/core/base/dependent_stateless_widget.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/models/message_input_type.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/models/preview.dart';

class MessageInputOverhangWidget
    extends BlocDependentStatelessWidget<ChatBloc, ChatEvent, ChatState> {
  const MessageInputOverhangWidget();

  @override
  bool buildWhen(ChatState previous, ChatState current) => whenExactStateUpdated<ChatStateLoaded>(
        previous: previous,
        current: current,
        test: (previous, current) => previous.messageInputType != current.messageInputType,
      );

  @override
  Widget builder(BuildContext context, ChatState state, ChatBloc bloc) {
    if (state is ChatStateLoaded && state.messageInputType != null) {
      return _buildWidget(
        type: state.messageInputType!,
        onClose: () => bloc.add(const ChatEvent.closeOverhang()),
        bloc: bloc,
      );
    }

    return const SizedBox.shrink();
  }

  Widget _buildWidget({
    required MessageInputType type,
    required VoidCallback onClose,
    required ChatBloc bloc,
  }) {
    late final _type;
    late final Message message;
    if (type.isEdit){
      _type = type as EditType;
      message = _type.message;
    }
    if (type.isLink){
      _type = type as LinkType;
    }
    if (type.isReply){
      _type = type as ReplyType;
      message = _type.message;
    }

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        const Divider(height: 1, thickness: 1, color: CustomColors.divider),
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 8, 12, 8),
          child: Row(
            children: [
              SvgIcon(
                type.iconPath,
                color: type.isLink?CustomColors.primary:CustomColors.gray400,
              ),
              const SizedBox(width: 8),
              Container(
                color: type.isLink?CustomColors.primary:CustomColors.gray400,
                width: 1,
                height: 30,
              ),
              const SizedBox(width: 11),
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Flexible(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Text(
                                  type.isReply||type.isEdit?'${type.titlePrefix} ':_type.linkText,
                                  maxLines: 1,
                                  style: TextStyles.subtitle1(color: type.isLink?CustomColors.primary:CustomColors.textMain),
                                ),
                                Flexible(
                                  child: Text(
                                    type.isReply ? message.talker.user.name : '',
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyles.subtitle1(color: CustomColors.textMain),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Flexible(
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Flexible(
                                   child:  type.isReply||type.isEdit?
                                   Text(
                                    message.text,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyles.subtitle1(color: CustomColors.textSecondary),
                                  )
                                  :FutureBuilder(
                                      future: bloc.getPreview(url: _type.linkText),
                                      builder: (context, AsyncSnapshot<Preview?> snapshot) {
                                        if (snapshot.hasData &&
                                            snapshot.connectionState == ConnectionState.done &&
                                            snapshot.data != null) {
                                          final preview = snapshot.data!;
                                          return Text(
                                            preview.description,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyles.subtitle1(color: CustomColors.textSecondary),
                                          );
                                        }
                                        return Container();
                                      }),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    GestureDetector(
                      onTap: onClose,
                      child: const Padding(
                        padding: EdgeInsets.all(8.0),
                        child: SvgIcon(
                          Resources.close,
                          color: CustomColors.danger,
                          width: 13,
                          height: 13,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
