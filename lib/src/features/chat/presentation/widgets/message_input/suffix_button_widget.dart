import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:watchers_widget/src/core/base/bloc_whens.dart';
import 'package:watchers_widget/src/core/base/dependent_stateless_widget.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/models/message_input_type.dart';
import 'package:watchers_widget/src/features/common/models/message.dart';
import 'package:watchers_widget/src/features/common/models/preview.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class SuffixButtonWidget
    extends BlocDependentStatelessWidget<ChatBloc, ChatEvent, ChatState> {
  const SuffixButtonWidget();

  @override
  bool buildWhen(ChatState previous, ChatState current) => whenExactStateUpdated<ChatStateLoaded>(
    previous: previous,
    current: current,
    test: (previous, current) => previous.stickerMode != current.stickerMode,
  );

  @override
  Widget builder(BuildContext context, ChatState state, ChatBloc bloc) {
    if (state is ChatStateLoaded) {
      return _buildWidget(
        stickerMode: state.stickerMode,
        bloc: bloc,
      );
    }

    return const SizedBox.shrink();
  }

  Widget _buildWidget({
    required bool stickerMode,
    required ChatBloc bloc,
  }) {
    if (stickerMode) {
      return InkWell(
        splashColor: Colors.transparent,
        onTap: (){
          bloc.add(const ChatEvent.setStickerMode(stickerMode: false));
        },
        child:
        SvgPicture.asset(
            Resources.keyboard,
          width: 8,
          height: 8,
          fit: BoxFit.scaleDown,
          package: 'watchers_widget',
          color: Colors.black,
        ),
      );
    }
    return InkWell(
      splashColor: Colors.transparent,
      onTap: (){
        bloc.add(const ChatEvent.setStickerMode(stickerMode: true));
      },
      child: SvgPicture.asset(
        Resources.sticker,
        width: 8,
        height: 8,
        fit: BoxFit.scaleDown,
        package: 'watchers_widget',
      ),
    );
  }
}
