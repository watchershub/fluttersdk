import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/dependent_stateless_widget.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/sticker_view.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';
import 'package:watchers_widget/src/features/tooltips/send_interruption_tooltip.dart';

class StickerPickerWidget extends BlocDependentStatelessWidget<ChatBloc, ChatEvent, ChatState> {
  const StickerPickerWidget();

  @override
  Widget builder(BuildContext context, ChatState state, ChatBloc bloc) {
    if (state is ChatStateLoaded) {
      return Container(
        width: MediaQuery.of(context).size.width,
        height: 266,
        decoration: const BoxDecoration(color: CustomColors.chatBackground),
        child: Column(
          children: [
            const SizedBox(
              height: 19,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Padding(
                  padding: const EdgeInsets.only(right: 21),
                  child: InkWell(
                    onTap: () {
                      bloc.add(const ChatEvent.setStickerMode(stickerMode: false));
                    },
                    child: const UniversalPicture(
                      Resources.close,
                      14,
                      14,
                    ),
                  ),
                ),
              ],
            ),
            const SizedBox(
              height: 22,
            ),
            Expanded(
                child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
              itemBuilder: (context, index) {
                if ((bloc.state as ChatStateLoaded).isSlowmodeInterrupted) {
                  return SendInterruptionTooltip(
                    tooltipText: 'Вы можете отправлять сообщение в интервале ${Resources.slowmodeDelayMap[(bloc.state as ChatStateLoaded).room.slowmodeDelayMS]}',
                    preferredDirection: AxisDirection.up,
                    offset: 75,
                    child: StickerView(sticker: state.stickerPack[index]),
                  );
                } else {
                  return InkWell(
                    splashColor: Colors.transparent,
                    onTap: () {
                      bloc.add(ChatEvent.sendSticker(stickerId: state.stickerPack[index].id));
                    },
                    child: StickerView(sticker: state.stickerPack[index]),
                  );
                }
              },
              itemCount: state.stickerPack.length,
            )),
          ],
        ),
      );
    }

    return Container();
  }
}
