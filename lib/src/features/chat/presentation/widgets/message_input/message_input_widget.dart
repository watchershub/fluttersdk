import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:watchers_widget/src/core/constants/constants.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/core/utils/fitted_text.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/domain/models/emotion.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/screens/hands_screen.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/interruption_button.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message_input/message_input_overhang_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message_input/mic_off_icon.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message_input/pannel_button.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message_input/sticker_picker_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/message_input/suffix_button_widget.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/tooltips/domain/models/tooltip_trigger.dart';
import 'package:watchers_widget/src/features/tooltips/tooltip_widget.dart';

class MessageInputWidget extends StatefulWidget {
  final void Function() onSettingsTap;
  final void Function() onSendTap;
  final List<Emotion> emotions;
  final TextEditingController controller;
  final FocusNode focusNode;
  final void Function(String)? onSubmitted;
  final String? hintText;
  final OutlineInputBorder? outlineInputBorder;
  final InputDecoration? inputDecoration;
  final bool enabled;
  final Emotion selectedEmotion;
  final void Function() onSendEmotion;
  final void Function() onLongPressEmotion;
  final bool isDisabledSending;
  final void Function() hideEmotionPannel;
  final bool isSpeak;
  final bool isHandRaised;
  final Talker talker;
  final bool haveRaisedHands;
  final void Function() toggleHand;
  final ChatBloc chatBloc;
  final Function()? onMicOffTap;
  final Function()? onMicOnTap;

  const MessageInputWidget({
    required this.chatBloc,
    required this.isDisabledSending,
    required this.onSettingsTap,
    required this.controller,
    required this.onSendTap,
    required this.focusNode,
    required this.emotions,
    required this.enabled,
    required this.selectedEmotion,
    required this.onSendEmotion,
    required this.onLongPressEmotion,
    required this.hideEmotionPannel,
    required this.isSpeak,
    required this.isHandRaised,
    required this.haveRaisedHands,
    required this.talker,
    required this.toggleHand,
    this.onMicOffTap,
    this.onMicOnTap,
    this.outlineInputBorder,
    this.inputDecoration,
    this.onSubmitted,
    this.hintText,
  });

  @override
  _MessageInputWidgetState createState() => _MessageInputWidgetState();
}

class _MessageInputWidgetState extends State<MessageInputWidget>
    with SingleTickerProviderStateMixin {
  late AnimationController _colorAnimationController;
  late Animation<Color?> _borderColorAnimation;
  late Animation<Color?> _counterTextColorAnimation;
  late Animation<Color?> _inputFillingColorAnimation;

  @override
  void initState() {
    widget.focusNode.addListener(_updateFocusStatus);

    widget.controller.addListener(_updateShowSendStatus);

    _colorAnimationController =
        AnimationController(duration: const Duration(milliseconds: 300), vsync: this);

    _inputFillingColorAnimation =
        ColorTween(begin: CustomColors.inputFilling, end: CustomColors.danger.withOpacity(0.2))
            .animate(_colorAnimationController);

    _counterTextColorAnimation = ColorTween(begin: CustomColors.textMain, end: CustomColors.danger)
        .animate(_colorAnimationController);

    _borderColorAnimation = ColorTween(begin: CustomColors.divider, end: CustomColors.danger)
        .animate(_colorAnimationController)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          _colorAnimationController.reverse();
        }
      });

    super.initState();
  }

  bool _showSend = false;
  bool _hasFocus = false;

  void _updateShowSendStatus() {
    setState(() {
      _showSend = widget.controller.text.trim().isNotEmpty;
    });
  }

  void _updateFocusStatus() {
    setState(() {
      _hasFocus = widget.focusNode.hasFocus;
    });
    if (_hasFocus){
      widget.chatBloc.sendAmplitudeEvent(eventName: 'Social_Input_Message');
    }
    widget.hideEmotionPannel();
  }

  bool _isInputLimitReached = false;

  @override
  Widget build(BuildContext context) {
    final inputBorder = widget.outlineInputBorder ??
        OutlineInputBorder(
          borderSide: BorderSide(
            color: _borderColorAnimation.value ?? CustomColors.divider,
            width: 1,
          ),
          borderRadius: BorderRadius.circular(4),
        );

    final isShowCounter = _hasFocus && widget.controller.text.length >= 450;

    return Container(
      color: CustomColors.onPrimary,
      child: Column(
        children: [
          const MessageInputOverhangWidget(),
          const Divider(height: 1, thickness: 1, color: CustomColors.divider),
          const SizedBox(height: 8),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              if (!_hasFocus) ...[
                // PannelButton(
                //   padding: const EdgeInsets.only(left: 12),
                //   iconPath: Resources.settings,
                //   onTap: widget.onSettingsTap,
                // ),
                if (widget.isSpeak && widget.talker.isModerOrAdmin)
                  (widget.chatBloc.state as ChatStateLoaded).currentTip == TooltipTrigger.hasHands
                      ? TooltipWidget(
                          tooltipTrigger: TooltipTrigger.hasHands,
                          preferredDirection: AxisDirection.down,
                          delayBeforeShow: 0,
                          showTime: 4,
                          useDefaultTailBuilder: true,
                          offset: 10,
                          child: PannelButton(
                            iconColor: widget.haveRaisedHands
                                ? CustomColors.primary
                                : CustomColors.settingIconColor,
                            padding: const EdgeInsets.only(left: 12),
                            iconPath: Resources.hand_list,
                            onTap: () {
                              Navigator.of(context).push(HandsScreen.route(
                                awaitedTalkers: [],
                                bloc: widget.chatBloc,
                              ));
                            },
                          ))
                      : PannelButton(
                          iconColor: widget.haveRaisedHands
                              ? CustomColors.primary
                              : CustomColors.settingIconColor,
                          padding: const EdgeInsets.only(left: 12),
                          iconPath: Resources.hand_list,
                          onTap: () {
                            Navigator.of(context).push(HandsScreen.route(
                              awaitedTalkers: [],
                              bloc: widget.chatBloc,
                            ));
                          },
                        ),
              ],
              const SizedBox(width: 8),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left: 4),
                  child: widget.talker.isBanned
                      ? const Center(
                          child: FittedText(
                          'Вы не можете писать в чат',
                          style: TextStyles.hint,
                        ))
                      : Stack(
                          clipBehavior: Clip.none,
                          children: [
                            TextField(
                              inputFormatters: [
                                LengthLimitingTextInputFormatter(Constants.maxMessageInputLength),
                              ],
                              onChanged: (value) {
                                if (value.length == Constants.maxMessageInputLength) {
                                  if (_isInputLimitReached) {
                                    _colorAnimationController.reset();
                                    _colorAnimationController.forward();
                                    return;
                                  }
                                }

                                _isInputLimitReached =
                                    value.length == Constants.maxMessageInputLength;
                              },
                              minLines: 1,
                              maxLines: 3,
                              enabled: widget.enabled,
                              onSubmitted: widget.onSubmitted,
                              focusNode: widget.focusNode,
                              controller: widget.controller,
                              decoration: widget.inputDecoration ??
                                  InputDecoration(
                                    suffixIcon: const SuffixButtonWidget(),
                                    contentPadding: EdgeInsets.only(
                                      top: 10.0,
                                      bottom: 10.0,
                                      right: isShowCounter ? 75 : 20.0,
                                      left: 20,
                                    ),
                                    isCollapsed: true,
                                    isDense: true,
                                    hintText: widget.hintText ?? 'Сообщение...',
                                    border: inputBorder,
                                    disabledBorder: inputBorder,
                                    enabledBorder: inputBorder,
                                    focusedBorder: inputBorder,
                                    hintStyle: TextStyles.hint,
                                    filled: true,
                                    fillColor:
                                        _inputFillingColorAnimation.value?.withOpacity(0.2) ??
                                            CustomColors.inputFilling,
                                  ),
                              style: TextStyles.input,
                            ),
                            if (isShowCounter)
                              Positioned(
                                right: 20,
                                bottom: 10,
                                child: Text(
                                  '${widget.controller.text.length}/${Constants.maxMessageInputLength}',
                                  style: TextStyles.input.copyWith(
                                    fontSize: 14,
                                    color: _counterTextColorAnimation.value,
                                  ),
                                ),
                              ),
                          ],
                        ),
                ),
              ),
              const SizedBox(width: 8),
              MessageButtonWidget(
                onMicOffTap: widget.onMicOffTap,
                onMicOnTap: widget.onMicOnTap,
                isDisableSending: widget.isDisabledSending,
                onLongPressEmotion: widget.onLongPressEmotion,
                onSendEmotion: widget.onSendEmotion,
                onSendTap: widget.onSendTap,
                selectedEmotion: widget.selectedEmotion,
                hasFocus: _hasFocus,
                showSend: _showSend,
                isSpeak: widget.isSpeak,
                isHandRaised: widget.isHandRaised,
                toggleHand: widget.toggleHand,
                talker: widget.talker,
                bloc: widget.chatBloc,
              ),
            ],
          ),
          const SizedBox(height: 8),
        ],
      ),
    );
  }

  @override
  void dispose() {
    widget.focusNode.removeListener(_updateFocusStatus);
    widget.controller.removeListener(_updateShowSendStatus);
    super.dispose();
  }
}

class MessageButtonWidget extends StatefulWidget {
  final bool hasFocus;
  final bool showSend;
  final VoidCallback onSendTap;
  final VoidCallback onSendEmotion;
  final VoidCallback onLongPressEmotion;
  final Emotion selectedEmotion;
  final bool isDisableSending;
  final bool isSpeak;
  final bool isHandRaised;
  final void Function() toggleHand;
  final Talker talker;
  final void Function()? onMicOffTap;
  final void Function()? onMicOnTap;
  final ChatBloc bloc;

  const MessageButtonWidget({
    required this.hasFocus,
    required this.showSend,
    required this.onSendTap,
    required this.onSendEmotion,
    required this.onLongPressEmotion,
    required this.selectedEmotion,
    required this.isDisableSending,
    required this.isSpeak,
    required this.isHandRaised,
    required this.toggleHand,
    required this.talker,
    this.onMicOnTap,
    this.onMicOffTap,
    required this.bloc,
  });

  @override
  State<MessageButtonWidget> createState() => _MessageButtonWidgetState();
}

class _MessageButtonWidgetState extends State<MessageButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        if (widget.showSend)
          Padding(
            padding: const EdgeInsets.only(right: 8),
            child: widget.showSend
                ? (widget.bloc.state as ChatStateLoaded).isSlowmodeInterrupted
                    ? InterruptionButton(
                        interruptionTime:
                                    (widget.bloc.state as ChatStateLoaded).room.slowmodeDelayMS! -
                                widget.bloc.interruptionStopwatch.elapsedMilliseconds - ((widget.bloc.state as ChatStateLoaded).initialElapsedDelay ?? 0),
                        onTimeEnd: () {},
                        intervalText: Resources.slowmodeDelayMap[
                            (widget.bloc.state as ChatStateLoaded).room.slowmodeDelayMS!]!,
                      )
                    : InkWell(
                        splashColor: Colors.transparent,
                        onTap: widget.isDisableSending ? null : widget.onSendTap,
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: SvgIcon(
                            Resources.send,
                            size: 38,
                            color: widget.isDisableSending
                                ? CustomColors.textSecondary
                                : CustomColors.primary,
                          ),
                        ),
                      )
                : const SizedBox.shrink(),
          ),
        if (!widget.hasFocus) ...[
          if (widget.isSpeak)
            if (widget.talker.role == TalkerRole.speaker.toStr || widget.talker.isModerOrAdmin)
              if (widget.talker.isMuted)
                PannelButton(
                  content: const MicOffIcon(
                    iconColor: CustomColors.settingIconColor,
                  ),
                  onTap: widget.onMicOffTap,
                )
              else
                PannelButton(
                  iconPath: Resources.mic_on,
                  onTap: widget.onMicOnTap,
                  iconColor: CustomColors.primary,
                )
            else
              (widget.bloc.state as ChatStateLoaded).currentTip == TooltipTrigger.requestMic
                  ? TooltipWidget(
                      tooltipTrigger: TooltipTrigger.requestMic,
                      preferredDirection: AxisDirection.up,
                      delayBeforeShow: 0,
                      showTime: 4,
                      offset: 10,
                      child: PannelButton(
                        onTap: widget.toggleHand,
                        iconPath: Resources.hand,
                        iconColor: widget.isHandRaised
                            ? CustomColors.primary
                            : CustomColors.settingIconColor,
                      ))
                  : PannelButton(
                      onTap: widget.toggleHand,
                      iconPath: Resources.hand,
                      iconColor: widget.isHandRaised
                          ? CustomColors.primary
                          : CustomColors.settingIconColor,
                    ),
          SizedBox(width: 12.fw),
          if ((widget.bloc.state as ChatStateLoaded).currentTip == TooltipTrigger.longTapEmoji)
            TooltipWidget(
              tooltipTrigger: TooltipTrigger.longTapEmoji,
              preferredDirection: AxisDirection.up,
              offset: 10,
              child: PannelButton(
                padding: const EdgeInsets.only(right: 12),
                iconPath: widget.selectedEmotion.path,
                onTap: widget.onSendEmotion,
                onLongPress: widget.onLongPressEmotion,
                isEmojiButton: true,
              ),
            )
          else
            PannelButton(
              padding: const EdgeInsets.only(right: 12),
              iconPath: widget.selectedEmotion.path,
              onTap: widget.onSendEmotion,
              onLongPress: widget.onLongPressEmotion,
              isEmojiButton: true,
            ),
        ],
      ],
    );
  }
}
