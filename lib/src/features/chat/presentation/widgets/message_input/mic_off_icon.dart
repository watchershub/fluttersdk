import 'dart:math';

import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class MicOffIcon extends StatelessWidget {
  final double? iconSize;
  final Color? iconColor;

  const MicOffIcon({
    this.iconSize,
    this.iconColor,
  });

  @override
  Widget build(BuildContext context) {
    final size = iconSize ?? 20;
    return Stack(
      alignment: Alignment.center,
      children: [
        const Positioned.fill(
          child: SizedBox(
            width: 20,
            height: 20,
          ),
        ),
        SvgIcon(
          Resources.mic_on,
          size: size,
          color: iconColor,
        ),
        Transform.rotate(
          angle: pi / 4,
          child: Container(
            color: CustomColors.microOffStripBelow,
            height: 1,
            width: size,
          ),
        ),
        Transform.translate(
          offset: const Offset(1, -1),
          child: Transform.rotate(
            angle: pi / 4,
            child: Container(
              color: CustomColors.microOffStripAbove,
              height: 1,
              width: size,
            ),
          ),
        ),
      ],
    );
  }
}
