import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/emoji_widget.dart';

class PannelButton extends StatelessWidget {
  final VoidCallback? onTap;
  final VoidCallback? onLongPress;
  final String iconPath;
  final bool isEmojiButton;
  final EdgeInsetsGeometry? padding;
  final Color? iconColor;
  final double? iconSize;
  final double? buttonSize;
  final Color? borderColor;
  final Widget? content;

  const PannelButton({
    this.iconPath = '',
    this.padding,
    this.onTap,
    this.onLongPress,
    this.isEmojiButton = false,
    this.iconColor,
    this.iconSize,
    this.buttonSize,
    this.borderColor,
    this.content,
  });

  @override
  Widget build(BuildContext context) {
    final button = InkWell(
        splashColor: Colors.transparent,
        onTap: onTap,
        onLongPress: onLongPress,
        child: Container(
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.fromBorderSide(BorderSide(color: borderColor ?? Colors.transparent)),
          ),
          child: CircleAvatar(
            radius: buttonSize ?? 19,
            backgroundColor: CustomColors.secondary,
            child: isEmojiButton
                ? EmojiWidget(
                    key: ValueKey(iconPath),
                    emotionPath: iconPath,
                    isShort: true,
                  )
                : CircleAvatar(
                    backgroundColor: CustomColors.emojiBackground,
                    child: content ??
                        SvgIcon(
                          iconPath,
                          size: iconSize ?? 20,
                          color: iconColor,
                        ),
                  ),
          ),
        ));

    return padding != null
        ? Padding(
            padding: padding!,
            child: button,
          )
        : button;
  }
}
