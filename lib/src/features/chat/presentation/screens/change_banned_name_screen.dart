import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/common/widgets/inputs/user_name_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/submit_button.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/logic/onboarding_bloc.dart';

class ChangeBannedNameScreen extends StatefulWidget {
  final OnboardingBloc onboardingBloc;
  final ChatBloc chatBloc;

  const ChangeBannedNameScreen({required this.onboardingBloc,required this.chatBloc});

  @override
  _ChangeBannedNameScreenState createState() => _ChangeBannedNameScreenState();
}
  
  class _ChangeBannedNameScreenState extends State<ChangeBannedNameScreen>{
  
  late String? errorDescription;
  
  @override
  void initState() {
    super.initState();
    errorDescription = 'Недопустимое имя';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: CustomColors.chatBackground,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  const SvgIcon(
                    Resources.info,
                    size: 48,
                  ),
                  const SizedBox(width: 10),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Измените имя в чате',
                        style: TextStyles.title(fontSize: 16, fontWeight: FontWeight.w400),
                      ),
                      Text(
                        'Вы заблокированы за недопустимое имя',
                        style: TextStyles.secondary(fontSize: 13),
                      ),
                    ],
                  )
                ],
              ),
            ),
            const Divider(
              height: 1,
              thickness: 1,
              color: CustomColors.divider,
            ),
            Padding(
              padding: const EdgeInsets.all(24.0),
              child: UserNameWidget(
                userNameFocusNode: widget.onboardingBloc.userNameFocusNode,
                onFieldSubmitted: (_)  {
                  setState(() async {
                    errorDescription = await widget.onboardingBloc.validateUserName(userName: widget.onboardingBloc.userNameTextEditingController.text);
                  });

                },
                onChange: (_)  {
                  setState(() async {
                    errorDescription = await widget.onboardingBloc.validateUserName(userName: widget.onboardingBloc.userNameTextEditingController.text);
                  });
                },
                textEditingController: widget.onboardingBloc.userNameTextEditingController,
                userName: widget.onboardingBloc.userNameTextEditingController.text,
                errorDescription: errorDescription,
              ),
            ),
            const Spacer(),
            SubmitButton.textual(
              text: 'Сохранить',
              isActive: errorDescription == null,
              onTap: () async {
                final result = await widget.onboardingBloc.updateUserName(userName: widget.onboardingBloc.userNameTextEditingController.text);
                if (result.isSuccess){
                  widget.chatBloc.add(ChatEvent.updateLoaded((p0) => p0.copyWith(ban: null)));
                }
              },
            ),
          ],
        ),
      ),
    );
  }
  
}
