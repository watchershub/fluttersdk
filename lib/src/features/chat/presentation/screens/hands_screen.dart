import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/bloc_injectable_state.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/core/utils/transitions.dart';
import 'package:watchers_widget/src/features/chat/data/request/reject_hand_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/closable_modal_screen.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/report_type_widget.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/talker_item_widget.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';

class HandsScreen extends StatefulWidget {
  final List<Talker> awaitedTalkers;
  final ChatBloc bloc;

  const HandsScreen({
    required this.awaitedTalkers,
    required this.bloc,
  });

  static Route route({
    required List<Talker> awaitedTalkers,
    required ChatBloc bloc,
  }) =>
      Transitions.buildFadeTransition(HandsScreen(
        awaitedTalkers: awaitedTalkers,
        bloc: bloc,
      ));

  @override
  State<HandsScreen> createState() => _HandsScreenState();
}

class _HandsScreenState extends BlocInjectableState<HandsScreen, ChatBloc, ChatEvent, ChatState> {
  _HandsScreenState() : super.empty();

  @override
  void initState() {
    cubit = widget.bloc;
    super.initState();
  }

  @override
  Widget builder(BuildContext context, ChatState state) {
    if (state is! ChatStateLoaded) return const SizedBox.shrink();

    return ClosableModalScreen(
      leadingIcon: CircleAvatar(
        backgroundColor: CustomColors.emojiBackground,
        child: SvgIcon(
          Resources.hand_list,
          color: state.talkersWithHands.isNotEmpty
              ? CustomColors.primary
              : CustomColors.settingIconColor,
        ),
      ),
      titleText: 'Подняли руку',
      subtitleText: 'Ожидающие спикеры',
      body: Expanded(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: state.talkersWithHands.isEmpty
              ? Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'У вас нет ожидающих спикеров',
                      style: TextStyles.title(),
                    ),
                  ],
                )
              : ListView.builder(
                  itemCount: state.talkersWithHands.length,
                  itemBuilder: (BuildContext context, int index) {
                    final talker = state.talkersWithHands[index];
                    return TalkerItemWidget(
                      talker: talker,
                      actions: [
                        InkWell(
                          onTap: () {
                            bloc.rejectHand(ChatEvent.rejectHand(
                                rejectHandRequest: RejectHandRequest(talkerId: talker.id)));
                          },
                          child: const SvgIcon(
                            Resources.decline_request,
                            size: 36,
                          ),
                        ),
                        SizedBox(width: 16.fw),
                        InkWell(
                          onTap: () {
                            bloc.setRole(ChatEvent.setRole(
                              setRoleRequest: SetRoleRequest(
                                userId: talker.user.id,
                                role: TalkerRole.speaker,
                              ),
                              context: context,
                              talker: talker,
                            ));
                          },
                          child: const SvgIcon(
                            Resources.add_speaker,
                            color: CustomColors.gray200,
                            size: 36,
                          ),
                        ),
                        SizedBox(width: 12.fw),
                      ],
                    );
                  },
                ),
        ),
      ),
    );
  }
}
