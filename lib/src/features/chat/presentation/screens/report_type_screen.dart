import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/utils/transitions.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/report_type_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/confirm_dialog.dart';

class ReportTypeScreen extends StatelessWidget {
  final String userName;
  final void Function(ReportType reportType) onConfirm;
  final void Function(ReportType reportType) onSelect;
  final void Function(ReportType reportType) onCancel;

  const ReportTypeScreen({
    required this.userName,
    required this.onConfirm,
    required this.onSelect,
    required this.onCancel,
  });

  static Route route({
    required String userName,
    required void Function(ReportType reportType) onConfirm,
    required void Function(ReportType reportType) onSelect,
    required void Function(ReportType reportType) onCancel,
  }) =>
      Transitions.buildZeroTransition(ReportTypeScreen(
        userName: userName,
        onConfirm: onConfirm, onSelect: onSelect,
        onCancel: onCancel,
      ));

  @override
  Widget build(BuildContext context) {
    return ReportTypeWidget(
      onSelected: (reportType) async {
        onSelect(reportType);
        await showDialog(
          useRootNavigator: false,
          context: context,
          builder: (context) => ConfirmDialog(
            titleText: 'Сообщить о неприемлемом контенте от $userName',
            confirmButtonText: 'Отправить',
            onConfirm: () {
              Navigator.pop(context);
              onConfirm(reportType);
            },
            onCancel: () {
              onCancel(reportType);
              Navigator.pop(context);
            },
          ),
        );
      },
    );
  }
}
