import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/strings.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class ChooseBanReasonScreen extends StatefulWidget {
  final Function(int banReason) onItemChoosen;

  const ChooseBanReasonScreen({
    required this.onItemChoosen,
  });

  @override
  _ChooseBanReasonScreenState createState() => _ChooseBanReasonScreenState();
}

class _ChooseBanReasonScreenState extends State<ChooseBanReasonScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: CustomColors.chatBackground,
        body:   Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Row(
                  children: [
                    const SvgIcon(
                      Resources.report,
                      size: 48,
                    ),
                    const SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Заблокировать пользователя',
                          style: TextStyles.title(fontSize: 16, fontWeight: FontWeight.w400),
                        ),
                        Text(
                          'Укажите причину блокировки',
                          style: TextStyles.secondary(fontSize: 13),
                        ),
                      ],
                    ),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 16),
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: InkWell(
                          onTap: () => Navigator.maybePop(context),
                          child: const SvgIcon(Resources.close, color: CustomColors.danger),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const Divider(
                height: 1,
                thickness: 1,
                color: CustomColors.divider,
              ),
              Expanded(
                  child: Padding(
                      padding: const EdgeInsets.only(top: 34),
                      child: ListView.builder(
                          padding: EdgeInsets.zero,
                          physics: const AlwaysScrollableScrollPhysics(),
                          shrinkWrap: true,
                          itemCount: Strings.banReasons.length,
                          itemBuilder: (context, index) {
                            if (index!=9) {
                              return Padding(
                                  padding: const EdgeInsets.only(left: 36, right: 20, bottom: 30),
                                  child: InkWell(
                                    onTap: () {
                                      widget.onItemChoosen(index);
                                    },
                                    splashColor: Colors.transparent,
                                    child: Text(
                                      Strings.banReasons[index],
                                      style: TextStyles.title(
                                          fontSize: 17,
                                          fontWeight: FontWeight.w400,
                                          color: CustomColors.chatRuleButtonColor),
                                    ),
                                  ));
                            }
                            return Container();
                          }))),
            ],
          ),
        );
  }
}
