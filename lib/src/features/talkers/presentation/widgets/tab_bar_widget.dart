import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class TalkersTabBarWidget extends StatelessWidget {
  final void Function(int) onUpdateIndex;
  final int currentSelectedIndex;
  final bool isSpeakRoom;

  const TalkersTabBarWidget({
    required this.onUpdateIndex,
    required this.currentSelectedIndex,
    required this.isSpeakRoom,
  });

  Widget _buildTab(int index, String text, String iconPath) {
    final isSeleted = index == currentSelectedIndex;
    final color = isSeleted ? CustomColors.primary : CustomColors.textTertiary;
    return InkWell(
      onTap: () {
        onUpdateIndex.call(index);
      },
      child: Container(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              width: 1,
              color: isSeleted ? CustomColors.primary : Colors.transparent,
            ),
          ),
        ),
        child: Padding(
          padding: EdgeInsets.all(8.fw),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SvgIcon(
                iconPath,
                color: color,
              ),
              SizedBox(width: 7.fw),
              Text(
                text.toUpperCase(),
                style: TextStyle(
                  color: color,
                  fontSize: 12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        _buildTab(0, 'Все', Resources.chat_bubble),
        if (isSpeakRoom) ...[
          SizedBox(width: 8.fw),
          _buildTab(1, 'Спикеры', Resources.speakers),
        ],
      ],
    );
  }
}
