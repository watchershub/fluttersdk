import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/base/bloc_injectable_state.dart';
import 'package:watchers_widget/src/core/utils/transitions.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_mute_request.dart';
import 'package:watchers_widget/src/features/chat/data/request/set_role_request.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/common/widgets/loading_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/modal_title_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/modal_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/talker_action_menu.dart';
import 'package:watchers_widget/src/features/talkers/presentation/widgets/tab_bar_widget.dart';

import '../../common/models/talker.dart';

class TalkersScreen extends StatefulWidget {
  final void Function(Talker talker, bool isBanned) onToggleUserBan;
  final void Function(Talker talker, bool isVisible) onToggleMessagesVisibility;

  //  Удалить кастыль после добавления локальных координаторов (навигаторов)
  final ChatBloc bloc;

  const TalkersScreen({
    required this.onToggleUserBan,
    required this.onToggleMessagesVisibility,
    required this.bloc,
  });

  static Route route({
    required ChatBloc bloc,
    required void Function(Talker talker, bool isBanned) onToggleUserBan,
    required void Function(Talker talker, bool isVisible) onToggleMessagesVisibility,
  }) =>
      Transitions.buildFadeTransition(TalkersScreen(
        bloc: bloc,
        onToggleUserBan: onToggleUserBan,
        onToggleMessagesVisibility: onToggleMessagesVisibility,
      ));

  @override
  State<TalkersScreen> createState() => _TalkersScreenState();
}

class _TalkersScreenState
    extends BlocInjectableState<TalkersScreen, ChatBloc, ChatEvent, ChatState> {
  _TalkersScreenState() : super.empty();

  int _currentTabIndex = 0;

  @override
  void initState() {
    cubit = widget.bloc;
    super.initState();
  }

  @override
  Widget builder(BuildContext context, ChatState state) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: state.map(
            loading: (value) {
              return ModalWidget(
                title: ModalTitleWidget(
                  titleText: 'Профиль',
                  onBackTap: () {},
                ),
                isEmpty: true,
                emptyWidget: const LoadingWidget(),
              );
            },
            loaded: (state) {
              return ModalWidget(
                title: ModalTitleWidget(
                  titleText: 'Участники',
                  onBackTap: () {
                    bloc.sendAmplitudeEvent(eventName: 'Social_NavigationClick_UserListClosed');
                    Navigator.maybePop(context);
                  },
                ),
                children: [
                  TalkersTabBarWidget(
                    currentSelectedIndex: _currentTabIndex,
                    onUpdateIndex: (newIndex) {
                      _currentTabIndex = newIndex;
                      setState(() {});
                    },
                    isSpeakRoom: state.room.isSpeak && state.speakers.isNotEmpty,
                  ),
                  const SizedBox(height: 16),
                  Expanded(
                    child: IndexedStack(
                      index: _currentTabIndex,
                      children: [
                        _buildList(state.talkers, state),
                        if (state.room.isSpeak && state.speakers.isNotEmpty) _buildList(state.speakers, state),
                      ],
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  Widget _buildList(List<Talker> talkers, ChatStateLoaded state) {
    return ListView.builder(
      itemCount: talkers.length,
      itemBuilder: (BuildContext context, int index) {
        final myTalker = state.talker;
        final iAmAdminOrModer = myTalker.isModer || myTalker.role == 'ADMIN';
        final isMe = state.talker == state.talkers[index];
        final talker = talkers[index];

        return TalkerActionMenu(
          iAmAdminOrModer: iAmAdminOrModer,
          onToggleMessagesVisibility: widget.onToggleMessagesVisibility,
          onToggleUserBan: widget.onToggleUserBan,
          talker: talker,
          isMe: state.talker == talker,
          isShowActions: (talker.role != 'ADMIN') && !isMe && !talker.isModer && iAmAdminOrModer,
          isSpeaker: talker.role == TalkerRole.speaker.toStr,
          isMuted: talker.isMuted,
          onMuteTalker: (talker) {
            bloc.setMute(ChatEvent.setMute(
              setMuteRequest: SetMuteRequest(
                userId: talker.user.id,
                isMuted: true,
              ),
            ));
          },
          onRemoveFromSpeakers: (talker) {
            bloc.setRole(
              ChatEvent.setRole(
                setRoleRequest: SetRoleRequest(
                  userId: talker.user.id,
                  role: TalkerRole.guest,
                ),
                context: context,
                talker: talker,
              ),
            );
          },
        );
      },
    );
  }
}
