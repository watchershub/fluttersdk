import 'package:dio/dio.dart';

class OnboardingApi {
  final Dio _dio;

  OnboardingApi(
    Dio dio,
  ) : _dio = dio;

  Future<Response> register(String requestBody) => _dio.post(
        'auth/register',
        data: requestBody,
      );

  Future<Response> updateUser(String requestBody) => _dio.patch(
        'user/me',
        data: requestBody,
      );
}
