import 'dart:async';

import 'package:bloc_concurrency/bloc_concurrency.dart' as bloc_concurrency;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:watchers_widget/src/app/di/locator.dart';
import 'package:watchers_widget/src/app/watchers_params.dart';
import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/close_socket_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/models/avatar.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/auth/register_user_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/avatar/get_all_avatars_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/user/update_user_use_case.dart';
import 'package:watchers_widget/src/features/common/models/user.dart';
import 'package:watchers_widget/src/features/onboarding/domain/licence.dart';
import 'package:watchers_widget/src/features/onboarding/domain/user_name.dart';
import 'package:watchers_widget/watchers_widget.dart';

part 'onboarding_bloc.freezed.dart';
part 'onboarding_event.dart';
part 'onboarding_state.dart';

enum OnboardStage {
  name,
  avatar,
}

class OnboardingBloc extends Bloc<OnboardingEvent, OnboardingState> {
  final RegisterUserUseCase _registerUserUseCase;
  final UpdateUserUseCase _updateUserUseCase;
  final GetAllAvatarsUseCase _getAllAvatarsUseCase;
  final LazyFutureAntiSwearing _antiSwearingFuture;
  final CloseSocketUseCase _closeSocketUseCase;
  final WatchersParamsProvider _watchersParamsProvider;

  OnboardingBloc({
    required WatchersParamsProvider watchersParamsProvider,
    required RegisterUserUseCase registerUserUseCase,
    required UpdateUserUseCase updateUserUseCase,
    required GetAllAvatarsUseCase getAllAvatarsUseCase,
    required LazyFutureAntiSwearing antiSwearingFuture,
    required CloseSocketUseCase closeSocketUseCase,
  })  : _watchersParamsProvider = watchersParamsProvider,
        _registerUserUseCase = registerUserUseCase,
        _updateUserUseCase = updateUserUseCase,
        _getAllAvatarsUseCase = getAllAvatarsUseCase,
        _antiSwearingFuture = antiSwearingFuture,
        _closeSocketUseCase = closeSocketUseCase,
        super(OnboardingState.loading()) {
    on<OnboardingEvent>(
      (event, emit) => event.mapOrNull<Future<void>>(
        init: (event) => _init(event, emit),
        showLicence: (event) => _showLicence(event, emit),
        backToMain: (event) => _backToMain(event, emit),
        backToForm: (event) => _backToForm(event, emit),
        acceptLicences: (event) => _acceptLicences(event, emit),
        validateInput: (event) => _onValidateInput(event, emit),
        submitForm: (event) => _submitForm(event, emit),
        selectAvatar: (event) => _selectAvatar(event, emit),
        submitAvatar: (event) => _submitAvatar(event, emit),
        teleportToOnboarding: (event) => _onTeleportToOnboarding(event, emit),
      ),
      transformer: bloc_concurrency.droppable(),
    );

    on<_ShowChatEvent>(_onShowChat);
    on<_ShowDeletedEvent>(_onShowDeleted);

    add(OnboardingEvent.init(
      externalId: _watchersParamsProvider.params.userId,
      statusName: _watchersParamsProvider.params.statusName,
    ));
  }

  final TextEditingController _userNameTextEditingController = TextEditingController();
  TextEditingController get userNameTextEditingController => _userNameTextEditingController;

  final FocusNode _userNameFocusNode = FocusNode();
  FocusNode get userNameFocusNode => _userNameFocusNode;

  Future<void> _init(_Init event, Emitter<OnboardingState> emit) async {
    final result = await _registerUserUseCase.call(
      externalId: event.externalId,
      statusName: event.statusName,
    );

    if (result.isSuccess) {
      final registerResponse = result.successValue;

      if (registerResponse.isDeleted) {
        emit(OnboardingState.showDeleted(deletedAt: registerResponse.user.user.deletedAt!));
        return;
      }

      _userNameTextEditingController.text = registerResponse.user.uniqueName;

      if (registerResponse.isNew ||
          registerResponse.user.user.name.isEmpty ||
          registerResponse.user.user.pic.isEmpty) {
        await _antiSwearingFuture();

        emit(OnboardingState.showChat());

        // emit(OnboardingState.form(
        //   userName: '',
        //   hideChangeButton: registerResponse.isNew || registerResponse.user.user.name.isEmpty,
        // ));
        return;
      }

      emit(OnboardingState.showChat());
      return;
    }

    emit(OnboardingState.showError());
  }

  Future<void> _showLicence(_ShowLicence event, Emitter<OnboardingState> emit) async {
    emit(OnboardingState.licenceDetails(licenceText: event.licence.text));
  }

  Future<void> _backToMain(_BackToMain event, Emitter<OnboardingState> emit) async {
    emit(OnboardingState.main());
  }

  Future<void> _backToForm(_BackToForm event, Emitter<OnboardingState> emit) async {
    emit(OnboardingState.form(userName: event.userName));
  }

  Future<void> _acceptLicences(_AcceptLicences event, Emitter<OnboardingState> emit) async {
    emit(OnboardingState.form(userName: ''));
  }

  Future<void> _onValidateInput(_ValidateInput event, Emitter<OnboardingState> emit) async {
    _validateInput(emit);
  }

  Future<bool> _validateInput(Emitter<OnboardingState> emit) async {
    final userName = UserName.dirty(_userNameTextEditingController.text);
    final isSwearing = (await _antiSwearingFuture()).containsSwearing(userName.value);

    final errorText = isSwearing ? 'Недопустимое имя' : userName.error.description;

    emit(OnboardingState.form(
      userName: userName.value,
      errorDescription: errorText,
    ));

    return errorText != null;
  }

  Future<String?> validateUserName({required String userName}) async {
    final userName = UserName.dirty(_userNameTextEditingController.text);
    final isSwearing = (await _antiSwearingFuture()).containsSwearing(userName.value);

    final errorText = isSwearing ? 'Недопустимое имя' : userName.error.description;

    return errorText;
  }

  Future<Result<void, Exception>> updateUserName({required String userName}) async {
    return _updateUserUseCase.call(name:userName,);
  }

  Future<void> _submitForm(_SubmitForm event, Emitter<OnboardingState> emit) async {
    _userNameTextEditingController.text = _userNameTextEditingController.text.trim();
    final isNotValid = await _validateInput(emit);

    if (isNotValid) return;

    final updateNameResult =
        await _updateUserUseCase.call(name: event.userName, isOnboard: false, onboardStage: 1);

    final result = await _getAllAvatarsUseCase.call();

    if (result.isSuccess && updateNameResult.isSuccess) {
      final avatars = result.successValue;

      emit(OnboardingState.avatarPicker(
        userName: event.userName,
        avatars: avatars,
        selectedAvatar: avatars[0],
      ));
    }
  }

  Future<void> _selectAvatar(_SelectAvatar event, Emitter<OnboardingState> emit) async {
    emit(OnboardingState.avatarPicker(
      selectedAvatar: event.selectedAvatar,
      avatars: event.avatars,
      userName: event.userName,
    ));
  }

  Future<void> _submitAvatar(_SubmitAvatar event, Emitter<OnboardingState> emit) async {
    final result = await _updateUserUseCase.call(
        pic: event.selectedAvatar.pic, name: event.userName, isOnboard: true, onboardStage: 0);

    if (result.isSuccess) {
      emit(OnboardingState.showChat());
    }
  }

  void _onShowChat(_ShowChatEvent event, Emitter<OnboardingState> emit) =>
      emit(OnboardingState.showChat());

  void _onShowDeleted(_ShowDeletedEvent event, Emitter<OnboardingState> emit) =>
      emit(OnboardingState.showDeleted(
        deletedAt: DateTime.now(),
      ));

  void teleportToOnboarding(OnboardingEventTeleportToOnboardingEvent event) => add(event);

  Future<void> _onTeleportToOnboarding(
      OnboardingEventTeleportToOnboardingEvent event, Emitter<OnboardingState> emit) async {
    if (event.onboardStage == OnboardStage.name) {
      _userNameFocusNode.requestFocus();

      emit(OnboardingState.form(
        userName: _userNameTextEditingController.text,
        hideChangeButton: true,
      ));

      return;
    }
    if (event.onboardStage == OnboardStage.avatar) {
      final result = await _getAllAvatarsUseCase.call();

      if (result.isSuccess) {
        final avatars = result.successValue;
        _userNameTextEditingController.text = event.user.name;
        emit(OnboardingState.avatarPicker(
          userName: event.user.name,
          avatars: avatars,
          selectedAvatar: avatars[0],
        ));
      }
    }
  }

  @override
  Future<void> close() async {
    await _closeSocketUseCase.call();

    return super.close();
  }
}
