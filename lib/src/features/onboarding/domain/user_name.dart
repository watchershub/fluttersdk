import 'package:formz/formz.dart';

enum UserNameValidationError { empty, tooShort, tooLong }

class UserName extends FormzInput<String, UserNameValidationError> {
  const UserName.pure() : super.pure('');
  const UserName.dirty([String value = '']) : super.dirty(value);

  @override
  UserNameValidationError? validator(String? value) {
    if (value == null || value.trim().isEmpty == true) {
      return UserNameValidationError.empty;
    }

    if (value.trim().length > 25) {
      return UserNameValidationError.tooLong;
    }

    return null;
  }
}

extension NameValidationErrorX on UserNameValidationError? {
  String? get description {
    if (this == null) return null;

    // return "Недопустимое имя";
    return '';
    // switch (this!) {
    //   case UserNameValidationError.empty:
    //     return 'Имя не может быть пустым';
    //   case UserNameValidationError.tooShort:
    //     return 'Имя должно содержать хотя бы 3 символа';
    //   case UserNameValidationError.tooLong:
    //     return 'Имя должно содержать не больше 25 символов';
    // }
  }
}
