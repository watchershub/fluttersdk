import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:watchers_widget/src/core/constants/constants.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/utils/transitions.dart';
import 'package:watchers_widget/src/features/common/widgets/modal_title_widget.dart';

class ChatRulesScreen extends StatelessWidget {
  final String chatRulesHtml;

  static Route route({required String chatRulesHtml}) => Transitions.buildFadeTransition(ChatRulesScreen(chatRulesHtml: chatRulesHtml,));

  const ChatRulesScreen({required this.chatRulesHtml});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Container(
        decoration: const BoxDecoration(
          color: CustomColors.modalBackground,
        ),
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ModalTitleWidget(
              titleText: 'Правила чата',
              onBackTap: () => Navigator.maybePop(context),
            ),
            const SizedBox(height: 8),
            Expanded(
              child: ListView(
                children: [
                  HtmlWidget(chatRulesHtml,textStyle: const TextStyle(color: Colors.black),),
                  // _paragraph(
                  //   'Чат к трансляции — место, где можно общаться, делиться эмоциями и обсуждать увиденное в сервисе. '
                  //   'А еще это место, где каждый пользователь должен чувствовать себя комфортно и в безопасности. '
                  //   'Мы считаем, что люди охотнее и свободнее общаются и высказывают свое мнение, когда не подвергаются нападкам из-за своей идентичности.',
                  // ),
                  // _paragraph(
                  //   'Поэтому мы пристально следим за соблюдением правил использования чатов. '
                  //   'В случае их нарушения сообщения удалят, а аккаунт могут заблокировать.',
                  // ),
                  // _h1('Общий функционал текстовых публичных чатов'),
                  // _h2('Установка имени и аватара'),
                  // _paragraph(
                  //   'Пользователь вводит имя, которое проверяется на соответствие правилам (см. ниже). '
                  //   'Затем пользователя выбирает юзерпик из предложенных вариантов.',
                  // ),
                  // _h2('Отправка текстовых сообщений'),
                  // _paragraph(
                  //   'Сразу после авторизации пользователю станет доступен чат, где он сможет писать текстовые сообщения и отправлять эмодзи.',
                  // ),
                  // _paragraph(
                  //   'Все тексты сообщений проверяются на нецензурную лексику и на соответствие правилам чата.',
                  // ),
                  // _h2('Отправка реакций'),
                  // _paragraph(
                  //   'Пользователь может поделиться эмоцией, не отправляя сообщение в чат. '
                  //   'Отправленную эмоцию увидят все активные пользователи, через несколько секунд она исчезнет.',
                  // ),
                  // _paragraph(
                  //   'Панель с эмодзи, с которой можно отправлять анимированные исчезающие смайлы, открывается по нажатию на иконку реакций в правом нижнем углу.',
                  // ),
                  // _h2('Удаление профиля'),
                  // _paragraph('Пользователь может удалить свой профиль двумя способами.'),
                  // _li(
                  //   'Нажать кнопку «удалить аккаунт». В таком случае профиль деактивируется, а через 30 суток удалится полностью. '
                  //   'Сообщения, отправленные пользователем, сохранятся в чатах, но вместо никнейма появится «user_deleted». '
                  //   'В течение 30 суток вы сможете его восстановить.',
                  // ),
                  // _li('Отправить запрос на ${Constants.partnerEmail}'),
                  // _h1('Что запрещено в чате?'),
                  // _h2('Ссылки'),
                  // _paragraph(
                  //   'Все, кроме внутренних (на сервис, к которому прикреплен чат). '
                  //   'К тому же, любая ссылка, отправленная в чат обычным пользователем, будет некликабельной.',
                  // ),
                  // _h2('Ссылки на ресурсы конкурентов'),
                  // _paragraph(
                  //   'Модератор партнера может завести в административной панели редактируемый список конкурентных доменов, '
                  //   'по которому проверяются все сообщения пользователей. '
                  //   'При обнаружении совпадения блокируется отправка сообщения в чат: кнопка "отправить" '
                  //   'и появляется алерт “попробуйте переформулировать”. Таким образом ссылки на ресурсы конкурентов не будут отправлены в чат. '
                  //   'Но в контексте общения пользователи могут называть бренды конкурентов в сравнении или рекомендации, '
                  //   'такие сообщения будут оправлены в чат.',
                  // ),
                  // _h2('Важно!'),
                  // _paragraph(
                  //   'Отправка возможна, если в ссылке нарушена целостность или она сокращена или обёрнута.',
                  // ),
                  // _h2('Мат и другие нецензурные высказывания'),
                  // _paragraph(
                  //   'Мат и оскорбления в соцсетях и публичных местах запрещен N 149 ФЗ РФ '
                  //   '«Об информации, информационных технологиях и защите информации» (ред. от 30.12.2021)',
                  // ),
                  // _h2('Оскорбления, ругань и враждебные высказывания'),
                  // _paragraph(
                  //   'Обращения к одному человеку или ко всему чату в целом, направленные на ущемление чести и достоинства присутствующих, запрещены.',
                  // ),
                  // _paragraph(
                  //   'Под враждебными высказываниями мы подразумеваем прямые нападки на людей, мотивированные расовой или этнической принадлежностью, '
                  //   'национальностью, вероисповеданием, сексуальной ориентацией, полом, гендерной идентичностью и наличием серьезного заболевания.',
                  // ),
                  // _paragraph(
                  //   'Запрещены агрессивные или унижающие человеческое достоинство высказывания, вредные стереотипы, утверждения о неполноценности.',
                  // ),
                  // _paragraph(
                  //   'Запрещено использование вредных стереотипов, под которыми мы понимаем унижающие человеческое достоинство сравнения.',
                  // ),
                  // _paragraph(
                  //   'Запрещены высказывания, унижающие человеческое достоинство посредством сравнения, обобщения или описания поведения.',
                  // ),
                  // _h2('Флуд'),
                  // _paragraph(
                  //     'Запрещено злонамеренное систематическое засорение чата неинформативными сообщениями.'),
                  // _h2('Реклама'),
                  // _paragraph('Запрещено рекламировать сторонние сервисы, товары и услуги.'),
                  // _h2('Акты и угрозы насилия'),
                  // _paragraph(
                  //   'Запрещены угрозы причинить физический вред, угрозы взлома, запугивание, вербальная поддержка терроризма '
                  //   'или действий экстремистов, угрозы или поощрение других к совершению действий, которые '
                  //   'могут привести к физическому ущербу для групп людей или уничтожению имущества.',
                  // ),
                  // _paragraph(
                  //   'Пользователям запрещено заниматься террористической или экстремистской пропагандой '
                  //   'или ссылаться на нее (даже в целях осуждения такого контента).',
                  // ),
                  // _h2('Приватная информация'),
                  // _paragraph(
                  //   'Запрещено публиковать номера телефонов, адреса, паспортные данные, '
                  //   'данные банковских карт и так далее, причем как собственные, так и посторонние. '
                  //   'Любая приватная информация будет удалена, за распространение чужих данных пользователя, отправившего '
                  //   'подобные сообщения, ждет блокировка в чате.',
                  // ),
                  // _h2('Мошенничество'),
                  // _paragraph(
                  //   'Запрещены взаимодействие и координация действий с другими пользователями с целью получения '
                  //   'финансовой или личной выгоды мошенническим способом в ущерб стороннему лицу '
                  //   '(например, другим людям, компаниям или организациям).',
                  // ),
                  // _h2('Нарушение кибербезопасности сервиса'),
                  // _paragraph(
                  //   'Запрещены попытки сбора конфиденциальной информации пользователей '
                  //   'или получение несанкционированного доступа к платформе, продукту или сервису.',
                  // ),
                  // _h2('Ники/юзернеймы'),
                  // _paragraph('Ники/юзернеймы, содержание которых подпадает под остальные пункты'),
                  // _h2('Пропаганда и провокации'),
                  // _paragraph(
                  //   'Запрещена политическая пропаганда и любые провокации. '
                  //   'Развлекательные и обучающие чаты — не место для политических споров и осуждения чужих убеждений. '
                  //   'Помните о комфорте других пользователей.',
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _paragraph(String text) => Padding(
        padding: const EdgeInsets.only(bottom: 8.0),
        child: Text(
          text,
          textAlign: TextAlign.left,
          style: TextStyles.input,
        ),
      );

  Widget _h1(String text) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Text(
          text,
          textAlign: TextAlign.left,
          style: TextStyles.title(
            fontWeight: FontWeight.w700,
            fontSize: 20,
          ),
        ),
      );

  Widget _h2(String text) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 14),
        child: Text(
          text,
          textAlign: TextAlign.left,
          style: TextStyles.title(
            fontWeight: FontWeight.w600,
            fontSize: 18,
          ),
        ),
      );

  Widget _h3(String text) => Padding(
        padding: const EdgeInsets.symmetric(vertical: 12),
        child: Text(
          text,
          textAlign: TextAlign.left,
          style: TextStyles.title(
            fontWeight: FontWeight.w600,
            fontSize: 16,
          ),
        ),
      );

  Widget _li(String text) => Padding(
        padding: const EdgeInsets.only(left: 8, bottom: 8),
        child: Text(
          '\u2022 ' + text,
          textAlign: TextAlign.left,
          style: TextStyles.input,
        ),
      );
}
