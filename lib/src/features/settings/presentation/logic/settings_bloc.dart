import 'dart:async';
import 'dart:ui';

import 'package:amplitude_flutter/amplitude.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:watchers_widget/src/app/di/locator.dart';
import 'package:watchers_widget/src/app/watchers_params.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/analitics_dto.dart';
import 'package:watchers_widget/src/features/chat/data/dtos/slomode_dto.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/on_connectivity_changed_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_slowmode_use_case.dart';
import 'package:watchers_widget/src/features/common/anti_swearing/anti_swearing.dart';
import 'package:watchers_widget/src/features/common/domain/models/avatar.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/avatar/get_all_avatars_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/block/add_block_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/block/get_blocks_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/block/remove_block_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/settings/get_chat_rules_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/talker/get_room_and_talkers_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/user/delete_user_user_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/user/get_user_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/user/update_user_use_case.dart';
import 'package:watchers_widget/src/features/common/models/room.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/common/models/user.dart';
import 'package:watchers_widget/src/features/common/widgets/dialogs/confirm_dialog.dart';
import 'package:watchers_widget/src/features/common/widgets/snacks/info_snackbar.dart';
import 'package:watchers_widget/src/features/onboarding/domain/user_name.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/logic/onboarding_bloc.dart';

part 'settings_bloc.freezed.dart';
part 'settings_event.dart';
part 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  final GetBlocksUseCase _getBlocksUseCase;
  final GetUserUseCase _getUserUseCase;
  final UpdateUserUseCase _updateUserUseCase;
  final GetAllAvatarsUseCase _getAllAvatarsUseCase;
  final RemoveBlockUseCase _removeBlockUseCase;
  final DeleteUserUseCase _deleteUserUseCase;
  final LazyFutureAntiSwearing _antiSwearingFuture;
  final GetConnectivityChangeStreamUseCase _connectivityChangeStreamUseCase;
  final Talker _talker;
  final String _externalRoomId;
  final AddBlockUseCase _addBlockUseCase;
  final GetChatRulesUseCase _getChatRulesUseCase;
  final SetSlowmodeUseCase _setSlowmodeUseCase;
  final GetRoomAndTalkersUseCase _getRoomAndTalkersUseCase;
  final Amplitude _amplitude;
  final WatchersParamsProvider _paramsProvider;

  SettingsBloc({
    required GetBlocksUseCase getBlocksUseCase,
    required GetUserUseCase getUserUseCase,
    required UpdateUserUseCase updateUserUseCase,
    required GetAllAvatarsUseCase getAllAvatarsUseCase,
    required RemoveBlockUseCase removeBlockUseCase,
    required DeleteUserUseCase deleteUserUseCase,
    required LazyFutureAntiSwearing antiSwearingFuture,
    required GetConnectivityChangeStreamUseCase getConnectivityChangeStreamUseCase,
    required Talker talker,
    required AddBlockUseCase addBlockUseCase,
    required GetChatRulesUseCase getChatRulesUseCase,
    required SetSlowmodeUseCase setSlowmodeUseCase,
    required GetRoomAndTalkersUseCase getRoomAndTalkersUseCase,
    required String externalRoomId,
    required Amplitude amplitude,
    required WatchersParamsProvider paramsProvider,
  })  : _getBlocksUseCase = getBlocksUseCase,
        _getUserUseCase = getUserUseCase,
        _updateUserUseCase = updateUserUseCase,
        _getAllAvatarsUseCase = getAllAvatarsUseCase,
        _removeBlockUseCase = removeBlockUseCase,
        _deleteUserUseCase = deleteUserUseCase,
        _antiSwearingFuture = antiSwearingFuture,
        _connectivityChangeStreamUseCase = getConnectivityChangeStreamUseCase,
        _talker = talker,
        _addBlockUseCase = addBlockUseCase,
        _getChatRulesUseCase = getChatRulesUseCase,
        _setSlowmodeUseCase = setSlowmodeUseCase,
        _getRoomAndTalkersUseCase = getRoomAndTalkersUseCase,
        _externalRoomId = externalRoomId,
        _amplitude = amplitude,
        _paramsProvider = paramsProvider,
        super(const SettingsState.settings()) {
    on<SettingsEvent>(
      (event, emit) => event.maybeMap<Future<void>>(
        orElse: () async {},
        init: (event) => _init(event, emit),
        toBlackList: (event) => _toBlackList(event, emit),
        toProfile: (event) => _toProfile(event, emit),
        backToSettings: (event) => _backToSettings(event, emit),
        toAvatarPicker: (event) => _toAvatarPicker(event, emit),
        selectAvatar: (event) => _selectAvatar(event, emit),
        submitAvatar: (event) => _submitAvatar(event, emit),
        validateInput: (event) => _onValidateInput(event, emit),
        submitInput: (event) => _submitInput(event, emit),
        unblock: (event) => _unblock(event, emit),
        block: (event) => _block(event, emit),
        toSlowmodeSettings: (event) => _toSlowmodeSettings(event, emit),
        toSlowmodeDelayPicker: (event) => _toSlowmodeDelayPicker(event, emit),
        setSlowmode: (event) => _onSetSlowmode(event, emit),
      ),
    );

    on<_DeleteUser>(_onDeleteUser);
    on<_OnConnectivityChanged>(_onOnConnectivityChanged);
    on<SettingsEventEditName>(_onEditName);

    add(const SettingsEvent.init());
  }

  final TextEditingController _userNameTextEditingController = TextEditingController();
  TextEditingController get userNameTextEditingController => _userNameTextEditingController;

  final FocusNode _userNameFocusNode = FocusNode();
  FocusNode get userNameFocusNode => _userNameFocusNode;

  /// Stored data
  List<User>? _blocks;
  User? _user;
  Room? _room;
  late int _roomDelay;
  void sendAmplitudeEvent({required String eventName}){
    final timeStampFormatted = DateTime.now().toIso8601String();

    Map<String, dynamic> eventProperties = {'user_id':_paramsProvider.params.userId,'screen_size':'${window.physicalSize.width}x${window.physicalSize.height}','timeStamp':timeStampFormatted};
    final analiticsMap = AnaliticsDto(user_id: _paramsProvider.params.userId,event_id: _externalRoomId,user_type: _talker.user.vipStatus.toUpperCase(),user_role:_talker.role,).toMap();
    eventProperties.addAll(analiticsMap);
      _amplitude.logEvent(eventName,eventProperties: eventProperties);
      _amplitude.uploadEvents();
    return;
  }

  Room? get room => _room;
  int get roomDelay => _roomDelay;

  ConnectivityResult? _connectivityResult;

  final List<StreamSubscription> _subs = [];

  Future<void> _init(_Init event, Emitter<SettingsState> emit) async {
    sendAmplitudeEvent(eventName: 'Social_ButtonClick_Settings');
    _connectivityResult = await Connectivity().checkConnectivity();
    _subs.add(
      _connectivityChangeStreamUseCase.call().listen((connectivityResult) {
        add(SettingsEvent.onConnectivityChanged(connectivityResult));
      }),
    );

    if (_connectivityResult != ConnectionState.none) {
      await Future.wait([
        _loadBlocks(emit),
        _loadProfile(emit),
        _loadRoom(emit),
      ]);
    }
  }

  Future<void> _loadBlocks(Emitter<SettingsState> emit) async {
    final result = await _getBlocksUseCase();
    if (result.isSuccess) {
      _blocks = result.successValue.initiator;

      if (state is _Settings) {
        return emit(SettingsState.settings(blocks: _blocks));
      }

      if (state is _BlockUsers) {
        return emit(SettingsState.blackList(blocks: _blocks,initialBlocks: _blocks));
      }
    }
  }

  Future<void> _loadProfile(Emitter<SettingsState> emit) async {
    final result = await _getUserUseCase.call();

    if (result.isSuccess) {
      _user = result.successValue.user;

      if (_user != null) {
        userNameTextEditingController.text = _user!.name;
      }

      if (state is _Profile) {
        return emit((state as _Profile).copyWith(user: _user,talker: _talker));
      }
    }
  }

  Future<void> _loadRoom(Emitter<SettingsState> emit) async {
    final result = await _getRoomAndTalkersUseCase.call(_externalRoomId);

    if (result.isSuccess) {
      _room = result.successValue.first.room;
      _roomDelay = _room!.slowmodeDelayMS ?? 1000;

      if (state is _SlowmodeSettings) {
        return emit((state as _SlowmodeSettings).copyWith(room: _room));
      }
    }
  }

  Future<void> _toBlackList(_ToBlackList event, Emitter<SettingsState> emit) async {
    sendAmplitudeEvent(eventName: 'Social_ButtonClick_Banned');
    emit(SettingsState.blackList(blocks: _blocks,initialBlocks: _blocks));
  }

  Future<void> _toProfile(_ToProfile event, Emitter<SettingsState> emit) async {
    sendAmplitudeEvent(eventName: 'Social_ButtonClick_Profile');
    if (_user != null) {
      _userNameTextEditingController.text = _user!.name;
    }
    return emit(SettingsState.profile(
      user: _user,
      userNameInput: _user?.name ?? '',
      talker: _talker,
    ));
  }

  Future<void> _toSlowmodeSettings(_ToSlowmodeSettings event, Emitter<SettingsState> emit) async {
    emit(SettingsState.slowmodeSettings(room: _room,));
  }

  Future<void> _toSlowmodeDelayPicker(_ToSlowmodeDelayPicker event, Emitter<SettingsState> emit) async {
    emit(SettingsState.slowmodeDelayPicker(slowmodeDelay: _roomDelay,));
  }

  Future<void> _backToSettings(_BackToSettings event, Emitter<SettingsState> emit) async {
    emit(SettingsState.settings(blocks: _blocks));
  }

  Future<void> _toAvatarPicker(_ToAvatarPicker event, Emitter<SettingsState> emit) async {
    sendAmplitudeEvent(eventName: 'Social_ButtonClick_AvatarChange');
    if (_connectivityResult == ConnectivityResult.none || _connectivityResult == null) return;

    final avatarsResult = await _getAllAvatarsUseCase.call();

    if (avatarsResult.isError) {
      return;
    }

    if (_user == null) return;

    emit(SettingsState.avatarPicker(
      avatars: avatarsResult.successValue,
      selectedAvatar: _user!.avatar,
      isAvatarChanged: false,
    ));
  }

  Future<void> _selectAvatar(_SelectAvatar event, Emitter<SettingsState> emit) async {
    if (_user == null) return;

    emit(SettingsState.avatarPicker(
      avatars: event.avatars,
      selectedAvatar: event.selectedAvatar,
      isAvatarChanged: event.selectedAvatar != _user!.avatar,
    ));
  }

  Future<void> _submitAvatar(_SubmitAvatar event, Emitter<SettingsState> emit) async {
    sendAmplitudeEvent(eventName: 'Social_ButtonClick_AvatarChangesSaved');
    final newPic = event.selectedAvatar.pic;

    await _updateUserUseCase.call(pic: newPic);

    if (_user == null) return;

    _user = _user!.copyWith(pic: newPic);

    emit(SettingsState.profile(
      user: _user,
      userNameInput: _userNameTextEditingController.text,
      talker:_talker,
    ));
  }

  void editName(SettingsEventEditName event) => add(event);
  void _onEditName(SettingsEventEditName event, Emitter<SettingsState> emit) {
    sendAmplitudeEvent(eventName: 'Social_ButtonClick_NameChange');
    if (_user == null) return;
    if (_connectivityResult == ConnectivityResult.none || _connectivityResult == null) return;

    state.mapOrNull(profile: (state) {
      _userNameFocusNode.requestFocus();

      emit(SettingsState.userName(
        user: _user,
        userNameInput: _user!.name,
      ));
    });
  }

  Future<void> _onValidateInput(_ValidateInput event, Emitter<SettingsState> emit) async {
    await _validateInput(emit);
  }

  Future<bool> _validateInput(Emitter<SettingsState> emit) async {
    final userName = UserName.dirty(_userNameTextEditingController.text);
    final isSwearing = (await _antiSwearingFuture()).containsSwearing(userName.value);

    final errorText = isSwearing ? 'Недопустимое имя' : userName.error.description;

    state.mapOrNull(
      userName: (state) {
        emit(state.copyWith(
          errorDescription: errorText,
          userNameInput: _userNameTextEditingController.text,
        ));
      },
    );

    return errorText != null;
  }

  void submitInput(SettingsEventSubmitInput event) => add(event);
  Future<void> _submitInput(SettingsEventSubmitInput event, Emitter<SettingsState> emit) async {
    sendAmplitudeEvent(eventName: 'Social_ButtonClick_NameChangesSaved');
    _userNameTextEditingController.text = _userNameTextEditingController.text.trim();
    final userName = UserName.dirty(_userNameTextEditingController.text);

    final isNotValid = await _validateInput(emit);

    if (isNotValid) return;

    if (_user == null) return;

    if (_user!.name.trim() != userName.value.trim()) {
      final result = await _updateUserUseCase.call(name: userName.value);

      if (result.isError) {
        return;
      }
    }

    _user = _user!.copyWith(name: userName.value);

    add(const SettingsEvent.toProfile());
  }

  Future<void> _unblock(_Unblock event, Emitter<SettingsState> emit) async {
    final result = await _removeBlockUseCase(targetId: event.block.id);

    if (result.isError) {
      return;
    }

    _blocks = List<User>.from(_blocks!)..remove(event.block);
    state.mapOrNull(
      blackList: (blackListState){
        emit(SettingsState.blackList(blocks: _blocks,initialBlocks: blackListState.initialBlocks));
      }
    );
  }

  Future<void> _block(_Block event, Emitter<SettingsState> emit) async {
    final result = await _addBlockUseCase(targetId: event.block.id);

    if (result.isError) {
      return;
    }

    _blocks = List<User>.from(_blocks!)..add(event.block);
    state.mapOrNull(
        blackList: (blackListState){
          emit(SettingsState.blackList(blocks: _blocks,initialBlocks: blackListState.initialBlocks));
        }
    );
  }

  void _showSnackbar({
    required BuildContext context,
    required SnackBar snackBar,
  }) {
    ScaffoldMessenger.of(context).hideCurrentSnackBar();
    ScaffoldMessenger.of(context).showSnackBar(
      snackBar,
    );
  }

  /// [SettingsEvent.deleteUser]
  void deleteUser(SettingsEvent event) => add(event);
  Future<void> _onDeleteUser(
    _DeleteUser event,
    Emitter<SettingsState> emit,
  ) async {
    sendAmplitudeEvent(eventName: 'Social_ButtonClick_ProfileDelete');
    await showDialog(
      useRootNavigator: false,
      context: event.context,
      builder: (context) => ConfirmDialog(
        titleText: 'Удаление профиля',
        subtitleText: 'Все данные будут удалены. Вы сможете восстановить профиль в течение 30 дней',
        confirmButtonText: 'Удалить профиль',
        cancelButtonText: 'Отмена',
        onConfirm: () async {
          sendAmplitudeEvent(eventName: 'Social_ButtonClick_ProfileDeleteConfirmed');
          final result = await _deleteUserUseCase.call();

          if (result.isError) {
            return;
          }

          Navigator.of(context)..pop();
          Navigator.of(event.context)..popUntil((route) => route.isFirst);

          event.context.read<OnboardingBloc>().add(const OnboardingEvent.showDeleted());
        },
        onCancel: () {
          sendAmplitudeEvent(eventName: 'Social_ButtonClick_ProfileDeleteCancelled');
          Navigator.pop(context);
        },
      ),
    );
  }

  Future<void> _onOnConnectivityChanged(
      _OnConnectivityChanged event, Emitter<SettingsState> emit) async {
    _connectivityResult = event.connectivityResult;
    if (event.connectivityResult != ConnectionState.none) {
      await Future.wait([
        if (_blocks == null) _loadBlocks(emit),
        if (_user == null) _loadProfile(emit),
      ]);
    }
  }

  Future<String> getChatRules() async {
    final result = await _getChatRulesUseCase();
    if (result.isError){
      return '';
    }
    return result.successValue.rulesHtml;
  }

  Future<void> _onSetSlowmode(_SetSlowmode event, Emitter<SettingsState> emit) async {
    _room = _room!.copyWith(slowmodeDelayMS: event.slowModeDelay,isSlowmode:event.isSlowmode);
    _roomDelay = event.slowModeDelay;

    await _setSlowmodeUseCase.call(slowmodeDto: SlowmodeDto(isSlowmode: event.isSlowmode,slowmodeDelayMS: event.slowModeDelay));
    if (state is _SlowmodeSettings) {
      return emit((state as _SlowmodeSettings).copyWith(room: _room));
    }
    if (state is _SlowmodeDelayPicker) {
      return emit((state as _SlowmodeDelayPicker).copyWith(slowmodeDelay: event.slowModeDelay));
    }
  }

  @override
  Future<void> close() {
    sendAmplitudeEvent(eventName: 'Social_NavigationClick_SettingsClose');
    for (final sub in _subs) {
      sub.cancel();
    }
    return super.close();
  }
}
