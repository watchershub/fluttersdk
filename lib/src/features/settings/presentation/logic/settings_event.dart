part of 'settings_bloc.dart';

@freezed
class SettingsEvent with _$SettingsEvent {
  const factory SettingsEvent.init() = _Init;

  const factory SettingsEvent.toProfile() = _ToProfile;

  const factory SettingsEvent.toBlackList() = _ToBlackList;

  const factory SettingsEvent.backToSettings() = _BackToSettings;

  const factory SettingsEvent.toAvatarPicker() = _ToAvatarPicker;

  const factory SettingsEvent.selectAvatar({
    required List<Avatar> avatars,
    required Avatar selectedAvatar,
  }) = _SelectAvatar;

  const factory SettingsEvent.submitAvatar({
    required Avatar selectedAvatar,
  }) = _SubmitAvatar;

  const factory SettingsEvent.editName() = SettingsEventEditName;

  const factory SettingsEvent.closeEditing() = _CloseEditing;

  const factory SettingsEvent.validateInput() = _ValidateInput;

  const factory SettingsEvent.submitInput() = SettingsEventSubmitInput;

  const factory SettingsEvent.unblock({
    required User block,
  }) = _Unblock;

  const factory SettingsEvent.block({
    required User block,
  }) = _Block;

  const factory SettingsEvent.deleteUser({
    required BuildContext context,
  }) = _DeleteUser;

  const factory SettingsEvent.onConnectivityChanged(
    ConnectivityResult connectivityResult,
  ) = _OnConnectivityChanged;

  const factory SettingsEvent.toSlowmodeSettings() = _ToSlowmodeSettings;

  const factory SettingsEvent.toSlowmodeDelayPicker() = _ToSlowmodeDelayPicker;

  const factory SettingsEvent.setSlowmode({
    required bool isSlowmode,
    required int slowModeDelay,
  }) = _SetSlowmode;
}
