part of 'settings_bloc.dart';

@freezed
class SettingsState with _$SettingsState {
  const factory SettingsState.loading() = _Loading;

  const factory SettingsState.settings({
    List<User>? blocks,
  }) = _Settings;

  const factory SettingsState.profile({
    required User? user,
    required String userNameInput,
    required Talker talker,
  }) = _Profile;

  const factory SettingsState.userName({
    required User? user,
    String? errorDescription,
    required String userNameInput,
  }) = _UserName;

  const factory SettingsState.avatarPicker({
    required List<Avatar> avatars,
    required Avatar selectedAvatar,
    required bool isAvatarChanged,
  }) = _AvatarPicker;

  const factory SettingsState.blackList({
    required List<User>? blocks,
    required List<User>? initialBlocks,
  }) = _BlockUsers;

  const factory SettingsState.slowmodeSettings({
    required Room? room,
  }) = _SlowmodeSettings;

  const factory SettingsState.slowmodeDelayPicker({
    required int slowmodeDelay,
  }) = _SlowmodeDelayPicker;
}

extension UserNameX on _UserName {
  bool get isSubmitActive {
    if (user == null) return false;
    return errorDescription == null && user!.name.trim() != userNameInput.trim();
  }
}
