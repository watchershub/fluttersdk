// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'settings_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$SettingsEvent {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsEventCopyWith<$Res> {
  factory $SettingsEventCopyWith(
          SettingsEvent value, $Res Function(SettingsEvent) then) =
      _$SettingsEventCopyWithImpl<$Res>;
}

/// @nodoc
class _$SettingsEventCopyWithImpl<$Res>
    implements $SettingsEventCopyWith<$Res> {
  _$SettingsEventCopyWithImpl(this._value, this._then);

  final SettingsEvent _value;
  // ignore: unused_field
  final $Res Function(SettingsEvent) _then;
}

/// @nodoc
abstract class _$$_InitCopyWith<$Res> {
  factory _$$_InitCopyWith(_$_Init value, $Res Function(_$_Init) then) =
      __$$_InitCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_InitCopyWithImpl<$Res> extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_InitCopyWith<$Res> {
  __$$_InitCopyWithImpl(_$_Init _value, $Res Function(_$_Init) _then)
      : super(_value, (v) => _then(v as _$_Init));

  @override
  _$_Init get _value => super._value as _$_Init;
}

/// @nodoc

class _$_Init implements _Init {
  const _$_Init();

  @override
  String toString() {
    return 'SettingsEvent.init()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Init);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return init();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return init?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return init(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return init?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (init != null) {
      return init(this);
    }
    return orElse();
  }
}

abstract class _Init implements SettingsEvent {
  const factory _Init() = _$_Init;
}

/// @nodoc
abstract class _$$_ToProfileCopyWith<$Res> {
  factory _$$_ToProfileCopyWith(
          _$_ToProfile value, $Res Function(_$_ToProfile) then) =
      __$$_ToProfileCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ToProfileCopyWithImpl<$Res> extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_ToProfileCopyWith<$Res> {
  __$$_ToProfileCopyWithImpl(
      _$_ToProfile _value, $Res Function(_$_ToProfile) _then)
      : super(_value, (v) => _then(v as _$_ToProfile));

  @override
  _$_ToProfile get _value => super._value as _$_ToProfile;
}

/// @nodoc

class _$_ToProfile implements _ToProfile {
  const _$_ToProfile();

  @override
  String toString() {
    return 'SettingsEvent.toProfile()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ToProfile);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return toProfile();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return toProfile?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toProfile != null) {
      return toProfile();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return toProfile(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return toProfile?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toProfile != null) {
      return toProfile(this);
    }
    return orElse();
  }
}

abstract class _ToProfile implements SettingsEvent {
  const factory _ToProfile() = _$_ToProfile;
}

/// @nodoc
abstract class _$$_ToBlackListCopyWith<$Res> {
  factory _$$_ToBlackListCopyWith(
          _$_ToBlackList value, $Res Function(_$_ToBlackList) then) =
      __$$_ToBlackListCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ToBlackListCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_ToBlackListCopyWith<$Res> {
  __$$_ToBlackListCopyWithImpl(
      _$_ToBlackList _value, $Res Function(_$_ToBlackList) _then)
      : super(_value, (v) => _then(v as _$_ToBlackList));

  @override
  _$_ToBlackList get _value => super._value as _$_ToBlackList;
}

/// @nodoc

class _$_ToBlackList implements _ToBlackList {
  const _$_ToBlackList();

  @override
  String toString() {
    return 'SettingsEvent.toBlackList()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ToBlackList);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return toBlackList();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return toBlackList?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toBlackList != null) {
      return toBlackList();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return toBlackList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return toBlackList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toBlackList != null) {
      return toBlackList(this);
    }
    return orElse();
  }
}

abstract class _ToBlackList implements SettingsEvent {
  const factory _ToBlackList() = _$_ToBlackList;
}

/// @nodoc
abstract class _$$_BackToSettingsCopyWith<$Res> {
  factory _$$_BackToSettingsCopyWith(
          _$_BackToSettings value, $Res Function(_$_BackToSettings) then) =
      __$$_BackToSettingsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_BackToSettingsCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_BackToSettingsCopyWith<$Res> {
  __$$_BackToSettingsCopyWithImpl(
      _$_BackToSettings _value, $Res Function(_$_BackToSettings) _then)
      : super(_value, (v) => _then(v as _$_BackToSettings));

  @override
  _$_BackToSettings get _value => super._value as _$_BackToSettings;
}

/// @nodoc

class _$_BackToSettings implements _BackToSettings {
  const _$_BackToSettings();

  @override
  String toString() {
    return 'SettingsEvent.backToSettings()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_BackToSettings);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return backToSettings();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return backToSettings?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (backToSettings != null) {
      return backToSettings();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return backToSettings(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return backToSettings?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (backToSettings != null) {
      return backToSettings(this);
    }
    return orElse();
  }
}

abstract class _BackToSettings implements SettingsEvent {
  const factory _BackToSettings() = _$_BackToSettings;
}

/// @nodoc
abstract class _$$_ToAvatarPickerCopyWith<$Res> {
  factory _$$_ToAvatarPickerCopyWith(
          _$_ToAvatarPicker value, $Res Function(_$_ToAvatarPicker) then) =
      __$$_ToAvatarPickerCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ToAvatarPickerCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_ToAvatarPickerCopyWith<$Res> {
  __$$_ToAvatarPickerCopyWithImpl(
      _$_ToAvatarPicker _value, $Res Function(_$_ToAvatarPicker) _then)
      : super(_value, (v) => _then(v as _$_ToAvatarPicker));

  @override
  _$_ToAvatarPicker get _value => super._value as _$_ToAvatarPicker;
}

/// @nodoc

class _$_ToAvatarPicker implements _ToAvatarPicker {
  const _$_ToAvatarPicker();

  @override
  String toString() {
    return 'SettingsEvent.toAvatarPicker()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ToAvatarPicker);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return toAvatarPicker();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return toAvatarPicker?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toAvatarPicker != null) {
      return toAvatarPicker();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return toAvatarPicker(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return toAvatarPicker?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toAvatarPicker != null) {
      return toAvatarPicker(this);
    }
    return orElse();
  }
}

abstract class _ToAvatarPicker implements SettingsEvent {
  const factory _ToAvatarPicker() = _$_ToAvatarPicker;
}

/// @nodoc
abstract class _$$_SelectAvatarCopyWith<$Res> {
  factory _$$_SelectAvatarCopyWith(
          _$_SelectAvatar value, $Res Function(_$_SelectAvatar) then) =
      __$$_SelectAvatarCopyWithImpl<$Res>;
  $Res call({List<Avatar> avatars, Avatar selectedAvatar});
}

/// @nodoc
class __$$_SelectAvatarCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_SelectAvatarCopyWith<$Res> {
  __$$_SelectAvatarCopyWithImpl(
      _$_SelectAvatar _value, $Res Function(_$_SelectAvatar) _then)
      : super(_value, (v) => _then(v as _$_SelectAvatar));

  @override
  _$_SelectAvatar get _value => super._value as _$_SelectAvatar;

  @override
  $Res call({
    Object? avatars = freezed,
    Object? selectedAvatar = freezed,
  }) {
    return _then(_$_SelectAvatar(
      avatars: avatars == freezed
          ? _value._avatars
          : avatars // ignore: cast_nullable_to_non_nullable
              as List<Avatar>,
      selectedAvatar: selectedAvatar == freezed
          ? _value.selectedAvatar
          : selectedAvatar // ignore: cast_nullable_to_non_nullable
              as Avatar,
    ));
  }
}

/// @nodoc

class _$_SelectAvatar implements _SelectAvatar {
  const _$_SelectAvatar(
      {required final List<Avatar> avatars, required this.selectedAvatar})
      : _avatars = avatars;

  final List<Avatar> _avatars;
  @override
  List<Avatar> get avatars {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_avatars);
  }

  @override
  final Avatar selectedAvatar;

  @override
  String toString() {
    return 'SettingsEvent.selectAvatar(avatars: $avatars, selectedAvatar: $selectedAvatar)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SelectAvatar &&
            const DeepCollectionEquality().equals(other._avatars, _avatars) &&
            const DeepCollectionEquality()
                .equals(other.selectedAvatar, selectedAvatar));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_avatars),
      const DeepCollectionEquality().hash(selectedAvatar));

  @JsonKey(ignore: true)
  @override
  _$$_SelectAvatarCopyWith<_$_SelectAvatar> get copyWith =>
      __$$_SelectAvatarCopyWithImpl<_$_SelectAvatar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return selectAvatar(avatars, selectedAvatar);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return selectAvatar?.call(avatars, selectedAvatar);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (selectAvatar != null) {
      return selectAvatar(avatars, selectedAvatar);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return selectAvatar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return selectAvatar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (selectAvatar != null) {
      return selectAvatar(this);
    }
    return orElse();
  }
}

abstract class _SelectAvatar implements SettingsEvent {
  const factory _SelectAvatar(
      {required final List<Avatar> avatars,
      required final Avatar selectedAvatar}) = _$_SelectAvatar;

  List<Avatar> get avatars;
  Avatar get selectedAvatar;
  @JsonKey(ignore: true)
  _$$_SelectAvatarCopyWith<_$_SelectAvatar> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SubmitAvatarCopyWith<$Res> {
  factory _$$_SubmitAvatarCopyWith(
          _$_SubmitAvatar value, $Res Function(_$_SubmitAvatar) then) =
      __$$_SubmitAvatarCopyWithImpl<$Res>;
  $Res call({Avatar selectedAvatar});
}

/// @nodoc
class __$$_SubmitAvatarCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_SubmitAvatarCopyWith<$Res> {
  __$$_SubmitAvatarCopyWithImpl(
      _$_SubmitAvatar _value, $Res Function(_$_SubmitAvatar) _then)
      : super(_value, (v) => _then(v as _$_SubmitAvatar));

  @override
  _$_SubmitAvatar get _value => super._value as _$_SubmitAvatar;

  @override
  $Res call({
    Object? selectedAvatar = freezed,
  }) {
    return _then(_$_SubmitAvatar(
      selectedAvatar: selectedAvatar == freezed
          ? _value.selectedAvatar
          : selectedAvatar // ignore: cast_nullable_to_non_nullable
              as Avatar,
    ));
  }
}

/// @nodoc

class _$_SubmitAvatar implements _SubmitAvatar {
  const _$_SubmitAvatar({required this.selectedAvatar});

  @override
  final Avatar selectedAvatar;

  @override
  String toString() {
    return 'SettingsEvent.submitAvatar(selectedAvatar: $selectedAvatar)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SubmitAvatar &&
            const DeepCollectionEquality()
                .equals(other.selectedAvatar, selectedAvatar));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(selectedAvatar));

  @JsonKey(ignore: true)
  @override
  _$$_SubmitAvatarCopyWith<_$_SubmitAvatar> get copyWith =>
      __$$_SubmitAvatarCopyWithImpl<_$_SubmitAvatar>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return submitAvatar(selectedAvatar);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return submitAvatar?.call(selectedAvatar);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (submitAvatar != null) {
      return submitAvatar(selectedAvatar);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return submitAvatar(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return submitAvatar?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (submitAvatar != null) {
      return submitAvatar(this);
    }
    return orElse();
  }
}

abstract class _SubmitAvatar implements SettingsEvent {
  const factory _SubmitAvatar({required final Avatar selectedAvatar}) =
      _$_SubmitAvatar;

  Avatar get selectedAvatar;
  @JsonKey(ignore: true)
  _$$_SubmitAvatarCopyWith<_$_SubmitAvatar> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SettingsEventEditNameCopyWith<$Res> {
  factory _$$SettingsEventEditNameCopyWith(_$SettingsEventEditName value,
          $Res Function(_$SettingsEventEditName) then) =
      __$$SettingsEventEditNameCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SettingsEventEditNameCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$SettingsEventEditNameCopyWith<$Res> {
  __$$SettingsEventEditNameCopyWithImpl(_$SettingsEventEditName _value,
      $Res Function(_$SettingsEventEditName) _then)
      : super(_value, (v) => _then(v as _$SettingsEventEditName));

  @override
  _$SettingsEventEditName get _value => super._value as _$SettingsEventEditName;
}

/// @nodoc

class _$SettingsEventEditName implements SettingsEventEditName {
  const _$SettingsEventEditName();

  @override
  String toString() {
    return 'SettingsEvent.editName()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$SettingsEventEditName);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return editName();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return editName?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (editName != null) {
      return editName();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return editName(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return editName?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (editName != null) {
      return editName(this);
    }
    return orElse();
  }
}

abstract class SettingsEventEditName implements SettingsEvent {
  const factory SettingsEventEditName() = _$SettingsEventEditName;
}

/// @nodoc
abstract class _$$_CloseEditingCopyWith<$Res> {
  factory _$$_CloseEditingCopyWith(
          _$_CloseEditing value, $Res Function(_$_CloseEditing) then) =
      __$$_CloseEditingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_CloseEditingCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_CloseEditingCopyWith<$Res> {
  __$$_CloseEditingCopyWithImpl(
      _$_CloseEditing _value, $Res Function(_$_CloseEditing) _then)
      : super(_value, (v) => _then(v as _$_CloseEditing));

  @override
  _$_CloseEditing get _value => super._value as _$_CloseEditing;
}

/// @nodoc

class _$_CloseEditing implements _CloseEditing {
  const _$_CloseEditing();

  @override
  String toString() {
    return 'SettingsEvent.closeEditing()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_CloseEditing);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return closeEditing();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return closeEditing?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (closeEditing != null) {
      return closeEditing();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return closeEditing(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return closeEditing?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (closeEditing != null) {
      return closeEditing(this);
    }
    return orElse();
  }
}

abstract class _CloseEditing implements SettingsEvent {
  const factory _CloseEditing() = _$_CloseEditing;
}

/// @nodoc
abstract class _$$_ValidateInputCopyWith<$Res> {
  factory _$$_ValidateInputCopyWith(
          _$_ValidateInput value, $Res Function(_$_ValidateInput) then) =
      __$$_ValidateInputCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ValidateInputCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_ValidateInputCopyWith<$Res> {
  __$$_ValidateInputCopyWithImpl(
      _$_ValidateInput _value, $Res Function(_$_ValidateInput) _then)
      : super(_value, (v) => _then(v as _$_ValidateInput));

  @override
  _$_ValidateInput get _value => super._value as _$_ValidateInput;
}

/// @nodoc

class _$_ValidateInput implements _ValidateInput {
  const _$_ValidateInput();

  @override
  String toString() {
    return 'SettingsEvent.validateInput()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ValidateInput);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return validateInput();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return validateInput?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (validateInput != null) {
      return validateInput();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return validateInput(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return validateInput?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (validateInput != null) {
      return validateInput(this);
    }
    return orElse();
  }
}

abstract class _ValidateInput implements SettingsEvent {
  const factory _ValidateInput() = _$_ValidateInput;
}

/// @nodoc
abstract class _$$SettingsEventSubmitInputCopyWith<$Res> {
  factory _$$SettingsEventSubmitInputCopyWith(_$SettingsEventSubmitInput value,
          $Res Function(_$SettingsEventSubmitInput) then) =
      __$$SettingsEventSubmitInputCopyWithImpl<$Res>;
}

/// @nodoc
class __$$SettingsEventSubmitInputCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$SettingsEventSubmitInputCopyWith<$Res> {
  __$$SettingsEventSubmitInputCopyWithImpl(_$SettingsEventSubmitInput _value,
      $Res Function(_$SettingsEventSubmitInput) _then)
      : super(_value, (v) => _then(v as _$SettingsEventSubmitInput));

  @override
  _$SettingsEventSubmitInput get _value =>
      super._value as _$SettingsEventSubmitInput;
}

/// @nodoc

class _$SettingsEventSubmitInput implements SettingsEventSubmitInput {
  const _$SettingsEventSubmitInput();

  @override
  String toString() {
    return 'SettingsEvent.submitInput()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SettingsEventSubmitInput);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return submitInput();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return submitInput?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (submitInput != null) {
      return submitInput();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return submitInput(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return submitInput?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (submitInput != null) {
      return submitInput(this);
    }
    return orElse();
  }
}

abstract class SettingsEventSubmitInput implements SettingsEvent {
  const factory SettingsEventSubmitInput() = _$SettingsEventSubmitInput;
}

/// @nodoc
abstract class _$$_UnblockCopyWith<$Res> {
  factory _$$_UnblockCopyWith(
          _$_Unblock value, $Res Function(_$_Unblock) then) =
      __$$_UnblockCopyWithImpl<$Res>;
  $Res call({User block});
}

/// @nodoc
class __$$_UnblockCopyWithImpl<$Res> extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_UnblockCopyWith<$Res> {
  __$$_UnblockCopyWithImpl(_$_Unblock _value, $Res Function(_$_Unblock) _then)
      : super(_value, (v) => _then(v as _$_Unblock));

  @override
  _$_Unblock get _value => super._value as _$_Unblock;

  @override
  $Res call({
    Object? block = freezed,
  }) {
    return _then(_$_Unblock(
      block: block == freezed
          ? _value.block
          : block // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }
}

/// @nodoc

class _$_Unblock implements _Unblock {
  const _$_Unblock({required this.block});

  @override
  final User block;

  @override
  String toString() {
    return 'SettingsEvent.unblock(block: $block)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Unblock &&
            const DeepCollectionEquality().equals(other.block, block));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(block));

  @JsonKey(ignore: true)
  @override
  _$$_UnblockCopyWith<_$_Unblock> get copyWith =>
      __$$_UnblockCopyWithImpl<_$_Unblock>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return unblock(this.block);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return unblock?.call(this.block);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (unblock != null) {
      return unblock(this.block);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return unblock(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return unblock?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (unblock != null) {
      return unblock(this);
    }
    return orElse();
  }
}

abstract class _Unblock implements SettingsEvent {
  const factory _Unblock({required final User block}) = _$_Unblock;

  User get block;
  @JsonKey(ignore: true)
  _$$_UnblockCopyWith<_$_Unblock> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_BlockCopyWith<$Res> {
  factory _$$_BlockCopyWith(_$_Block value, $Res Function(_$_Block) then) =
      __$$_BlockCopyWithImpl<$Res>;
  $Res call({User block});
}

/// @nodoc
class __$$_BlockCopyWithImpl<$Res> extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_BlockCopyWith<$Res> {
  __$$_BlockCopyWithImpl(_$_Block _value, $Res Function(_$_Block) _then)
      : super(_value, (v) => _then(v as _$_Block));

  @override
  _$_Block get _value => super._value as _$_Block;

  @override
  $Res call({
    Object? block = freezed,
  }) {
    return _then(_$_Block(
      block: block == freezed
          ? _value.block
          : block // ignore: cast_nullable_to_non_nullable
              as User,
    ));
  }
}

/// @nodoc

class _$_Block implements _Block {
  const _$_Block({required this.block});

  @override
  final User block;

  @override
  String toString() {
    return 'SettingsEvent.block(block: $block)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Block &&
            const DeepCollectionEquality().equals(other.block, block));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(block));

  @JsonKey(ignore: true)
  @override
  _$$_BlockCopyWith<_$_Block> get copyWith =>
      __$$_BlockCopyWithImpl<_$_Block>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return block(this.block);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return block?.call(this.block);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (block != null) {
      return block(this.block);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return block(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return block?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (block != null) {
      return block(this);
    }
    return orElse();
  }
}

abstract class _Block implements SettingsEvent {
  const factory _Block({required final User block}) = _$_Block;

  User get block;
  @JsonKey(ignore: true)
  _$$_BlockCopyWith<_$_Block> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_DeleteUserCopyWith<$Res> {
  factory _$$_DeleteUserCopyWith(
          _$_DeleteUser value, $Res Function(_$_DeleteUser) then) =
      __$$_DeleteUserCopyWithImpl<$Res>;
  $Res call({BuildContext context});
}

/// @nodoc
class __$$_DeleteUserCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_DeleteUserCopyWith<$Res> {
  __$$_DeleteUserCopyWithImpl(
      _$_DeleteUser _value, $Res Function(_$_DeleteUser) _then)
      : super(_value, (v) => _then(v as _$_DeleteUser));

  @override
  _$_DeleteUser get _value => super._value as _$_DeleteUser;

  @override
  $Res call({
    Object? context = freezed,
  }) {
    return _then(_$_DeleteUser(
      context: context == freezed
          ? _value.context
          : context // ignore: cast_nullable_to_non_nullable
              as BuildContext,
    ));
  }
}

/// @nodoc

class _$_DeleteUser implements _DeleteUser {
  const _$_DeleteUser({required this.context});

  @override
  final BuildContext context;

  @override
  String toString() {
    return 'SettingsEvent.deleteUser(context: $context)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_DeleteUser &&
            const DeepCollectionEquality().equals(other.context, context));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(context));

  @JsonKey(ignore: true)
  @override
  _$$_DeleteUserCopyWith<_$_DeleteUser> get copyWith =>
      __$$_DeleteUserCopyWithImpl<_$_DeleteUser>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return deleteUser(context);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return deleteUser?.call(context);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (deleteUser != null) {
      return deleteUser(context);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return deleteUser(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return deleteUser?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (deleteUser != null) {
      return deleteUser(this);
    }
    return orElse();
  }
}

abstract class _DeleteUser implements SettingsEvent {
  const factory _DeleteUser({required final BuildContext context}) =
      _$_DeleteUser;

  BuildContext get context;
  @JsonKey(ignore: true)
  _$$_DeleteUserCopyWith<_$_DeleteUser> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_OnConnectivityChangedCopyWith<$Res> {
  factory _$$_OnConnectivityChangedCopyWith(_$_OnConnectivityChanged value,
          $Res Function(_$_OnConnectivityChanged) then) =
      __$$_OnConnectivityChangedCopyWithImpl<$Res>;
  $Res call({ConnectivityResult connectivityResult});
}

/// @nodoc
class __$$_OnConnectivityChangedCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_OnConnectivityChangedCopyWith<$Res> {
  __$$_OnConnectivityChangedCopyWithImpl(_$_OnConnectivityChanged _value,
      $Res Function(_$_OnConnectivityChanged) _then)
      : super(_value, (v) => _then(v as _$_OnConnectivityChanged));

  @override
  _$_OnConnectivityChanged get _value =>
      super._value as _$_OnConnectivityChanged;

  @override
  $Res call({
    Object? connectivityResult = freezed,
  }) {
    return _then(_$_OnConnectivityChanged(
      connectivityResult == freezed
          ? _value.connectivityResult
          : connectivityResult // ignore: cast_nullable_to_non_nullable
              as ConnectivityResult,
    ));
  }
}

/// @nodoc

class _$_OnConnectivityChanged implements _OnConnectivityChanged {
  const _$_OnConnectivityChanged(this.connectivityResult);

  @override
  final ConnectivityResult connectivityResult;

  @override
  String toString() {
    return 'SettingsEvent.onConnectivityChanged(connectivityResult: $connectivityResult)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_OnConnectivityChanged &&
            const DeepCollectionEquality()
                .equals(other.connectivityResult, connectivityResult));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(connectivityResult));

  @JsonKey(ignore: true)
  @override
  _$$_OnConnectivityChangedCopyWith<_$_OnConnectivityChanged> get copyWith =>
      __$$_OnConnectivityChangedCopyWithImpl<_$_OnConnectivityChanged>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return onConnectivityChanged(connectivityResult);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return onConnectivityChanged?.call(connectivityResult);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (onConnectivityChanged != null) {
      return onConnectivityChanged(connectivityResult);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return onConnectivityChanged(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return onConnectivityChanged?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (onConnectivityChanged != null) {
      return onConnectivityChanged(this);
    }
    return orElse();
  }
}

abstract class _OnConnectivityChanged implements SettingsEvent {
  const factory _OnConnectivityChanged(
      final ConnectivityResult connectivityResult) = _$_OnConnectivityChanged;

  ConnectivityResult get connectivityResult;
  @JsonKey(ignore: true)
  _$$_OnConnectivityChangedCopyWith<_$_OnConnectivityChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ToSlowmodeSettingsCopyWith<$Res> {
  factory _$$_ToSlowmodeSettingsCopyWith(_$_ToSlowmodeSettings value,
          $Res Function(_$_ToSlowmodeSettings) then) =
      __$$_ToSlowmodeSettingsCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ToSlowmodeSettingsCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_ToSlowmodeSettingsCopyWith<$Res> {
  __$$_ToSlowmodeSettingsCopyWithImpl(
      _$_ToSlowmodeSettings _value, $Res Function(_$_ToSlowmodeSettings) _then)
      : super(_value, (v) => _then(v as _$_ToSlowmodeSettings));

  @override
  _$_ToSlowmodeSettings get _value => super._value as _$_ToSlowmodeSettings;
}

/// @nodoc

class _$_ToSlowmodeSettings implements _ToSlowmodeSettings {
  const _$_ToSlowmodeSettings();

  @override
  String toString() {
    return 'SettingsEvent.toSlowmodeSettings()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ToSlowmodeSettings);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return toSlowmodeSettings();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return toSlowmodeSettings?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toSlowmodeSettings != null) {
      return toSlowmodeSettings();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return toSlowmodeSettings(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return toSlowmodeSettings?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toSlowmodeSettings != null) {
      return toSlowmodeSettings(this);
    }
    return orElse();
  }
}

abstract class _ToSlowmodeSettings implements SettingsEvent {
  const factory _ToSlowmodeSettings() = _$_ToSlowmodeSettings;
}

/// @nodoc
abstract class _$$_ToSlowmodeDelayPickerCopyWith<$Res> {
  factory _$$_ToSlowmodeDelayPickerCopyWith(_$_ToSlowmodeDelayPicker value,
          $Res Function(_$_ToSlowmodeDelayPicker) then) =
      __$$_ToSlowmodeDelayPickerCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_ToSlowmodeDelayPickerCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_ToSlowmodeDelayPickerCopyWith<$Res> {
  __$$_ToSlowmodeDelayPickerCopyWithImpl(_$_ToSlowmodeDelayPicker _value,
      $Res Function(_$_ToSlowmodeDelayPicker) _then)
      : super(_value, (v) => _then(v as _$_ToSlowmodeDelayPicker));

  @override
  _$_ToSlowmodeDelayPicker get _value =>
      super._value as _$_ToSlowmodeDelayPicker;
}

/// @nodoc

class _$_ToSlowmodeDelayPicker implements _ToSlowmodeDelayPicker {
  const _$_ToSlowmodeDelayPicker();

  @override
  String toString() {
    return 'SettingsEvent.toSlowmodeDelayPicker()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_ToSlowmodeDelayPicker);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return toSlowmodeDelayPicker();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return toSlowmodeDelayPicker?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toSlowmodeDelayPicker != null) {
      return toSlowmodeDelayPicker();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return toSlowmodeDelayPicker(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return toSlowmodeDelayPicker?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (toSlowmodeDelayPicker != null) {
      return toSlowmodeDelayPicker(this);
    }
    return orElse();
  }
}

abstract class _ToSlowmodeDelayPicker implements SettingsEvent {
  const factory _ToSlowmodeDelayPicker() = _$_ToSlowmodeDelayPicker;
}

/// @nodoc
abstract class _$$_SetSlowmodeCopyWith<$Res> {
  factory _$$_SetSlowmodeCopyWith(
          _$_SetSlowmode value, $Res Function(_$_SetSlowmode) then) =
      __$$_SetSlowmodeCopyWithImpl<$Res>;
  $Res call({bool isSlowmode, int slowModeDelay});
}

/// @nodoc
class __$$_SetSlowmodeCopyWithImpl<$Res>
    extends _$SettingsEventCopyWithImpl<$Res>
    implements _$$_SetSlowmodeCopyWith<$Res> {
  __$$_SetSlowmodeCopyWithImpl(
      _$_SetSlowmode _value, $Res Function(_$_SetSlowmode) _then)
      : super(_value, (v) => _then(v as _$_SetSlowmode));

  @override
  _$_SetSlowmode get _value => super._value as _$_SetSlowmode;

  @override
  $Res call({
    Object? isSlowmode = freezed,
    Object? slowModeDelay = freezed,
  }) {
    return _then(_$_SetSlowmode(
      isSlowmode: isSlowmode == freezed
          ? _value.isSlowmode
          : isSlowmode // ignore: cast_nullable_to_non_nullable
              as bool,
      slowModeDelay: slowModeDelay == freezed
          ? _value.slowModeDelay
          : slowModeDelay // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SetSlowmode implements _SetSlowmode {
  const _$_SetSlowmode({required this.isSlowmode, required this.slowModeDelay});

  @override
  final bool isSlowmode;
  @override
  final int slowModeDelay;

  @override
  String toString() {
    return 'SettingsEvent.setSlowmode(isSlowmode: $isSlowmode, slowModeDelay: $slowModeDelay)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SetSlowmode &&
            const DeepCollectionEquality()
                .equals(other.isSlowmode, isSlowmode) &&
            const DeepCollectionEquality()
                .equals(other.slowModeDelay, slowModeDelay));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(isSlowmode),
      const DeepCollectionEquality().hash(slowModeDelay));

  @JsonKey(ignore: true)
  @override
  _$$_SetSlowmodeCopyWith<_$_SetSlowmode> get copyWith =>
      __$$_SetSlowmodeCopyWithImpl<_$_SetSlowmode>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() init,
    required TResult Function() toProfile,
    required TResult Function() toBlackList,
    required TResult Function() backToSettings,
    required TResult Function() toAvatarPicker,
    required TResult Function(List<Avatar> avatars, Avatar selectedAvatar)
        selectAvatar,
    required TResult Function(Avatar selectedAvatar) submitAvatar,
    required TResult Function() editName,
    required TResult Function() closeEditing,
    required TResult Function() validateInput,
    required TResult Function() submitInput,
    required TResult Function(User block) unblock,
    required TResult Function(User block) block,
    required TResult Function(BuildContext context) deleteUser,
    required TResult Function(ConnectivityResult connectivityResult)
        onConnectivityChanged,
    required TResult Function() toSlowmodeSettings,
    required TResult Function() toSlowmodeDelayPicker,
    required TResult Function(bool isSlowmode, int slowModeDelay) setSlowmode,
  }) {
    return setSlowmode(isSlowmode, slowModeDelay);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
  }) {
    return setSlowmode?.call(isSlowmode, slowModeDelay);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? init,
    TResult Function()? toProfile,
    TResult Function()? toBlackList,
    TResult Function()? backToSettings,
    TResult Function()? toAvatarPicker,
    TResult Function(List<Avatar> avatars, Avatar selectedAvatar)? selectAvatar,
    TResult Function(Avatar selectedAvatar)? submitAvatar,
    TResult Function()? editName,
    TResult Function()? closeEditing,
    TResult Function()? validateInput,
    TResult Function()? submitInput,
    TResult Function(User block)? unblock,
    TResult Function(User block)? block,
    TResult Function(BuildContext context)? deleteUser,
    TResult Function(ConnectivityResult connectivityResult)?
        onConnectivityChanged,
    TResult Function()? toSlowmodeSettings,
    TResult Function()? toSlowmodeDelayPicker,
    TResult Function(bool isSlowmode, int slowModeDelay)? setSlowmode,
    required TResult orElse(),
  }) {
    if (setSlowmode != null) {
      return setSlowmode(isSlowmode, slowModeDelay);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Init value) init,
    required TResult Function(_ToProfile value) toProfile,
    required TResult Function(_ToBlackList value) toBlackList,
    required TResult Function(_BackToSettings value) backToSettings,
    required TResult Function(_ToAvatarPicker value) toAvatarPicker,
    required TResult Function(_SelectAvatar value) selectAvatar,
    required TResult Function(_SubmitAvatar value) submitAvatar,
    required TResult Function(SettingsEventEditName value) editName,
    required TResult Function(_CloseEditing value) closeEditing,
    required TResult Function(_ValidateInput value) validateInput,
    required TResult Function(SettingsEventSubmitInput value) submitInput,
    required TResult Function(_Unblock value) unblock,
    required TResult Function(_Block value) block,
    required TResult Function(_DeleteUser value) deleteUser,
    required TResult Function(_OnConnectivityChanged value)
        onConnectivityChanged,
    required TResult Function(_ToSlowmodeSettings value) toSlowmodeSettings,
    required TResult Function(_ToSlowmodeDelayPicker value)
        toSlowmodeDelayPicker,
    required TResult Function(_SetSlowmode value) setSlowmode,
  }) {
    return setSlowmode(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
  }) {
    return setSlowmode?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Init value)? init,
    TResult Function(_ToProfile value)? toProfile,
    TResult Function(_ToBlackList value)? toBlackList,
    TResult Function(_BackToSettings value)? backToSettings,
    TResult Function(_ToAvatarPicker value)? toAvatarPicker,
    TResult Function(_SelectAvatar value)? selectAvatar,
    TResult Function(_SubmitAvatar value)? submitAvatar,
    TResult Function(SettingsEventEditName value)? editName,
    TResult Function(_CloseEditing value)? closeEditing,
    TResult Function(_ValidateInput value)? validateInput,
    TResult Function(SettingsEventSubmitInput value)? submitInput,
    TResult Function(_Unblock value)? unblock,
    TResult Function(_Block value)? block,
    TResult Function(_DeleteUser value)? deleteUser,
    TResult Function(_OnConnectivityChanged value)? onConnectivityChanged,
    TResult Function(_ToSlowmodeSettings value)? toSlowmodeSettings,
    TResult Function(_ToSlowmodeDelayPicker value)? toSlowmodeDelayPicker,
    TResult Function(_SetSlowmode value)? setSlowmode,
    required TResult orElse(),
  }) {
    if (setSlowmode != null) {
      return setSlowmode(this);
    }
    return orElse();
  }
}

abstract class _SetSlowmode implements SettingsEvent {
  const factory _SetSlowmode(
      {required final bool isSlowmode,
      required final int slowModeDelay}) = _$_SetSlowmode;

  bool get isSlowmode;
  int get slowModeDelay;
  @JsonKey(ignore: true)
  _$$_SetSlowmodeCopyWith<_$_SetSlowmode> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
mixin _$SettingsState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SettingsStateCopyWith<$Res> {
  factory $SettingsStateCopyWith(
          SettingsState value, $Res Function(SettingsState) then) =
      _$SettingsStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$SettingsStateCopyWithImpl<$Res>
    implements $SettingsStateCopyWith<$Res> {
  _$SettingsStateCopyWithImpl(this._value, this._then);

  final SettingsState _value;
  // ignore: unused_field
  final $Res Function(SettingsState) _then;
}

/// @nodoc
abstract class _$$_LoadingCopyWith<$Res> {
  factory _$$_LoadingCopyWith(
          _$_Loading value, $Res Function(_$_Loading) then) =
      __$$_LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$_LoadingCopyWithImpl<$Res> extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$_LoadingCopyWith<$Res> {
  __$$_LoadingCopyWithImpl(_$_Loading _value, $Res Function(_$_Loading) _then)
      : super(_value, (v) => _then(v as _$_Loading));

  @override
  _$_Loading get _value => super._value as _$_Loading;
}

/// @nodoc

class _$_Loading implements _Loading {
  const _$_Loading();

  @override
  String toString() {
    return 'SettingsState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$_Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class _Loading implements SettingsState {
  const factory _Loading() = _$_Loading;
}

/// @nodoc
abstract class _$$_SettingsCopyWith<$Res> {
  factory _$$_SettingsCopyWith(
          _$_Settings value, $Res Function(_$_Settings) then) =
      __$$_SettingsCopyWithImpl<$Res>;
  $Res call({List<User>? blocks});
}

/// @nodoc
class __$$_SettingsCopyWithImpl<$Res> extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$_SettingsCopyWith<$Res> {
  __$$_SettingsCopyWithImpl(
      _$_Settings _value, $Res Function(_$_Settings) _then)
      : super(_value, (v) => _then(v as _$_Settings));

  @override
  _$_Settings get _value => super._value as _$_Settings;

  @override
  $Res call({
    Object? blocks = freezed,
  }) {
    return _then(_$_Settings(
      blocks: blocks == freezed
          ? _value._blocks
          : blocks // ignore: cast_nullable_to_non_nullable
              as List<User>?,
    ));
  }
}

/// @nodoc

class _$_Settings implements _Settings {
  const _$_Settings({final List<User>? blocks}) : _blocks = blocks;

  final List<User>? _blocks;
  @override
  List<User>? get blocks {
    final value = _blocks;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'SettingsState.settings(blocks: $blocks)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Settings &&
            const DeepCollectionEquality().equals(other._blocks, _blocks));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(_blocks));

  @JsonKey(ignore: true)
  @override
  _$$_SettingsCopyWith<_$_Settings> get copyWith =>
      __$$_SettingsCopyWithImpl<_$_Settings>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) {
    return settings(blocks);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) {
    return settings?.call(blocks);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (settings != null) {
      return settings(blocks);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) {
    return settings(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) {
    return settings?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (settings != null) {
      return settings(this);
    }
    return orElse();
  }
}

abstract class _Settings implements SettingsState {
  const factory _Settings({final List<User>? blocks}) = _$_Settings;

  List<User>? get blocks;
  @JsonKey(ignore: true)
  _$$_SettingsCopyWith<_$_Settings> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_ProfileCopyWith<$Res> {
  factory _$$_ProfileCopyWith(
          _$_Profile value, $Res Function(_$_Profile) then) =
      __$$_ProfileCopyWithImpl<$Res>;
  $Res call({User? user, String userNameInput, Talker talker});
}

/// @nodoc
class __$$_ProfileCopyWithImpl<$Res> extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$_ProfileCopyWith<$Res> {
  __$$_ProfileCopyWithImpl(_$_Profile _value, $Res Function(_$_Profile) _then)
      : super(_value, (v) => _then(v as _$_Profile));

  @override
  _$_Profile get _value => super._value as _$_Profile;

  @override
  $Res call({
    Object? user = freezed,
    Object? userNameInput = freezed,
    Object? talker = freezed,
  }) {
    return _then(_$_Profile(
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User?,
      userNameInput: userNameInput == freezed
          ? _value.userNameInput
          : userNameInput // ignore: cast_nullable_to_non_nullable
              as String,
      talker: talker == freezed
          ? _value.talker
          : talker // ignore: cast_nullable_to_non_nullable
              as Talker,
    ));
  }
}

/// @nodoc

class _$_Profile implements _Profile {
  const _$_Profile(
      {required this.user, required this.userNameInput, required this.talker});

  @override
  final User? user;
  @override
  final String userNameInput;
  @override
  final Talker talker;

  @override
  String toString() {
    return 'SettingsState.profile(user: $user, userNameInput: $userNameInput, talker: $talker)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Profile &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality()
                .equals(other.userNameInput, userNameInput) &&
            const DeepCollectionEquality().equals(other.talker, talker));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(userNameInput),
      const DeepCollectionEquality().hash(talker));

  @JsonKey(ignore: true)
  @override
  _$$_ProfileCopyWith<_$_Profile> get copyWith =>
      __$$_ProfileCopyWithImpl<_$_Profile>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) {
    return profile(user, userNameInput, talker);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) {
    return profile?.call(user, userNameInput, talker);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (profile != null) {
      return profile(user, userNameInput, talker);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) {
    return profile(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) {
    return profile?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (profile != null) {
      return profile(this);
    }
    return orElse();
  }
}

abstract class _Profile implements SettingsState {
  const factory _Profile(
      {required final User? user,
      required final String userNameInput,
      required final Talker talker}) = _$_Profile;

  User? get user;
  String get userNameInput;
  Talker get talker;
  @JsonKey(ignore: true)
  _$$_ProfileCopyWith<_$_Profile> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_UserNameCopyWith<$Res> {
  factory _$$_UserNameCopyWith(
          _$_UserName value, $Res Function(_$_UserName) then) =
      __$$_UserNameCopyWithImpl<$Res>;
  $Res call({User? user, String? errorDescription, String userNameInput});
}

/// @nodoc
class __$$_UserNameCopyWithImpl<$Res> extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$_UserNameCopyWith<$Res> {
  __$$_UserNameCopyWithImpl(
      _$_UserName _value, $Res Function(_$_UserName) _then)
      : super(_value, (v) => _then(v as _$_UserName));

  @override
  _$_UserName get _value => super._value as _$_UserName;

  @override
  $Res call({
    Object? user = freezed,
    Object? errorDescription = freezed,
    Object? userNameInput = freezed,
  }) {
    return _then(_$_UserName(
      user: user == freezed
          ? _value.user
          : user // ignore: cast_nullable_to_non_nullable
              as User?,
      errorDescription: errorDescription == freezed
          ? _value.errorDescription
          : errorDescription // ignore: cast_nullable_to_non_nullable
              as String?,
      userNameInput: userNameInput == freezed
          ? _value.userNameInput
          : userNameInput // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$_UserName implements _UserName {
  const _$_UserName(
      {required this.user, this.errorDescription, required this.userNameInput});

  @override
  final User? user;
  @override
  final String? errorDescription;
  @override
  final String userNameInput;

  @override
  String toString() {
    return 'SettingsState.userName(user: $user, errorDescription: $errorDescription, userNameInput: $userNameInput)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UserName &&
            const DeepCollectionEquality().equals(other.user, user) &&
            const DeepCollectionEquality()
                .equals(other.errorDescription, errorDescription) &&
            const DeepCollectionEquality()
                .equals(other.userNameInput, userNameInput));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(user),
      const DeepCollectionEquality().hash(errorDescription),
      const DeepCollectionEquality().hash(userNameInput));

  @JsonKey(ignore: true)
  @override
  _$$_UserNameCopyWith<_$_UserName> get copyWith =>
      __$$_UserNameCopyWithImpl<_$_UserName>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) {
    return userName(user, errorDescription, userNameInput);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) {
    return userName?.call(user, errorDescription, userNameInput);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (userName != null) {
      return userName(user, errorDescription, userNameInput);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) {
    return userName(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) {
    return userName?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (userName != null) {
      return userName(this);
    }
    return orElse();
  }
}

abstract class _UserName implements SettingsState {
  const factory _UserName(
      {required final User? user,
      final String? errorDescription,
      required final String userNameInput}) = _$_UserName;

  User? get user;
  String? get errorDescription;
  String get userNameInput;
  @JsonKey(ignore: true)
  _$$_UserNameCopyWith<_$_UserName> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_AvatarPickerCopyWith<$Res> {
  factory _$$_AvatarPickerCopyWith(
          _$_AvatarPicker value, $Res Function(_$_AvatarPicker) then) =
      __$$_AvatarPickerCopyWithImpl<$Res>;
  $Res call(
      {List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged});
}

/// @nodoc
class __$$_AvatarPickerCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$_AvatarPickerCopyWith<$Res> {
  __$$_AvatarPickerCopyWithImpl(
      _$_AvatarPicker _value, $Res Function(_$_AvatarPicker) _then)
      : super(_value, (v) => _then(v as _$_AvatarPicker));

  @override
  _$_AvatarPicker get _value => super._value as _$_AvatarPicker;

  @override
  $Res call({
    Object? avatars = freezed,
    Object? selectedAvatar = freezed,
    Object? isAvatarChanged = freezed,
  }) {
    return _then(_$_AvatarPicker(
      avatars: avatars == freezed
          ? _value._avatars
          : avatars // ignore: cast_nullable_to_non_nullable
              as List<Avatar>,
      selectedAvatar: selectedAvatar == freezed
          ? _value.selectedAvatar
          : selectedAvatar // ignore: cast_nullable_to_non_nullable
              as Avatar,
      isAvatarChanged: isAvatarChanged == freezed
          ? _value.isAvatarChanged
          : isAvatarChanged // ignore: cast_nullable_to_non_nullable
              as bool,
    ));
  }
}

/// @nodoc

class _$_AvatarPicker implements _AvatarPicker {
  const _$_AvatarPicker(
      {required final List<Avatar> avatars,
      required this.selectedAvatar,
      required this.isAvatarChanged})
      : _avatars = avatars;

  final List<Avatar> _avatars;
  @override
  List<Avatar> get avatars {
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_avatars);
  }

  @override
  final Avatar selectedAvatar;
  @override
  final bool isAvatarChanged;

  @override
  String toString() {
    return 'SettingsState.avatarPicker(avatars: $avatars, selectedAvatar: $selectedAvatar, isAvatarChanged: $isAvatarChanged)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_AvatarPicker &&
            const DeepCollectionEquality().equals(other._avatars, _avatars) &&
            const DeepCollectionEquality()
                .equals(other.selectedAvatar, selectedAvatar) &&
            const DeepCollectionEquality()
                .equals(other.isAvatarChanged, isAvatarChanged));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_avatars),
      const DeepCollectionEquality().hash(selectedAvatar),
      const DeepCollectionEquality().hash(isAvatarChanged));

  @JsonKey(ignore: true)
  @override
  _$$_AvatarPickerCopyWith<_$_AvatarPicker> get copyWith =>
      __$$_AvatarPickerCopyWithImpl<_$_AvatarPicker>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) {
    return avatarPicker(avatars, selectedAvatar, isAvatarChanged);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) {
    return avatarPicker?.call(avatars, selectedAvatar, isAvatarChanged);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (avatarPicker != null) {
      return avatarPicker(avatars, selectedAvatar, isAvatarChanged);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) {
    return avatarPicker(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) {
    return avatarPicker?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (avatarPicker != null) {
      return avatarPicker(this);
    }
    return orElse();
  }
}

abstract class _AvatarPicker implements SettingsState {
  const factory _AvatarPicker(
      {required final List<Avatar> avatars,
      required final Avatar selectedAvatar,
      required final bool isAvatarChanged}) = _$_AvatarPicker;

  List<Avatar> get avatars;
  Avatar get selectedAvatar;
  bool get isAvatarChanged;
  @JsonKey(ignore: true)
  _$$_AvatarPickerCopyWith<_$_AvatarPicker> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_BlockUsersCopyWith<$Res> {
  factory _$$_BlockUsersCopyWith(
          _$_BlockUsers value, $Res Function(_$_BlockUsers) then) =
      __$$_BlockUsersCopyWithImpl<$Res>;
  $Res call({List<User>? blocks, List<User>? initialBlocks});
}

/// @nodoc
class __$$_BlockUsersCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$_BlockUsersCopyWith<$Res> {
  __$$_BlockUsersCopyWithImpl(
      _$_BlockUsers _value, $Res Function(_$_BlockUsers) _then)
      : super(_value, (v) => _then(v as _$_BlockUsers));

  @override
  _$_BlockUsers get _value => super._value as _$_BlockUsers;

  @override
  $Res call({
    Object? blocks = freezed,
    Object? initialBlocks = freezed,
  }) {
    return _then(_$_BlockUsers(
      blocks: blocks == freezed
          ? _value._blocks
          : blocks // ignore: cast_nullable_to_non_nullable
              as List<User>?,
      initialBlocks: initialBlocks == freezed
          ? _value._initialBlocks
          : initialBlocks // ignore: cast_nullable_to_non_nullable
              as List<User>?,
    ));
  }
}

/// @nodoc

class _$_BlockUsers implements _BlockUsers {
  const _$_BlockUsers(
      {required final List<User>? blocks,
      required final List<User>? initialBlocks})
      : _blocks = blocks,
        _initialBlocks = initialBlocks;

  final List<User>? _blocks;
  @override
  List<User>? get blocks {
    final value = _blocks;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  final List<User>? _initialBlocks;
  @override
  List<User>? get initialBlocks {
    final value = _initialBlocks;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'SettingsState.blackList(blocks: $blocks, initialBlocks: $initialBlocks)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BlockUsers &&
            const DeepCollectionEquality().equals(other._blocks, _blocks) &&
            const DeepCollectionEquality()
                .equals(other._initialBlocks, _initialBlocks));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(_blocks),
      const DeepCollectionEquality().hash(_initialBlocks));

  @JsonKey(ignore: true)
  @override
  _$$_BlockUsersCopyWith<_$_BlockUsers> get copyWith =>
      __$$_BlockUsersCopyWithImpl<_$_BlockUsers>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) {
    return blackList(blocks, initialBlocks);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) {
    return blackList?.call(blocks, initialBlocks);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (blackList != null) {
      return blackList(blocks, initialBlocks);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) {
    return blackList(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) {
    return blackList?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (blackList != null) {
      return blackList(this);
    }
    return orElse();
  }
}

abstract class _BlockUsers implements SettingsState {
  const factory _BlockUsers(
      {required final List<User>? blocks,
      required final List<User>? initialBlocks}) = _$_BlockUsers;

  List<User>? get blocks;
  List<User>? get initialBlocks;
  @JsonKey(ignore: true)
  _$$_BlockUsersCopyWith<_$_BlockUsers> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SlowmodeSettingsCopyWith<$Res> {
  factory _$$_SlowmodeSettingsCopyWith(
          _$_SlowmodeSettings value, $Res Function(_$_SlowmodeSettings) then) =
      __$$_SlowmodeSettingsCopyWithImpl<$Res>;
  $Res call({Room? room});
}

/// @nodoc
class __$$_SlowmodeSettingsCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$_SlowmodeSettingsCopyWith<$Res> {
  __$$_SlowmodeSettingsCopyWithImpl(
      _$_SlowmodeSettings _value, $Res Function(_$_SlowmodeSettings) _then)
      : super(_value, (v) => _then(v as _$_SlowmodeSettings));

  @override
  _$_SlowmodeSettings get _value => super._value as _$_SlowmodeSettings;

  @override
  $Res call({
    Object? room = freezed,
  }) {
    return _then(_$_SlowmodeSettings(
      room: room == freezed
          ? _value.room
          : room // ignore: cast_nullable_to_non_nullable
              as Room?,
    ));
  }
}

/// @nodoc

class _$_SlowmodeSettings implements _SlowmodeSettings {
  const _$_SlowmodeSettings({required this.room});

  @override
  final Room? room;

  @override
  String toString() {
    return 'SettingsState.slowmodeSettings(room: $room)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SlowmodeSettings &&
            const DeepCollectionEquality().equals(other.room, room));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(room));

  @JsonKey(ignore: true)
  @override
  _$$_SlowmodeSettingsCopyWith<_$_SlowmodeSettings> get copyWith =>
      __$$_SlowmodeSettingsCopyWithImpl<_$_SlowmodeSettings>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) {
    return slowmodeSettings(room);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) {
    return slowmodeSettings?.call(room);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (slowmodeSettings != null) {
      return slowmodeSettings(room);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) {
    return slowmodeSettings(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) {
    return slowmodeSettings?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (slowmodeSettings != null) {
      return slowmodeSettings(this);
    }
    return orElse();
  }
}

abstract class _SlowmodeSettings implements SettingsState {
  const factory _SlowmodeSettings({required final Room? room}) =
      _$_SlowmodeSettings;

  Room? get room;
  @JsonKey(ignore: true)
  _$$_SlowmodeSettingsCopyWith<_$_SlowmodeSettings> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$_SlowmodeDelayPickerCopyWith<$Res> {
  factory _$$_SlowmodeDelayPickerCopyWith(_$_SlowmodeDelayPicker value,
          $Res Function(_$_SlowmodeDelayPicker) then) =
      __$$_SlowmodeDelayPickerCopyWithImpl<$Res>;
  $Res call({int slowmodeDelay});
}

/// @nodoc
class __$$_SlowmodeDelayPickerCopyWithImpl<$Res>
    extends _$SettingsStateCopyWithImpl<$Res>
    implements _$$_SlowmodeDelayPickerCopyWith<$Res> {
  __$$_SlowmodeDelayPickerCopyWithImpl(_$_SlowmodeDelayPicker _value,
      $Res Function(_$_SlowmodeDelayPicker) _then)
      : super(_value, (v) => _then(v as _$_SlowmodeDelayPicker));

  @override
  _$_SlowmodeDelayPicker get _value => super._value as _$_SlowmodeDelayPicker;

  @override
  $Res call({
    Object? slowmodeDelay = freezed,
  }) {
    return _then(_$_SlowmodeDelayPicker(
      slowmodeDelay: slowmodeDelay == freezed
          ? _value.slowmodeDelay
          : slowmodeDelay // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$_SlowmodeDelayPicker implements _SlowmodeDelayPicker {
  const _$_SlowmodeDelayPicker({required this.slowmodeDelay});

  @override
  final int slowmodeDelay;

  @override
  String toString() {
    return 'SettingsState.slowmodeDelayPicker(slowmodeDelay: $slowmodeDelay)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_SlowmodeDelayPicker &&
            const DeepCollectionEquality()
                .equals(other.slowmodeDelay, slowmodeDelay));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(slowmodeDelay));

  @JsonKey(ignore: true)
  @override
  _$$_SlowmodeDelayPickerCopyWith<_$_SlowmodeDelayPicker> get copyWith =>
      __$$_SlowmodeDelayPickerCopyWithImpl<_$_SlowmodeDelayPicker>(
          this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() loading,
    required TResult Function(List<User>? blocks) settings,
    required TResult Function(User? user, String userNameInput, Talker talker)
        profile,
    required TResult Function(
            User? user, String? errorDescription, String userNameInput)
        userName,
    required TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)
        avatarPicker,
    required TResult Function(List<User>? blocks, List<User>? initialBlocks)
        blackList,
    required TResult Function(Room? room) slowmodeSettings,
    required TResult Function(int slowmodeDelay) slowmodeDelayPicker,
  }) {
    return slowmodeDelayPicker(slowmodeDelay);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
  }) {
    return slowmodeDelayPicker?.call(slowmodeDelay);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? loading,
    TResult Function(List<User>? blocks)? settings,
    TResult Function(User? user, String userNameInput, Talker talker)? profile,
    TResult Function(
            User? user, String? errorDescription, String userNameInput)?
        userName,
    TResult Function(
            List<Avatar> avatars, Avatar selectedAvatar, bool isAvatarChanged)?
        avatarPicker,
    TResult Function(List<User>? blocks, List<User>? initialBlocks)? blackList,
    TResult Function(Room? room)? slowmodeSettings,
    TResult Function(int slowmodeDelay)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (slowmodeDelayPicker != null) {
      return slowmodeDelayPicker(slowmodeDelay);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(_Loading value) loading,
    required TResult Function(_Settings value) settings,
    required TResult Function(_Profile value) profile,
    required TResult Function(_UserName value) userName,
    required TResult Function(_AvatarPicker value) avatarPicker,
    required TResult Function(_BlockUsers value) blackList,
    required TResult Function(_SlowmodeSettings value) slowmodeSettings,
    required TResult Function(_SlowmodeDelayPicker value) slowmodeDelayPicker,
  }) {
    return slowmodeDelayPicker(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
  }) {
    return slowmodeDelayPicker?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(_Loading value)? loading,
    TResult Function(_Settings value)? settings,
    TResult Function(_Profile value)? profile,
    TResult Function(_UserName value)? userName,
    TResult Function(_AvatarPicker value)? avatarPicker,
    TResult Function(_BlockUsers value)? blackList,
    TResult Function(_SlowmodeSettings value)? slowmodeSettings,
    TResult Function(_SlowmodeDelayPicker value)? slowmodeDelayPicker,
    required TResult orElse(),
  }) {
    if (slowmodeDelayPicker != null) {
      return slowmodeDelayPicker(this);
    }
    return orElse();
  }
}

abstract class _SlowmodeDelayPicker implements SettingsState {
  const factory _SlowmodeDelayPicker({required final int slowmodeDelay}) =
      _$_SlowmodeDelayPicker;

  int get slowmodeDelay;
  @JsonKey(ignore: true)
  _$$_SlowmodeDelayPickerCopyWith<_$_SlowmodeDelayPicker> get copyWith =>
      throw _privateConstructorUsedError;
}
