import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:watchers_widget/src/core/base/bloc_injectable_state.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/core/utils/transitions.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/vip_badge.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/common/widgets/avatar_picker_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/contribution_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/inputs/fake_user_name_input.dart';
import 'package:watchers_widget/src/features/common/widgets/inputs/user_name_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/loading_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/modal_title_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/modal_widget.dart';
import 'package:watchers_widget/src/features/common/widgets/submit_button.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';
import 'package:watchers_widget/src/features/common/widgets/user_list_tile.dart';
import 'package:watchers_widget/src/features/settings/presentation/screens/chat_rules_screen.dart';

import 'logic/settings_bloc.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({required this.talker, required this.externalRoomId});

  final Talker talker;
  final String externalRoomId;

  static Route route({required Talker talker, required String externalRoomId}) =>
      Transitions.buildFadeTransition(SettingsScreen(
        talker: talker,
        externalRoomId: externalRoomId,
      ));

  @override
  State<SettingsScreen> createState() => _SettingsScreenState(talker, externalRoomId);
}

class _SettingsScreenState
    extends BlocInjectableState<SettingsScreen, SettingsBloc, SettingsEvent, SettingsState> {
  _SettingsScreenState(Talker talker, String externalRoomId)
      : super.withParams(param1: talker, param2: externalRoomId);

  @override
  Widget builder(BuildContext context, SettingsState state) {
    return Scaffold(
      backgroundColor: CustomColors.chatBackground,
      body: SafeArea(
        child: Align(
          alignment: Alignment.bottomCenter,
          child: state.map(
            loading: (value) {
              return ModalWidget(
                title: ModalTitleWidget(
                  titleText: 'Профиль',
                  onBackTap: () {},
                ),
                isEmpty: true,
                emptyWidget: const LoadingWidget(),
              );
            },
            settings: (state) {
              return ModalWidget(
                title: ModalTitleWidget(
                  titleText: 'Профиль',
                  onBackTap: () {
                    Navigator.maybePop(context);
                  },
                ),
                childrenMainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(16.fw, 16.fh, 0.fw, 8.fh),
                        child: SizedBox(
                          width: double.infinity,
                          child: Text(
                            'Персональные настройки',
                            style: TextStyles.title(fontSize: 15),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: CustomColors.secondary,
                        ),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            InkWell(
                              onTap: () => bloc.add(const SettingsEvent.toProfile()),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: const [
                                    Text(
                                      'Профиль',
                                      style: TextStyles.setting,
                                    ),
                                    SvgIcon(
                                      Resources.chevron,
                                      color: CustomColors.textTertiary,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            const Padding(
                              padding: EdgeInsets.only(left: 16.0),
                              child: Divider(
                                height: 1,
                                thickness: 1,
                                color: CustomColors.divider,
                              ),
                            ),
                            InkWell(
                              onTap: (state.blocks != null && state.blocks!.isNotEmpty)
                                  ? () {
                                      bloc.add(const SettingsEvent.toBlackList());
                                    }
                                  : () {},
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Заблокированные',
                                      style: (state.blocks == null || state.blocks!.isEmpty)
                                          ? TextStyles.title(
                                              fontSize: 15,
                                              fontWeight: FontWeight.w400,
                                              color: CustomColors.textSecondary.withOpacity(0.5),
                                            )
                                          : TextStyles.setting,
                                    ),
                                    const Spacer(),
                                    if (state.blocks != null)
                                      Text(
                                        state.blocks!.isEmpty
                                            ? ''
                                            : state.blocks!.length.toString(),
                                        style: TextStyles.contribution,
                                      ),
                                    const SizedBox(width: 16),
                                    SvgIcon(
                                      Resources.chevron,
                                      color: (state.blocks == null || state.blocks!.isEmpty)
                                          ? CustomColors.textTertiary.withOpacity(0.5)
                                          : CustomColors.textTertiary,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      if (widget.talker.isModerOrAdmin) ...[
                        Padding(
                          padding: EdgeInsets.fromLTRB(16.fw, 16.fh, 0.fw, 8.fh),
                          child: SizedBox(
                            width: double.infinity,
                            child: Text(
                              'Настройки комнаты',
                              style: TextStyles.title(fontSize: 15),
                              textAlign: TextAlign.left,
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(8),
                            color: CustomColors.secondary,
                          ),
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              InkWell(
                                onTap: () => bloc.add(const SettingsEvent.toSlowmodeSettings()),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Медленный режим',
                                        style: TextStyles.setting,
                                      ),
                                      const SvgIcon(
                                        Resources.chevron,
                                        color: CustomColors.textTertiary,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              // const Padding(
                              //   padding: EdgeInsets.only(left: 16.0),
                              //   child: Divider(
                              //     height: 1,
                              //     thickness: 1,
                              //     color: CustomColors.divider,
                              //   ),
                              // ),
                            ],
                          ),
                        ),
                      ],
                    ],
                  ),
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      TextButton(
                        style: TextButton.styleFrom(
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                        onPressed: () async {
                          final chatRules = await bloc.getChatRules();
                          Navigator.of(context)
                              .push(ChatRulesScreen.route(chatRulesHtml: chatRules));
                        },
                        child: Text(
                          'Правила чата',
                          style: TextStyles.secondary(),
                        ),
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                        ),
                        onPressed: () =>
                            bloc.deleteUser(SettingsEvent.deleteUser(context: context)),
                        child: Text(
                          'Удалить профиль',
                          style: TextStyles.secondary(),
                        ),
                      ),
                      const SizedBox(height: 12),
                      const ContributionWidget(),
                    ],
                  ),
                ],
              );
            },
            profile: (state) {
              final user = state.user;

              if (user == null) {
                return ModalWidget(
                  title: ModalTitleWidget(
                    titleText: 'Профиль',
                    onBackTap: () => bloc.add(const SettingsEvent.backToSettings()),
                  ),
                  isEmpty: true,
                  emptyWidget: const LoadingWidget(),
                );
              }

              return ModalWidget(
                title: ModalTitleWidget(
                  titleText: 'Профиль',
                  onBackTap: () => bloc.add(const SettingsEvent.backToSettings()),
                ),
                children: [
                  SizedBox(height: 16.fh),
                  Stack(
                    alignment: AlignmentDirectional.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ClipOval(
                          child: CircleAvatar(
                            radius: 33.fw,
                            child: UniversalPicture(
                              user.pic,
                            ),
                          ),
                        ),
                      ),
                      // Todo(dartloli): get status from backend
                      if (user.statusName != null)
                        Positioned(
                          bottom: 0,
                          child: VipBadgeWidget(
                            statusName: user.statusName!,
                            isSettings: true,
                          ),
                        ),
                    ],
                  ),
                  TextButton(
                    onPressed: () => bloc.add(
                      const SettingsEvent.toAvatarPicker(),
                    ),
                    child: const Text(
                      'Выбрать другой аватар',
                      style: TextStyles.highlighted,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: [
                        Text(
                          'Имя в чате',
                          style: TextStyles.secondary(),
                        ),
                      ],
                    ),
                  ),
                  FakeUserNameInput(
                    onEditTap: () => bloc.editName(const SettingsEventEditName()),
                    userNameFocusNode: bloc.userNameFocusNode,
                    textEditingController: bloc.userNameTextEditingController,
                    onFieldSubmitted: (_) => bloc.add(const SettingsEvent.validateInput()),
                    onChange: (_) => bloc.add(const SettingsEvent.validateInput()),
                    userName: user.name,
                    errorDescription: null,
                    readOnly: true,
                    isModer: state.talker.isModerOrAdmin,
                    isInvitedGuest: user.isInvitedGuest,
                  ),
                ],
              );
            },
            userName: (state) {
              return ModalWidget(
                title: ModalTitleWidget(
                  titleText: 'Имя в чате',
                  onBackTap: () => bloc.add(const SettingsEvent.toProfile()),
                ),
                submitButton: SubmitButton.textual(
                  text: 'Сохранить и продолжить',
                  isActive: state.isSubmitActive,
                  onTap: () => bloc.add(const SettingsEvent.submitInput()),
                ),
                children: [
                  UserNameWidget(
                    userNameFocusNode: bloc.userNameFocusNode,
                    onFieldSubmitted: (_) => bloc.add(const SettingsEvent.validateInput()),
                    onChange: (_) => bloc.add(const SettingsEvent.validateInput()),
                    textEditingController: bloc.userNameTextEditingController,
                    userName: state.userNameInput,
                    errorDescription: state.errorDescription,
                  ),
                ],
              );
            },
            avatarPicker: (state) {
              return AvatarPickerWidget(
                avatars: state.avatars,
                onBackTap: () => bloc.add(const SettingsEvent.toProfile()),
                onSelectAvatar: (avatar) => bloc.add(
                  SettingsEvent.selectAvatar(
                    selectedAvatar: avatar,
                    avatars: state.avatars,
                  ),
                ),
                onSubmit: () => bloc.add(SettingsEvent.submitAvatar(
                  selectedAvatar: state.selectedAvatar,
                )),
                selectedAvatar: state.selectedAvatar,
                isSubmitActive: state.isAvatarChanged,
              );
            },
            blackList: (state) {
              final blocks = state.blocks;
              final initialBlocks = state.initialBlocks;

              return ModalWidget(
                title: ModalTitleWidget(
                  onBackTap: () => bloc.add(const SettingsEvent.backToSettings()),
                  titleText: 'Заблокированные',
                ),
                emptyWidget: initialBlocks == null
                    ? const LoadingWidget()
                    : Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const SvgIcon(
                            Resources.no_blocks,
                            height: 92,
                            width: 103,
                          ),
                          const SizedBox(
                            height: 24,
                          ),
                          Text(
                            'Нет заблокированных пользователей',
                            style: TextStyles.title(fontSize: 15),
                            textAlign: TextAlign.left,
                          )
                        ],
                      ),
                isEmpty: initialBlocks == null || initialBlocks.isEmpty,
                children: [
                  SizedBox(height: 16.fh),
                  if (initialBlocks != null)
                    Expanded(
                        child: ListView.builder(
                      physics: const ClampingScrollPhysics(),
                      shrinkWrap: true,
                      itemCount: initialBlocks.length,
                      itemBuilder: (context, index) {
                        final block = initialBlocks[index];
                        return UserListTile(
                          user: block,
                          isInBlackList: true,
                          isBlocked: blocks?.contains(block),
                          onBlock: () { 
                            bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_UblockCanceled');
                            bloc.add(SettingsEvent.block(
                            block: block,
                          ));},
                          onUnBlock: () { 
                            bloc.sendAmplitudeEvent(eventName: 'Social_ButtonClick_UblockConfirmed');
                            bloc.add(SettingsEvent.unblock(
                            block: block,
                          ));},
                          onTap: () => bloc.add(SettingsEvent.unblock(
                            block: block,
                          )),
                        );
                      },
                    )),
                ],
              );
            },
            slowmodeSettings: (state) {
              final room = state.room;

              if (room == null) {
                return ModalWidget(
                  title: ModalTitleWidget(
                    titleText: 'Медленный режим',
                    onBackTap: () => bloc.add(const SettingsEvent.backToSettings()),
                  ),
                  isEmpty: true,
                  emptyWidget: const LoadingWidget(),
                );
              }

              bool slowModeValue = room.isSlowmode ? true : false;

              return ModalWidget(
                  title: ModalTitleWidget(
                    titleText: 'Медленный режим',
                    onBackTap: () {
                      bloc.add(const SettingsEvent.backToSettings());
                    },
                  ),
                  childrenMainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: CustomColors.secondary,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text(
                                  'Включить медленный режим',
                                  style: TextStyles.setting,
                                ),
                                FlutterSwitch(
                                  width: 46,
                                  height: 28,
                                  toggleSize: 24,
                                  value: slowModeValue,
                                  borderRadius: 34.7,
                                  padding: 2.0,
                                  activeColor: CustomColors.primary,
                                  inactiveColor: CustomColors.contributionTextColor,
                                  activeIcon: const SvgIcon(
                                    Resources.defaultToggleIcon,
                                    boxFit: BoxFit.scaleDown,
                                  ),
                                  inactiveIcon: const SvgIcon(
                                    Resources.defaultToggleIcon,
                                    boxFit: BoxFit.scaleDown,
                                  ),
                                  onToggle: (val) {
                                    bloc.add(SettingsEvent.setSlowmode(
                                        isSlowmode: val, slowModeDelay: bloc.roomDelay));
                                  },
                                ),
                              ],
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 16.0),
                            child: Divider(
                              height: 1,
                              thickness: 1,
                              color: CustomColors.divider,
                            ),
                          ),
                          InkWell(
                            onTap: () {
                              bloc.add(const SettingsEvent.toSlowmodeDelayPicker());
                            },
                            child: Padding(
                              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    'Таймаут отправки',
                                    style: TextStyles.setting,
                                  ),
                                  const Spacer(),
                                  Text(
                                    Resources.slowmodeDelayMap[bloc.roomDelay] ?? '1 секунда',
                                    style: TextStyles.title(
                                        fontSize: 13, color: CustomColors.settingIconColor),
                                  ),
                                  const SizedBox(width: 16),
                                  const SvgIcon(
                                    Resources.chevron,
                                    color: CustomColors.textTertiary,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 21, horizontal: 15),
                      child: Text(
                        'Участники не смогут отправлять сообщения в чат чаще, чем в выбранный промежуток времени.',
                        style: TextStyles.title(
                            fontSize: 13, color: CustomColors.contributionTextColor),
                      ),
                    )
                  ]);
            },
            slowmodeDelayPicker: (state) {
              int delay = state.slowmodeDelay;
              return ModalWidget(
                title: ModalTitleWidget(
                  titleText: 'Таймаут отправки',
                  onBackTap: () => bloc.add(const SettingsEvent.toSlowmodeSettings()),
                ),
                childrenMainAxisAlignment: MainAxisAlignment.start,
                children: [
              Expanded(
              child:Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: CustomColors.secondary,
                      ),
                      child: ListView.separated(
                          padding: EdgeInsets.zero,
                          physics: const AlwaysScrollableScrollPhysics(),
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                return InkWell(
                                  onTap: () {
                                    bloc.add(SettingsEvent.setSlowmode(
                                        isSlowmode: bloc.room!.isSlowmode,
                                        slowModeDelay:
                                            Resources.slowmodeDelayMap.keys.toList()[index]));
                                  },
                                  child: Padding(
                                    padding:
                                        const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
                                    child: Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      children: [
                                        Text(
                                          Resources.slowmodeDelayMap.values.toList()[index],
                                          style: TextStyles.setting,
                                        ),
                                        const Spacer(),
                                        if (Resources.slowmodeDelayMap.keys.toList()[index] == delay)
                                        const SvgIcon(
                                          Resources.check,
                                          color: CustomColors.textTertiary,
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                              separatorBuilder: (context, index) {
                                return const Padding(
                                  padding: EdgeInsets.only(left: 16.0,right: 16.0),
                                  child: Divider(
                                    height: 1,
                                    thickness: 1,
                                    color: CustomColors.divider,
                                  ),
                                );
                              },
                              itemCount: Resources.slowmodeDelayMap.length)))
                ],
              );
            },
          ),
        ),
      ),
    );
  }
}
