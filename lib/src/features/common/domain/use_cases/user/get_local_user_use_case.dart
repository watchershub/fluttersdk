import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/common/data/repositories/user_repository.dart';
import 'package:watchers_widget/src/features/common/domain/models/user.dart';

class GetLocalUserUseCase {
  final IUserRepository _userRepository;

  const GetLocalUserUseCase({
    required IUserRepository userRepository,
  }) : _userRepository = userRepository;

  Future<Result<AuthUser, Exception>> call() async {
    try {
      return Success(await _userRepository.getLocalUser());
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
