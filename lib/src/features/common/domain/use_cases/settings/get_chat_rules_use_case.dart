import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/common/data/settings/i_settings_repository.dart';
import 'package:watchers_widget/src/features/common/models/chat_rule.dart';

class GetChatRulesUseCase {
  final ISettingsRepository _settingsRepository;

  const GetChatRulesUseCase(
      this._settingsRepository,
      );

  Future<Result<ChatRule, Exception>> call() async {
    try {
      final chatRule = await _settingsRepository.getChatRules();
      return Success(chatRule);
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
