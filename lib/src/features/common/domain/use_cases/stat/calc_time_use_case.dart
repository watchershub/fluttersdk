import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/common/data/apis/stat/dtos/stat_calc_time_event_dto.dart';
import 'package:watchers_widget/src/features/common/data/repositories/stat_repository.dart';

class CalcTimeUseCase {
  final IStatRepository _statRepository;

  const CalcTimeUseCase(
    this._statRepository,
  );

  Future<Result<void, Exception>> call({
    required CalcTimeRequest calcTimeDto,
  }) async {
    try {
      final response = await _statRepository.calcGetMessagesTime(calcTimeDto: calcTimeDto);
      return Success(response);
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
