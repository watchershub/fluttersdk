import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/common/data/repositories/talker_repository.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';

class GetRoomAndTalkersUseCase {
  final ITalkerRepository _talkerRepository;

  const GetRoomAndTalkersUseCase(this._talkerRepository);

  Future<Result<List<TalkerAndRoom>, Exception>> call(String externalRoomId) async {
    try {
      return Success(await _talkerRepository.getRoomAndTalkers(externalRoomId));
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}
