import 'package:watchers_widget/src/core/fp/result.dart';
import 'package:watchers_widget/src/features/common/data/repositories/feedback_repository.dart';
import 'package:watchers_widget/src/features/common/models/sound_check.dart';

class PostSoundcheckFeedback {
  final IFeedbackRepository _feedbackRepository;

  const PostSoundcheckFeedback(
      this._feedbackRepository,
      );

  Future<Result<void, Exception>> call({
    required SoundCheck soundCheck,
  }) async {
    try {
      final response = await _feedbackRepository.sendFeedback(soundCheck: soundCheck);
      return Success(response);
    } on Exception catch (error, stackTrace) {
      return Error(error, stackTrace);
    }
  }
}