import 'dart:math' as math;

import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/popup_menu_item.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/talker_item_widget.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/common/widgets/talker_action_menu_tile.dart';

class TalkerActionMenu extends StatefulWidget {
  final Talker talker;
  final bool isMe;
  final void Function(Talker talker, bool isBanned) onToggleUserBan;
  final void Function(Talker talker, bool isVisible) onToggleMessagesVisibility;
  final void Function(Talker talker) onMuteTalker;
  final void Function(Talker talker) onRemoveFromSpeakers;
  final bool iAmAdminOrModer;
  final bool isShowActions;
  final bool isSpeaker;
  final bool isMuted;

  const TalkerActionMenu({
    required this.talker,
    required this.isMe,
    required this.onToggleMessagesVisibility,
    required this.onToggleUserBan,
    required this.onMuteTalker,
    required this.onRemoveFromSpeakers,
    required this.iAmAdminOrModer,
    required this.isShowActions,
    required this.isSpeaker,
    required this.isMuted,
  });
  @override
  _TalkerActionMenuState createState() => _TalkerActionMenuState();
}

class _TalkerActionMenuState extends State<TalkerActionMenu> {
  late bool showActions;

  @override
  void initState() {
    showActions = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: (widget.isShowActions)
          ? () {
              setState(() {
                showActions = !showActions;
              });
            }
          : () {},
      child: Column(
        children: [
          TalkerItemWidget(
            talker: widget.talker,
            actions: [
              if (widget.isShowActions) ...[
                SizedBox(width: 8.fw),
                Padding(padding: EdgeInsets.only(right: showActions? 7:0),child:
                Transform.rotate(
                  angle: showActions? -math.pi/2:0,
                  child: const SvgIcon(
                  Resources.chevron,
                  color: CustomColors.textSecondary,
                ),)),
              ],
            ],
          ),
          if (showActions)
            Padding(
              padding: const EdgeInsets.only(left: 53),
              child: _buildActions(),
            ),
        ],
      ),
    );
  }

  Widget _buildDivider() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: const [
        SizedBox(
          height: 8,
        ),
        Divider(
          height: 1.32,
          color: CustomColors.divider,
        ),
        SizedBox(
          height: 8,
        ),
      ],
    );
  }

  Widget _buildActions() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (!widget.isMuted) ...[
          TalkerActionMenuTile(
            title: 'Заглушить участника',
            subtitle: 'Выключить звук микрофона',
            onTap: () => widget.onMuteTalker(widget.talker),
            talker: widget.talker,
            iconPath: Resources.mute_button,
            // type: PopupMenuItemType.,
          ),
          _buildDivider(),
        ],
        if (widget.isSpeaker) ...[
          TalkerActionMenuTile(
            title: 'Убрать из спикеров',
            subtitle: 'Только текстовый чат',
            onTap: () => widget.onRemoveFromSpeakers(widget.talker),
            talker: widget.talker,
            iconPath: Resources.remove_from_speakers,
            type: PopupMenuItemType.destructive,
          ),
          _buildDivider(),
        ],
        if (widget.talker.isSupressed) ...[
          TalkerActionMenuTile(
            title: 'Показать сообщения',
            subtitle: 'Сообщения будут видны всем',
            onTap: () => widget.onToggleMessagesVisibility(widget.talker, true),
            talker: widget.talker,
            iconPath: Resources.show_message,
            type: PopupMenuItemType.positive,
          ),
          _buildDivider(),
        ] else ...[
          TalkerActionMenuTile(
            title: 'Скрыть сообщения',
            subtitle: 'Видно только админу',
            onTap: () => widget.onToggleMessagesVisibility(widget.talker, false),
            talker: widget.talker,
            iconPath: Resources.hide_message,
            type: PopupMenuItemType.destructive,
          ),
          _buildDivider(),
        ],
        if (widget.talker.isBanned)
          TalkerActionMenuTile(
            title: 'Разблокировать',
            subtitle: 'Разблокируйте пользователя',
            onTap: () => widget.onToggleUserBan(widget.talker, false),
            talker: widget.talker,
            iconPath: Resources.show_message,
            type: PopupMenuItemType.positive,
          )
        else
          TalkerActionMenuTile(
            title: 'Заблокировать',
            subtitle: 'Заблокируйте пользователя',
            onTap: () => widget.onToggleUserBan(widget.talker, true),
            talker: widget.talker,
            iconPath: Resources.block,
            type: PopupMenuItemType.destructive,
          ),
        _buildDivider(),
      ],
    );
  }
}
