import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';

class MultiConfirmDialog extends StatelessWidget {
  final String titleText;
  final String? subtitleText;
  final List<String> confirmButtonTexts;
  final String? cancelButtonText;
  final List<void Function()> onConfirms;
  final void Function() onCancel;
  final bool isPositiveDialog;

  const MultiConfirmDialog({
    required this.titleText,
    required this.confirmButtonTexts,
    required this.onConfirms,
    required this.onCancel,
    this.subtitleText,
    this.cancelButtonText,
    this.isPositiveDialog = false,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.symmetric(horizontal: 40.fw, vertical: 38.fh),
      alignment: Alignment.topCenter,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          color: CustomColors.modalBackground,
          padding: EdgeInsets.fromLTRB(11.fh, 0, 11.fw, 0),
          width: 300.fw,
          child: LayoutBuilder(
            builder: (context, constrains) {
              if (constrains.minHeight < 300) {
                return ListView(
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  children: _buildBody(includePadding: true),
                );
              } else {
                return Container(
                    padding: EdgeInsets.only(top: 19.fw),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: _buildBody(),
                    ));
              }
            },
          ),
        ),
      ),
    );
  }

  List<Widget> _buildBody({bool includePadding = false}) {
    return <Widget>[
      if (includePadding)
        SizedBox(
          height: 19.fw,
        ),
      Text(
        titleText,
        style: TextStyles.title(
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
        textAlign: TextAlign.center,
      ),
      if (subtitleText != null) ...[
        SizedBox(height: 7.fh),
        Text(
          subtitleText!,
          style: TextStyles.secondary(),
          textAlign: TextAlign.center,
        ),
      ],
      SizedBox(height: 20.fh),

      ...confirmButtonTexts.map<Widget>((e) => Column(
        children: [
          TextButton(
            onPressed: onConfirms[confirmButtonTexts.indexOf(e)],
            child: Text(
              e,
              style: isPositiveDialog?TextStyles.title(
                fontSize: 16,
                fontWeight: FontWeight.w600,
                color: CustomColors.positive,
              ):TextStyles.errorTextStyle(
                fontSize: 16,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const Divider(color: CustomColors.divider, height: 1, thickness: 1),
        ],
      )).toList(),

      TextButton(
        onPressed: onCancel,
        child: Text(
          cancelButtonText ?? 'Отмена',
          style: TextStyles.highlighted,
        ),
      ),
    ];
  }
}
