import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';

class RatingDialog extends StatefulWidget {
  final String titleText;
  final String? subtitleText;
  final String confirmButtonText;
  final String? cancelButtonText;
  final void Function(int) onConfirm;
  final void Function() onCancel;

  const RatingDialog({
    required this.titleText,
    required this.confirmButtonText,
    required this.onConfirm,
    required this.onCancel,
    this.subtitleText,
    this.cancelButtonText,
  });

  @override
  _RatingDialogState createState() => _RatingDialogState();
}

class _RatingDialogState extends State<RatingDialog> {
  late int rating;

  @override
  void initState() {
    rating = 0;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.symmetric(horizontal: 40.fw, vertical: 38.fh),
      alignment: Alignment.topCenter,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          color: CustomColors.modalBackground,
          padding: EdgeInsets.fromLTRB(11.fh, 0, 11.fw, 0),
          width: 300.fw,
          child: LayoutBuilder(
            builder: (context, constrains) {
              if (constrains.minHeight < 300) {
                return ListView(
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  children: _buildBody(includePadding: true),
                );
              } else {
                return Container(
                    padding: EdgeInsets.only(top: 19.fw),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: _buildBody(),
                    ));
              }
            },
          ),
        ),
      ),
    );
  }

  List<Widget> _buildBody({bool includePadding = false}) {
    return <Widget>[
      if (includePadding)
        SizedBox(
          height: 19.fw,
        ),
      Text(
        widget.titleText,
        style: TextStyles.title(
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
        textAlign: TextAlign.center,
      ),
      if (widget.subtitleText != null) ...[
        SizedBox(height: 7.fh),
        Text(
          widget.subtitleText!,
          style: TextStyles.secondary(),
          textAlign: TextAlign.center,
        ),
      ],
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 20),
              child: RatingBar.builder(
                wrapAlignment: WrapAlignment.center,
                glow: false,
                initialRating: 0,
                minRating: 0,
                direction: Axis.horizontal,
                allowHalfRating: false,
                itemCount: 5,
                unratedColor: CustomColors.primary,
                itemPadding: const EdgeInsets.symmetric(horizontal: 4.0),
                updateOnDrag: true,
                itemBuilder: (context, index) => Icon(
                  rating <= index ? Icons.star_outline : Icons.star,
                  color: CustomColors.primary,
                ),
                onRatingUpdate: (updateRating) {
                  setState(() {
                    rating = updateRating.toInt();
                  });
                },
              )),
        ],
      ),
      TextButton(
        onPressed: rating != 0
            ? () {
                widget.onConfirm(rating);
              }
            : () {},
        child: Text(
          widget.confirmButtonText,
          style: TextStyles.title(
            fontSize: 16,
            fontWeight: FontWeight.w600,
            color: rating != 0 ? CustomColors.primary : CustomColors.gray200,
          ),
        ),
      ),
      TextButton(
        onPressed: widget.onCancel,
        child: Text(
          widget.cancelButtonText ?? 'Отмена',
          style: TextStyles.highlighted,
        ),
      ),
    ];
  }
}
