import 'dart:async';

import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';

class ProgressDialog<T> extends StatefulWidget {
  final String titleText;
  final String? confirmText;
  final void Function()? onConfirm;
  final Future<T> Function() progress;
  final void Function(T) onProgressFinished;

  const ProgressDialog({
    required this.titleText,
    required this.progress,
    required this.onProgressFinished,
    this.confirmText,
    this.onConfirm,
  });

  @override
  _ProgressDialogState<T> createState() => _ProgressDialogState<T>(progress: progress,onProgressFinished: onProgressFinished);
}

class _ProgressDialogState<T> extends State<ProgressDialog> {
  final Future<T> Function() progress;
  final void Function(T) onProgressFinished;

  _ProgressDialogState({required this.progress,required this.onProgressFinished});

  late final Future<T> progressResultFuture;
  late double progressPercentage;
  double? headDisplayingNumber;
  bool stop = false;

  @override
  void initState() {
    progressPercentage = 0;
    progressResultFuture = progress();
    displayPercentage();
    progressResultFuture.then((value) {
      stop = true;
      setState(() {
        headDisplayingNumber = 0.85;
      });
      Future.delayed(const Duration(milliseconds: 200), () {
        setState(() {
          headDisplayingNumber = 1.0;
        });
      });
      Future.delayed(const Duration(milliseconds: 300), () {
        onProgressFinished(value);
      });
    });
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      alignment: Alignment.topCenter,
      insetPadding: EdgeInsets.symmetric(horizontal: 40.fw, vertical: 38.fh),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          color: CustomColors.modalBackground,
          padding: EdgeInsets.fromLTRB(11.fh, 19.fw, 11.fw, 0),
          width: 300.fw,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                widget.titleText,
                style: TextStyles.title(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16,vertical: 20),
                child: LinearPercentIndicator(
                  lineHeight: 8,
                  barRadius: const Radius.circular(8),
                  progressColor: CustomColors.primary,
                  percent: headDisplayingNumber ?? progressPercentage,
                  backgroundColor: CustomColors.onPrimary,
                ),
              ),
              if (widget.onConfirm != null)
                TextButton(
                  onPressed: () {
                    widget.onConfirm!();
                    Navigator.of(context).maybePop();
                  },
                  child: Text(
                    widget.confirmText ?? 'ОК',
                    style: TextStyles.highlighted,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> displayPercentage() async {
      for (int i = 0; i < 700; i++) {
          await Future.delayed(const Duration(milliseconds: 10), () {
            if (!stop && context.mounted) {
              setState(() {
                progressPercentage = i / 1000;
              });
            }
          });
      }
  }
}
