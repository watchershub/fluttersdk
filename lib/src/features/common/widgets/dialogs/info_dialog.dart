import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';

class InfoDialog extends StatelessWidget {
  final String titleText;
  final String? confirmText;
  final String? subtitleText;
  final void Function()? onConfirm;

  const InfoDialog({
    required this.titleText,
    this.subtitleText,
    this.confirmText,
    this.onConfirm,
  });

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      alignment: Alignment.topCenter,
      insetPadding: EdgeInsets.symmetric(horizontal: 40.fw, vertical: 38.fh),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          color: CustomColors.modalBackground,
          padding: EdgeInsets.fromLTRB(11.fh, 19.fw, 11.fw, 0),
          width: 300.fw,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Text(
                titleText,
                style: TextStyles.title(
                  fontSize: 16,
                  fontWeight: FontWeight.w600,
                ),
                textAlign: TextAlign.center,
              ),
              if (subtitleText != null) ...[
                SizedBox(height: 7.fh),
                Text(
                  subtitleText!,
                  style: TextStyles.secondary(),
                  textAlign: TextAlign.center,
                ),
              ],
              const SizedBox(height: 20),
              TextButton(
                onPressed: () {
                  if (onConfirm != null) {
                    onConfirm!();
                  }
                  Navigator.of(context).maybePop();
                },
                child: Text(
                  confirmText ?? 'ОК',
                  style: TextStyles.highlighted,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
