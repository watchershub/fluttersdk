import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class ItemPickDialog extends StatefulWidget {
  final String titleText;
  final String? subtitleText;
  final String confirmButtonText;
  final String? cancelButtonText;
  final List<String> items;
  final void Function(List<String>) onConfirm;
  final void Function() onCancel;

  const ItemPickDialog({
    required this.titleText,
    required this.confirmButtonText,
    required this.onConfirm,
    required this.onCancel,
    required this.items,
    this.subtitleText,
    this.cancelButtonText,
  });

  @override
  _ItemPickDialogState createState() => _ItemPickDialogState();
}

class _ItemPickDialogState extends State<ItemPickDialog> {
  List<String> pickedItems = [];
  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.symmetric(horizontal: 40.fw, vertical: 38.fh),
      alignment: Alignment.topCenter,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8),
        child: Container(
          color: CustomColors.modalBackground,
          padding: EdgeInsets.fromLTRB(11.fh, 0, 11.fw, 0),
          width: 300.fw,
          child: LayoutBuilder(
            builder: (context, constrains) {
              if (constrains.minHeight < 300) {
                return ListView(
                  shrinkWrap: true,
                  physics: const ClampingScrollPhysics(),
                  children: _buildBody(includePadding: true),
                );
              } else {
                return Container(
                    padding: EdgeInsets.only(top: 19.fw),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: _buildBody(),
                    ));
              }
            },
          ),
        ),
      ),
    );
  }

  List<Widget> _buildBody({bool includePadding = false}) {
    return <Widget>[
      if (includePadding)
        SizedBox(
          height: 19.fw,
        ),
      Text(
        widget.titleText,
        style: TextStyles.title(
          fontSize: 16,
          fontWeight: FontWeight.w600,
        ),
        textAlign: TextAlign.center,
      ),
      if (widget.subtitleText != null) ...[
        SizedBox(height: 7.fh),
        Text(
          widget.subtitleText!,
          style: TextStyles.secondary(),
          textAlign: TextAlign.center,
        ),
      ],
      Padding(
          padding: const EdgeInsets.only(top: 20, right: 20, left: 20),
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: widget.items.length,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child: _DialogItem(
                      onTap: (value) {
                        setState(() {
                          if (value) {
                            pickedItems.add(widget.items[index]);
                          } else {
                            pickedItems.remove(widget.items[index]);
                          }
                        });
                      },
                      itemName: widget.items[index]),
                );
              })),
      TextButton(
        onPressed: pickedItems.isNotEmpty
            ? () {
                widget.onConfirm(pickedItems);
              }
            : () {},
        child: Text(
          widget.confirmButtonText,
          style: TextStyles.title(
              fontSize: 16,
              fontWeight: FontWeight.w600,
              color: pickedItems.isNotEmpty ? CustomColors.primary : CustomColors.gray200),
        ),
      ),
      TextButton(
        onPressed: widget.onCancel,
        child: Text(
          widget.cancelButtonText ?? 'Отмена',
          style: TextStyles.highlighted,
        ),
      ),
    ];
  }
}

class _DialogItem extends StatefulWidget {
  final void Function(bool) onTap;
  final String itemName;

  const _DialogItem({required this.onTap, required this.itemName});

  @override
  __DialogItemState createState() => __DialogItemState();
}

class __DialogItemState extends State<_DialogItem> {
  late bool isPicked;

  @override
  void initState() {
    isPicked = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          isPicked = !isPicked;
        });
        widget.onTap(isPicked);
      },
      child: Row(
        children: [
          Container(
            width: 22,
            height: 22,
            decoration: BoxDecoration(
              border: isPicked ? null : Border.all(width: 2, color: CustomColors.primary),
              color: isPicked ? CustomColors.primary : Colors.transparent,
              borderRadius: BorderRadius.circular(4),
            ),
            child:
                isPicked ? const UniversalPicture(Resources.check, 18, 18, BoxFit.scaleDown) : null,
          ),
          const SizedBox(
            width: 8,
          ),
          Flexible(
            child: Text(
              widget.itemName,
              style: TextStyles.subtitle1(color: CustomColors.textMain, fontSize: 14),
            ),
          ),
        ],
      ),
    );
  }
}
