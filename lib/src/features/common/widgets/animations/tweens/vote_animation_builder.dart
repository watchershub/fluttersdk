import 'package:flutter/material.dart';

abstract class VoteAnimationBuilder {
  final List<double> _weights = [8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 8, 4];

  List<double> get opacity;
  List<double> get top;
  List<double> get translateX;
  List<double> get scale;

  Animation<double> opacityAnimation(AnimationController controller) =>
      _buildTweenSequenceFromList(opacity).animate(controller);

  Animation<double> topAnimation(AnimationController controller) =>
      _buildTweenSequenceFromList(top).animate(controller);

  Animation<double> translateXAnimation(AnimationController controller) =>
      _buildTweenSequenceFromList(translateX).animate(controller);

  Animation<double> scaleAnimation(AnimationController controller) =>
      _buildTweenSequenceFromList(scale).animate(controller);

  TweenSequence<double> _buildTweenSequenceFromList(List<double> values) {
    assert(values.length - 1 == _weights.length);
    final result = <TweenSequenceItem<double>>[];
    for (int i = 0; i < _weights.length; i++) {
      result.add(TweenSequenceItem<double>(
        tween: Tween<double>(begin: values[i], end: values[i + 1]),
        weight: _weights[i],
      ));
    }

    return TweenSequence<double>(result);
  }
}

class VoteAnim1 extends VoteAnimationBuilder {
  @override
  List<double> get opacity =>
      <double>[0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5, 0.45, 0.4, 0];

  @override
  List<double> get top => <double>[0, 4, 8, 10, 10, 6, 4, 2, 0, 0, 0, 0, 0, 0];

  @override
  List<double> get scale =>
      <double>[1, 1, 0.89, 0.82, 0.71, 0.71, 0.64, 0.57, 0.54, 0.43, 0.36, 0.36, 0.32, 0.32];

  @override
  List<double> get translateX =>
      <double>[0, -4, -13, -23, -34, -41, -48, -56, -63, -72, -78, -83, -89, -92];
}

class VoteAnim2 extends VoteAnimationBuilder {
  @override
  List<double> get opacity =>
      [0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5, 0.45, 0.4, 0];

  @override
  List<double> get scale =>
      [1, 1, 0.89, 0.82, 0.71, 0.71, 0.64, 0.57, 0.54, 0.43, 0.36, 0.36, 0.32, 0.32];

  @override
  List<double> get top => [0, 4, 8, 10, 13, 15, 19, 22, 26, 29, 32, 34, 26, 38];

  @override
  List<double> get translateX =>
      [0, -5, -13, -23, -34, -41, -48, -56, -63, -72, -78, -83, -89, -92];
}

class VoteAnim3 extends VoteAnimationBuilder {
  @override
  List<double> get opacity =>
      [0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5, 0.45, 0.4, 0];

  @override
  List<double> get scale =>
      [1, 1, 0.89, 0.82, 0.71, 0.71, 0.64, 0.57, 0.54, 0.43, 0.36, 0.36, 0.32, 0.32];

  @override
  List<double> get top => [0, 4, 8, 10, 10, 9, 12, 15, 17, 21, 24, 25, 26, 27];

  @override
  List<double> get translateX =>
      [0, -5, -13, -23, -34, -41, -48, -56, -63, -72, -78, -83, -89, -92];
}

class VoteAnim4 extends VoteAnimationBuilder {
  @override
  List<double> get opacity =>
      [0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5, 0.45, 0.4, 0];

  @override
  List<double> get scale =>
      [1, 1, 0.89, 0.82, 0.71, 0.71, 0.64, 0.57, 0.54, 0.43, 0.36, 0.36, 0.32, 0.32];

  @override
  List<double> get top => [0, 7, 16, 21, 27, 25, 24, 23, 21, 20, 19, 17, 19, 21];

  @override
  List<double> get translateX =>
      [0, -5, -13, -23, -34, -41, -48, -56, -63, -72, -78, -83, -89, -92];
}

class VoteAnim5 extends VoteAnimationBuilder {
  @override
  List<double> get opacity =>
      [0, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6, 0.55, 0.5, 0.45, 0.4, 0];

  @override
  List<double> get scale =>
      [1, 1, 0.89, 0.82, 0.71, 0.71, 0.64, 0.57, 0.54, 0.43, 0.36, 0.36, 0.32, 0.32];

  @override
  List<double> get top => [0, 3, 7, 6, 5, 2, 2, 4, 7, 11, 15, 17, 20, 20];

  @override
  List<double> get translateX =>
      [0, -5, -13, -23, -34, -41, -48, -56, -63, -72, -78, -83, -89, -92];
}
