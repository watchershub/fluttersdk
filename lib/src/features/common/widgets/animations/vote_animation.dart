import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/constants.dart';

import 'tweens/vote_animation_builder.dart';

class VoteAnimation extends StatefulWidget {
  final Widget child;
  final String animationId;
  final Function(String) onCompleted;
  final VoteAnimationBuilder voteAnimationBuilder;

  const VoteAnimation({
    required this.child,
    required this.animationId,
    required this.onCompleted,
    required this.voteAnimationBuilder,
  });

  @override
  State<VoteAnimation> createState() => _VoteAnimationState();
}

class _VoteAnimationState extends State<VoteAnimation> with SingleTickerProviderStateMixin {
  late final AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(
      vsync: this,
      duration: Constants.voteAnimationDuration,
    )..addStatusListener(
        (status) {
          if (status == AnimationStatus.completed) {
            widget.onCompleted(widget.animationId);
          }
        },
      );

    _startAnimation();

    super.initState();
  }

  void _startAnimation() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _controller
        ..reset()
        ..forward();
    });
  }

  @override
  void didUpdateWidget(covariant VoteAnimation oldWidget) {
    // _startAnimation();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final xOffeset = widget.voteAnimationBuilder.translateXAnimation(_controller);
    final yOffeset = widget.voteAnimationBuilder.topAnimation(_controller);
    final scale = widget.voteAnimationBuilder.scaleAnimation(_controller);
    final opacity = widget.voteAnimationBuilder.opacityAnimation(_controller);

    return AnimatedBuilder(
      animation: _controller,
      builder: (context, child) {
        return Transform.translate(
          offset: Offset(xOffeset.value, yOffeset.value),
          child: Transform.scale(
            scale: scale.value,
            child: Opacity(
              opacity: opacity.value,
              child: widget.child,
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}
