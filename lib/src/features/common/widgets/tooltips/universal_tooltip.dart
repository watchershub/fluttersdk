import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:just_the_tooltip/just_the_tooltip.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';
import 'package:watchers_widget/src/core/utils/text_utils.dart';
import 'package:watchers_widget/src/features/common/widgets/tooltips/tooltip_tail_builder_factory.dart';

class UniversalToolTip extends StatefulWidget {
  final String toolTipText;
  final void Function()? onTimeOutCLose;
  final void Function()? onCloseIconTap;
  final Color? backGroundColor;
  final TooltipTailDirection? tailDirection;

  const UniversalToolTip({
    required this.toolTipText,
    this.onTimeOutCLose,
    this.tailDirection,
    this.backGroundColor,
    this.onCloseIconTap,
  });

  @override
  _UniversalToolTipState createState() => _UniversalToolTipState();
}

class _UniversalToolTipState extends State<UniversalToolTip> {
  final _tooltipController = JustTheController();

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _tooltipController.showTooltip();
      Future.delayed(const Duration(seconds: 3),(){
        if (widget.onTimeOutCLose != null){
          widget.onTimeOutCLose!();
        }
        _tooltipController.hideTooltip();
      });
    });
    super.initState();
  }

  @override
  void dispose (){
    _tooltipController.hideTooltip();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final questionTextStyle = TextStyles.subhead.copyWith(color: CustomColors.onPrimary);

    final textHeight = TextUtils.calcTextHeight(
      text: widget.toolTipText,
      textStyle: questionTextStyle,
      maxWidth: 80.w - 48,
    );

    return JustTheTooltip(
      onDismiss: widget.onTimeOutCLose,
      elevation: 0,
      triggerMode: TooltipTriggerMode.manual,
      isModal: true,
      backgroundColor: CustomColors.primary,
      controller: _tooltipController,
      tailBaseWidth: 20.0,
      preferredDirection: AxisDirection.left,
      borderRadius: BorderRadius.circular(8.0),
      tailLength: 8,
      offset: 6,
      tailBuilder: (point1, point2, point3) => _tailBuilder(point1, point2, point3, textHeight / 2),
      content: Padding(
        padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
        child: ConstrainedBox(
          constraints: BoxConstraints(maxWidth: 80.w),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: Text(
                      widget.toolTipText,
                      textAlign: TextAlign.left,
                      textWidthBasis: TextWidthBasis.longestLine,
                      style: questionTextStyle,
                    ),
                  ),
                  const SizedBox(width: 4),
                  if (widget.onCloseIconTap != null)
                    InkWell(
                      onTap: () {
                        _tooltipController.hideTooltip();
                        widget.onCloseIconTap!();
                      },
                      child: const Padding(
                        padding: EdgeInsets.all(4.0),
                        child: SvgIcon(
                          Resources.close,
                          color: CustomColors.tooltipCloseIcon,
                          size: 10,
                        ),
                      ),
                    ),
                ],
              ),
            ],
          ),
        ),
      ),
      child: const SizedBox.shrink(),
    );
  }

  static Path _tailBuilder(
    Offset tip,
    Offset point2,
    Offset point3,
    double yOffset,
  ) {
    return Path()
      ..moveTo(tip.dx, tip.dy - yOffset)
      ..lineTo(point2.dx, point2.dy - yOffset)
      ..lineTo(point3.dx, point3.dy - yOffset)
      ..close();
  }
}
