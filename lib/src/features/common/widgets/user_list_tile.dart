import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/common/models/user.dart';
import 'package:watchers_widget/src/features/common/widgets/block_button.dart';
import 'package:watchers_widget/src/features/common/widgets/universal_picture.dart';

class UserListTile extends StatelessWidget {
  final User user;
  final void Function() onTap;
  final bool? isInBlackList;
  final void Function()? onBlock;
  final void Function()? onUnBlock;
  final bool? isBlocked;

  const UserListTile({
    required this.user,
    required this.onTap,
    this.isInBlackList,
    this.onBlock,
    this.onUnBlock,
    this.isBlocked,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Flexible(
                child: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                ClipOval(
                  child: UniversalPicture(
                    user.pic,
                    36.fw,
                  ),
                ),
                SizedBox(width: 12.fw),
                Flexible(
                  child: Text(
                    user.name,
                    style: TextStyles.title(fontSize: 15),
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                  ),
                ),
              ],
            )),
            if (isInBlackList == true)
              BlockButton(onBlock: onBlock!, onUnBlock: onUnBlock!, isBlocked: isBlocked!)
            else
              GestureDetector(
                onTap: onTap,
                child: const Icon(
                  Icons.more_vert,
                  color: CustomColors.gray400,
                ),
              ),
          ],
        ),
        SizedBox(
          height: 8.fh,
        ),
        Padding(
            padding: EdgeInsets.only(left: 36.fw),
            child: const Divider(
              indent: 12,
              height: 1,
              thickness: 1,
              color: CustomColors.divider,
            )),
        SizedBox(
          height: 8.fh,
        ),
      ],
    );
  }
}
