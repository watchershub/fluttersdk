import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class UniversalPicture extends StatelessWidget {
  final String url;
  final double? width;
  final double? height;
  final BoxFit? fit;

  const UniversalPicture(this.url, [this.width, this.height, this.fit]);

  @override
  Widget build(BuildContext context) {
    if (url.isEmpty) return const SizedBox.shrink();

    if (url.contains('.svg')) {
      if (url.contains('http')) {
        return SvgPicture.network(
          url,
          width: width,
          height: height,
          fit: fit ?? BoxFit.contain,
        );
      } else {
        return SvgPicture.asset(
          url,
          width: width,
          height: height,
          fit: fit ?? BoxFit.contain,
          package: 'watchers_widget',
        );
      }
    }

    if (!url.contains('http')) {
      return Image.asset(
        url,
        width: width,
        height: height,
        fit: fit,
        package: 'watchers_widget',
      );
    }

    return Image.network(
      url,
      width: width,
      height: height,
      fit: fit,
    );
  }
}
