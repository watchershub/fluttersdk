import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';

class UserNameWidget extends StatelessWidget {
  const UserNameWidget({
    required this.userName,
    required this.errorDescription,
    this.onFieldSubmitted,
    this.onChange,
    required this.textEditingController,
    required this.userNameFocusNode,
    this.enabled = true,
    this.readOnly = false,
    this.onEditTap,
  });

  final String userName;
  final String? errorDescription;
  final void Function(String)? onChange;
  final void Function(String)? onFieldSubmitted;
  final TextEditingController textEditingController;
  final FocusNode userNameFocusNode;
  final bool enabled;
  final bool readOnly;
  final void Function()? onEditTap;

  static final GlobalKey<FormState> _key = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    final border = OutlineInputBorder(
      borderSide: const BorderSide(
        color: CustomColors.inputBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(8.0),
    );

    return TextField(
      maxLength: 25,
      key: _key,
      focusNode: userNameFocusNode,
      enabled: enabled,
      onSubmitted: onFieldSubmitted,
      controller: textEditingController,
      onChanged: onChange,
      style: TextStyles.primary,
      decoration: InputDecoration(
        counterText: '',
        errorText: errorDescription != null && errorDescription!.isEmpty ? null : errorDescription,
        errorMaxLines: 2,
        contentPadding: const EdgeInsets.all(16),
        isDense: true,
        border: border,
        focusedBorder: border,
        enabledBorder: border,
        disabledBorder: border,
        errorBorder: border,
        focusedErrorBorder: border,
        filled: true,
        fillColor: CustomColors.inputFilling,
        hintText: 'Введите имя',
        hintStyle: TextStyles.secondary(),
        suffixIcon: readOnly
            ? Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: InkWell(
                  onTap: onEditTap,
                  child: const Text(
                    'изменить',
                    style: TextStyles.highlighted,
                  ),
                ),
              )
            : null,
        suffixIconConstraints: const BoxConstraints(),
      ),
      readOnly: readOnly,
    );

    //  InkWell(
    //   onTap: onTap,
    //   child: Container(
    //     height: 50,
    //     width: double.infinity,
    //     decoration: BoxDecoration(
    //       border: Border.all(
    //         width: 1,
    //         color: CustomColors.inputBorder,
    //       ),
    //       color: CustomColors.inputFilling,
    //       borderRadius: const BorderRadius.all(Radius.circular(8.0)),
    //     ),
    //     child: Padding(
    //       padding: const EdgeInsets.symmetric(horizontal: 12),
    //       child: Row(
    //         crossAxisAlignment: CrossAxisAlignment.center,
    //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
    //         children: [
    //           if (userName.isEmpty)
    //             Text(
    //               'Введите имя',
    //               style: TextStyles.secondary(),
    //             )
    //           else
    //             Text(
    //               userName,
    //               style: TextStyles.primary,
    //               textAlign: TextAlign.left,
    //             ),
    // if (showChangeTrailing)
    //   const Text(
    //     'изменить',
    //     style: TextStyles.highlighted,
    //   ),
    //         ],
    //       ),
    //     ),
    //   ),
    // );
  }
}
