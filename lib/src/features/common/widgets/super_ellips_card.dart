import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:superellipse_shape/superellipse_shape.dart';

class SuperEllipseCard extends StatelessWidget {
  const SuperEllipseCard({this.elevation = 1.0, this.child, this.side});

  final double elevation;
  final Widget? child;
  final BorderSide? side;

  @override
  Widget build(BuildContext context) {
    return Material(
      clipBehavior: Clip.antiAlias,
      shape: SuperellipseShape(
        side: side ?? BorderSide.none,
        borderRadius: BorderRadius.circular(28.0),
      ),
      elevation: elevation,
      child: child,
    ); // Material
  }
}
