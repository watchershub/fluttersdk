import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';

class BlockButton extends StatelessWidget{
  final Function() onBlock;
  final Function() onUnBlock;
  final bool isBlocked;

  const BlockButton({required this.onBlock,required this.onUnBlock,required this.isBlocked});

  @override
  Widget build(BuildContext context) {
    if (isBlocked){
      return buildUnBlockButton();
    }
    return buildBlockButton();
  }

  Widget buildUnBlockButton(){
    return Container(
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        color: CustomColors.unblockButtonColor
      ),
      child: InkWell(
        splashColor: Colors.transparent,
        onTap: onUnBlock,
        child: Text(
          "Разблокировать",
          style: TextStyles.title(
            fontSize: 12,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }

  Widget buildBlockButton(){
    return Container(
      padding: const EdgeInsets.all(4),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          color: CustomColors.blockButtonColor
      ),
      child: InkWell(
        splashColor: Colors.transparent,
        onTap: onBlock,
        child: Text(
          "Заблокировать",
          style: TextStyles.title(
            fontSize: 12,
            fontWeight: FontWeight.w500,
            color: CustomColors.danger
          ),
        ),
      ),
    );
  }
}