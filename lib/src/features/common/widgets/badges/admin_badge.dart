import 'package:flutter/material.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/constants/text_styles.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class AdminBadge extends StatelessWidget {
  const AdminBadge({super.key,this.showBadge = true});
  final bool showBadge;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        const SvgIcon(
          Resources.admin_check_mark,
          size: 14,
        ),
        const SizedBox(width: 4),
        if (showBadge) const Text(
          'admin',
          style: TextStyles.hint,
        ),
      ],
    );
  }
}
