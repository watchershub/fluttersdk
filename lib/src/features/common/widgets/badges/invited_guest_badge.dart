import 'package:flutter/src/widgets/container.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/core/svg_icon.dart';

class InvitedGuestBadge extends StatelessWidget {
  const InvitedGuestBadge({super.key});

  @override
  Widget build(BuildContext context) {
    return const SvgIcon(
      Resources.admin_check_mark,
      size: 16,
      color: CustomColors.primary,
    );
  }
}
