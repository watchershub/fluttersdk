import 'package:flutter/material.dart';

import 'package:watchers_widget/src/core/constants/custom_colors.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';

class ModalWidget extends StatelessWidget {
  final Widget title;
  final Widget? submitButton;
  final List<Widget> children;
  final Widget? emptyWidget;
  final bool isEmpty;
  final double? height;
  final CrossAxisAlignment crossAxisAlignment;
  final MainAxisAlignment childrenMainAxisAlignment;

  const ModalWidget({
    required this.title,
    this.children = const [],
    this.emptyWidget,
    this.isEmpty = false,
    this.height,
    this.submitButton,
    this.crossAxisAlignment = CrossAxisAlignment.center,
    this.childrenMainAxisAlignment = MainAxisAlignment.start,
  });

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      bottom: false,
      child: Container(
        decoration: const BoxDecoration(
          color: CustomColors.modalBackground,
        ),
        padding: const EdgeInsets.fromLTRB(16, 16, 16, 0),
        child: Column(
          crossAxisAlignment: crossAxisAlignment,
          textDirection: TextDirection.ltr,
          mainAxisSize: MainAxisSize.min,
          children: [
            title,
            Expanded(
              child: LayoutBuilder(
                builder: (context, constraints) {
                  if (constraints.minHeight < 300) {
                    return ListView(
                      shrinkWrap: true,
                      physics: const ClampingScrollPhysics(),
                      children: [
                        SizedBox(height: 16.fh),
                        if (isEmpty && emptyWidget != null)
                          Expanded(
                            child: Center(
                              child: emptyWidget,
                            ),
                          )
                        else
                          ...children,
                        SizedBox(height: 16.fh),
                        if (submitButton != null) submitButton!,
                        SizedBox(height: 21.fh),
                      ],
                    );
                  }

                  return Stack(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(vertical: 16.fh),
                        child: Column(
                          mainAxisAlignment: childrenMainAxisAlignment,
                          children: [
                            if (isEmpty && emptyWidget != null)
                              Expanded(
                                child: Center(
                                  child: emptyWidget,
                                ),
                              )
                            else
                              ...children,
                          ],
                        ),
                      ),
                      if (submitButton != null)
                        Positioned(
                          bottom: 21.fh,
                          left: 0,
                          right: 0,
                          child: submitButton!,
                        ),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
