import 'package:watchers_widget/src/features/common/models/chat_rule.dart';

abstract class ISettingsRepository {
  Future<ChatRule> getChatRules();
}
