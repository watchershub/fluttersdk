import 'package:dio/dio.dart';

class SettingsApi {
  final Dio _dio;

  const SettingsApi(
      this._dio,
      );

  Future<Response> getChatRules() => _dio.get(
    'settings/rules',
  );
}