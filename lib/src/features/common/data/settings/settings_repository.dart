import 'package:watchers_widget/src/features/common/data/settings/i_settings_repository.dart';
import 'package:watchers_widget/src/features/common/data/settings/settings_api.dart';
import 'package:watchers_widget/src/features/common/models/chat_rule.dart';

class SettingsRepository implements ISettingsRepository{

  final SettingsApi _settingsApi;

  const SettingsRepository(this._settingsApi);

  @override
  Future<ChatRule> getChatRules() {
    return _settingsApi.getChatRules().then((response) => ChatRule.fromMap(response.data));
  }

}