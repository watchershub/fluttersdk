import 'package:watchers_widget/src/features/common/data/apis/stat/dtos/stat_calc_time_event_dto.dart';
import 'package:watchers_widget/src/features/common/data/apis/stat/stat_api.dart';

abstract class IStatRepository {
  Future<void> calcGetMessagesTime({required CalcTimeRequest calcTimeDto});
}

class StatRepository implements IStatRepository {
  final StatApi _statApi;

  const StatRepository(
    this._statApi,
  );

  @override
  Future<void> calcGetMessagesTime({required CalcTimeRequest calcTimeDto}) =>
      _statApi.calcTime(calcTimeDto);
}
