import 'dart:convert';

class WordlistResponse {
  final List<String> white;
  final List<String> black;
  final List<String> phrase;
  final List<String> site;
  final List<Autocorrect> autocorrect;

  WordlistResponse({
    required this.white,
    required this.black,
    required this.phrase,
    required this.site,
    required this.autocorrect,
  });

  factory WordlistResponse.fromMap(Map<String, dynamic> map) {
    return WordlistResponse(
      white: _toList(map['white'] ?? ''),
      black: _toList(map['black'] ?? ''),
      phrase: _toList(map['phrase'] ?? ''),
      site: _toList(map['site'] ?? ''),
      autocorrect: List<Autocorrect>.from(map['autocorrect']?.map((x) => Autocorrect.fromMap(x))),
    );
  }

  static List<String> _toList(String input) {
    if (input.isEmpty) return [];
    return input.split(', ');
  }

  factory WordlistResponse.fromJson(String source) => WordlistResponse.fromMap(json.decode(source));
}

class Autocorrect {
  final String from;
  final String to;

  Autocorrect({
    required this.from,
    required this.to,
  });

  factory Autocorrect.fromMap(Map<String, dynamic> map) {
    return Autocorrect(
      from: map['from'] ?? '',
      to: map['to'] ?? '',
    );
  }

  factory Autocorrect.fromJson(String source) => Autocorrect.fromMap(json.decode(source));
}
