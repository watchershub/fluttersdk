import 'package:watchers_widget/src/features/common/data/apis/feedback/feedback_api.dart';
import 'package:watchers_widget/src/features/common/models/sound_check.dart';

abstract class IFeedbackRepository {
  Future<void> sendFeedback({required SoundCheck soundCheck});
}

class FeedbackRepository implements IFeedbackRepository {
  final FeedbackApi _feedbackApi;

  const FeedbackRepository(
      this._feedbackApi,
      );

  @override
  Future<void> sendFeedback({required SoundCheck soundCheck}) => _feedbackApi.postFeedback(soundCheck);
}
