import 'dart:convert';

class UserUpdateDto {
  final String? name;
  final String? pic;
  final bool? isOnboard;
  final int? onboardStage;

  UserUpdateDto({
    this.name,
    this.pic,
  this.isOnboard,
  this.onboardStage,
  });

  Map<String, dynamic> toMap() {
    return {
      if (name != null) 'name': name,
      if (pic != null) 'pic': pic,
      if (isOnboard != null) 'isOnboard': isOnboard,
      if (onboardStage != null) 'onboardStage': onboardStage,
    };
  }

  String toJson() => json.encode(toMap());
}
