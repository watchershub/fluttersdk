import 'dart:convert';

import 'package:watchers_widget/src/features/common/data/apis/stat/dtos/stat_event_dto.dart';

class CalcTimeRequest extends StatEventRequest {
  final String roomId;
  final int userId;

  CalcTimeRequest(
      {required this.roomId,
      required this.userId,
      super.event = 'getMessages',
      required super.value,
      required super.key});

  @override
  Map<String, dynamic> toMap() {
    return {...super.toMap(), 'userId': userId, 'roomId': roomId};
  }

  @override
  String toJson() => json.encode(toMap());
}
