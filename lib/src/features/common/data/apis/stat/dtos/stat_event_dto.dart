import 'dart:convert';

class StatEventRequest {
  final String event;
  final String value;
  final String key;

  StatEventRequest({
    required this.event,
    required this.value,
    required this.key,
  });

  Map<String, dynamic> toMap() {
    return {"event": event, "value": value, "key": key};
  }

  String toJson() => json.encode(toMap());
}
