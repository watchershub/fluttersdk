import 'package:dio/dio.dart';
import 'package:watchers_widget/src/features/common/data/apis/stat/dtos/stat_calc_time_event_dto.dart';

class StatApi {
  final Dio _dio;

  const StatApi(
    this._dio,
  );

  Future<Response> calcTime(CalcTimeRequest calcTimeDto) => _dio.post(
        'stat/event',
        data: calcTimeDto.toJson(),
      );
}
