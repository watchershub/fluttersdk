import 'package:dio/dio.dart';
import 'package:watchers_widget/src/features/common/models/sound_check.dart';

class FeedbackApi {
  final Dio _dio;

  const FeedbackApi(this._dio);

  Future<Response> postFeedback(SoundCheck soundCheck) => _dio.post(
    'feedback',
    data: soundCheck.toJson(),
  );
}