import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GradientBorderSide extends BorderSide {
  const GradientBorderSide(
      {this.color = const Color(0x00000000),
      this.width = 5.0,
      this.style = BorderStyle.solid,
      this.gradient = const LinearGradient(colors: [Colors.green, Colors.yellow])})
      : super(color: color, width: width, style: style);

  final Gradient gradient;
  final Color color;
  final double width;
  final BorderStyle style;

  @override
  Paint toPaint() {
    switch (style) {
      case BorderStyle.solid:
        return Paint()
          ..strokeWidth = width
          ..style = PaintingStyle.stroke
          ..shader = gradient.createShader(Rect.fromCircle(center: Offset.zero, radius: 28));
      case BorderStyle.none:
        return Paint()
          ..color = const Color(0x00000000)
          ..strokeWidth = 0.0
          ..style = PaintingStyle.stroke;
    }
  }
}
