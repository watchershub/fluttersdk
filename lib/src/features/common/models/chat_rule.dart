import 'dart:convert';

import 'package:equatable/equatable.dart';

class ChatRule extends Equatable {
  final String rulesHtml;

  const ChatRule({
    required this.rulesHtml,
  });

  Map<String, dynamic> toMap() {
    return {
      "rulesHtml": rulesHtml,
    };
  }

  factory ChatRule.fromMap(Map<String, dynamic> map) {
    return ChatRule(
      rulesHtml: map['rulesHTML'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory ChatRule.fromJson(String source) => ChatRule.fromMap(json.decode(source));

  ChatRule copyWith({
    String? rulesHtml,
  }) {
    return ChatRule(
      rulesHtml: rulesHtml ?? this.rulesHtml,
    );
  }

  @override
  List<Object?> get props => [rulesHtml];
}
