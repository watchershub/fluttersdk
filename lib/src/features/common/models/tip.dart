import 'dart:convert';

import 'package:equatable/equatable.dart';

class Tip extends Equatable {
  final bool alreadyShown;
  final String name;
  final String text;


  const Tip(
      {
        required this.alreadyShown,
        required this.name,
        required this.text,
      });


  Map<String, dynamic> toMap() {
    return {
      'alreadyShown' : alreadyShown,
      'name' : name,
      'text' : text,
    };
  }

  factory Tip.fromMap(Map<String, dynamic> map) {
    return Tip(
      alreadyShown: map['shouldShow'] ?? false,
        name: map['name'] ?? '',
        text: map['text'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Tip.fromJson(String source) => Tip.fromMap(json.decode(source));

  Tip copyWith({
    bool? alreadyShown,
    String? name,
    String? text,
  }) {
    return Tip(
      alreadyShown: alreadyShown ?? this.alreadyShown,
      name: name ?? this.name,
      text: text ?? this. text,
    );
  }

  @override
  List<Object?> get props => [
    alreadyShown,
    name,
    text,
  ];
}
