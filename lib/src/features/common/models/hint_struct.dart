import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:watchers_widget/src/features/common/models/tip.dart';

class HintStruct extends Equatable {
  final bool shouldShowTips;
  final List<Tip> tips;

  const HintStruct({
    required this.shouldShowTips,
    required this.tips,
  });

  Map<String, dynamic> toMap() {
    return {
      'shouldShowTips': shouldShowTips,
      'tips': tips.map<Map<String, dynamic>>((e) => e.toMap()).toList(),
    };
  }

  factory HintStruct.fromMap(Map<String, dynamic> map) {
    return HintStruct(
        shouldShowTips: map['shouldShowTips'] ?? false,
        tips: map['tips'] != null ? map['tips'].map<Tip>((e) => Tip.fromMap(e)).toList() : null);
  }

  String toJson() => json.encode(toMap());

  factory HintStruct.fromJson(String source) => HintStruct.fromMap(json.decode(source));

  HintStruct copyWith({
    bool? shouldShowTips,
    List<Tip>? tips,
  }) {
    return HintStruct(
      shouldShowTips: shouldShowTips ?? this.shouldShowTips,
      tips: tips ?? this.tips,
    );
  }

  @override
  List<Object?> get props => [
        shouldShowTips,
        tips,
      ];
}
