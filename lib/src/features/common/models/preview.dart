import 'dart:convert';

import 'package:equatable/equatable.dart';

class Preview extends Equatable {
  final String title;
  final String description;
  final String? image;
  final String siteName;
  final String hostName;
  final String url;

  const Preview({
    required this.title,
    required this.description,
    required this.siteName,
    required this.hostName,
    this.image,
    required this.url,
  });

  Map<String, dynamic> toMap() {
    return {
      "title": title,
      "description": description,
      "siteName": siteName,
      "hostName": hostName,
      "image": image,
      "url": url,
    };
  }

  factory Preview.fromMap(Map<String, dynamic> map) {
    return Preview(
      title: map['title'] ?? '',
      description: map['description'] ?? '',
      siteName: map['siteName'] ?? '',
      hostName: map['hostname'] ?? '',
      image: map['image'],
      url: map['url'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Preview.fromJson(String source) => Preview.fromMap(json.decode(source));

  Preview copyWith({
    String? title,
    String? description,
    String? image,
    String? siteName,
    String? hostName,
    String? url,
  }) {
    return Preview(
      title: title ?? this.title,
      description: description ?? this.description,
      siteName: siteName ?? this.siteName,
      hostName: hostName ?? this.hostName,
      image: image ?? this.image,
      url: url ?? this.url,
    );
  }

  @override
  List<Object?> get props => [title, description, siteName, hostName, image, url];
}
