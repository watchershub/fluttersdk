import 'dart:convert';

import 'package:watchers_widget/src/features/common/models/talker.dart';

class Ban {
  final int id;
  final DateTime? expires;
  final String type;
  final String? description;
  final int reason;
  final Talker talker;
  final String externalRoomId;
  final int creatorId;
  final DateTime? deletedAt;
  final DateTime createdAt;

  Ban({
    required this.id,
    this.expires,
    required this.type,
    this.description,
    required this.reason,
    required this.talker,
    required this.externalRoomId,
    required this.creatorId,
    this.deletedAt,
    required this.createdAt,
  });

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'expires': expires?.toIso8601String(),
      'type': type,
      'description': description,
      'reason': reason,
      'talker': talker,
      'room': {'room':{'externalRoomId':externalRoomId}},
      'creatorId': creatorId,
      'deletedAt': deletedAt?.toIso8601String(),
      'createdAt': createdAt.toIso8601String(),
    };
  }

  factory Ban.fromMap(Map<String, dynamic> map) {
    return Ban(
        id: map['id']?.toInt() ?? 0,
        type: map['type'] ?? '',
        reason: map['reason']?.toInt() ?? 0,
        talker: Talker.empty().copyWith(id:map['talker']?['id']?.toInt() ?? 0),
        externalRoomId: map['room']?['externalRoomId']?? '',
        description: map['description'],
        creatorId: map['creatorId']?.toInt() ?? 0,
        createdAt: DateTime.parse(map['createdAt']),
        deletedAt: map['deletedAt'] == null ? null : DateTime.parse(map['deletedAt']),
        expires: map['expires'] == null ? null : DateTime.parse(map['expires']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Ban.fromJson(String source) => Ban.fromMap(json.decode(source));

  Ban copyWith({
     int? id,
     DateTime? expires,
     String? type,
     String? description,
     int? reason,
     Talker? talker,
     String? externalRoomId,
     int? creatorId,
     DateTime? deletedAt,
     DateTime? createdAt,
  }) {
    return Ban(
      id: id ?? this.id,
      type: type ?? this.type,
      reason: reason ?? this.reason,
      talker: talker ?? this.talker,
      externalRoomId: externalRoomId ?? this.externalRoomId,
      description: description ?? this.description,
      creatorId: creatorId ?? this.creatorId,
      createdAt: createdAt ?? this.createdAt,
      deletedAt: deletedAt ?? this.deletedAt,
      expires: expires ?? this.expires,
    );
  }
}
