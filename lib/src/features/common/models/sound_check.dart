import 'dart:convert';

class SoundCheck {
  final String? osName;
  final String? osVersion;
  final String? deviceType;
  final List<String>? issue;
  final int connectRate;
  final String? speed;
  final String? comment;
  final String externalRoomId;

  SoundCheck({
    required this.connectRate,
    required this.externalRoomId,
    this.osName,
    this.osVersion,
    this.deviceType,
    this.issue,
    this.speed,
    this.comment,
  });

  Map<String, dynamic> toMap() {
    String issueString = '';
    if (issue != null) {
      for (String element in issue!) {
        String enumValue = '';
        switch (element){
          case 'Ничего не было слышно':
            enumValue = "NOTHEAR";
            break;
          case 'Звук прерывался':
            enumValue = 'CONNECTION';
            break;
          case 'Не получилось воспользоваться микрофоном':
            enumValue = "MICROPHONE";
            break;
        }
        issueString = issueString + enumValue;
        if (issue!.last != element) {
          issueString = issueString + ', ';
        }
      }
    }
    return {
      'info': jsonEncode({
        if (osName != null) "osName": osName,
        if (osVersion != null) "osVersion": osVersion,
        if (deviceType != null) "deviceType": deviceType,
        if (speed != null) "speed": speed,
      }),
      if (issue != null) "issue": issueString,
      if (comment != null) "comment": comment,
      "rating": connectRate,
      "externalRoomId" : externalRoomId,
    };
  }

  factory SoundCheck.empty() {
    return SoundCheck(
      connectRate: 0,
      externalRoomId: '',
    );
  }

  String toJson() => json.encode(toMap());

  SoundCheck copyWith({
    int? connectRate,
    String? osName,
    String? osVersion,
    String? deviceType,
    String? speed,
    List<String>? issue,
    String? comment,
    String? externalRoomId,
  }) {
    return SoundCheck(
      connectRate: connectRate ?? this.connectRate,
      externalRoomId: externalRoomId ?? this.externalRoomId,
      osName: osName ?? this.osName,
      osVersion: osVersion ?? this.osVersion,
      deviceType: deviceType ?? this.deviceType,
      speed: speed ?? this.speed,
      issue: issue ?? this.issue,
      comment: comment ?? this.comment,
    );
  }
}
