import 'dart:convert';

import 'package:equatable/equatable.dart';
import 'package:watchers_widget/src/core/constants/resources.dart';
import 'package:watchers_widget/src/features/common/domain/models/avatar.dart';
import 'package:watchers_widget/src/features/common/domain/models/status_name.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/logic/onboarding_bloc.dart';

class User extends Equatable {
  final int id;
  final String token;
  final String externalId;
  final String name;
  final String pic;
  final String email;
  final bool isModer;
  final bool isBanned;
  final int color;
  final bool isVip;
  final String vipStatus;
  final bool isInvitedGuest;
  final DateTime? deletedAt;
  final bool isOnboard;
  final int onboardStage;


  Avatar get avatar => Avatar.fromPic(pic);
  StatusName? get statusName => StatusNameX.fromString(vipStatus);
  OnboardStage get onboardStageAsEnum => OnboardStage.values[onboardStage];

  const User(
      {required this.id,
      required this.token,
      required this.externalId,
      required this.name,
      required this.pic,
      required this.email,
      required this.isModer,
      required this.isBanned,
      required this.color,
      required this.isVip,
      required this.vipStatus,
      required this.isInvitedGuest,
      required this.deletedAt,
      required this.isOnboard,
      required this.onboardStage,
      });

  factory User.deleted() => const User(
        id: -1,
        token: '',
        externalId: '',
        name: 'User deleted',
        pic: Resources.deleted_user,
        email: '',
        isModer: false,
        isBanned: false,
        color: 1,
        isVip: false,
        vipStatus: '',
        isInvitedGuest: false,
        deletedAt: null,
        isOnboard: false,
        onboardStage: 0,
      );

  bool get isDeleted => id == -1;

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'token': token,
      'externalId': externalId,
      'name': name,
      'pic': pic,
      'email': email,
      'isModer': isModer,
      'isBanned': isBanned,
      'color': color,
      'isVip': isVip,
      'vipStatus': vipStatus,
      'isInvitedGuest': isInvitedGuest,
      'deletedAt': deletedAt?.millisecondsSinceEpoch,
      'isOnboard': isOnboard,
      'onboardStage': onboardStage,
    };
  }

  factory User.fromMap(Map<String, dynamic> map) {
    return User(
      id: map['id']?.toInt() ?? 0,
      token: map['token'] ?? '',
      externalId: map['externalId'] ?? '',
      name: map['name'] ?? '',
      pic:
          map['pic'] == null || map['pic'].toString().isEmpty ? Resources.deleted_user : map['pic'],
      email: map['email'] ?? '',
      isModer: map['isModer'] ?? false,
      isBanned: map['isBanned'] ?? false,
      color: map['color']?.toInt() ?? 0,
      isVip: map['isVip'] ?? false,
      vipStatus: map['vipStatus'] ?? '',
      isInvitedGuest: map['isInvitedGuest'] ?? false,
      deletedAt: map['deletedAt'] != null ? DateTime.parse(map['deletedAt']) : null,
      isOnboard: map['isOnboard'] ?? false,
      onboardStage: map['onboardStage'] ?? 0,
    );
  }

  String toJson() => json.encode(toMap());

  factory User.fromJson(String source) => User.fromMap(json.decode(source));

  User copyWith({
    int? id,
    String? token,
    String? externalId,
    String? name,
    String? pic,
    String? email,
    bool? isModer,
    bool? isBanned,
    int? color,
    bool? isVip,
    String? vipStatus,
    bool? isInvitedGuest,
    DateTime? deletedAt,
    bool? isOnboard,
    int? onboardStage,
  }) {
    return User(
        id: id ?? this.id,
        token: token ?? this.token,
        externalId: externalId ?? this.externalId,
        name: name ?? this.name,
        pic: pic ?? this.pic,
        email: email ?? this.email,
        isModer: isModer ?? this.isModer,
        isBanned: isBanned ?? this.isBanned,
        color: color ?? this.color,
        isVip: isVip ?? this.isVip,
        vipStatus: vipStatus ?? this.vipStatus,
        isInvitedGuest: isInvitedGuest ?? this.isInvitedGuest,
        deletedAt: deletedAt ?? this.deletedAt,
        isOnboard: isOnboard ?? this.isOnboard,
        onboardStage: onboardStage ?? this.onboardStage,
    );
  }

  @override
  String toString() {
    return 'User(id: $id, token: $token, externalId: $externalId, name: $name, pic: $pic, email: $email, isModer: $isModer, isBanned: $isBanned, color: $color, isVip: $isVip, vipStatus: $vipStatus, isInvitedGuest: $isInvitedGuest, deletedAt: $deletedAt, onboardStage: $onboardStage, isOnboard: $isOnboard )';
  }

  @override
  List<Object?> get props => [
        id,
        token,
        externalId,
        name,
        pic,
        email,
        isModer,
        isBanned,
        color,
        isVip,
        vipStatus,
        isInvitedGuest,
        deletedAt,
        isOnboard,
        onboardStage,
      ];
}
