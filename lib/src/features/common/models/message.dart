import 'dart:convert';

import 'package:watchers_widget/src/app/di/locator.dart';
import 'package:watchers_widget/src/core/extensions/date_time_x.dart';
import 'package:watchers_widget/src/features/chat/domain/models/reaction.dart';
import 'package:watchers_widget/src/features/chat/domain/models/sticker.dart';
import 'package:watchers_widget/src/features/common/models/advertisement.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/common/models/user.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/models/poll_option.dart';

class Message {
  final int id;
  final String text;
  final String type;
  final bool isVisible;
  final bool isPinned;
  final bool hasPreview;
  final String externalRoomId;
  final int creatorId;
  final Message? mentionMessage;
  final Talker talker;
  final String? updatedAt;
  final String createdAt;
  final String serverDateTime;
  final bool showMention;
  final Poll? poll;
  final List<PollOption>? optionsChosen;
  final Advertisement? advertisement;
  final Sticker? sticker;
  final List<Reaction>? reactions;

  // Todo(Dartloli): add message factory (check user.id before constuctor)
  bool? isMyMessage;
  bool? isMentionMe;

  bool get isToday => DateTime.parse(serverDateTime).isToday;

  bool get isPollResults => type == 'POLL_RESULTS';

  Message({
    this.showMention = true,
    this.poll,
    this.optionsChosen,
    required this.id,
    required this.text,
    required this.type,
    required this.isVisible,
    required this.isPinned,
    required this.hasPreview,
    required this.externalRoomId,
    required this.creatorId,
    required this.serverDateTime,
    this.mentionMessage,
    required this.talker,
    this.updatedAt,
    required this.createdAt,
    this.advertisement,
    this.isMyMessage,
    this.isMentionMe,
    this.sticker,
    this.reactions,
  });

  Map<String, dynamic> toMap() {
    return {
      'text': text,
      'externalRoomId': externalRoomId,
      'mentionMessageId': mentionMessage?.id,
    };
  }

  factory Message.fromMap(Map<String, dynamic> map) {
    String? _formatDate(String? serverDate) {
      if (serverDate == null) {
        return null;
      } else {
        final DateTime serverDateTime = DateTime.parse(serverDate);
        final DateTime today = DateTime.now();
        if ((serverDateTime.day == today.day) &&
            (serverDateTime.month == today.month) &&
            (serverDateTime.year == today.year)) {
          return buildDateFormat('HH:mm').format(serverDateTime.toLocal());
        } else {
          return buildDateFormat('HH:mm').format(serverDateTime.toLocal());
        }
      }
    }

    return Message(
      id: map['id']?.toInt() ?? '',
      text: map['text'] ?? '',
      type: map['type'] ?? '',
      isVisible: map['isVisible'] ?? false,
      isPinned: map['isPinned'] ?? false,
      hasPreview: map['hasPreview'] ?? false,
      externalRoomId: map['externalRoomId'] ?? '',
      creatorId: map['creatorId']?.toInt() ?? 0,
      mentionMessage: map['mentionMessage'] != null ? Message.fromMap(map['mentionMessage']) : null,
      talker: map['talker'] != null ? Talker.fromMap(map['talker']) : Talker.empty(),
      updatedAt: _formatDate(map['updatedAt']),
      createdAt: _formatDate(map['createdAt']) ?? '',
      serverDateTime: map['createdAt'],
      poll: _isNullOrEmptyString(map['pollText'])
          ? null
          : Poll.fromMap(jsonDecode(map['pollText'])['poll']),
      optionsChosen: _isNullOrEmptyString(map['pollText']) || _getPollTextOptions(map) == null
          ? null
          : List<PollOption>.from(_getPollTextOptions(map)!.map((x) => PollOption.fromMap(x))),
      advertisement:
          map['advertisement'] != null ? Advertisement.fromMap(map['advertisement']) : null,
      sticker:
          map['sticker'] != null ? Sticker.fromMap(map['sticker']) : null,
      reactions: map['reactions'] != null ? map['reactions'].map<Reaction>((e) => Reaction.fromMap(e)).toList() : null,
    );
  }

  static Iterable<dynamic>? _getPollTextOptions(Map<String, dynamic> map) {
    final pollTextDecoded = jsonDecode(map['pollText']);
    return pollTextDecoded['pollOptionsChosen'] ?? pollTextDecoded['options'];
  }

  static bool _isNullOrEmptyString(String? field) => field == null || field.isEmpty;

  String toJson() => json.encode(toMap());

  factory Message.fromJson(String source) => Message.fromMap(json.decode(source));

  Message copyWith({
    int? id,
    String? text,
    String? type,
    bool? isVisible,
    bool? isPinned,
    bool? hasPreview,
    String? externalRoomId,
    int? creatorId,
    Message? mentionMessage,
    Talker? talker,
    User? user,
    String? updatedAt,
    String? createdAt,
    String? serverDateTime,
    bool? showMention,
    Poll? poll,
    List<PollOption>? optionsChosen,
    Advertisement? advertisement,
    Sticker? sticker,
    List<Reaction>? reactions,
  }) {
    return Message(
      id: id ?? this.id,
      text: text ?? this.text,
      type: type ?? this.type,
      isVisible: isVisible ?? this.isVisible,
      isPinned: isPinned ?? this.isPinned,
      hasPreview: hasPreview ?? this.hasPreview,
      externalRoomId: externalRoomId ?? this.externalRoomId,
      creatorId: creatorId ?? this.creatorId,
      mentionMessage: mentionMessage ?? this.mentionMessage,
      talker: talker ?? this.talker,
      updatedAt: updatedAt ?? this.updatedAt,
      createdAt: createdAt ?? this.createdAt,
      serverDateTime: serverDateTime ?? this.serverDateTime,
      showMention: showMention ?? this.showMention,
      isMyMessage: isMyMessage,
      isMentionMe: isMentionMe,
      poll: poll ?? this.poll,
      optionsChosen: optionsChosen ?? this.optionsChosen,
      advertisement: advertisement ?? this.advertisement,
      sticker: sticker ?? this.sticker,
      reactions: reactions ?? this.reactions,
    );
  }
}
