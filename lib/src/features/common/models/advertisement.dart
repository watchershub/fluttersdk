import 'dart:convert';

class Advertisement {
  final int id;
  final String? link;
  final String? title;
  final String? text;
  final String? status;
  final String? linkText;
  final int template;
  final DateTime? sendTime;
  final DateTime? deleteTime;
  final String? pic;
  // templatePic

  Advertisement(
      {required this.id,
      this.link,
      this.title,
      this.text,
      this.status,
      this.linkText,
      required this.template,
      this.sendTime,
      this.deleteTime,
      this.pic});

  Map<String, dynamic> toMap() {
    return {};
  }

  factory Advertisement.fromMap(Map<String, dynamic> map) {
    return Advertisement(
        id: map['id']?.toInt() ?? '',
        link: map['link'],
        title: map['title'],
        text: map['text'],
        status: map['status'],
        linkText: map['linkText'],
        template: map['template'] ?? 0,
        sendTime: map['sendTime'] != null ? DateTime.parse(map['sendTime']) : null,
        deleteTime: map['deleteTime'] != null ? DateTime.parse(map['deleteTime']) : null,
        pic: map['pic']);
  }

  String toJson() => json.encode(toMap());

  factory Advertisement.fromJson(String source) => Advertisement.fromMap(json.decode(source));

  Advertisement copyWith({
    int? id,
    String? link,
    String? title,
    String? text,
    String? status,
    String? linkText,
    int? template,
    DateTime? sendTime,
    DateTime? deleteTime,
    String? pic,
  }) {
    return Advertisement(
      id: id ?? this.id,
      link: link ?? this.link,
      title: title ?? this.title,
      text: text ?? this.text,
      status: status ?? this.status,
      linkText: linkText ?? this.linkText,
      template: template ?? this.template,
      sendTime: sendTime ?? this.sendTime,
      deleteTime: deleteTime ?? this.deleteTime,
      pic: pic ?? this.pic,
    );
  }
}
