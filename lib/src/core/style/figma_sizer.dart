//

const double _figmaWidth = 375;
const double _figmaHeight = 549;

const _maxDesignScale = 1.0;
const _minDesignScale = 0.4;

class SizerUtil {
  static late double height;
  static late double width;
}

extension FigmaSizerExtension on num {
  double get h => this * SizerUtil.height / 100;

  double get w => this * SizerUtil.width / 100;

  double get fh => (100.h / _figmaHeight).clamp(_minDesignScale, _maxDesignScale) * this;

  double get fw => (100.w / _figmaWidth).clamp(_minDesignScale, _maxDesignScale) * this;
}
