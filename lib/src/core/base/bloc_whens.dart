bool whenExactStateUpdated<E>({
  required Object? previous,
  required Object? current,
  required bool Function(E previous, E current) test,
}) {
  if (current is! E) return false;
  if (previous is! E) return true;

  return test(previous, current);
}

bool whenStateUpdated<E>({
  required Object? previous,
  required Object? current,
  required bool Function(E previous, E current) test,
}) {
  if (current is! E || previous is! E) return false;
  return test(previous, current);
}

void doWhenStateUpdated<E>({
  required Object? previous,
  required Object? current,
  required bool Function(E previous, E current) test,
  required void Function(E previous, E current) whenTrue,
}) {
  if (whenStateUpdated(previous: previous, current: current, test: test)) {
    if (current is! E || previous is! E) return;
    whenTrue(previous, current);
  }
}

extension StateX<S> on S {
  T as<T extends S>() => this as T;
  bool isA<T extends S>() => this is T;
  bool isNotA<T extends S>() => !isA<T>();
}
