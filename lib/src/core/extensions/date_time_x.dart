import 'package:watchers_widget/src/app/di/locator.dart';

extension DateTimeX on DateTime {
  bool get isToday {
    final today = DateTime.now();
    return (day == today.day) && (month == today.month) && (year == today.year);
  }

  bool differFrom(DateTime other) =>
      (day != other.day) || (month != other.month) || (year != other.year);

  String get startDateFormatted {
    return buildDateFormat('d MMMM в HH:mm').format(toLocal());
  }
}
