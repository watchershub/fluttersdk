import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

mixin ContextAware {
  BuildContext? _context;

  BuildContext setContext(BuildContext context) => _context = context;

  BuildContext get context {
    if (_context == null) {
      throw 'setContext was not called';
    }

    return _context!;
  }
}

mixin WithLocalNavigator {
  GlobalKey<NavigatorState>? _navigatorKey;

  set navigatorKey(GlobalKey<NavigatorState>? navigatorKey) {
    _navigatorKey = navigatorKey;
  }

  NavigatorState get navigatorState {
    if (_navigatorKey == null) {
      throw 'NavigatorKey is null so navigator might not have been created';
    }

    return _navigatorKey!.currentState!;
  }

  BuildContext get navigatorContext {
    if (_navigatorKey == null) {
      throw 'NavigatorKey is null so navigator might not have been created';
    }

    return _navigatorKey!.currentContext!;
  }
}

mixin OnWithState<Event, State> on Bloc<Event, State> {
  void onWithState<E extends Event, S extends State>(
    FutureOr<void> Function(
      Event event,
      Emitter<State> emit,
      State state,
    )
        handler, {
    EventTransformer<E>? transformer,
  }) {
    final currentState = state;
    if (state is! S) return;

    on<E>((event, emit) => handler(event, emit, currentState));
  }
}
