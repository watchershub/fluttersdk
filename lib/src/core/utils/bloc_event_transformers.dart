import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/transformers.dart';

class BlocEventTransformers {
  BlocEventTransformers._();

  static EventTransformer<Event> debounceSequential<Event>(Duration duration) {
    return (events, mapper) => events.debounceTime(duration).asyncExpand(mapper);
  }

  static EventTransformer<Event> throttle<Event>(
    Duration duration, {
    bool trailing = false,
    bool leading = true,
  }) =>
      (events, mapper) => events
          .throttleTime(
            duration,
            trailing: trailing,
            leading: leading,
          )
          .switchMap(mapper);

  static EventTransformer<Event> delay<Event>(Duration duration) =>
      (events, mapper) => events.delay(duration).switchMap(mapper);

  EventTransformer<Event> skip<Event>(int count) =>
      (events, mapper) => events.skip(count).switchMap(mapper);
}
