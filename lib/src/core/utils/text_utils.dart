import 'package:flutter/painting.dart';

class TextUtils {
  static bool canFitText({
    required String text,
    required TextStyle textStyle,
    required double width,
  }) {
    final TextPainter textPainter = TextPainter(
        text: TextSpan(text: text, style: textStyle), textDirection: TextDirection.ltr, maxLines: 1)
      ..layout();

    return textPainter.size.width <= width;
  }

  static double calcTextHeight({
    required String text,
    required TextStyle textStyle,
    required double maxWidth,
    int? maxLines,
  }) {
    final TextPainter textPainter = TextPainter(
      text: TextSpan(text: text, style: textStyle),
      textDirection: TextDirection.ltr,
      maxLines: maxLines,
    )..layout(maxWidth: maxWidth);

    return textPainter.size.height;
  }

  static Size calcTextSize({
    required String text,
    required TextStyle textStyle,
    required double maxWidth,
    int? maxLines,
  }) {
    final TextPainter textPainter = TextPainter(
      text: TextSpan(text: text, style: textStyle),
      textDirection: TextDirection.ltr,
      maxLines: maxLines,
    )..layout(maxWidth: maxWidth);

    return textPainter.size;
  }
}
