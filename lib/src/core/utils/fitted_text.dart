import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

class FittedText extends StatelessWidget {
  static const stepGranularity = 0.25;

  final String? text;
  final int maxLines;
  final TextAlign? textAlign;
  final TextStyle? style;
  final TextSpan? textSpan;

  const FittedText(
    this.text, {
    this.style,
    this.textAlign,
    this.maxLines = 1,
    Key? key,
  })  : textSpan = null,
        super(key: key);

  const FittedText.rich(
    this.textSpan, {
    this.style,
    this.textAlign,
    this.maxLines = 1,
  }) : text = null;

  bool get _isRich => textSpan != null;

  static const double _minFontSize = 8.0;
  static const double _maxFontSize = 58.0;

  @override
  Widget build(BuildContext context) {
    return _isRich
        ? AutoSizeText.rich(
            textSpan!,
            textAlign: textAlign,
            minFontSize: _minFontSize ~/ stepGranularity * stepGranularity,
            maxFontSize: _maxFontSize ~/ stepGranularity * stepGranularity,
            overflow: TextOverflow.visible,
            maxLines: maxLines,
            style: style,
            stepGranularity: 0.25,
          )
        : AutoSizeText(
            text!,
            textAlign: textAlign,
            minFontSize: _minFontSize ~/ stepGranularity * stepGranularity,
            maxFontSize: _maxFontSize ~/ stepGranularity * stepGranularity,
            overflow: TextOverflow.visible,
            maxLines: maxLines,
            style: style,
            stepGranularity: 0.25,
          );
  }
}
