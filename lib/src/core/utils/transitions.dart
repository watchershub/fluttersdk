import 'package:flutter/material.dart';

class Transitions {
  Transitions._();

  static Route buildFadeTransition(Widget screen) => PageRouteBuilder(
        pageBuilder: (_, __, ___) => screen,
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return FadeTransition(
            opacity: animation,
            child: child,
          );
        },
      );

  static Route buildZeroTransition(Widget screen) => PageRouteBuilder(
        pageBuilder: (_, __, ___) => screen,
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          return child;
        },
      );
}
