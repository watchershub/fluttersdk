import 'package:flutter/material.dart';

class AnimatedVisibility extends StatelessWidget {
  final Widget child;
  final bool visible;
  final Duration duration;

  const AnimatedVisibility({
    required this.child,
    required this.visible,
    this.duration = const Duration(milliseconds: 200),
  });

  @override
  Widget build(BuildContext context) {
    return AnimatedSwitcher(
      duration: duration,
      layoutBuilder: (currentChild, previousChildren) {
        return currentChild!;
      },
      child: visible ? child : const SizedBox.shrink(),
    );
  }
}
