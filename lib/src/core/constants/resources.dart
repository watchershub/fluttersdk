// ignore_for_file: constant_identifier_names

class Resources {
  Resources._();

  // Icons
  static const close = 'assets/icons/decline.svg';
  static const arrow_button = 'assets/icons/arrow_button.svg';
  static const selected_mark = 'assets/icons/selected_mark.svg';
  static const leave = 'assets/icons/leave.svg';
  static const people = 'assets/icons/people.svg';
  static const settings = 'assets/icons/settings.svg';
  static const check = 'assets/icons/check.svg';
  static const admin_check_mark = 'assets/icons/admin_mark.svg';
  static const chevron = 'assets/icons/chevron.svg';
  static const pin = 'assets/icons/pin.svg';
  static const block = 'assets/icons/block.svg';
  static const brick = 'assets/icons/brick.svg';
  static const speaker = 'assets/icons/speaker.svg';
  static const send = 'assets/icons/send.svg';
  static const email = 'assets/icons/email.svg';
  static const arrow_down = 'assets/icons/arrow_down.svg';
  static const warning = 'assets/icons/warning.svg';
  static const user_blocked = 'assets/icons/user_blocked.svg';
  static const poll = 'assets/icons/poll.svg';

  // popup menu
  static const reply = 'assets/icons/answer.svg';
  static const edit = 'assets/icons/edit.svg';
  static const copy = 'assets/icons/copy.svg';
  static const remove = 'assets/icons/remove.svg';
  static const report = 'assets/icons/report.svg';
  static const report_message = 'assets/icons/report_message.svg';
  static const report_user = 'assets/icons/report_user.svg';
  static const report_badge_simple = 'assets/icons/report_badge_simple.svg';
  static const report_badge = 'assets/icons/report_badge.svg';
  static const hide_message = 'assets/icons/hide_message.svg';
  static const show_message = 'assets/icons/show_message.svg';
  static const disabled = 'assets/icons/disabled.svg';

  static const loader = 'assets/icons/loader.svg';
  static const be_first = 'assets/icons/be_first.svg';

  static const no_internet = 'assets/icons/no_internet.svg';
  static const unpin = 'assets/icons/unpin.svg';

  static const info = 'assets/icons/info.svg';

  static const deleted_user = 'assets/avatars/deleted_user.jpg';

  static const link = 'assets/icons/link.svg';

  static const hand = 'assets/icons/hand.svg';
  static const mic_on = 'assets/icons/mic_on.svg';
  static const mic_off = 'assets/icons/mic_off.svg';
  static const hand_list = 'assets/icons/hand_list.svg';
  static const add_speaker = 'assets/icons/add_speaker.svg';
  static const decline_request = 'assets/icons/decline_request.svg';
  static const chat_bubble = 'assets/icons/chat_bubble.svg';
  static const speakers = 'assets/icons/speakers.svg';
  static const mute_button = 'assets/icons/mute_button.svg';
  static const remove_from_speakers = 'assets/icons/remove_from_speakers.svg';
  static const no_blocks = 'assets/icons/no_blocks_icon.svg';
  static const plus = 'assets/icons/plus.svg';
  static const sticker = 'assets/icons/sticker_icon.svg';
  static const keyboard = 'assets/icons/keyboard_icon.svg';
  static const defaultToggleIcon = 'assets/icons/default_toggle_icon.svg';
  static const slowmodeIcon = 'assets/icons/slow_mode_icon.svg';

  //advertisement pictures
  static const advertisements = [
    {
      'icon': 'assets/icons/adv_icon_template_3.svg',
      'picture': 'assets/pictures/adv_background_template_3.png',
    },
    {
      'picture': 'assets/pictures/adv_background_template_4.jpeg',
    },
    {
      'picture': 'assets/pictures/adv_background_template_5.png',
      'icon': 'assets/icons/adv_icon_template_5.svg',
    },
    {
      'picture': 'assets/pictures/adv_background_template_6.png',
      'icon': 'assets/icons/adv_icon_template_6.svg',
    },
    {'icon': 'assets/icons/adv_icon_template_7.svg'},
    {
      'icon_left': 'assets/icons/adv_icon_left_template_8.svg',
      'icon_right': 'assets/icons/adv_icon_right_template_8.svg',
    },
    {
      'picture': 'assets/pictures/adv_background_template_9.png',
    },
  ];

  //Reactions
  static const clappingHands = 'assets/icons/clappingHands.svg';
  static const faceWithHeartEyes = 'assets/icons/faceWithHeartEyes.svg';
  static const flexedBiceps = 'assets/icons/flexedBiceps.svg';
  static const grinningFace = 'assets/icons/grinningFace.svg';
  static const poutingFace = 'assets/icons/poutingFace.svg';
  static const thumbsUp = 'assets/icons/thumbsUp.svg';
  static const yawningFace = 'assets/icons/yawningFace.svg';

  static String reactionToAssetName(String reactionName) {
    return 'assets/icons/$reactionName.png';
  }

  //Slow mode
  static const Map<int,String> slowmodeDelayMap = {
    1000 : '1 секунда',
    2000 : '2 секунды',
    3000 : '3 секунды',
    5000 : '5 секунд',
    10000 : '10 секунд',
    15000 : '15 секунд',
    20000 : '20 секунд',
    30000 : '30 секунд',
    60000 : '1 минута',
    120000 : '2 минуты',
    180000 : '3 минуты',
    300000 : '5 минут',
  };

}
