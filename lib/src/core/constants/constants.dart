class Constants {
  Constants._();

  static const baseUrlDev = 'https://webbackend-liga.dev.watchers.io/';
  static const baseUrlProd = 'https://webbackend-liga.prod.watchers.io/';
  static const previewBaseUrl = 'https://proxy.watchers.io/';

  static const emojiAnimationDuration = Duration(milliseconds: 2500);
  static const voteAnimationDuration = Duration(milliseconds: 1200);

  // static const messageInputHeight = 63.0;
  static const maxMessageInputLength = 500;

  static const partnerEmail = 'ligastavok@social-solutions.ru';

  static const analiticsApiKeyDev = '4ae12908f11a7fa1775c5cffe351d1e1';
  static const analiticsApiKeyProd = 'd8d84e2d3e02e381f5144fb59226a7c1';

}
