import 'package:flutter/material.dart';

class CustomColors {
  CustomColors._();

  static const tooltipBackground = Color(0xff05ac4d);

  static const emojiBackground = Color(0xfff1f1f1);

  static const primary = Color(0xFF00AB4E);

  static const onPrimary = Colors.white;

  static const secondary = Color(0xFFF0F0F0);

  static const gray200 = Color(0xFF555555);

  static const gray400 = Color(0xFF8A8B8A);

  static const gray600 = Color(0xFFE4E4E4);

  static const gray800 = Color(0xFFE9E9E9);

  static const transparentBarrer = Colors.black54;

  static const inputFilling = Color(0xFFFAFAFA);

  static const inputBorder = Color(0xFFD5D3D3);

  static const divider = Color(0xFFE5E5E5);

  static const myMessageColor = Color(0xFFF2FFF8);

  static const chatBackground = Color(0xFFF5F5F5);

  static const textMain = Color(0xFF292929);

  static const textSecondary = Color(0xFF7A7A7A);

  static const textTertiary = Color(0xFFA1A3A1);

  static const paragraphTextColor = Color.fromRGBO(0, 0, 0, 0.8);

  static const dateTextColor = Color.fromRGBO(0, 0, 0, 0.3);

  static const contributionTextColor = Color.fromRGBO(255, 255, 255, 0.4);

  static const modalBackground = Colors.white;

  static const danger = Color(0xFFFF2D55);

  static const blockColor = Color.fromRGBO(255, 45, 85, 0.2);

  static const positive = Color(0xFF00B347);

  static const highlightColor = Color.fromRGBO(121, 121, 121, 1);

  static const previewWidgetBorderColor = Color(0xFF292929);

  static const beFirstContainerBackground = Color(0xFFFFFFFF);

  static const shimmerAnimationColor = Color.fromRGBO(186, 186, 186, 1);

  static const highlightMessageBackground = Color.fromRGBO(52, 52, 52, 0.7);

  static const settingIconColor = gray400;

  static const talkersCounterIconColor = Color(0xFF8A8B8A);

  static const unblockButtonColor = secondary;
  static const blockButtonColor = blockColor;

  static const mentionShadowColor = Color(0xff00001A);
  static const counterShadowColor = Color(0xff00001F);
  static const arrowShadowColor = Color(0xff00001A);


  // Poll colors
  static const choseColor = Color(0xffffffff);
  static const oddPollItem = Color(0xFFF5F5F5);
  static const evenPollItem = Color(0xFFE9E9E9);
  static const rightAnswer = Color.fromRGBO(21, 159, 62, 1);
  static const wrongAnswer = Color(0xFFFF4944);
  static const quizAnswerHint = Color.fromRGBO(255, 255, 255, 0.6);
  static const quizAnswerQustion = Color.fromRGBO(255, 255, 255, 0.8);
  static const dialogFontColor = textMain;
  static const dialogIconColor = Color(0xFFF0F0F0);
  static const dialogIconInactiveBackgroundColor = CustomColors.gray800;
  static const pollAnswerBackground = primary;

  // Poll icon widget
  static const pollIconActiveBackground = primary;
  static const pollIconActiveColor = Color(0xFFF0F0F0);
  static const pollIconInactiveBackground = Color(0xffC1C1C1);
  static const pollIconInactiveColor = Color(0xff242424);

  // Vip statuses
  static const club = Color(0xFF00AB4E);
  static const silver = Color(0xFF555859);
  static const gold = Color(0xFF6C5C33);
  static const platinum = Color(0xFF323232);

  static Color getUserColor(int index) {
    if (index >= _userColors.length) {
      return _userColors[0];
    }
    return _userColors[index];
  }

  static const _userColors = [
    Color(0xff000099),
    Color(0xff00ab4e),
    Color(0xff8a8b8a),
    Color(0xffc73834),
    Color(0xffd46e3b),
    Color(0xff00a775),
    Color(0xff007dc5),
    Color(0xffb1a000),
  ];

  // Toasts
  static const toastCloseIcon = CustomColors.textMain;
  static const toastIcon = CustomColors.textMain;
  static const toastTextColor = Colors.white;
  static const toastBlurSigma = 16.0;
  static const toastBackground = Color(0xB8A4A4A4);

  // Tooltip
  static const tooltipPollBackground = Color(0xffe9e9e9);
  static const tooltipCloseIcon = textMain;

  // Micro off icon
  static const microOffStripAbove = Colors.white;
  static const microOffStripBelow = Colors.red;

  //Highlights

  static const underscoreColor = Color(0xffffed00);

  //Modal colors
  static const chatRuleButtonColor = Color(0xffFF2D55);
  static const wrightUsButtonColor = Color(0xff00B347);

  // Poll results colors
  static const pollResultsBackgroundMyMessage = primary;
  static const pollResultsTitleMyMessage = onPrimary;
  static final pollResultsSubtitleMyMessage = onPrimary.withOpacity(0.8);
  static const pollResultsBackground = CustomColors.modalBackground;
  static const pollResultsTitle = textMain;
  static const pollResultsSubtitle = textSecondary;

  // Internet
  static const noInternetIcon = Colors.red;

  //Reactions
  static const myReactionColor = primary;
  static const reactionBackground = Color.fromRGBO(236,236, 236, 1);
  static const reactionCountText = Color.fromRGBO(103,103,103,1);
  static const myReactionCountText = onPrimary;

}
