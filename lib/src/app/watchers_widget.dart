import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:provider/provider.dart';
import 'package:watchers_widget/src/app/di/locator.dart';
import 'package:watchers_widget/src/app/watchers_params.dart';
import 'package:watchers_widget/src/app/watchers_widget_controller.dart';
import 'package:watchers_widget/src/core/style/figma_sizer.dart';
import 'package:watchers_widget/src/features/common/widgets/loading_widget.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/onboarding_screen.dart';

class WatchersWidget extends StatefulWidget {
  final WatchersWidgetController controller;

  const WatchersWidget({
    required this.controller,
  });

  @override
  State<WatchersWidget> createState() => _WatchersWidgetState();
}

class _WatchersWidgetState extends State<WatchersWidget> {
  late final WatchersParamsProvider watchersParamsProvider;

  Completer _locatorInitedCompleter = Completer();
  GetIt? _locator;

  @override
  void initState() {
    watchersParamsProvider = WatchersParamsProvider(
      widget.controller.params,
    );

    _initChatLocator();

    super.initState();
  }

  void _initChatLocator() {
    _locator?.reset();

    init(watchersParamsProvider)
        .then((locator) {
          setState(() {
            _locator = locator;
            _locatorInitedCompleter = Completer();
          });
          return locator;
        })
        .then((locator) => locator.allReady())
        .then((_isReady) => _locatorInitedCompleter.complete(true));
  }

  @override
  void dispose() {
    _locator?.reset();

    super.dispose();
  }

  @override
  void didUpdateWidget(covariant WatchersWidget oldWidget) {
    watchersParamsProvider.updateParams(
      widget.controller.params,
    );

    if (oldWidget.controller.params.roomId != widget.controller.params.roomId) {
      _initChatLocator();
    }

    super.didUpdateWidget(oldWidget);
  }

  void _setConstraints(BoxConstraints constraints) {
    SizerUtil.height = constraints.maxHeight;
    SizerUtil.width = constraints.maxWidth;
  }

  @override
  Widget build(BuildContext context) {
    WidgetsFlutterBinding.ensureInitialized();

    return FutureBuilder<void>(
      key: ValueKey(_locator),
      future: _locatorInitedCompleter.future,
      builder: (context, snapshot) {
        return LayoutBuilder(
          builder: (_, constraints) {
            _setConstraints(constraints);

            return snapshot.hasData
                ? ChangeNotifierProvider(
                    create: (context) => watchersParamsProvider,
                    child: MediaQuery(
                      data: MediaQuery.of(context).copyWith(
                        textScaleFactor: 1.0,
                      ),
                      child: ScrollConfiguration(
                        behavior: const ScrollBehavior(
                          // ignore: deprecated_member_use
                          androidOverscrollIndicator: AndroidOverscrollIndicator.stretch,
                        ),
                        child: Navigator(
                          onGenerateRoute: (_) => OnboardingScreen.route(),
                        ),
                      ),
                    ),
                  )
                : const LoadingScreen();
          },
        );
      },
    );
  }
}
