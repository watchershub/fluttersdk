import 'package:amplitude_flutter/amplitude.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:watchers_widget/src/app/watchers_params.dart';
import 'package:watchers_widget/src/core/constants/constants.dart';
import 'package:watchers_widget/src/core/custom_bloc_observer.dart';
import 'package:watchers_widget/src/features/chat/data/agora_repository.dart';
import 'package:watchers_widget/src/features/chat/data/chat_api.dart';
import 'package:watchers_widget/src/features/chat/data/chat_repository.dart';
import 'package:watchers_widget/src/features/chat/data/sticker_api.dart';
import 'package:watchers_widget/src/features/chat/domain/i_agora_repository.dart';
import 'package:watchers_widget/src/features/chat/domain/i_chat_repository.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/close_socket_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/create_reaction_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/delete_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/delete_reaction_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/edit_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/emotion/get_all_emotions_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/emotion/get_emotion_by_name_scenario.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_message_array_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_pinned_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_preview_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_stickers_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/get_user_last_message.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/join_room_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/on_connectivity_changed_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/report_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/send_emotion_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/send_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_ban_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_message_visible_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_messages_visible_request.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_pinned_message_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/set_slowmode_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/join_agora_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/leave_voice_room_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/reject_hand_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/set_local_audio_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/set_mute_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/set_role_use_case.dart';
import 'package:watchers_widget/src/features/chat/domain/use_cases/speak/toggle_hand_use_case.dart';
import 'package:watchers_widget/src/features/chat/presentation/logic/chat_bloc.dart';
import 'package:watchers_widget/src/features/chat/presentation/widgets/animation_layer/logic/animation_layer_cubit.dart';
import 'package:watchers_widget/src/features/common/anti_swearing/anti_swearing.dart';
import 'package:watchers_widget/src/features/common/data/apis/feedback/feedback_api.dart';
import 'package:watchers_widget/src/features/common/data/apis/room/room_api.dart';
import 'package:watchers_widget/src/features/common/data/apis/stat/stat_api.dart';
import 'package:watchers_widget/src/features/common/data/apis/talker/talker_api.dart';
import 'package:watchers_widget/src/features/common/data/apis/wordlist/wordlist_api.dart';
import 'package:watchers_widget/src/features/common/data/interceptors/add_token_interceptor.dart';
import 'package:watchers_widget/src/features/common/data/repositories/feedback_repository.dart';
import 'package:watchers_widget/src/features/common/data/repositories/room_repository.dart';
import 'package:watchers_widget/src/features/common/data/repositories/stat_repository.dart';
import 'package:watchers_widget/src/features/common/data/repositories/talker_repository.dart';
import 'package:watchers_widget/src/features/common/data/repositories/wordlist_repository.dart';
import 'package:watchers_widget/src/features/common/data/settings/i_settings_repository.dart';
import 'package:watchers_widget/src/features/common/data/settings/settings_api.dart';
import 'package:watchers_widget/src/features/common/data/settings/settings_repository.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/feedback/post_soundcheck_feedback.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/room/get_room_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/settings/get_chat_rules_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/stat/calc_time_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/talker/get_room_and_talkers_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/user/get_local_user_use_case.dart';
import 'package:watchers_widget/src/features/common/domain/use_cases/user/restore_user_use_case.dart';
import 'package:watchers_widget/src/features/common/models/talker.dart';
import 'package:watchers_widget/src/features/deleted_profile/logic/deleted_profile_bloc.dart';
import 'package:watchers_widget/src/features/deleted_profile/logic/deleted_profile_bloc_params.dart';
import 'package:watchers_widget/src/features/onboarding/presentation/logic/onboarding_bloc.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/poll_api.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/data/poll_repository.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/use_cases/get_poll_use_case.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/domain/use_cases/vote_use_case.dart';
import 'package:watchers_widget/src/features/polls-and-quiz/presentation/logic/polls_bloc.dart';
import 'package:watchers_widget/src/features/sound_check/logic/sound_check_bloc.dart';
import 'package:watchers_widget/src/features/tooltips/domain/use_cases/complete_tooltip_use_case.dart';
import 'package:watchers_widget/src/features/tooltips/domain/use_cases/is_show_tooltip_use_case.dart';
import 'package:watchers_widget/src/features/tooltips/logic/tooltip_cubit.dart';

import '../../features/chat/data/agora_api.dart';
import '../../features/chat/domain/use_cases/check_message_use_case.dart';
import '../../features/common/data/apis/auth/auth_api.dart';
import '../../features/common/data/apis/block/block_api.dart';
import '../../features/common/data/apis/user/user_api.dart';
import '../../features/common/data/repositories/auth_repository.dart';
import '../../features/common/data/repositories/block_repository.dart';
import '../../features/common/data/repositories/user_repository.dart';
import '../../features/common/data/shared_preferences_storage/shared_preferences_storage.dart';
import '../../features/common/domain/use_cases/auth/register_user_use_case.dart';
import '../../features/common/domain/use_cases/avatar/get_all_avatars_use_case.dart';
import '../../features/common/domain/use_cases/block/add_block_use_case.dart';
import '../../features/common/domain/use_cases/block/get_blocks_use_case.dart';
import '../../features/common/domain/use_cases/block/remove_block_use_case.dart';
import '../../features/common/domain/use_cases/user/delete_user_user_case.dart';
import '../../features/common/domain/use_cases/user/get_user_by_id_use_case.dart';
import '../../features/common/domain/use_cases/user/get_user_use_case.dart';
import '../../features/common/domain/use_cases/user/update_user_use_case.dart';
import '../../features/settings/presentation/logic/settings_bloc.dart';

part 'register_apies.dart';
part 'register_blocs.dart';
part 'register_intercepters.dart';
part 'register_repositories.dart';
part 'register_storages.dart';
part 'register_use_cases.dart';
part 'register_utils.dart';

late GetIt locator;

// Todo(dartloli): create class and use everywhere
DateFormat buildDateFormat([String? newPattern]) {
  initializeDateFormatting('ru', null);
  return DateFormat(newPattern, 'ru');
}

Future<void> initParams({
  required WatchersParams params,
}) async {
  locator.resetLazySingleton(instance: locator<WatchersParams>());
  locator.registerLazySingleton(() => params);
}

Future<GetIt> init(WatchersParamsProvider watchersParamsProvider) async {
  locator = GetIt.asNewInstance();

  await initializeDateFormatting('ru', null);

  Bloc.observer = CustomBlocObserver();

  locator.registerLazySingleton(() => watchersParamsProvider);

  final baseUrl =
      watchersParamsProvider.params.enableDev ? Constants.baseUrlDev : Constants.baseUrlProd;
  final apiKey =
      watchersParamsProvider.params.enableDev ? Constants.analiticsApiKeyDev : Constants.analiticsApiKeyProd;

  _registerUtils(baseUrl,apiKey);

  _registerStorages();

  _registerIntercepters();

  _registerApies(baseUrl);

  _registerRepositories();

  Future<AntiSwearing> _antiSwearingFuture() async {
    if (AntiSwearing.instance != null) return AntiSwearing.instance!;

    final wordlist = await locator<IWordlistRepository>().getWordlist();
    return AntiSwearing(wordlistResponse: wordlist);
  }

  locator.registerSingleton<LazyFutureAntiSwearing>(_antiSwearingFuture);

  _registerUseCases();

  _registerBlocs();

  return locator;
}

typedef LazyFutureAntiSwearing = Future<AntiSwearing> Function();
