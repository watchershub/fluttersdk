part of 'locator.dart';

void _registerUseCases() {
  // Auth
  locator.registerFactory(() => RegisterUserUseCase(authRepository: locator()));

  // Blocks
  locator.registerFactory(() => AddBlockUseCase(blockRepository: locator()));
  locator.registerFactory(() => RemoveBlockUseCase(blockRepository: locator()));
  locator.registerFactory(() => GetBlocksUseCase(blockRepository: locator()));

  // User
  locator.registerFactory(() => DeleteUserUseCase(userRepository: locator()));
  locator.registerFactory(() => RestoreUserUseCase(userRepository: locator()));
  locator.registerFactory(() => GetUserByIdUseCase(userRepository: locator()));
  locator.registerFactory(() => GetUserUseCase(userRepository: locator()));
  locator.registerFactory(() => UpdateUserUseCase(userRepository: locator()));
  locator.registerFactory(() => GetLocalUserUseCase(userRepository: locator()));

  // Avatar
  locator.registerFactory(() => GetAllAvatarsUseCase(locator()));

  // Chat
  locator.registerFactory(() => GetMessageArrayUseCase(chatRepository: locator()));
  locator.registerFactory(() => GetMessageUseCase(chatRepository: locator()));
  locator.registerFactory(() => GetPinnedMessageUseCase(chatRepository: locator()));
  locator.registerFactory(() => JoinRoomUseCase(chatRepository: locator()));
  locator.registerFactory(() => SendMessageUseCase(chatRepository: locator()));
  locator.registerFactory(() => EditMessageUseCase(chatRepository: locator()));
  locator.registerFactory(() => DeleteMessageUseCase(chatRepository: locator()));
  locator.registerFactory(() => CheckMessageUseCase(chatRepository: locator()));
  locator.registerFactory(() => SendEmotionUseCase(chatRepository: locator()));
  locator.registerFactory(() => ReportMessageUseCase(chatRepository: locator()));
  locator.registerFactory(() => SetMessageVisibleUseCase(chatRepository: locator()));
  locator.registerFactory(() => SetMessagesVisibleUseCase(chatRepository: locator()));
  locator.registerFactory(() => SetBanUseCase(chatRepository: locator()));
  locator.registerFactory(() => CloseSocketUseCase(chatRepository: locator()));
  locator.registerFactory(() => GetConnectivityChangeStreamUseCase(chatRepository: locator()));
  locator.registerFactory(() => SetPinnedMessageUseCase(locator()));
  locator.registerFactory(() => GetPreviewUseCase(chatRepository: locator()));
  locator.registerFactory(() => ToggleHandUseCase(chatRepository: locator()));
  locator.registerFactory(() => RejectHandUseCase(chatRepository: locator()));
  locator.registerFactory(() => SetRoleUseCase(chatRepository: locator()));
  locator.registerFactory(() => SetMuteUseCase(chatRepository: locator()));
  locator.registerFactory(() => GetStickersUseCase(chatRepository: locator()));
  locator.registerFactory(() => CreateReactionUseCase(chatRepository: locator()));
  locator.registerFactory(() => DeleteReactionUseCase(chatRepository: locator()));
  locator.registerFactory(() => SetSlowmodeUseCase(chatRepository: locator()));
  locator.registerFactory(() => GetUserLastMessageUseCase(chatRepository: locator()));


  // Emotions
  locator.registerFactory(() => const GetAllEmotionsUseCase());
  locator.registerFactory(() => GetEmotionByNameScenario(locator()));

  // Room
  locator.registerFactory(() => GetRoomUseCase(locator()));

  // Talkers
  locator.registerFactory(() => GetRoomAndTalkersUseCase(locator()));

  // Tooltips
  locator.registerFactory(() => IsShowTooltipUseCase(locator()));
  locator.registerFactory(() => CompleteTooltipUseCase(locator()));

  // Poll
  locator.registerFactory(() => VoteUseCase(locator()));
  locator.registerFactory(() => GetPollUseCase(locator()));

  //Agora
  locator.registerFactory(() => JoinAgoraUseCase(agoraRepository: locator()));
  locator.registerFactory(() => SetLocalAudioUseCase(agoraRepository: locator()));
  locator.registerFactory(() => LeaveVoiceRoomUseCase(agoraRepository: locator()));

  //Stat
  locator.registerFactory(() => CalcTimeUseCase(locator()));

  //Settings
  locator.registerFactory(() => GetChatRulesUseCase(locator()));

  //Feedback
  locator.registerFactory(() => PostSoundcheckFeedback(locator()));



}
