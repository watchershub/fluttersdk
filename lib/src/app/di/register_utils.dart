part of 'locator.dart';

void _registerUtils(String baseUrl,String amplitudeApiKey) {
  locator.registerLazySingleton(() {
    return Dio(BaseOptions(
      baseUrl: baseUrl,
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
    ))
      ..interceptors.add(locator<PrettyDioLogger>())
      ..interceptors.add(locator<AddTokenInterceptor>());
  });
  locator.registerLazySingleton(() {

    final benchMark = Stopwatch();
    benchMark.start();

    return benchMark;
  });

  locator.registerLazySingleton<Amplitude>(() {

    final Amplitude analytics = Amplitude.getInstance(instanceName: "431219");

    //Initialize SDK
    analytics.init(amplitudeApiKey);

    analytics.uploadEvents();

    return analytics;
  });
}
