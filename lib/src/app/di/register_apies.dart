part of 'locator.dart';

void _registerApies(String baseUrl) {
  locator.registerLazySingleton(() => UserApi(locator()));
  locator.registerLazySingleton(() => AuthApi(locator()));
  locator.registerLazySingleton(() => BlockApi(locator()));
  locator.registerLazySingleton(() => ChatApi(locator(), baseUrl));
  locator.registerLazySingleton(() => TalkerApi(locator()));
  locator.registerLazySingleton(() => RoomApi(locator()));
  locator.registerLazySingleton(() => WordlistApi(locator()));
  locator.registerLazySingleton(() => AgoraApi());

  locator.registerLazySingleton(() => PollApi(
        locator(),
        locator<ChatApi>().getRealExternalRoomId,
      ));

  locator.registerLazySingleton(() => StatApi(locator()));
  locator.registerLazySingleton(() => StickerApi(locator()));
  locator.registerLazySingleton(() => SettingsApi(locator()));
  locator.registerLazySingleton(() => FeedbackApi(locator()));

}
