part of 'locator.dart';

void _registerBlocs() {
  locator.registerFactory(() => OnboardingBloc(
        registerUserUseCase: locator(),
        updateUserUseCase: locator(),
        getAllAvatarsUseCase: locator(),
        antiSwearingFuture: locator(),
        closeSocketUseCase: locator(),
        watchersParamsProvider: locator(),
      ));

  locator.registerFactoryParam((Talker talker,String externalRoomId) => SettingsBloc(
        getBlocksUseCase: locator(),
        getUserUseCase: locator(),
        getAllAvatarsUseCase: locator(),
        updateUserUseCase: locator(),
        removeBlockUseCase: locator(),
        deleteUserUseCase: locator(),
        antiSwearingFuture: locator(),
        getConnectivityChangeStreamUseCase: locator(),
        talker: talker,
        addBlockUseCase: locator(),
        getChatRulesUseCase: locator(),
        setSlowmodeUseCase: locator(),
        getRoomAndTalkersUseCase: locator(),
        externalRoomId: externalRoomId,
        amplitude: locator(),
        paramsProvider: locator(),
      ));

  locator.registerFactory(() => ChatBloc(
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
        locator(),
      ));

  locator.registerFactoryParam((DeletedProfileBlocParams params, _) => DeletedProfileBloc(
        params,
        locator(),
      ));

  locator.registerFactory(() => TooltipCubit(locator(), locator()));

  locator.registerFactory(() => AnimationLayerCubit(locator(), locator(), locator()));

  locator.registerFactory(() => PollsBloc(locator(), locator(), locator(), locator()));
  locator.registerFactory(() => SoundCheckBloc(locator(),locator()));

}
