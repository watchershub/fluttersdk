import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:watchers_widget/src/features/common/domain/models/status_name.dart';

class WatchersParams extends Equatable {
  final String userId;
  final String roomId;
  final StatusName? statusName;
  final String? title;
  final bool enableDev;
  final String? nickName;
  final String? avatar;

  const WatchersParams({
    required this.userId,
    required this.roomId,
    this.statusName,
    this.title,
    required this.enableDev,
    this.nickName,
    this.avatar,
  });

  @override
  List<Object?> get props => [
        userId,
        roomId,
        statusName,
        title,
        enableDev,
        nickName,
        avatar,
      ];
}

class WatchersParamsProvider extends ChangeNotifier {
  WatchersParams params;

  WatchersParamsProvider(this.params);

  void updateParams(WatchersParams params) {
    this.params = params;
    notifyListeners();
  }
}
