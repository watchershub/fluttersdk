import 'package:equatable/equatable.dart';
import 'package:watchers_widget/src/app/watchers_params.dart';

class WatchersWidgetController extends Equatable {
  final WatchersParams params;

  const WatchersWidgetController({
    required this.params,
  });

  @override
  List<Object?> get props => [params];
}
